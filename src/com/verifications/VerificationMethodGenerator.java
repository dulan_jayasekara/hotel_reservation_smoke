package com.verifications;

import com.reservation.ReservationScenarioRunner;

public class VerificationMethodGenerator {
	
	private StringBuffer   PrintWriter    = null;
	
	
	public VerificationMethodGenerator(StringBuffer buffer)
	{
		this.PrintWriter = buffer;
	}

	
	public void verifyTrue(String Expected,String Actual,String Message)
	{
		PrintWriter.append("<tr><td>"+ReservationScenarioRunner.TestCaseCount+"</td><td>"+Message+"</td>");
		ReservationScenarioRunner.TestCaseCount++;
		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {
			
			if(Actual.trim().equalsIgnoreCase(Expected.trim()))
				PrintWriter.append("<td class='Passed'>PASS</td>");
		    else
		        PrintWriter.append("<td class='Failed'>FAIL</td>");
	  
	        PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}
	}

	public void verifyContains(String Expected,String Actual,String Message)
	{
		PrintWriter.append("<tr><td>"+ReservationScenarioRunner.TestCaseCount+"</td><td>"+Message+"</td>");
		ReservationScenarioRunner.TestCaseCount++;
		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {
			
			if((Actual.trim().contains(Expected.trim()))||(Expected.trim().contains(Actual.trim())))
				PrintWriter.append("<td class='Passed'>PASS</td>");
		    else
		        PrintWriter.append("<td class='Failed'>FAIL</td>");
	  
	        PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}
	}

	public void verifyTrueValue(String Expected,String Actual,String Message)
	{
		Double ExpectedRate         = Math.ceil(Double.parseDouble(Expected));
		String ExpectedRateString   = Double.toString(ExpectedRate).replace(".0", "");
	    Double FloorExpectedRate    = Math.floor(Double.parseDouble(Expected));
		String F_ExpectedRateString = Double.toString(FloorExpectedRate).replace(".0", "");;
		
		PrintWriter.append("<tr><td>"+ReservationScenarioRunner.TestCaseCount+"</td><td>"+Message+"</td>");
		ReservationScenarioRunner.TestCaseCount++;
		PrintWriter.append("<td>"+ExpectedRateString+" Or "+F_ExpectedRateString+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {
			
			if((ExpectedRateString.equalsIgnoreCase(Actual.replace(",", ""))) || (F_ExpectedRateString.equalsIgnoreCase(Actual.replace(",", ""))))
				PrintWriter.append("<td class='Passed'>PASS</td>");
		    else
		        PrintWriter.append("<td class='Failed'>FAIL</td>");
	  
	        PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}
	}

	public void verifyTrue(double Expected,double Actual,String Message)
	{
		PrintWriter.append("<tr><td>"+ReservationScenarioRunner.TestCaseCount+"</td><td>"+Message+"</td>");
		ReservationScenarioRunner.TestCaseCount++;
		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {
			if(Actual == Expected)
				PrintWriter.append("<td class='Passed'>PASS</td>");
		    else
		        PrintWriter.append("<td class='Failed'>FAIL</td>");
	  
	        PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}
	}


	public void verifyTrue(boolean Expected,boolean Actual,String Message)
	{
		PrintWriter.append("<tr><td>"+ReservationScenarioRunner.TestCaseCount+"</td><td>"+Message+"</td>");
		ReservationScenarioRunner.TestCaseCount++;
		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {
			if(Actual == Expected)
				PrintWriter.append("<td class='Passed'>PASS</td>");
		    else
		        PrintWriter.append("<td class='Failed'>FAIL</td>");
	  
	        PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}
	}


}
