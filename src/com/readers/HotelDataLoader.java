package com.readers;



import java.text.ParseException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;


import Hotel.Utilities.Hotel;
import Hotel.Utilities.HotelContract;
import Hotel.Utilities.HotelPolicy;
import Hotel.Utilities.HotelRoom;
import com.utilities.*;
import Hotel.Utilities.ProfitMarkup;
//import Supplier.setup.Supplier;

import com.types.ChargeByType;
import com.types.HotelInventoryType;
import com.types.InventoryActionType;


public class HotelDataLoader {
	
org.apache.log4j.Logger dataLogger = null;
public HotelDataLoader()
{
	dataLogger = org.apache.log4j.Logger.getLogger(this.getClass());
}

		
public TreeMap<String,Hotel> loadHotelDetails(Map<Integer,String> map)
{
	TreeMap<String,Hotel> hotelList = new TreeMap<String, Hotel>();

Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

while(it.hasNext())
{
	Hotel CurrentHotel = new Hotel();
	String[]   AllValues     = it.next().getValue().split(",");
	
	System.out.println(AllValues[1]);

	CurrentHotel.setHotelName(AllValues[0]);
	CurrentHotel.setSupplier(AllValues[1]);
	CurrentHotel.setHotelCurrency(AllValues[2]);
	CurrentHotel.setAddressLine1(AllValues[3]);
	CurrentHotel.setAddressLine2(AllValues[4]);
	CurrentHotel.setCountry(AllValues[5]);
	CurrentHotel.setCity(AllValues[7]);
	CurrentHotel.setState(AllValues[6].trim());
	CurrentHotel.setHotelGroup(AllValues[8]);
	CurrentHotel.setStarCategory(AllValues[9]);
	CurrentHotel.setFeaturedStatus(AllValues[10]);
	CurrentHotel.setPassportrequired(AllValues[11]);
	CurrentHotel.setDisplayinCC(AllValues[12]);
	CurrentHotel.setDisplayinWeb(AllValues[13]);
	CurrentHotel.setChildrenAllowed(AllValues[14]);
	CurrentHotel.setChargesFreeAgeFrom(AllValues[15]);
	CurrentHotel.setChargesFreeAgeTo(AllValues[16]);
	CurrentHotel.setChildRateApplicableFrom(AllValues[17]);
	CurrentHotel.setChildRateApplicableTo(AllValues[18]);
	CurrentHotel.setStandardCheckIn(AllValues[19]);
	CurrentHotel.setStandardCheckOut(AllValues[20]);
	CurrentHotel.setInventoryObtainedType(com.types.InventoryObtainedByType.getinveType(AllValues[21]));
	CurrentHotel.setRateSetUpByType(com.types.RateSetupBy.getRateSetupType(AllValues[22]));
	CurrentHotel.setRateContract(com.types.RateContractByType.getRateContractType(AllValues[23]));
	CurrentHotel.setHotelContractFromDate(AllValues[24]);
	CurrentHotel.setHotelContractToDate(AllValues[25]);
	CurrentHotel.setCommHotelType(AllValues[26].trim());
	CurrentHotel.setShortDescription(AllValues[27].trim());
	CurrentHotel.setLongDescription(AllValues[28].trim());
	CurrentHotel.setLatitude(AllValues[29].trim());
	CurrentHotel.setLongitude(AllValues[30].trim());
	CurrentHotel.setFilter(AllValues[31].trim());
	CurrentHotel.setPath(AllValues[32].trim());
	CurrentHotel.setSetupNeeded(AllValues[33].trim());
	
	hotelList.put(AllValues[0], CurrentHotel);
}
	return hotelList;
	
}


public TreeMap<String,Hotel> loadRoomDetails(Map<Integer,String> map,TreeMap<String,Hotel>  HotelList)
{
	
	
    Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();
   
	while(it.hasNext())
	{
		HotelRoom Room = new HotelRoom();
		String[]   AllValues     = it.next().getValue().split(",");
		Room.setRoomType(AllValues[1]);
		Room.setBedType(AllValues[2]);
		Room.setRatePlan(AllValues[3]);
		Room.setStdAdults(AllValues[4]);
		Room.setAAdults(AllValues[5]);
		Room.setChildren(AllValues[6]);
		
		
		if(AllValues[7].equalsIgnoreCase("yes"))
		  Room.setMultipleChildRatesApplied(true);
		else
	      Room.setMultipleChildRatesApplied(false);

        
		if(AllValues[8].equalsIgnoreCase("yes"))
	      Room.setCombinationActive(true);
		else
		  Room.setCombinationActive(false);
		
		Room.setRegion(AllValues[9]);
		Room.setTourOperator(AllValues[10]);
		Room.setTotalRooms(AllValues[11]);
		Room.setMinimumNightsStay(AllValues[12]);
		Room.setMaximumNightsStay(AllValues[13]);
		Room.setCutOff(AllValues[14]);
		Room.setNetRate(AllValues[15]);
		Room.setAdditionalAdultRate(AllValues[16]);
		Room.setChildNetRate(AllValues[17]);

        
        
        try {
        	HotelList.get(AllValues[0]).AddRoom(Room);
		} catch (Exception e) {
		  dataLogger.warn(AllValues[0]+" not found in the hotel list");
		}
		
	}
		return HotelList;
	
	
}


public TreeMap<String,Hotel> loadContractDetails(Map<Integer,String> map,TreeMap<String,Hotel>  HotelList) throws ParseException
{
	

	Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

	while(it.hasNext())
	{
		HotelContract   Contract = new HotelContract();
		String[]   AllValues     = it.next().getValue().split(",");
     /*   Contract.setInventoryObtainedType(com.types.InventoryObtainedByType.getinveType(AllValues[1]));
        Contract.setRateSetUpByType(com.types.RateSetupBy.getRateSetupType(AllValues[2]));
        Contract.setRateContract(com.types.RateContractByType.getRateContractType(AllValues[3]));*/
        Contract.setInventoryType(HotelInventoryType.getInventoryType(AllValues[1]));
        Contract.setSearchSatistied(InventoryActionType.getInventoryActionType(AllValues[2]));
        Contract.setInventoryExhausted(InventoryActionType.getInventoryActionType(AllValues[3]));
        Contract.setCutoffApplied(InventoryActionType.getInventoryActionType(AllValues[4]));
        Contract.setMinNightRestriction(InventoryActionType.getInventoryActionType(AllValues[5]));
        Contract.setMAxNightRestriction(InventoryActionType.getInventoryActionType(AllValues[6]));
        Contract.setBlackout(InventoryActionType.getInventoryActionType(AllValues[7]));
        Contract.setNoArrival(InventoryActionType.getInventoryActionType(AllValues[8]));
      
        //Contract.setHotelCommission(AllValues[9]);
        Contract.setCommType(AllValues[12]);
        Contract.setCommvalue(AllValues[13]);
       
        Contract.setContractFrom(AllValues[10]);
        Contract.setContractTo(AllValues[11]);
        
        try {
        	HotelList.get(AllValues[0]).AddContract(Contract);
		} catch (Exception e) {
		  dataLogger.warn(AllValues[0]+" not found in the hotel list");
		}
        
	}
	
	    
		return HotelList;
	
	
}



public TreeMap<String,Hotel> loadPolicyDetails(Map<Integer,String> map,TreeMap<String,Hotel>  HotelList)
{
	

	Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

	while(it.hasNext())
	{
		HotelPolicy    Policy    = new HotelPolicy();
		String[]   AllValues     = it.next().getValue().split(",");

		Policy.setFrom(AllValues[1]);
		Policy.setTo(AllValues[2]);
		Policy.setArrivalLessThan(AllValues[3]);
		Policy.setCancellatrionBuffer(AllValues[4]);
		Policy.setStdChargeByType(com.types.ChargeByType.getChargeType(AllValues[5].trim()));
		Policy.setStdValue(AllValues[6]);
		Policy.setNoShowChargeByType(com.types.ChargeByType.getChargeType(AllValues[7].trim()));
		Policy.setNoshowValue(AllValues[8]);
 
        try {
        	HotelList.get(AllValues[0]).AddPolicy(Policy);
		} catch (Exception e) {
		  dataLogger.warn(AllValues[0]+" not found in the hotel list");
		}
		
	}
		return HotelList;
	
	
}



public TreeMap<String,Hotel> loadTaxDetails(Map<Integer,String> map,TreeMap<String,Hotel>  HotelList)
{
	

	Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

	while(it.hasNext())
	{
		HotelTax    tax   = new HotelTax();
		String[]   AllValues     = it.next().getValue().split(",");

		tax.setFromT(AllValues[1]);
		tax.setToT(AllValues[2]);
		tax.setSalesTaxChargeType(com.types.ChargeByType.getChargeType(AllValues[3].trim()));
		tax.setSalesTaxValue(AllValues[4]);
		tax.setOccupancyChargeType(com.types.ChargeByType.getChargeType(AllValues[5].trim()));
		tax.setOccupancyTaxValue(AllValues[6]);
		tax.setEnergyChargeType(com.types.ChargeByType.getChargeType(AllValues[7].trim()));
		tax.setEnergyTaxValue(AllValues[8]);
		tax.setMiscellaneousFees(AllValues[9]);

        
        
        try {
        	HotelList.get(AllValues[0].trim()).AddTax(tax);
		} catch (Exception e) {
		  dataLogger.warn(AllValues[0]+" not found in the hotel list");
		}
		
	}
		return HotelList;
	
	
}



public TreeMap<String,Hotel> loadMarkupDetails(Map<Integer,String> map,TreeMap<String,Hotel>  HotelList)
{
	
	Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

	while(it.hasNext())
	{
		ProfitMarkup  MarkUp     = new ProfitMarkup();
		String[]   AllValues     = it.next().getValue().split(",");

		MarkUp.setBookingChannel(AllValues[1]);
		MarkUp.setPartnerType(AllValues[2]);
		MarkUp.setChargeType(ChargeByType.getChargeType(AllValues[3]));
		MarkUp.setApplyProfitMarkupTo(AllValues[4]);
		
		if(AllValues[5].equalsIgnoreCase("yes"))
		   MarkUp.setOverriteSpecific(true);
	    else
	       MarkUp.setOverriteSpecific(false);
		
	     MarkUp.setApplicablePattern(AllValues[6]);
		 MarkUp.setAdultProfitMarkup(AllValues[7]);
         MarkUp.setAdditionalAdultProfitMarkup(AllValues[8]);
		 MarkUp.setChildProfitMarkup(AllValues[9]);
         MarkUp.setFrom(AllValues[10]);
         MarkUp.setTo(AllValues[11]);

        
        try {
            HotelList.get(AllValues[0]).AddMarkup(MarkUp);
		} catch (Exception e) {
		  dataLogger.warn(AllValues[0]+" not found in the hotel list");
		}
		
	}
		return HotelList;
	
	
}
	
/*public ArrayList<Supplier> loadSupplierDetails(Map<Integer,String> map)
{
	ArrayList<Supplier> SupplierList = new ArrayList<Supplier>();
	Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

	while(it.hasNext())
	{
		Supplier sup = new Supplier();
		String[]   AllValues     = it.next().getValue().split(",");

        sup.setSupplierCode(AllValues[0]);
		sup.setSupplierName(AllValues[1]);
		sup.setSupplierType(AllValues[2]);
		sup.setAddress(AllValues[3]);
		sup.setCountry(AllValues[4]);
		sup.setCity(AllValues[5]);
		sup.setActive(AllValues[6]);
		sup.setCurrency(AllValues[7]);
		sup.setContactName(AllValues[8]);
		sup.setEmail(AllValues[9]);
		sup.setContactMedia(AllValues[10]);
		sup.setContactType(AllValues[11]);
		
		SupplierList.add(sup);
		 
		
	}
		return SupplierList;
	
	
}*/

}
