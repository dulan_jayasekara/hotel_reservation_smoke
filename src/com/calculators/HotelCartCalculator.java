package com.calculators;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import Hotel.Utilities.Hotel;
import app.support.CartDetails;
import app.support.DataLoader;
import app.support.SearchType;

import com.types.ChargeByType;
import com.utilities.HotelTax;

public class HotelCartCalculator {
		
//private 
	

	private	Double              SalesTax           = 0.0;
	private	Double              OccupancyTax       = 0.0;
	private	Double              EnergyTax          = 0.0;
	private	Double              Miscellaneoustax   = 0.0;
	private	Double              Totaltax           = 0.0;
	private	Double              ServiceFee         = 0.0;
	private Double              TaxAndOtherCharges = 0.0;
	private Hotel               CurrentHotel       = null;
	private SearchType          CurrentSearch      = null;
    private HashMap<String, String>   PropertySet  = null;
       
	 
	

public HotelCartCalculator(Hotel CurrentHotel,SearchType CurrentSearch, HashMap<String, String> properties )
{
	       this.CurrentHotel  = CurrentHotel;
	       this.CurrentSearch = CurrentSearch;
	       this.PropertySet   = properties;
     
}





public HotelTax  getApplicableTax(ArrayList<HotelTax> taxList,String Checkin)
{
SimpleDateFormat sdf           = new SimpleDateFormat("MM/dd/yyyy");
SimpleDateFormat sdf2          = new SimpleDateFormat("dd-MMM-yyyy");
HotelTax         returnTax     = null;
Date             checkin       = null;

try {
	 checkin         = sdf.parse(Checkin);
} catch (Exception e) {
	 System.out.println("Parse Exception ");
}

for (Iterator<HotelTax> iterator = taxList.iterator(); iterator.hasNext();) {
	HotelTax hotelTax = (HotelTax) iterator.next();
    
	Date TaxFrom          = null;
	Date TaxTo            = null; 
	try {
		TaxFrom      =  sdf2.parse(hotelTax.getFromT());
		TaxTo        =  sdf2.parse(hotelTax.getToT());
	} catch (Exception e) {
		System.out.println("Parse Exception ");
	}

	if((checkin.before(TaxTo) || checkin.equals(TaxTo)) && (checkin.after(TaxFrom) || checkin.equals(TaxFrom)))
    returnTax = hotelTax;
	
}

   return returnTax;

}



public CartDetails performCartCalculations(CartDetails Cart,DataLoader Loader)
{
	
	ArrayList<HotelTax> TaxList          = CurrentHotel.getHotelTax();
	String              CheckIn          = CurrentSearch.getArrivalDate();
	HotelTax            ApplicableTax    = getApplicableTax(TaxList, CheckIn);

	int     NoOfNights   = Integer.parseInt(CurrentSearch.getNights());
	int     Rooms        = Integer.parseInt(CurrentSearch.getRooms());
	double  netRate      = Cart.getNetRate();
	

	if(ApplicableTax != null)
	{
		
		//Sales Tax Calculation
		ChargeByType SalesTaxType    = ApplicableTax.getSalesTaxChargeType();
		String       SalesTaxValue   = ApplicableTax.getSalesTaxValue();
		
	     if(SalesTaxType == ChargeByType.PERCENTAGE)
	     SalesTax             =(netRate*Double.parseDouble(SalesTaxValue))/100;
	     
	     else if(SalesTaxType == ChargeByType.VALUE)
	     SalesTax     	      = Double.parseDouble(SalesTaxValue) * Rooms * NoOfNights;
	     
	     //////////////////////////////////////////////////////////////////
	     
	     //Occupancy Tax Calculation
	     
	 	ChargeByType OccuTaxType    = ApplicableTax.getOccupancyChargeType();
	 	String       OccuTaxValue   = ApplicableTax.getOccupancyTaxValue();
	 	
	      if(OccuTaxType == ChargeByType.PERCENTAGE)
	      OccupancyTax             = (netRate*Double.parseDouble(OccuTaxValue))/100;
	      
	      else if(OccuTaxType == ChargeByType.VALUE)
	      OccupancyTax     	      = Double.parseDouble(OccuTaxValue) * NoOfNights;
	      
	      ////////////////////////////////////////////////////////////////////
	      //Energy Tax Calculation
	      
	  	ChargeByType EnergyTaxType    = ApplicableTax.getEnergyChargeType();
	  	String       EnergyTaxValue   = ApplicableTax.getEnergyTaxValue();
	  	
	       if(EnergyTaxType == ChargeByType.PERCENTAGE)
	       EnergyTax              = (netRate*Double.parseDouble(EnergyTaxValue))/100;
	       
	       else if(EnergyTaxType == ChargeByType.VALUE)
	       EnergyTax     	      = Double.parseDouble(EnergyTaxValue) * Rooms * NoOfNights;
	       
	     ////////////////////////////////////////////////////////////////////////  
	       //Miscellenious Tax Calculation
	       
	   	
	    String       MiscTaxValue   = ApplicableTax.getMiscellaneousFees();
	   // Miscellenious Tax will not be multiplied by number of nights according to randunu
	   // Miscellaneoustax     	    = Double.parseDouble(MiscTaxValue) * NoOfNights;
	    
	    Miscellaneoustax     	    = Double.parseDouble(MiscTaxValue);
		//////////////////////////////////////////////////////////////////////////
	        
	    Totaltax   = Loader.convertCurrency(Double.toString(EnergyTax + OccupancyTax + SalesTax + Miscellaneoustax),CurrentHotel.getHotelCurrency().trim(),PropertySet.get("portal.currency").trim()) ;
	     
		
	}else {
		
		 Totaltax   = 0.0 ;
	}

	//*********** Service Fee Calculation *************** //

	ChargeByType ServiceFeeType  = Cart.getServiceFeeType();
	String       ServiceFeeValue = Cart.getServiceFeeInPortalCurrency();

	    if(ServiceFeeType == ChargeByType.PERCENTAGE)
	    ServiceFee              =  Loader.convertCurrency(Double.toString((netRate*Double.parseDouble(ServiceFeeValue))/100),CurrentHotel.getHotelCurrency(), PropertySet.get("portal.currency").trim());
	    
	    else if(ServiceFeeType == ChargeByType.VALUE)
	    ServiceFee     	       =  Double.parseDouble(ServiceFeeValue);

	    TaxAndOtherCharges     = Totaltax + ServiceFee ;
	    
	    String Rate   = "";
	    
	    if(!CurrentHotel.getHotelCurrency().trim().equalsIgnoreCase(CurrentSearch.getSearchCurrency().trim()))
	    Rate = Double.toString(Loader.convertCurrency(Cart.getRate(), CurrentHotel.getHotelCurrency().trim(), CurrentSearch.getSearchCurrency().trim()));
	    else
	    Rate = Cart.getRate();
	    
	    Cart.setTotalHotelTaxesinBase(Totaltax);
	    Cart.setTaxesAndOtherCharges(TaxAndOtherCharges);
	    Cart.setCalculatedServiceFeeInSelling(Loader.convertCurrency(Double.toString(ServiceFee), PropertySet.get("portal.currency").trim(), CurrentSearch.getSearchCurrency().trim()));
	    Cart.setRate(Rate);
	    Cart.setSubTotal(Cart.getRate());
	    Cart.setTaxes(Double.toString(Loader.convertCurrency(Double.toString(TaxAndOtherCharges), PropertySet.get("portal.currency").trim(), CurrentSearch.getSearchCurrency().trim())));
	    Cart.setTotaPaybel(Double.toString(Double.parseDouble(Cart.getSubTotal()) + Double.parseDouble(Cart.getTaxes())));
	    
	    return Cart;

}


}
