package com.calculators;

import java.util.ArrayList;
import java.util.HashMap;

import Hotel.Utilities.Supplementary;

public class ResultsSupplemantary {

private boolean                                     isSupplemantaryApplicable                = false;
private HashMap<String, ArrayList<Supplementary>>   ApplicableSupplementary                  = new HashMap<String, ArrayList<Supplementary>>();
private HashMap<String, ArrayList<Supplementary>>   MandatorySupplementary                   = new HashMap<String, ArrayList<Supplementary>>();
private HashMap<String, ArrayList<Supplementary>>   OptionalSupplementary                    = new HashMap<String, ArrayList<Supplementary>>();
private double                                      MandatoryNetRate                         = 0.00;     
private double                                      MandatorySellRate                        = 0.00;   
private double                                      TotalSupplementaryNetRate                = 0.00;     
private double                                      TotalSupplementarySellRate               = 0.00;



public boolean isSupplemantaryApplicable() {
	return isSupplemantaryApplicable;
}
public void setSupplemantaryApplicable(boolean isSupplemantaryApplicable) {
	this.isSupplemantaryApplicable = isSupplemantaryApplicable;
}
public HashMap<String, ArrayList<Supplementary>> getApplicableSupplementary() {
	return ApplicableSupplementary;
}
public void setApplicableSupplementary(
		HashMap<String, ArrayList<Supplementary>> applicableSupplementary) {
	ApplicableSupplementary = applicableSupplementary;
}
public HashMap<String, ArrayList<Supplementary>> getMandatorySupplementary() {
	return MandatorySupplementary;
}
public void setMandatorySupplementary(
		HashMap<String, ArrayList<Supplementary>> mandatorySupplementary) {
	MandatorySupplementary = mandatorySupplementary;
}
public HashMap<String, ArrayList<Supplementary>> getOptionalSupplementary() {
	return OptionalSupplementary;
}
public void setOptionalSupplementary(
		HashMap<String, ArrayList<Supplementary>> optionalSupplementary) {
	OptionalSupplementary = optionalSupplementary;
}
public double getMandatoryNetRate() {
	return MandatoryNetRate;
}
public void setMandatoryNetRate(double mandatoryNetRate) {
	MandatoryNetRate = mandatoryNetRate;
}
public double getMandatorySellRate() {
	return MandatorySellRate;
}
public void setMandatorySellRate(double mandatorySellRate) {
	MandatorySellRate = mandatorySellRate;
}
public double getTotalSupplementaryNetRate() {
	return TotalSupplementaryNetRate;
}
public void setTotalSupplementaryNetRate(double totalSupplementaryNetRate) {
	TotalSupplementaryNetRate = totalSupplementaryNetRate;
}
public double getTotalSupplementarySellRate() {
	return TotalSupplementarySellRate;
}
public void setTotalSupplementarySellRate(double totalSupplementarySellRate) {
	TotalSupplementarySellRate = totalSupplementarySellRate;
}   



}
