package com.reservation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import Hotel.Utilities.Hotel;
import Hotel.Utilities.HotelContract;
import Hotel.Utilities.ResultsCancellationPolicyProcesser;
import app.support.CancellationPolicy;
import app.support.CartDetails;
import app.support.ConfirmationPage;
import app.support.DataLoader;
import app.support.MethodClass;
import app.support.PaymentGateway;
import app.support.PaymentsPage;
import app.support.ResultsType;
import app.support.RoomType;
import app.support.SearchAgainBEC;
import app.support.SearchType;
import app.support.ViewFullDetails;
import com.calculators.HotelCartCalculator;
import com.framework.reports.ReportTemplate;
import com.readers.HotelDataLoader;
import com.readers.ReadExcel;
import com.reports.ReportReader;
import com.reports.ReportVerificationClass;
import com.reports.ReservationReport;
import com.reports.profitAndLostReport;
import com.types.BookingType;
import com.types.ChargeByType;
import com.types.CommssionableType;
import com.types.RateContractByType;
import com.types.ResultsRoom;
import com.utilities.Calender;
import com.utilities.Occupancy;
import com.utilities.PerfomanceSingleton;

import com.utilities.ScreenshotPathSingleton;
//import com.utilities.ExcelReader;
import com.utilities.ReadProperties;
import org.apache.commons.io.FileUtils;
//import org.apache.jasper.tagplugins.jstl.core.If;
import org.apache.log4j.*;
import org.apache.log4j.xml.DOMConfigurator;

//******~~~ Master Branch ~~~*****//
public class ReservationScenarioRunner {

	private WebDriver driver;
	private HashMap<String, String> PropertySet;
	private Map<String, SearchType> SearchList;
	private ArrayList<Map<Integer, String>> SheeetList;
	private ArrayList<Map<Integer, String>> HotelSheetList;
	private MethodClass methodClass;
	private StringBuffer PrintWriter;
	private TreeMap<String, Hotel> HotelList;
	private int ScenarioCount;
	public static int TestCaseCount = 1;
	private Map<Integer, String> Createmap;
	private Map<Integer, String> Contractmap;
	private Map<Integer, String> Policymap;
	private Map<Integer, String> Occupancymap;
	private Map<Integer, String> PMmap;
	private Map<Integer, String> Taxmap;
	private Map<Integer, String> Supplimentarymap;
	private Map<Integer, String> Promotionmap;
	private DataLoader Loader;
	private Logger logger;
	private ReportTemplate PrintTemplate;
	private ConfirmationPage ActualConfPage;
	private ConfirmationPage ExpectedConfPage;

	@Before
	public void setUp() throws IOException {
		logger = Logger.getLogger(this.getClass());
		DOMConfigurator.configure("log4j.xml");
		SearchList = new HashMap<String, SearchType>();
		ReadExcel readExcel = new ReadExcel();
		PropertySet = ReadProperties.readpropeties();
		PrintWriter = new StringBuffer();
		PrintTemplate = ReportTemplate.getInstance();
		PrintTemplate.Initialize(PrintWriter, PropertySet);
		ActualConfPage = new ConfirmationPage();
		ExpectedConfPage = new ConfirmationPage();

		try {
			Loader = new DataLoader(PropertySet.get("portal.currency"));
		} catch (Exception e) {
			logger.fatal("Currecncy Exchange Updation Failed-->May be System unavailable-->" + e.toString());

		}
		HotelDataLoader HotelLoder = new HotelDataLoader();
		methodClass = new MethodClass(PropertySet);
		ScenarioCount = 0;

		try {
			logger.info("Initializing Excel WorkBooks -");
			logger.info("WorkBooks Paths -Search Excel---> " + PropertySet.get("ExcelPath") + "-Hotel Excel-->" + PropertySet.get("Create.Excel.Path"));
			SheeetList = readExcel.init(PropertySet.get("ExcelPath"));
			HotelSheetList = readExcel.init(PropertySet.get("Create.Excel.Path"));
		} catch (Exception e) {
			logger.fatal("Error when initializing the Excel WorkBooks -", e);
		}

		// HotelList = Loader.loadHotelDetails(SheeetList.get(2));
		// HotelList = Loader.loadRoomDetails(SheeetList.get(3),HotelList);

		Createmap = HotelSheetList.get(0);
		Contractmap = HotelSheetList.get(1);
		Policymap = HotelSheetList.get(2);
		Taxmap = HotelSheetList.get(5);
		Occupancymap = HotelSheetList.get(3);
		PMmap = HotelSheetList.get(4);
		Supplimentarymap = HotelSheetList.get(6);
		Promotionmap = HotelSheetList.get(7);

		// Loading Hotel Details
		try {
			HotelList = HotelLoder.loadHotelDetails(Createmap);
			logger.info("Hotel Details Loaded SuccessFully");
			logger.debug(HotelList);
		} catch (Exception e) {
			logger.fatal("Hotel Details not Loaded SuccessFully");
			logger.fatal(e.toString());
		}

		// Loading Contract Details

		try {
			HotelList = HotelLoder.loadContractDetails(Contractmap, HotelList);
			logger.info("Hotel Contracts Loaded SuccessFully");
			logger.debug(HotelList);
		} catch (Exception e) {
			logger.fatal("Hotel Contracts not Loaded SuccessFully");
			logger.fatal(e.toString());
		}

		// Loading Policy Details
		try {
			HotelList = HotelLoder.loadPolicyDetails(Policymap, HotelList);
			logger.info("Hotel policies Loaded SuccessFully");
			logger.debug(HotelList);
		} catch (Exception e) {
			logger.fatal("Hotel Policies not Loaded SuccessFully");
			logger.fatal(e.toString());
		}
		try {
			HotelList = Loader.loadTaxDetails(Taxmap, HotelList);
			logger.info("Tax Details Loaded SuccessFully");
			logger.debug(HotelList);
		} catch (Exception e) {
			logger.fatal("Tax Details not Loaded SuccessFully");
			logger.fatal(e.toString());
		}
		// Loading Room Details

		try {
			HotelList = HotelLoder.loadRoomDetails(Occupancymap, HotelList);
			logger.info("Hotel Room Details Loaded SuccessFully");
			logger.debug(HotelList);
		} catch (Exception e) {
			logger.fatal("Hotel Room Details not Loaded SuccessFully");
			logger.fatal(e.toString());
		}

		// Loading Profit Markup Details

		try {
			HotelList = HotelLoder.loadMarkupDetails(PMmap, HotelList);
			logger.info("Hotel Markup Details Loaded SuccessFully");
			logger.debug(HotelList);
		} catch (Exception e) {
			logger.fatal("Hotel Markup Details not Loaded SuccessFully");
			logger.fatal(e.toString());
		}

		// Loading Supplementary Details

		try {
			HotelList = Loader.loadSupplimentaryDetails(Supplimentarymap, HotelList);
			logger.info("Hotel Supplimentary Details Loaded SuccessFully");
			logger.debug(HotelList);
		} catch (Exception e) {
			logger.fatal("Hotel Supplimentary Details Loading Failed");
			logger.fatal(e.toString());
		}

		// Loading Promotion Details
		try {
			HotelList = Loader.loadPromoDetails(Promotionmap, HotelList);
			logger.info("Hotel Promotion Details Loaded SuccessFully");
			logger.debug(HotelList);
		} catch (Exception e) {
			logger.fatal("Hotel Promotion Details not Loaded SuccessFully");
			logger.fatal(e.toString());
		}
		// =========================Loading Search Details============================//

		try {
			SearchList = Loader.loadSearchDetails(SheeetList.get(0), HotelList);
			logger.info("Initial Search Details Loaded Successfully");
			logger.debug(SearchList);
		} catch (Exception e) {
			logger.fatal("Initial Search Details Not Loaded Sucessfully");
			logger.fatal(e.toString());
		}

		try {
			SearchList = Loader.loadRoomDetails(SheeetList.get(1), SearchList);

			// TODO:Delete this///////////////////////////////////////

			Iterator<Map.Entry<String, SearchType>> iter = SearchList.entrySet().iterator();

			while (iter.hasNext()) {
				Map.Entry<String, SearchType> type = iter.next();
				String index = type.getKey();
				SearchType Value = type.getValue();

				for (Iterator<Map.Entry<String, ArrayList<RoomType>>> iterator = Value.getResultsRoomMap().entrySet().iterator(); iterator.hasNext();) {
					Map.Entry<String, ArrayList<RoomType>> type2 = (Entry<String, ArrayList<RoomType>>) iterator.next();
					logger.info("Scenario " + index + " Room " + type2.getKey() + " rooms--->");
					System.out.println("Scenario " + index + " Room " + type2.getKey() + " rooms--->");

					for (Iterator<RoomType> iterator2 = type2.getValue().iterator(); iterator2.hasNext();) {
						RoomType type3 = (RoomType) iterator2.next();
						System.out.println(type3.getRoomType());

					}

				}
			}

			// //////////////////////////////////////////////////////////////////
			logger.info("Room Details Details Loaded Successfully");
			logger.debug(SearchList);
		} catch (Exception e) {
			logger.fatal("Room Details Details Not Loaded Sucessfully");
			logger.fatal(e.toString());
		}

		try {
			SearchList = Loader.loadCartDetails(SheeetList.get(2), SearchList);
			logger.info("Cart Details Loaded Successfully");
			logger.debug(SearchList);
		} catch (Exception e) {
			logger.fatal("Cart Details Not Loaded Sucessfully", e);
			// logger.fatal(e.toString());
		}

		/*
		 * try { SearchList = Loader.loadCancellationDetails(SheeetList.get(3), SearchList); logger.info("Initial Cancellation Details Loaded Successfully"); logger.debug(SearchList); } catch
		 * (Exception e) { logger.fatal("Initial Search Details Not Loaded Sucessfully"); logger.fatal(e.toString()); }
		 */

		logger.info("=====================End of data loading process And Starting Driver Initialization=========================");

		try {
			driver = methodClass.initalizeDriver();
			logger.info("Driver initialized Successfully");
		} catch (Exception e) {
			logger.fatal("Driver initialization failed");
			logger.fatal(e.toString());
		}
	}

	@Test
	public void runTest() throws IOException, InterruptedException {

		PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>V3- BASE NET RATE CHECK MAIN(" + Calender.getDate(Calendar.DATE, 0) + ")</p></div>");
		PrintWriter.append("<body>");

		PrintWriter.append("<br><p class='InfoSup'>Basic System Check</p><br>");
		PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

		PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>System availability and login function</td>");
		TestCaseCount++;
		PrintWriter.append("<td>System should available and should be able to logged in</td>");

		boolean isLoginSuccess = false;
		try {
			isLoginSuccess = (methodClass.login(driver));
			if (!isLoginSuccess) {

				PrintWriter.append("<td>login failed--->" + PropertySet.get("Portal.Url") + "</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				PrintWriter.append("</table>");
				driver.quit();

			} else {
				PrintWriter.append("<td> Logged in successfully </td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				PrintWriter.append("</table>");

			}
		} catch (Exception e) {
			File Scr = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(Scr, new File(Calender.getDate(Calendar.DATE, 0) + "_Errors/System_unavailable.jpg"));
			PrintWriter.append("<td>system not available--->" + PropertySet.get("Portal.Url") + "</td>");
			PrintWriter.append("<td class='Failed'>FAIL</td>");
			PrintWriter.append("<td></td></tr>");
			PrintWriter.append("</table>");
			driver.quit();

		}

		PrintWriter.append("</body></html>");
		BufferedWriter bwr = new BufferedWriter(new FileWriter(new File("Reports/V3_Base_Basic_Test.html")));
		bwr.write(PrintWriter.toString());
		bwr.flush();
		bwr.close();
		PrintWriter.setLength(0);

		Iterator<Map.Entry<String, SearchType>> it = SearchList.entrySet().iterator();
		logger.info("=============Starting Scenario Excecution===============");

		ScenarioLoop: while (it.hasNext() && isLoginSuccess) {

			String CurrentScenario = "ERR";
			try {

				Map.Entry<String, SearchType> entry = it.next();
				ScenarioCount++;
				ResultsType CurrentResult;
				CurrentScenario = entry.getKey();
				SearchType CurrentSearch = entry.getValue();
				logger.info("Current Scenario ======> " + CurrentScenario);
				ScreenshotPathSingleton.getInstance().setScenarioPaths(CurrentScenario);

				if (CurrentSearch.isScenarioExcecutionState()) {
					// ===============================================================================================//
					ResultsCancellationPolicyProcesser prcessor = new ResultsCancellationPolicyProcesser(CurrentSearch, HotelList.get(CurrentSearch.getHotelName()));
					CancellationPolicy ExpectedCancel = prcessor.getExpectedCancellationPolicy(Loader);
					CurrentSearch.setPolicy(ExpectedCancel);
					System.out.println(ExpectedCancel.getDeadlineString());
					System.out.println(ExpectedCancel.getRefundable());

					for (int i = 0; i < ExpectedCancel.getPolicies().size(); i++) {
						System.out.println(ExpectedCancel.getPolicies().get(i));

					}

					// ===============================================================================================//

					logger.info("<-- Excecution state set as true continue with the scenario --> ");

					PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
					PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>" + PropertySet.get("Portal.Name") + " HOTEL RESERVATION SCENARIO -" + CurrentScenario + "("
							+ Calender.getDate(Calendar.DATE, 0) + ")</p></div>");
					PrintWriter.append("<body>");

					PrintWriter.append("<br><br>");
					PrintWriter.append("<div style=\"border:1px solid;border-radius:3px 3px 3px 3px;background-color:#8D8D86; width:50%;\">");
					PrintWriter.append("<p class='InfoSup'style='font-weight: bold;'>" + ScenarioCount + ")<-------" + CurrentSearch.getDescription() + "-------><br></p>");
					PrintWriter.append("<p class='InfoSub'>Current Search Crieteria</p>");
					PrintWriter.append("<p class='InfoSub'>Scenatio ID:" + CurrentScenario + "</p>");
					PrintWriter.append("<p class='InfoSub'>Hotel Name: " + CurrentSearch.getHotelName() + "</p>");
					PrintWriter.append("<p class='InfoSub'>UserName: " + PropertySet.get("portal.username") + "</p>");
					PrintWriter.append("<p class='InfoSub'>Password: " + PropertySet.get("portal.password") + "</p>");
					PrintWriter.append("<p class='InfoSub'>URL   : " + PropertySet.get("Portal.Url").concat("/admin/common/LoginPage.do") + "</p>");
					PrintWriter.append("<p class='InfoSub'>Date  : " + CurrentSearch.getArrivalDate() + "</p>");
					PrintWriter.append("<p class='InfoSub'>Rooms : " + CurrentSearch.getRooms() + "</p>");
					PrintWriter.append("<p class='InfoSub'>From  : " + CurrentSearch.getCountryOFResidance() + "</p>");
					PrintWriter.append("<p class='InfoSub'>Nights: " + CurrentSearch.getNights() + "</p>");
					PrintWriter.append("<p class='InfoSub'>To    : " + CurrentSearch.getLocation() + "</p>");

					Map<Integer, String[]> ChildAges = CurrentSearch.getChildAges();
					for (int i = 0; i < (CurrentSearch.getAdults()).length; i++) {
						String Ad = CurrentSearch.getAdults()[i];
						String Ch = CurrentSearch.getChildren()[i];
						String Age = "";
						String[] Ages = ChildAges.get(i + 1);
						for (int j = 0; j < Ages.length; j++) {
							Age = Age + "," + Ages[j];
						}
						if (Age.equals(",0"))
							Age = "-";
						PrintWriter.append("<p class='InfoSub'>Occupancy: (Room " + (i + 1) + ")Adults- " + Ad + " Child Count: " + Ch + " Child Ages:" + Age + "</p>");
					}

					/*
					 * PrintWriter.append("<p class='InfoSub'>Occupancy: (Room 1)Adults- "+CurrentSearch.getAdults()+"Child Count:"+CurrentSearch.getChildren()+"</p>");
					 * PrintWriter.append("<p class='InfoSub'>Occupancy: (Room 1)Adults- "+CurrentSearch.getAdults()+"Child Count:"+CurrentSearch.getChildren()+"</p>");
					 */
					PrintWriter.append("</div>");
					PrintWriter.append("<br>");

					methodClass.getReservationPageCC(driver);
					CurrentResult = methodClass.search(driver, CurrentSearch, PrintWriter);

					if (!CurrentResult.isSearchPageLoaded())
						break;

					TestCaseCount = 1;
					PrintWriter.append("<br><p class='InfoSup'>Hotel Results General Validations</p><br>");
					PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

					PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Results Availability</td>");
					TestCaseCount++;
					PrintWriter.append("<td>Hotel Results Should Available</td>");

					if (CurrentResult.isAvailabilityStatus() == true) {
						CartDetails cart_details = null;
						SearchAgainBEC searchAgain = null;

						PrintWriter.append("<td>Hotel Results Available</td>");
						PrintWriter.append("<td class='Passed'>PASS</td>");
						PrintWriter.append("<td>" + CurrentResult.getErrorMessage() + "</td></tr>");

						PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Number Of Hotels Per Page</td>");
						TestCaseCount++;
						int expectedHotelsPerPage = (CurrentResult.getNumberOfHotels() < 10 ? CurrentResult.getNumberOfHotels() : 10);
						PrintWriter.append("<td>" + expectedHotelsPerPage + "</td>");
						PrintWriter.append("<td>" + CurrentResult.getItemsPerPage() + "</td>");

						if (expectedHotelsPerPage == CurrentResult.getItemsPerPage()) {
							PrintWriter.append("<td class='Passed'>PASS</td>");
							PrintWriter.append("<td></td></tr>");
						} else {
							PrintWriter.append("<td class='Failed'>FAIL</td>");
							PrintWriter.append("<td></td></tr>");
						}

						CurrentResult = methodClass.getHotelAvailability(driver, CurrentSearch, CurrentResult);

						PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Specified Hotel Availability</td>");
						TestCaseCount++;
						if (!CurrentSearch.getAvailability().equalsIgnoreCase("NotAvailable"))
							PrintWriter.append("<td>Specified Hotel Should Available in Results</td>");
						else
							PrintWriter.append("<td>Hotel Should not available in results</td>");

						if (CurrentResult.isHotelFound()) {
							PrintWriter.append("<td>Specified Hotel Available in results</td>");
							if (!CurrentSearch.getAvailability().equalsIgnoreCase("NotAvailable")) {
								PrintWriter.append("<td class='Passed'>PASS</td>");
								PrintWriter.append("<td>" + CurrentResult.getErrorMessage() + "</td></tr>");

							} else {
								PrintWriter.append("<td class='Failed'>FAIL</td>");
								PrintWriter.append("<td>Check Screenshots</td></tr>");
								PrintWriter.append("</table>");
								break ScenarioLoop;

							}

							// ******************Getting Search Again Details *****************************//
							try {
								searchAgain = methodClass.getSearchAgainBEC(driver);
								CurrentResult.setSearchAgainBEC(searchAgain);
							} catch (Exception e) {
								logger.fatal("Exception thrown in Add to Cart proccess of cart detail extraction process ", e);
							}

							// ******************Getting Search Again Details Ends *****************************//

							// ******************Adding Product to Cart *****************************//
							try {
								cart_details = methodClass.addToCart(driver, CurrentResult.getHotelElement());
								CurrentResult.setResultCart(cart_details);
							} catch (Exception e) {
								logger.fatal("Exception thrown in Add to Cart proccess of cart detail extraction process", e);
							}

							// ******************Adding Product to Cart Ends *****************************//

							if (cart_details.isIsAddedToCart()) {
								try {

									// ****************** Checking ViewFull Details *****************************//
									try {
										ViewFullDetails view_details = methodClass.getViewFullDetails(CurrentSearch.getRooms());
										CurrentResult.setViewFullDetails(view_details);
									} catch (Exception e) {
										logger.fatal("Exception thrown when extracting viewfull details  ", e);
									}

									// ****************** ViewFull Details Check Ends *****************************//

									try {
										methodClass.CheckOut();
									} catch (Exception e) {
										logger.fatal("Error in checkout", e);
									}

									PaymentsPage payment = methodClass.getPayments_Pg_BK_Details(CurrentSearch.getRooms());

									if (payment.isLoded()) {
										try {
											payment = methodClass.getPayments_Pg_Pay_Details(payment);
											payment = methodClass.fillCompleteCustomerInfo(payment);
											payment = methodClass.fillPayments_Pg_OC_Details(CurrentSearch, payment);
											methodClass.fillNotes(CurrentSearch, CurrentResult.getHiddenId().trim());
											CurrentResult.setResultPayment(payment);
											// TODO:Undo This
											CurrentResult = methodClass.ReservationConfirmation(CurrentResult, CurrentSearch);
										} catch (Exception e) {
											logger.fatal("Error Occured in payment page ", e);
											CurrentResult.setResultPayment(payment);
										}
									}

								} catch (Exception e) {
									logger.fatal("Exception", e);
								}
							}

							Map<String, ArrayList<RoomType>> ExpectedRoomMap = CurrentSearch.getResultsRoomMap();
							Map<String, ArrayList<ResultsRoom>> AvailableRoomMap = CurrentResult.getActualResultsRoomMap();

							Hotel CurrentHotel = HotelList.get(CurrentSearch.getHotelName());
							// String SupplierCurrency = ExpectedRoomMap.get("1").get(0).getCurrency();
							String SupplierCurrency = CurrentHotel.getHotelCurrency().trim();
							String Address = CurrentHotel.getAddressLine1() + " " + CurrentHotel.getAddressLine2() + " " + CurrentHotel.getCity();
							PrintTemplate.verifyTrue(Address, CurrentResult.getHotelAddress(), "Hotel Address Validation");
							PrintTemplate.verifyContains(CurrentHotel.getStarCategory(), CurrentResult.getStarRating(), "Hotel Star Rating Validation");
							PrintTemplate.verifyTrue(CurrentHotel.isFeaturedStatus(), CurrentResult.getFeaturedProperty(), "Hotel Featured Property Validation");
							PrintTemplate.verifyTrue(CurrentSearch.isBestRateApplied(), CurrentResult.isBestRate(), "Hotel Best Rate Property Validation");
							PrintTemplate.verifyTrue(CurrentSearch.isDealOfTheDayApplied(), CurrentResult.isDealOfTheDay(), "Hotel Deal Of the day Property Validation");
							PrintTemplate.verifyTrue(CurrentSearch.getAvailability(), CurrentResult.getHotelStatus(), "Hotel Availability Status Validation");
							PrintTemplate.verifyTrue(CurrentSearch.getSearchCurrency(), CurrentResult.getHotelCurrency(), "Hotel Currency Check Validation");

							String ConvertedRate = Double.toString(Loader.convertCurrency(CurrentSearch.getTotalRate(), SupplierCurrency, CurrentSearch.getSearchCurrency()));
							String ConAdjested = Double.toString(Loader.convertCurrency(CurrentSearch.getCart().getRate(), SupplierCurrency, CurrentSearch.getSearchCurrency()));
							PrintTemplate.verifyTrueValue(ConvertedRate, CurrentResult.getTotalRate().split(" ")[1], "Hotel Total Rate  Validation");
							PrintTemplate.verifyTrueValue(ConAdjested, CurrentResult.getAdjustedRate().split(" ")[1], "Hotel Total Adjusted Rate  Validation");
							PrintTemplate.verifyTrue(CurrentSearch.getRooms(), Integer.toString(AvailableRoomMap.size()), "Number Of Rooms Validation");

							for (int i = 1; i <= (Integer.parseInt(CurrentSearch.getRooms())); i++) {
								PrintTemplate.verifyTrue(Integer.toString(ExpectedRoomMap.get(Integer.toString(i)).size()), Integer.toString(AvailableRoomMap.get(Integer.toString(i)).size()), "Total Available Rooms For Room " + i);
							}

							PrintWriter.append("</table>");
							PrintWriter.append("<br><br>");
							PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">Individual Room Validations</p><br>");

							for (int i = 1; i <= ExpectedRoomMap.size(); i++) {

								PrintWriter.append("<p class=\"InfoSup\" style=\"font-weight: bold;\">Room" + i + "<br></p>");

								ArrayList<RoomType> OptionRoomList = ExpectedRoomMap.get(Integer.toString(i));
								ArrayList<ResultsRoom> AvailRoomList = AvailableRoomMap.get(Integer.toString(i));

								for (Iterator<RoomType> iterator = OptionRoomList.iterator(); iterator.hasNext();) {
									RoomType roomType = (RoomType) iterator.next();
									String UniqueStr = roomType.getRoomType() + "/" + roomType.getBedType() + "/" + roomType.getRateType();
									ResultsRoom ResultRm = CheckRoomAvailability(UniqueStr, AvailRoomList);

									PrintWriter.append("<br><table ><tr><td>ROOM CODE</td><td>" + roomType.getRoomType() + "/" + roomType.getBedType() + "/" + roomType.getRateType() + "</td></tr>");
									PrintWriter.append("<tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

									PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Room Availability Availability</td>");
									TestCaseCount++;
									PrintWriter.append("<td>Specified Room Should Available</td>");

									if (!ResultRm.getRoomString().equalsIgnoreCase("")) {
										PrintWriter.append("<td>Room  Available </td>");
										PrintWriter.append("<td class='Passed'>PASS</td>");
										PrintWriter.append("<td></td></tr>");

										PrintTemplate.verifyTrue(roomType.getRoomLable(), ResultRm.getRoomString(), "Room Lable");
										PrintTemplate.verifyTrue(roomType.getRoomType(), ResultRm.getRoomType(), "Room Type");
										PrintTemplate.verifyTrue(roomType.getBedType(), ResultRm.getBedType(), "Bed Type");
										PrintTemplate.verifyTrue(roomType.getRateType(), ResultRm.getRatePlan(), "Rate Plan");
										PrintTemplate.verifyTrue(roomType.isPromotionApplied(), ResultRm.isPromotionApplied(), "Promotion Applied");

										try {
											if (roomType.isPromotionApplied())
												PrintTemplate.verifyTrue(CurrentHotel.getHotelPromotions().get(roomType.getPromoCode().trim()).getNote(), ResultRm.getPromoNote(), "Promotion Note Validation");
											else
												PrintTemplate.verifyTrue("N/A", ResultRm.getPromoNote(), "Promotion Note Validation");
										} catch (Exception e) {
											logger.fatal("Script Error -> Promotion with specified code not available ", e);
										}

										PrintTemplate.verifyTrue(CurrentSearch.getSearchCurrency(), ResultRm.getRoomRate().split(" ")[0], "Room Display Currency");
										PrintTemplate.verifyTrueValue(Double.toString(Loader.convertCurrency(roomType.getTotalRate(), SupplierCurrency, CurrentSearch.getSearchCurrency())), ResultRm.getRoomRate().split(" ")[1], "Total Rate");
										PrintWriter.append("</table>");
									} else {
										PrintWriter.append("<td>Room Not Available in results </td>");
										PrintWriter.append("<td class='Failed'>FAIL</td>");
										PrintWriter.append("<td></td></tr>");

									}
								}
							}

							// Results Page Cancellation Policy

							CancellationPolicy Actualpolicy = CurrentResult.getResultsPageCancellation();
							CancellationPolicy ExpectedPolicy = CurrentSearch.getPolicy();

							PrintWriter.append("<br><br>");
							PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">Results Page Cancellation Policy Validation</p><br>");
							PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");
							PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Cancellation Popup Availability</td>");
							TestCaseCount++;
							PrintWriter.append("<td>Cancellation pop up should Available</td>");

							if (CurrentResult.isRP_Can_PopAvailable()) {
								PrintWriter.append("<td>Cancellation Policy PopUp  Available </td>");
								PrintWriter.append("<td class='Passed'>PASS</td>");
								PrintWriter.append("<td></td></tr>");

								PrintTemplate.verifyTrue(ExpectedPolicy.getNumberofpolicies().trim(), Actualpolicy.getNumberofpolicies().trim(), "Check Available Policy Sections");

								if (CurrentSearch.getBookingType() == BookingType.OUTSIDECANCELLATION) {
									/*
									 * PrintTemplate.verifyTrue(ExpectedPolicy.getDeadline().trim(), Actualpolicy.getDeadline().trim(),"Check Cancellation Deadline Section");
									 * PrintTemplate.verifyTrue(ExpectedPolicy.getRefundable().trim(), Actualpolicy.getRefundable().trim(),"Check Refundable Section");
									 */
								} else {
									/*
									 * PrintTemplate.verifyTrue("Not Available", Actualpolicy.getDeadline().trim(),"Check Cancellation Deadline Section"); PrintTemplate.verifyTrue("Not Available",
									 * Actualpolicy.getRefundable().trim(),"Check Refundable Section");
									 */
								}

								ArrayList<String> ActualPolicies = Actualpolicy.getPolicies();
								for (int i = 1; i <= ExpectedPolicy.getPolicies().size(); i++) {
									String Policy = null;
									/*
									 * if(i == 1) Policy = changeFirstPolicyDateTo(ExpectedPolicy.getPolicies().get(i-1)); else
									 */
									Policy = ExpectedPolicy.getPolicies().get(i - 1);

									if (ActualPolicies.size() != 0)
										PrintTemplate.verifyTrue(changePolicyCurrencyAndRate(Policy, CurrentHotel.getHotelCurrency().trim(), CurrentSearch.getSearchCurrency(), 0.0).trim(), ActualPolicies.get(i - 1).trim(),
												"Check Cancellation Policy Section " + i);
									else
										PrintTemplate.verifyTrue(changePolicyCurrencyAndRate(Policy, CurrentHotel.getHotelCurrency().trim(), CurrentSearch.getSearchCurrency(), 0.0).trim(), "Not Available", "Check Cancellation Policy Section " + i);

								}

							} else {
								PrintWriter.append("<td>Cancellation Policy PopUp not Available</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td>");
								PrintWriter.append("<td></td></tr>");

								PrintTemplate.verifyTrue(ExpectedPolicy.getNumberofpolicies().trim(), "-", "Check Available Policy Sections");
								PrintTemplate.verifyTrue(ExpectedPolicy.getDeadline().trim(), "-", "Check Cancellation Deadline Section");
								PrintTemplate.verifyTrue(ExpectedPolicy.getRefundable().trim(), "-", "Check Refundable Section");
							}

							// Search Again Validation
							SearchAgainBEC ValidationSearchAgain = CurrentResult.getSearchAgainBEC();
							Map<Integer, String[]> ValidateList = ValidationSearchAgain.getChildAges();
							Map<Integer, String[]> ActualList = CurrentSearch.getChildAges();

							Set<Integer> ActualKeySet = ActualList.keySet();
							String ActualAges = "";
							String AvailableAges = "";

							for (Iterator<Integer> iterator = ActualKeySet.iterator(); iterator.hasNext();) {
								Integer integer = (Integer) iterator.next();

								ActualAges.concat(Arrays.toString(ActualList.get(integer)));
								AvailableAges.concat(Arrays.toString(ValidateList.get(integer)));

							}

							PrintWriter.append("</table>");
							PrintWriter.append("<br><br>");
							PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">Search Again BEC Validations</p><br>");
							PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

							PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Search Again Booking Engine Availability</td>");
							TestCaseCount++;
							PrintWriter.append("<td>Booking Engine Availability</td>");

							if (ValidationSearchAgain.isBECAvailable()) {
								PrintWriter.append("<td>Search Again BEC Available</td>");
								PrintWriter.append("<td class='Passed'>PASS</td>");
								PrintWriter.append("<td></td></tr>");

								PrintTemplate.verifyTrue(false, ValidationSearchAgain.isCORAvailable(), "Search again BEC-Availability of Country of residance");
								PrintTemplate.verifyTrue(CurrentSearch.getLocation(), ValidationSearchAgain.getDestination(), "Search again BEC-Destination Filed Check ");
								PrintTemplate.verifyTrue(Calender.getDate("MM/dd/yyyy", "dd MMM yyyy", CurrentSearch.getArrivalDate()), ValidationSearchAgain.getCheckin(), "Search again  BEC-Check in Filed Check ");
								PrintTemplate.verifyTrue(Calender.getDate("MM/dd/yyyy", "dd MMM yyyy", CurrentSearch.getCheckOutDate()), ValidationSearchAgain.getCheckout(), "Search again BEC-Check out Filed Check ");
								PrintTemplate.verifyTrue(CurrentSearch.getNights(), ValidationSearchAgain.getNights(), "Search again BEC-Number of nights Filed Check ");
								PrintTemplate.verifyTrue(CurrentSearch.getRooms(), ValidationSearchAgain.getRooms(), "Search again BEC-NUmber of rooms Filed Check ");
								PrintTemplate.verifyTrue(CurrentSearch.getRooms(), Integer.toString(ValidationSearchAgain.getRoomsAvailableForOccupancy()), "Search again BEC-Rooms available for occupancy insertion");
								PrintTemplate.verifyTrue(Arrays.toString(CurrentSearch.getAdults()).trim(), Arrays.toString(ValidationSearchAgain.getAdults()).trim(), "Search again BEC-Occupancy Adults Stream ");
								PrintTemplate.verifyTrue(Arrays.toString(CurrentSearch.getChildren()).trim(), Arrays.toString(ValidationSearchAgain.getChildren()).trim(), "Search again BEC-Occupancy Child Stream ");
								PrintTemplate.verifyTrue(ActualAges.trim(), AvailableAges.trim(), "Search again BEC-Occupancy Child Age Stream");
								PrintTemplate.verifyTrue(true, ValidationSearchAgain.isAvailableChecked(), "Search again BEC-Check Whether Available Field is Checked");
								PrintTemplate.verifyTrue(true, ValidationSearchAgain.isOnRequestChecked(), "Search again BEC-Check Whether On Request Field is Checked");
								PrintTemplate.verifyTrue("", ValidationSearchAgain.getHotelName(), "Search again BEC-Hotel Name");
								PrintTemplate.verifyTrue("Select All", ValidationSearchAgain.getHotelType(), "Search again BEC-Hotel Type Selection Dropdown");
								PrintTemplate.verifyTrue("Select All", ValidationSearchAgain.getStarRating(), "Search again BEC-Star Rating Dropdown");
								PrintTemplate.verifyTrue("Select Currency", ValidationSearchAgain.getPrefferedCurrency(), "Search again BEC-Preffred Currency");
								PrintTemplate.verifyTrue("", ValidationSearchAgain.getPriceLevelFrom(), "Search again BEC-Price Level From");
								PrintTemplate.verifyTrue("", ValidationSearchAgain.getPriceLevelTo(), "Search again BEC-Price Level From");
								PrintTemplate.verifyTrue("", ValidationSearchAgain.getPromocode(), "Search again BEC-Promotion Code");
								PrintTemplate.verifyTrue(true, ValidationSearchAgain.isSearchButtonAvailable(), "Search again BEC-Search button availability");

							} else {

								PrintWriter.append("<td>BEC is not Available</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td>");
								PrintWriter.append("<td></td></tr>");

							}
							// Add to cart Validations
							HotelCartCalculator taxCalculator = new HotelCartCalculator(CurrentHotel, CurrentSearch, PropertySet);
							CartDetails ValidationCart = CurrentResult.getResultCart();
							CartDetails ExpectedCart = taxCalculator.performCartCalculations(CurrentSearch.getCart(), Loader);

							PrintWriter.append("</table>");
							PrintWriter.append("<br><br>");
							PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">Cart Details Validation</p><br>");

							PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

							PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Add to cart function</td>");
							TestCaseCount++;
							PrintWriter.append("<td>hotel Should add to cart</td>");

							if (ValidationCart.isIsAddedToCart()) {
								PrintWriter.append("<td>Hotel Added to cart</td>");
								PrintWriter.append("<td class='Passed'>PASS</td>");
								PrintWriter.append("<td></td></tr>");

								PrintTemplate.verifyTrue(false, ValidationCart.isRateChangeAlertAvailable(), "Rate Change Message Availaility");
								PrintTemplate.verifyTrue(CurrentSearch.getSearchCurrency().trim(), ValidationCart.getCartCurrency().trim(), "Cart Currency Validation");
								PrintTemplate.verifyTrue(ExpectedCart.getHotelLable().trim(), ValidationCart.getHotelLable().trim(), "Cart Hotel Label Validation");
								PrintTemplate.verifyTrueValue(ExpectedCart.getRate().trim(), ValidationCart.getRate().trim(), "Cart Total Rate Validation");
								PrintTemplate.verifyTrueValue(ExpectedCart.getTaxes(), ValidationCart.getTaxes().trim(), "Cart taxes Validation");
								PrintTemplate.verifyTrueValue(ExpectedCart.getSubTotal(), ValidationCart.getSubTotal().trim(), "Cart Hotel subtotal Validation");
								PrintTemplate.verifyTrueValue(ExpectedCart.getTotaPaybel(), ValidationCart.getTotaPaybel().trim(), "Cart total payble Validation");

								PrintWriter.append("</table>");

								// View Full Details Validation

								ViewFullDetails ViewDetails = CurrentResult.getViewFullDetails();
								CurrentSearch.setCartPolicy(prcessor.getExpectedCancellationPolicy(Loader, ExpectedCart.getCalculatedServiceFeeInSelling()));
								ViewFullDetails ExpectedView = createExpectedViewFull(ExpectedCart, CurrentSearch, CurrentHotel);

								PrintWriter.append("</table>");
								PrintWriter.append("<br><br>");
								PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">View Full Details Validation</p><br>");

								PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

								PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>View Full Details Window Availability</td>");
								TestCaseCount++;
								PrintWriter.append("<td>ViewFull Details Window Should Available</td>");

								if (ViewDetails.isLoded()) {
									PrintWriter.append("<td>ViewFull Details Window  Loaded</td>");
									PrintWriter.append("<td class='Passed'>PASS</td>");
									PrintWriter.append("<td></td></tr>");

									PrintTemplate.verifyTrue(ExpectedView.getView_HotelName().trim(), ViewDetails.getView_HotelName(), "View Full Details Hotel name Validation");
									PrintTemplate.verifyTrue(ExpectedView.getView_Address().trim(), ViewDetails.getView_Address(), "View Full Details  Address Validation");
									PrintTemplate.verifyTrue(ExpectedView.getView_BookingStatus(), ViewDetails.getView_BookingStatus(), "View Full Details  Availability Status Validation");
									PrintTemplate.verifyContains(CurrentSearch.getSearchCurrency(), ViewDetails.getView_Currency(), "View Full Details  Currency Check Validation");
									PrintTemplate.verifyTrue(getDateInFormat("MM/dd/yyyy", "dd MMM yyyy", CurrentSearch.getArrivalDate()), ViewDetails.getView_CheckIn(), "View Full Details  Check in details Validation");
									PrintTemplate.verifyTrue(getDateInFormat("MM/dd/yyyy", "dd MMM yyyy", CurrentSearch.getCheckOutDate()), ViewDetails.getView_CheckOut(), "View Full Details  Check out Validation");
									PrintTemplate.verifyTrue(CurrentSearch.getNights(), ViewDetails.getView_Nights(), "View Full Details  NumberOfNights Validation");
									PrintTemplate.verifyTrue(CurrentSearch.getRooms(), ViewDetails.getView_NumberOfRooms(), "View Full Details  Number of rooms Validation");
									PrintTemplate.verifyContains(ExpectedView.getView_CancellationDeadLine(), ViewDetails.getView_CancellationDeadLine(), "View Full Details  Cancellation DeadLine Validation");

									// Rates Validation

									PrintTemplate.verifyTrueValue(ExpectedView.getView_SubTotal(), ViewDetails.getView_SubTotal(), "View Full Hotel Sub Total Validation");
									PrintTemplate.verifyTrueValue(ExpectedView.getView_totalTaxAnServiceCharges(), ViewDetails.getView_totalTaxAnServiceCharges(), "View Full Details  Hotel Total Tax And Service Charges Validation");
									PrintTemplate.verifyTrueValue(ExpectedView.getView_TotalHotelBookingValue(), ViewDetails.getView_TotalHotelBookingValue(), "View Full Details Total Hotel Booking Value  Validation");
									PrintTemplate.verifyTrueValue(ExpectedView.getView_HotelAmountpayablenow(), ViewDetails.getView_HotelAmountpayablenow(), "View Full Details Hotel- Amount payable now ");
									PrintTemplate.verifyTrueValue(ExpectedView.getView_HotelAmountToPaidAtCheckIn(), ViewDetails.getView_HotelAmountToPaidAtCheckIn(), "View Full Details Hotel- Amount to be paid at check-in ");

									PrintTemplate.verifyTrueValue(ExpectedView.getView_TotalGrossPackageBookingValue(), ViewDetails.getView_TotalGrossPackageBookingValue(), "View Full Hotel Gross Booking Value Validation");
									PrintTemplate.verifyTrueValue(ExpectedView.getView_TotalTaxesAndOtherCharges(), ViewDetails.getView_TotalTaxesAndOtherCharges(), "View Full Details  Hotel Total Tax And Other Charges Validation");
									PrintTemplate.verifyTrueValue(ExpectedView.getView_TotalBookingValue(), ViewDetails.getView_TotalBookingValue(), "View Full Details Total Booking Value  Validation");
									PrintTemplate.verifyTrueValue(ExpectedView.getView_AmountbeingProcessedNow(), ViewDetails.getView_AmountbeingProcessedNow(), "View Full Details  Amount being Processed Now Validation");
									PrintTemplate.verifyTrueValue(ExpectedView.getView_AmountdueatCheckIn(), ViewDetails.getView_AmountdueatCheckIn(), "View Full Details  Amount due @ Check in Validation");

									ArrayList<RoomType> ExpectedRooms = ExpectedView.getRoomDetails();
									ArrayList<RoomType> AvailableRooms = ViewDetails.getRoomDetails();

									PrintTemplate.verifyTrue(Integer.toString(ExpectedRooms.size()), Integer.toString(AvailableRooms.size()), "View Full Details Expected Room Records VS Avilable Room Records");
									PrintWriter.append("</table>");
									PrintWriter.append("<br><br>");
									PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">View Full Details Window Individual Room Validations</p><br>");

									for (int i = 0; i < ExpectedRooms.size(); i++) {

										RoomType CurrentRoom = ExpectedRooms.get(i);
										// String UniqueStr = CurrentRoom.getRoomType()+"/"+CurrentRoom.getBedType()+"/"+CurrentRoom.getRateType();
										PrintWriter.append("<p class=\"InfoSup\" style=\"font-weight: bold;\">Room " + (i + 1) + "<br></p>");
										PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

										PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Check the Availability Of the Specified Room</td>");
										TestCaseCount++;
										PrintWriter.append("<td>Specified Room Should Available</td>");

										// RoomType AvailableRoom = getRoomFromList(UniqueStr, AvailableRooms);
										RoomType AvailableRoom = AvailableRooms.get(i);

										if (AvailableRoom != null) {
											PrintWriter.append("<td>Room  Available </td>");
											PrintWriter.append("<td class='Passed'>PASS</td>");
											PrintWriter.append("<td></td></tr>");

											PrintTemplate.verifyTrue(CurrentRoom.getRoomLable(), AvailableRoom.getRoomLable().replace(":", ""), "Room Lable");
											PrintTemplate.verifyTrue(CurrentRoom.getRoomType(), AvailableRoom.getRoomType(), "Room Type");
											PrintTemplate.verifyTrue(CurrentRoom.getBedType(), AvailableRoom.getBedType(), "Bed Type");
											PrintTemplate.verifyTrue(CurrentRoom.getRateType(), AvailableRoom.getRateType(), "Rate Plan");
											// PrintTemplate.verifyTrue(CurrentSearch.getSearchCurrency(),ResultRm.getRoomRate().split(" ")[0],"Room Display Currency");
											PrintTemplate.verifyTrueValue(Double.toString(Loader.convertCurrency(CurrentRoom.getTotalRate(), SupplierCurrency, CurrentSearch.getSearchCurrency())), AvailableRoom.getTotalRate(),
													"Room Total Rate Validation");
											PrintWriter.append("</table>");
										} else {
											PrintWriter.append("<td>Room not available in results</td>");
											PrintWriter.append("<td class='Failed'>FAIL</td>");
											PrintWriter.append("<td></td></tr>");
											PrintWriter.append("</table>");
										}

									}

									// View Full Details Cancellation Policy Validation
									ExpectedPolicy = CurrentSearch.getCartPolicy();
									PrintWriter.append("<br><br>");
									PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">ViewFull Details Cancellation Policy Validation</p><br>");

									PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

									CancellationPolicy AvailableViewFullCancelltion = ViewDetails.getCancellationPolicy();
									PrintTemplate.verifyTrue(ExpectedPolicy.getNumberofpolicies().trim(), AvailableViewFullCancelltion.getNumberofpolicies().trim(), "Check Available Policy Sections");

									if (CurrentSearch.getBookingType() == BookingType.OUTSIDECANCELLATION) {
										PrintTemplate.verifyContains(ExpectedPolicy.getDeadline().trim(), AvailableViewFullCancelltion.getDeadline().trim(), "Check Cancellation Deadline Section");
										PrintTemplate.verifyTrue(ExpectedPolicy.getRefundable().trim(), AvailableViewFullCancelltion.getRefundable().trim(), "Check Refundable Section");
									} else {
										PrintTemplate.verifyTrue("Not Available", Actualpolicy.getDeadline().trim(), "Check Cancellation Deadline Section");
										PrintTemplate.verifyTrue("Not Available", Actualpolicy.getRefundable().trim(), "Check Refundable Section");
									}

									ArrayList<String> ActualPolicies = AvailableViewFullCancelltion.getPolicies();
									for (int i = 1; i <= ExpectedPolicy.getPolicies().size(); i++) {
										String Policy = null;
										if (i == 1)
											Policy = changeFirstPolicyDateTo(ExpectedPolicy.getPolicies().get(i - 1));
										else
											Policy = ExpectedPolicy.getPolicies().get(i - 1);

										PrintTemplate.verifyTrue(changePolicyCurrencyAndRate(Policy, CurrentHotel.getHotelCurrency().trim(), CurrentSearch.getSearchCurrency(), 0.0).trim(), ActualPolicies.get(i - 1).trim(),
												"Check Cancellation Policy Section " + i);

									}

									PrintWriter.append("</table >");

									/*
									 * String ConvertedRate = Double.toString(Loader.convertCurrency(CurrentSearch.getTotalRate(), SupplierCurrency, CurrentSearch.getSearchCurrency()));
									 * PrintTemplate.verifyTrueValue(ConvertedRate,CurrentResult.getTotalRate().split(" ")[1],"Hotel Total Rate  Validation");
									 * PrintTemplate.verifyTrue(CurrentSearch.getRooms(),Integer.toString(AvailableRoomMap.size()),"Number Of Rooms Validation");
									 */

									// //////////////////////////////////

								} else {
									PrintWriter.append("<td>ViewFull Details Window not Loaded</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td>");
									PrintWriter.append("<td></td></tr>");

								}

								// ///////////////////PAYMENTS PAGE VALIDATION//////////////////////////

								PaymentsPage ActualPaymentPg = CurrentResult.getResultPayment();
								PaymentsPage ExpectedPaymentPg = createExpectedPaymentPg(ExpectedView, CurrentSearch, CurrentHotel);

								PrintWriter.append("</table>");
								PrintWriter.append("<br><br>");
								PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\"><< Payment Page Validation>></p><br>");

								PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

								PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Payment Page Availability</td>");
								TestCaseCount++;
								PrintWriter.append("<td>Payment Page Should Be Loaded</td>");

								if (ActualPaymentPg.isLoded()) {
									PrintWriter.append("<td>Payment Page Loaded</td>");
									PrintWriter.append("<td class='Passed'>PASS</td>");
									PrintWriter.append("<td></td></tr>");

									PrintTemplate.verifyTrue(ExpectedView.getView_HotelName().trim(), ActualPaymentPg.getPay_HotelName(), "Payment Page(Basic Details) Hotel name Validation");
									PrintTemplate.verifyTrue(ExpectedView.getView_Address().trim(), ActualPaymentPg.getPay_Address(), "Payment Page(Basic Details) Address Validation");
									PrintTemplate.verifyTrue(ExpectedView.getView_BookingStatus(), ActualPaymentPg.getPay_BookingStatus(), "Payment Page(Basic Details) Availability Status Validation");
									PrintTemplate.verifyContains(CurrentSearch.getSearchCurrency(), ActualPaymentPg.getPay_SummeryHotelcurrency(), "Payment Page(Basic Details) Currency Check Validation");
									PrintTemplate.verifyTrue(getDateInFormat("MM/dd/yyyy", "dd MMM yyyy", CurrentSearch.getArrivalDate()), ActualPaymentPg.getPay_CheckIn(), "Payment Page(Basic Details) Check in details Validation");
									PrintTemplate.verifyTrue(getDateInFormat("MM/dd/yyyy", "dd MMM yyyy", CurrentSearch.getCheckOutDate()), ActualPaymentPg.getPay_CheckOut(), "Payment Page(Basic Details) Check out Validation");
									PrintTemplate.verifyTrue(CurrentSearch.getNights(), ActualPaymentPg.getPay_Nights(), "Payment Page(Basic Details) NumberOfNights Validation");
									PrintTemplate.verifyTrue(CurrentSearch.getRooms(), ActualPaymentPg.getPay_Rooms(), "Payment Page(Basic Details) Number of rooms Validation");
									PrintTemplate.verifyContains(ExpectedView.getView_CancellationDeadLine(), ActualPaymentPg.getPay_CandellationDeadLine(), "Payment Page(Basic Details) Cancellation DeadLine Validation");

									// Misc Availability
									PrintTemplate.verifyTrue(true, ActualPaymentPg.isPay_MngOverUserFiledAvailable(), "Payment Page - Manager Override User Name Field Availability");
									PrintTemplate.verifyTrue(true, ActualPaymentPg.isPay_MngOverPassFiledAvailable(), "Payment Page - Manager Override Password  Field Availability");
									PrintTemplate.verifyTrue(true, ActualPaymentPg.isPay_MngOverActButtonAvailable(), "Payment Page - Manager Override Activate button Availability");
									PrintTemplate.verifyTrue(true, ActualPaymentPg.isPay_DiscountFiledAvailable(), "Payment Page - Discount Number Field Availability");
									PrintTemplate.verifyTrue(true, ActualPaymentPg.isPay_DiscountActButtonAvailable(), "Payment Page - Discount Activate Button Availability");
									PrintTemplate.verifyTrue(true, ActualPaymentPg.isPay_QuoteSaveButtonAvailable(), "Payment Page - Quotation Save Button Availability");

									PrintTemplate.verifyTrue(true, ActualPaymentPg.isPay_ProductRmButtonAvailable(), "Payment Page - Product Remove Button Availability");
									PrintTemplate.verifyTrue(true, ActualPaymentPg.isPay_QuoteMailSentByDafault(), "Payment Page - Quotation sent by default");
									PrintTemplate.verifyTrue(true, ActualPaymentPg.isPay_MailSendingOptionsAvailable(), "Payment Page - Mail Sending Options Availability");
									PrintTemplate.verifyTrue(false, ActualPaymentPg.isPay_CusMailSentByDafault(), "Payment Page - Customer sent by default");
									PrintTemplate.verifyTrue(false, ActualPaymentPg.isPay_SupMailSentByDafault(), "Payment Page - Supplier Mail sent by default");
									PrintTemplate.verifyTrue(false, ActualPaymentPg.isPay_VoucherMailSentByDafault(), "Payment Page - Voucher sent by default");
									PrintTemplate.verifyTrue(false, ActualPaymentPg.isPay_HtNotesCopiedInMailByDafault(), "Payment Page - Hotel Notes Copied to customer mail by default");

									// Rates Validation

									PrintTemplate.verifyTrueValue(ExpectedView.getView_SubTotal(), ActualPaymentPg.getPay_SubTotal(), "Payment Page Hotel- Sub Total Validation");
									PrintTemplate.verifyTrueValue(ExpectedView.getView_totalTaxAnServiceCharges(), ActualPaymentPg.getPay_totalTaxAnServiceCharges(), "Payment Page Details  Hotel- Total Tax And Service Charges Validation");
									PrintTemplate.verifyTrueValue(ExpectedView.getView_TotalHotelBookingValue(), ActualPaymentPg.getPay_TotalHotelBookingValue(), "Payment Page Hotel- Total Booking Value  Validation");
									// Adding two Columns Validation For Hotel total payable and hotel total due at check in
									PrintTemplate.verifyTrueValue(ExpectedView.getView_HotelAmountpayablenow(), ActualPaymentPg.getPay_HotelAmountpayablenow(), "Payment Page Hotel- Amount payable now ");
									PrintTemplate.verifyTrueValue(ExpectedView.getView_HotelAmountToPaidAtCheckIn(), ActualPaymentPg.getPay_HotelAmountToPaidAtCheckIn(), "Payment Page Hotel- Amount to be paid at check-in ");

									// Package Rates

									if (PropertySet.get("Portal.Online.Available").trim().equalsIgnoreCase("YES")) {
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_TotalGrossPackageBookingValue(), ActualPaymentPg.getPay_TotalGrossPackageBookingValue(), "Payment Page Details Hotel Gross Booking Value Validation");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_TotalTaxesAndOtherCharges(), ActualPaymentPg.getPay_TotalTaxesAndOtherCharges(), "Payment Page Details  Hotel Total Tax And Other Charges Validation");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_TotalBookingValue(), ActualPaymentPg.getPay_TotalBookingValue(), "Payment Page Details Total Booking Value  Validation");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_AmountbeingProcessedNow(), ActualPaymentPg.getPay_AmountbeingProcessedNow(), "Payment Page Details  Amount being Processed Now Validation");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_AmountdueatCheckIn(), ActualPaymentPg.getPay_AmountdueatCheckIn(), "Payment Page Details  Amount due @ Check in Validation");

										// Payment Detail Validation (Online)

										PrintTemplate.verifyContains(ExpectedPaymentPg.getPay_ON_DefaultSelectedCard(), ActualPaymentPg.getPay_ON_DefaultSelectedCard(), "Payment page(Payment Details) Default selected card type");
										PrintTemplate.verifyContains(ExpectedPaymentPg.getPay_ON_CurrencyLable(), ActualPaymentPg.getPay_ON_CurrencyLable(), "Payment page(Payment Details) payment details Currency");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_ON_TotalGrossBookingValue(), ActualPaymentPg.getPay_ON_TotalGrossBookingValue(),
												"Payment page(Payment Details) payment details Total Gross Package Booking Value");

										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_ON_TotalTaxesAndFees(), ActualPaymentPg.getPay_ON_TotalTaxesAndFees(), "Payment page payment details Total Taxes and Fees");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_ON_TotalPackageBookingValue(), ActualPaymentPg.getPay_ON_TotalPackageBookingValue(), "Payment page(Payment Details) Total Package Booking Value");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_ON_AmountBeingProcessedNow(), ActualPaymentPg.getPay_ON_AmountBeingProcessedNow(), "Payment page(Payment Details) Amount being Processed Now");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_ON_AmountDueAtCheckIn(), ActualPaymentPg.getPay_ON_AmountDueAtCheckIn(), "Payment page(Payment Details) Amount Due at Check-In / Utilization");
										PrintTemplate.verifyContains(ExpectedPaymentPg.getPay_ON_SummeryCurrency(), ActualPaymentPg.getPay_ON_SummeryCurrency(), "Payment page(Payment Details) payment summery currency");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_ON_SummeryValue(), ActualPaymentPg.getPay_ON_SummeryValue(), "Payment page(Payment Details) payment summery value");
									}

									// Payment Mode Offline Selected rates check
									PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_Off_TotalGrossPackageBookingValue(), ActualPaymentPg.getPay_Off_TotalGrossPackageBookingValue(), "Payment Method-Offline- Total Gross Package Booking Value");
									PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_Off_TotalTaxesAndOtherCharges(), ActualPaymentPg.getPay_Off_TotalTaxesAndOtherCharges(), "Payment Method-Offline- Total Taxes and Fees");
									PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_Off_TotalBookingValue(), ActualPaymentPg.getPay_Off_TotalBookingValue(), "Payment Method-Offline- Total  Booking Value");
									PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_Off_AmountbeingProcessedNow(), ActualPaymentPg.getPay_Off_AmountbeingProcessedNow(), "Payment Method-Offline-  Amount being Processed Now");
									PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_Off_AmountdueatCheckIn(), ActualPaymentPg.getPay_Off_AmountdueatCheckIn(), "Payment Method-Offline-  Amount Due at Check-In / Utilization");
									PrintTemplate.verifyContains(ExpectedPaymentPg.getPay_OFF_Currency(), ActualPaymentPg.getPay_OFF_Currency(), "Payment Method-Offline-  payment details payment summery currency");
									PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_OFF_Amount(), ActualPaymentPg.getPay_OFF_Amount(), "Payment Method-Offline-  payment details payment summery Value");

									if (CurrentSearch.getPaymentType().equalsIgnoreCase("online")) {
										// Different Card Type Selected

										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_CreditShift_TotalGrossPackageBookingValue(), ActualPaymentPg.getPay_CreditShift_TotalGrossPackageBookingValue(),
												"Payment Page Details(Card Changed) Hotel Gross Booking Value Validation");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_CreditShift_TotalTaxesAndOtherCharges(), ActualPaymentPg.getPay_CreditShift_TotalTaxesAndOtherCharges(),
												"Payment Page Details  Hotel(Card Changed) Total Tax And Other Charges Validation");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_CreditShift_TotalBookingValue(), ActualPaymentPg.getPay_CreditShift_TotalBookingValue(),
												"Payment Page Details(Card Changed) Total Booking Value  Validation");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_CreditShift_AmountbeingProcessedNow(), ActualPaymentPg.getPay_CreditShift_AmountbeingProcessedNow(),
												"Payment Page Details(Card Changed)  Amount being Processed Now Validation");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_CreditShift_AmountdueatCheckIn(), ActualPaymentPg.getPay_CreditShift_AmountdueatCheckIn(), "Payment Page Details  Amount due @ Check in Validation");

										// Payment Detail Validation (Online)

										PrintTemplate.verifyContains(ExpectedPaymentPg.getPay_ShiftedCardType().toUpperCase(), ActualPaymentPg.getPay_ShiftedCardType(), "Payment page Shifted card type");
										PrintTemplate.verifyContains(ExpectedPaymentPg.getPay_ON_Shift_CurrencyLable(), ActualPaymentPg.getPay_ON_Shift_CurrencyLable(), "Payment page(Card Type Shift) payment details Currency");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_ON_Shift_TotalGrossBookingValue(), ActualPaymentPg.getPay_ON_Shift_TotalGrossBookingValue(),
												"Payment page(Card Type Shift) payment details Total Gross Package Booking Value");

										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_ON_Shift_TotalTaxesAndFees(), ActualPaymentPg.getPay_ON_Shift_TotalTaxesAndFees(), "Payment page(Card Type Shift) payment details Total Taxes and Fees");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_ON_Shift_TotalPackageBookingValue(), ActualPaymentPg.getPay_ON_Shift_TotalPackageBookingValue(),
												"Payment page(Card Type Shift) payment details Total Package Booking Value");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_ON_Shift_AmountBeingProcessedNow(), ActualPaymentPg.getPay_ON_Shift_AmountBeingProcessedNow(),
												"Payment page(Card Type Shift) payment details Amount being Processed Now");
										// PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_ON,ActualPaymentPg.getPay_ON_AmountDueAtCheckIn(),"Payment page payment details Amount Due at Check-In / Utilization");
										PrintTemplate.verifyContains(ExpectedPaymentPg.getPay_ON_Shift_SummeryCurrency(), ActualPaymentPg.getPay_ON_Shift_SummeryCurrency(), "Payment page(Card Type Shift) payment details payment summery currency");
										PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_ON_Shift_SummeryValue(), ActualPaymentPg.getPay_ON_Shift_SummeryValue(), "Payment page(Card Type Shift) payment details payment summery value");

									}

									ArrayList<RoomType> ExpectedRooms = ExpectedView.getRoomDetails();
									ArrayList<RoomType> AvailableRooms = ActualPaymentPg.getPay_RoomDetails();

									PrintTemplate.verifyTrue(Integer.toString(ExpectedRooms.size()), Integer.toString(AvailableRooms.size()), "Payment page Expected Room Records VS Avilable Room Records");
									PrintWriter.append("</table>");
									PrintWriter.append("<br><br>");
									PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">Payment Page Individual Room Validations</p><br>");

									for (int i = 0; i < ExpectedRooms.size(); i++) {

										RoomType CurrentRoom = ExpectedRooms.get(i);
										String UniqueStr = CurrentRoom.getRoomType() + "/" + CurrentRoom.getBedType() + "/" + CurrentRoom.getRateType();
										PrintWriter.append("<p class=\"InfoSup\" style=\"font-weight: bold;\">Room " + (i + 1) + "<br></p>");
										PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

										PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Check the Availability Of the Specified Room</td>");
										TestCaseCount++;
										PrintWriter.append("<td>Specified Room Should Available</td>");

										RoomType AvailableRoom = getRoomFromList(UniqueStr, AvailableRooms);

										if (AvailableRoom != null) {
											PrintWriter.append("<td>Room  Available </td>");
											PrintWriter.append("<td class='Passed'>PASS</td>");
											PrintWriter.append("<td></td></tr>");

											PrintTemplate.verifyTrue(CurrentRoom.getRoomLable(), AvailableRoom.getRoomLable().replace(":", ""), "Room Lable");
											PrintTemplate.verifyTrue(CurrentRoom.getRoomType(), AvailableRoom.getRoomType(), "Room Type");
											PrintTemplate.verifyTrue(CurrentRoom.getBedType(), AvailableRoom.getBedType(), "Bed Type");
											PrintTemplate.verifyTrue(CurrentRoom.getRateType(), AvailableRoom.getRateType(), "Rate Plan");
											// PrintTemplate.verifyTrue(CurrentSearch.getSearchCurrency(),ResultRm.getRoomRate().split(" ")[0],"Room Display Currency");
											PrintTemplate.verifyTrueValue(Double.toString(Loader.convertCurrency(CurrentRoom.getTotalRate(), SupplierCurrency, CurrentSearch.getSearchCurrency())), AvailableRoom.getTotalRate(),
													"Room Total Rate Validation");
											PrintWriter.append("</table>");
										} else {
											PrintWriter.append("<td>Room not available in results</td>");
											PrintWriter.append("<td class='Failed'>FAIL</td>");
											PrintWriter.append("<td></td></tr>");
											PrintWriter.append("</table>");
										}

									}

									// Payment Page Cancellation Policy Validation

									PrintWriter.append("<br><br>");
									PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">Payment page Cancellation Policy Validation</p><br>");

									PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

									CancellationPolicy AvailableViewFullCancelltion = ViewDetails.getCancellationPolicy();
									PrintTemplate.verifyTrue(ExpectedPolicy.getNumberofpolicies().trim(), AvailableViewFullCancelltion.getNumberofpolicies().trim(), "Check Available Policy Sections");

									if (CurrentSearch.getBookingType() == BookingType.OUTSIDECANCELLATION) {
										PrintTemplate.verifyContains(ExpectedPolicy.getDeadline().trim(), AvailableViewFullCancelltion.getDeadline().trim(), "Check Cancellation Deadline Section");
										PrintTemplate.verifyTrue(ExpectedPolicy.getRefundable().trim(), AvailableViewFullCancelltion.getRefundable().trim(), "Check Refundable Section");
									} else {
										PrintTemplate.verifyTrue("Not Available", Actualpolicy.getDeadline().trim(), "Check Cancellation Deadline Section");
										PrintTemplate.verifyTrue("Not Available", Actualpolicy.getRefundable().trim(), "Check Refundable Section");
									}

									ArrayList<String> ActualPolicies = AvailableViewFullCancelltion.getPolicies();

									for (int i = 1; i <= ExpectedPolicy.getPolicies().size(); i++) {
										String Policy = null;
										if (i == 1)
											Policy = changeFirstPolicyDateTo(ExpectedPolicy.getPolicies().get(i - 1));
										else
											Policy = ExpectedPolicy.getPolicies().get(i - 1);

										PrintTemplate.verifyTrue(changePolicyCurrencyAndRate(Policy, CurrentHotel.getHotelCurrency().trim(), CurrentSearch.getSearchCurrency(), 0.0).trim(), ActualPolicies.get(i - 1).trim(),
												"Check Cancellation Policy Section " + i);

									}

									PrintWriter.append("</table >");

									/*
									 * String ConvertedRate = Double.toString(Loader.convertCurrency(CurrentSearch.getTotalRate(), SupplierCurrency, CurrentSearch.getSearchCurrency()));
									 * PrintTemplate.verifyTrueValue(ConvertedRate,CurrentResult.getTotalRate().split(" ")[1],"Hotel Total Rate  Validation");
									 * PrintTemplate.verifyTrue(CurrentSearch.getRooms(),Integer.toString(AvailableRoomMap.size()),"Number Of Rooms Validation");
									 */

									// //////////////////////////////////
									// ///////////////////////////////////////////////// INTERMEADIATE POPUP VALIDATIONS
									// //////////////////////////////////////////////////////////////////////////////////////////

									// PrintWriter.append("</table>");
									PrintWriter.append("<br><br>");
									PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\"><< Intermedeate Confirmation Pop Up Validation>></p><br>");

									PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

									PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Intermedeate Confirmation Pop Up Availability</td>");
									TestCaseCount++;
									PrintWriter.append("<td>Intermedeate Confirmation Pop Up Should Be Loaded</td>");

									if (CurrentResult.isIntermediateFrameLoaded()) {
										PrintWriter.append("<td>Intermedeate Confirmation Pop Up Loaded</td>");
										PrintWriter.append("<td class='Passed'>PASS</td>");
										PrintWriter.append("<td></td></tr>");

										PrintTemplate.verifyTrue("Available", CurrentResult.getIntermediateStatus(), "Intermediate Pop up availability Status");
										PrintTemplate.verifyTrue("-", CurrentResult.getIntermediateMessage().trim(), "Intermediate Pop up availability message");
									} else {
										PrintWriter.append("<td>Intermedeate Confirmation Pop Up not Loaded</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td>");
										PrintWriter.append("<td></td></tr>");
									}

									PrintWriter.append("</table>");
									// Checking whether payment done using gateway
									if (CurrentSearch.getPaymentType().trim().equalsIgnoreCase("ONLINE")) {
										// /////////////////////////////////////// PAYMENT GATEWAY VALIDATIONS
										// //////////////////////////////////////////////////////////////////////////////////////////
										PaymentGateway ActualPaymentGateway = CurrentResult.getGateWay();

										PrintWriter.append("</table>");
										PrintWriter.append("<br><br>");
										PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\"><< Payment Gateway Validation>></p><br>");

										PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

										PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Payment Gateway Loading functionality</td>");
										TestCaseCount++;
										PrintWriter.append("<td>Payment Gateway Should Be Loaded</td>");

										if (ActualPaymentGateway.isPaymentGatewayLoded()) {
											PrintWriter.append("<td>Payment Gateway Loaded</td>");
											PrintWriter.append("<td class='Passed'>PASS</td>");
											PrintWriter.append("<td></td></tr>");

											PrintTemplate.verifyContains(ExpectedPaymentPg.getPay_ShiftedCardType().toUpperCase(), ActualPaymentGateway.getDisplayCardType(), "Display Card Type");
											PrintTemplate.verifyContains(ExpectedPaymentPg.getPay_ON_Shift_CurrencyLable(), ActualPaymentGateway.getCurrencyLable(), "Display");
											PrintTemplate.verifyTrueValue(ExpectedPaymentPg.getPay_ON_Shift_SummeryValue(), ActualPaymentGateway.getAmount(), "Amount");
											PrintTemplate.verifyTrue("Pass", ActualPaymentGateway.getPaymentStatus(), "Payment Gateway Payment Status");
										} else {
											PrintWriter.append("<td>Payment Gateway not Loaded</td>");
											PrintWriter.append("<td class='Failed'>FAIL</td>");
											PrintWriter.append("<td></td></tr>");
										}

										PrintWriter.append("</table>");

									}
									// ///////////////////////////////////////////////// CONFIRMATION PAGE VALIDATIONS
									// //////////////////////////////////////////////////////////////////////////////////////////

									ActualConfPage = CurrentResult.getCon_Page();
									ExpectedConfPage = createExpectedConfPage(ExpectedPaymentPg, CurrentSearch, CurrentHotel);
									ExpectedConfPage.setBookingNumber(ActualConfPage.getBookingNumber());

									// PrintWriter.append("</table>");
									PrintWriter.append("<br>");
									PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\"><< Confirmation Page Validation>></p><br>");
									PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

									PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Confirmation Page Availability</td>");
									TestCaseCount++;
									PrintWriter.append("<td>Confirmation Page Should Be Loaded</td>");

									if (ActualConfPage.isLoded()) {
										PrintWriter.append("<td>Confirmation Page Loaded</td>");
										PrintWriter.append("<td class='Passed'>PASS</td>");
										PrintWriter.append("<td></td></tr>");

										TestCaseCount++;
										PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Booking Reference</td>");
										PrintWriter.append("<td>" + ActualConfPage.getBookingNumber() + "</td>");
										PrintWriter.append("<td>" + ActualConfPage.getBookingNumber() + "</td>");
										PrintWriter.append("<td class='Passed'>PASS</td>");
										PrintWriter.append("<td></td></tr>");

										PrintTemplate.verifyTrue("Hotel Bookings Saved Successfully", ActualConfPage.getConfirmationMessage(), "Confirmation Page Hotel name Validation");
										PrintTemplate.verifyTrue(ExpectedView.getView_HotelName().trim(), ActualConfPage.getCon_HotelName().replace("View Cancellation Policy", ""), "Confirmation Page Hotel name Validation");
										PrintTemplate.verifyTrue(ExpectedView.getView_Address().trim(), ActualConfPage.getCon_Address(), "Confirmation Page  Address Validation");
										PrintTemplate.verifyTrue(ExpectedConfPage.getCon_BookingStatus(), ActualConfPage.getCon_BookingStatus(), "Confirmation Page   Availability Status Validation");
										// PrintTemplate.verifyContains(CurrentSearch.getSearchCurrency(),ActualPaymentPg.getPay_SummeryHotelcurrency(),"Confirmation Page   Currency Check Validation");
										PrintTemplate.verifyTrue(getDateInFormat("MM/dd/yyyy", "dd MMM yyyy", CurrentSearch.getArrivalDate()), ActualConfPage.getCon_CheckIn(), "Confirmation Page   Check in details Validation");
										PrintTemplate.verifyTrue(getDateInFormat("MM/dd/yyyy", "dd MMM yyyy", CurrentSearch.getCheckOutDate()), ActualConfPage.getCon_CheckOut(), "Confirmation Page  Check out Validation");
										PrintTemplate.verifyTrue(ExpectedConfPage.getCon_Nights(), ActualConfPage.getCon_Nights(), "Confirmation Page  NumberOfNights Validation");
										PrintTemplate.verifyTrue(ExpectedConfPage.getCon_Rooms(), ActualConfPage.getCon_Rooms(), "Confirmation Page Number of rooms Validation");
										PrintTemplate.verifyContains(ExpectedConfPage.getCon_CandellationDeadLine(), ActualConfPage.getCon_CandellationDeadLine(), "Confirmation Page  Cancellation DeadLine Validation");

										// Rates Validation

										PrintTemplate.verifyTrueValue(ExpectedConfPage.getCon_SubTotal(), ActualConfPage.getCon_SubTotal(), "Confirmation Page Hotel Sub Total Validation");
										PrintTemplate.verifyTrueValue(ExpectedConfPage.getCon_totalTaxAnServiceCharges(), ActualConfPage.getCon_totalTaxAnServiceCharges(), "Confirmation Page Details  Hotel Total Tax And Service Charges Validation");
										PrintTemplate.verifyTrueValue(ExpectedConfPage.getCon_TotalHotelBookingValue(), ActualConfPage.getCon_TotalHotelBookingValue(), "Confirmation Page Total Hotel Booking Value  Validation");
										PrintTemplate.verifyTrueValue(ExpectedConfPage.getCon_HotelAmountpayablenow(), ActualConfPage.getCon_HotelAmountpayablenow(), "Confirmation Page Details  Hotel Total payble Now Section Validation ");
										PrintTemplate.verifyTrueValue(ExpectedConfPage.getCon_HotelAmountToPaidAtCheckIn(), ActualConfPage.getCon_HotelAmountToPaidAtCheckIn(), "Confirmation Page Details  Hotel Amount Due At Check in Validation");
										// Package Rates
										PrintTemplate.verifyTrueValue(ExpectedConfPage.getCon_TotalGrossPackageBookingValue(), ActualConfPage.getCon_TotalGrossPackageBookingValue(), "Confirmation Page Details Hotel Gross Booking Value Validation");
										PrintTemplate.verifyTrueValue(ExpectedConfPage.getCon_TotalTaxesAndOtherCharges(), ActualConfPage.getCon_TotalTaxesAndOtherCharges(), "Confirmation Page Details  Hotel Total Tax And Other Charges Validation");
										PrintTemplate.verifyTrueValue(ExpectedConfPage.getCon_TotalBookingValue(), ActualConfPage.getCon_TotalBookingValue(), "Confirmation Page Details Total Booking Value  Validation");
										PrintTemplate.verifyTrueValue(ExpectedConfPage.getCon_AmountbeingProcessedNow(), ActualConfPage.getCon_AmountbeingProcessedNow(), "Confirmation Page Details  Amount being Processed Now Validation");
										PrintTemplate.verifyTrueValue(ExpectedConfPage.getCon_AmountdueatCheckIn(), ActualConfPage.getCon_AmountdueatCheckIn(), "Confirmation Page Details  Amount due @ Check in Validation");

										ArrayList<RoomType> ExpectedConfRooms = ExpectedConfPage.getCon_RoomDetails();
										ArrayList<RoomType> AvailableConfRooms = ActualConfPage.getCon_RoomDetails();

										PrintTemplate.verifyTrue(Integer.toString(ExpectedRooms.size()), Integer.toString(AvailableRooms.size()), "Confirmation page Expected Room Records VS Avilable Room Records");
										PrintWriter.append("</table>");
										PrintWriter.append("<br><br>");
										PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">Confirmation Page Individual Room Validations</p><br>");

										for (int i = 0; i < ExpectedConfRooms.size(); i++) {

											RoomType CurrentRoom = ExpectedConfRooms.get(i);
											String UniqueStr = CurrentRoom.getRoomType() + "/" + CurrentRoom.getBedType() + "/" + CurrentRoom.getRateType();
											PrintWriter.append("<p class=\"InfoSup\" style=\"font-weight: bold;\">Room " + (i + 1) + "<br></p>");
											PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

											PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Check the Availability Of the Specified Room</td>");
											TestCaseCount++;
											PrintWriter.append("<td>Specified Room Should Available</td>");

											RoomType AvailableRoom = getRoomFromList(UniqueStr, AvailableConfRooms);

											if (AvailableRoom != null) {
												PrintWriter.append("<td>Room  Available </td>");
												PrintWriter.append("<td class='Passed'>PASS</td>");
												PrintWriter.append("<td></td></tr>");

												PrintTemplate.verifyTrue(CurrentRoom.getRoomLable(), AvailableRoom.getRoomLable().replace(":", ""), "Room Lable");
												PrintTemplate.verifyTrue(CurrentRoom.getRoomType(), AvailableRoom.getRoomType(), "Room Type");
												PrintTemplate.verifyTrue(CurrentRoom.getBedType(), AvailableRoom.getBedType(), "Bed Type");
												PrintTemplate.verifyTrue(CurrentRoom.getRateType(), AvailableRoom.getRateType(), "Rate Plan");
												// PrintTemplate.verifyTrue(CurrentSearch.getSearchCurrency(),ResultRm.getRoomRate().split(" ")[0],"Room Display Currency");
												PrintTemplate.verifyTrueValue(Double.toString(Loader.convertCurrency(CurrentRoom.getTotalRate(), SupplierCurrency, CurrentSearch.getSearchCurrency())), AvailableRoom.getTotalRate(),
														"Room Total Rate Validation");
												PrintWriter.append("</table>");
											} else {
												PrintWriter.append("<td>Room not available in results</td>");
												PrintWriter.append("<td class='Failed'>FAIL</td>");
												PrintWriter.append("<td></td></tr>");
												PrintWriter.append("</table>");
											}

										}

										// Confirmation page Cancellation Policy Validation

										PrintWriter.append("<br>");
										PrintWriter.append("<br><p class='InfoSup' style=\"font-weight: bold;\">Confirmation page Cancellation Policy Validation</p><br>");
										PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

										CancellationPolicy AvailableConfirmationCancelltion = ActualConfPage.getCon_CancellationPolicy();
										PrintTemplate.verifyTrue(ExpectedPolicy.getNumberofpolicies().trim(), AvailableConfirmationCancelltion.getNumberofpolicies().trim(), "Check Available Policy Sections");

										if (CurrentSearch.getBookingType() == BookingType.OUTSIDECANCELLATION) {
											PrintTemplate.verifyContains(ExpectedPolicy.getDeadline().trim(), AvailableConfirmationCancelltion.getDeadline().trim(), "Check Cancellation Deadline Section");
											PrintTemplate.verifyTrue(ExpectedPolicy.getRefundable().trim(), AvailableConfirmationCancelltion.getRefundable().trim(), "Check Refundable Section");
										} else {
											PrintTemplate.verifyTrue("Not Available", Actualpolicy.getDeadline().trim(), "Check Cancellation Deadline Section");
											PrintTemplate.verifyTrue("Not Available", Actualpolicy.getRefundable().trim(), "Check Refundable Section");
										}

										ArrayList<String> ActualConfPolicies = AvailableConfirmationCancelltion.getPolicies();

										for (int i = 1; i <= ExpectedPolicy.getPolicies().size(); i++) {
											String Policy = null;
											if (i == 1)
												Policy = changeFirstPolicyDateTo(ExpectedPolicy.getPolicies().get(i - 1));
											else
												Policy = ExpectedPolicy.getPolicies().get(i - 1);

											PrintTemplate.verifyTrue(changePolicyCurrencyAndRate(Policy, CurrentHotel.getHotelCurrency().trim(), CurrentSearch.getSearchCurrency(), 0.0).trim(), ActualConfPolicies.get(i - 1).trim(),
													"Check Cancellation Policy Section " + i);

										}

										PrintWriter.append("</table >");

										// Confirmation Occupancy Details Validation

										HashMap<Integer, HashMap<String, Occupancy>> ExpectedRoomOccupancyMap = ActualPaymentPg.getFilledOccuDetailsMap();
										HashMap<Integer, HashMap<String, Occupancy>> ActualRoomOccupancyMap = ActualConfPage.getActualOccuDetailsMap();
										HashMap<Integer, String> AditionalOccupancyDetails = ActualConfPage.getAdditionalDetails();

										PrintWriter.append("<br><br>");
										PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">Room wise occupancy detail validation</p><br>");

										PrintWriter.append("<table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

										for (int i = 1; i <= Integer.parseInt(CurrentSearch.getRooms()); i++) {

											HashMap<String, Occupancy> RoomSpecificExpectecOccupancyMap = ExpectedRoomOccupancyMap.get(i);
											HashMap<String, Occupancy> RoomSpecificActualOccupancyMap = ActualRoomOccupancyMap.get(i);
											String AditionalDetails = AditionalOccupancyDetails.get(i);
											Iterator<Entry<String, Occupancy>> ExpcRoomItr = RoomSpecificExpectecOccupancyMap.entrySet().iterator();

											PrintWriter.append("<tr><td>Room " + i + "</td></tr>");
											PrintTemplate.verifyContains("Hotel Name : " + CurrentSearch.getHotelName() + ",Estimated Check in Time : " + "01:30", AditionalDetails, "Conf Page Occupancy Grid - Hotel name and estimated arrival time");

											while (ExpcRoomItr.hasNext()) {
												Entry<String, Occupancy> ent = ExpcRoomItr.next();
												String Key = ent.getKey();
												Occupancy CurrentOccuPancy = ent.getValue();
												Occupancy ActualOccuPancy = RoomSpecificActualOccupancyMap.get(Key);

												String ExpectedOccuString = CurrentOccuPancy.getRoomLable() + "/" + CurrentOccuPancy.getTitle() + "/" + CurrentOccuPancy.getFirstName() + "/" + CurrentOccuPancy.getLastName() + "/"
														+ CurrentOccuPancy.isSmoking() + "/" + CurrentOccuPancy.isHandicapped() + "/" + CurrentOccuPancy.isWheelChairNeeded();
												String ActualOccuString = "";
												try {
													ActualOccuString = ActualOccuPancy.getRoomLable() + "/" + ActualOccuPancy.getTitle() + "/" + ActualOccuPancy.getFirstName() + "/" + ActualOccuPancy.getLastName() + "/" + ActualOccuPancy.isSmoking()
															+ "/" + ActualOccuPancy.isHandicapped() + "/" + ActualOccuPancy.isWheelChairNeeded();
												} catch (Exception e) {
													ActualOccuString = "Error";
												}

												PrintTemplate.verifyTrue(ExpectedOccuString, ActualOccuString, "Room " + i + "Occupancy Check");
											}

										}

										// Confirmation Customer Details Validation
										PrintWriter.append("</table >");
										PrintWriter.append("<br><br>");
										PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">Confirmation Page Customer Details Validation</p><br>");
										// PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\"><<Confirmation Page Customer Details Validation>></p><br>");
										PrintWriter.append("<table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

										PrintTemplate.verifyTrue(ExpectedConfPage.getCon_Customer_Fname(), ActualConfPage.getCon_Customer_Fname(), "Customer First Name Validation");
										PrintTemplate.verifyTrue(ExpectedConfPage.getCon_Customer_Lname(), ActualConfPage.getCon_Customer_Lname(), "Customer Last Name Validation");
										PrintTemplate.verifyTrue(ExpectedConfPage.getCon_Customer_Email(), ActualConfPage.getCon_Customer_Email(), "Customer Email 1 Validation");
										PrintTemplate.verifyTrue(ExpectedConfPage.getCon_Customer_Email2(), ActualConfPage.getCon_Customer_Email2(), "Customer Email 2 Validation");
										PrintTemplate.verifyTrue(ExpectedConfPage.getCon_Customer_City(), ActualConfPage.getCon_Customer_City(), "Customer City Validation");
										PrintTemplate.verifyTrue(ExpectedConfPage.getCon_Customer_Country(), ActualConfPage.getCon_Customer_Country(), "Customer Country Validation");
										PrintTemplate.verifyTrue(ExpectedConfPage.getCon_Customer_Emergency().replace("-", ""), ActualConfPage.getCon_Customer_Emergency(), "Customer Emergency Number Validation");
										PrintTemplate.verifyTrue(ExpectedConfPage.getCon_Customer_Fax(), ActualConfPage.getCon_Customer_Fax(), "Customer Fax number Validation");
										PrintTemplate.verifyTrue(ExpectedConfPage.getCon_Customer_PostCode(), ActualConfPage.getCon_Customer_PostCode(), "Customer PostalCode Validation");
										PrintTemplate.verifyTrue(ExpectedConfPage.getCon_Customer_State(), ActualConfPage.getCon_Customer_State(), "Customer State Validation");
										PrintTemplate.verifyTrue(ExpectedConfPage.getCon_Customer_Tel(), ActualConfPage.getCon_Customer_AreaCode() + "-" + ActualConfPage.getCon_Customer_Phone(), "Cutomer Telephone Validation");
										// PrintTemplate.verifyTrue(ExpectedConfPage.getCon_Customer_Fax(),ActualConfPage.getCon_Customer_Fax(),"Cutomer First Name Validation");

										PrintWriter.append("</table>");

										if (CurrentSearch.getPaymentType().equalsIgnoreCase("ONLINE")) {
											// Validation of online payment details in confirmation page
											PrintWriter.append("<br><br>");
											PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">Confirmation Page-Online Payment Details validation</p><br>");
											PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

											PrintTemplate.verifyTrue(true, ActualConfPage.isIs_Con_ON_DetailsAvailble(), "Verify Whether Online Payment Details Section Available");

											if (ActualConfPage.isIs_Con_ON_DetailsAvailble()) {
												// PaymentGateway ActualPaymentGateway = CurrentResult.getGateWay();
												PrintTemplate.verifyTrue(ExpectedPaymentPg.getPay_ON_Shift_SummeryValue(), ActualConfPage.getCon_ON_Amount(), "Online Payment Details Section- Amount");
												PrintTemplate.verifyContains(PropertySet.get("Gateway.Currency").trim(), ActualConfPage.getCon_ON_Currency(), "Online Payment Details Section- Currency");

												if (ActualConfPage.getCon_ON_MerchantTrack().equalsIgnoreCase("N/A")) {
													PrintTemplate.verifyTrue(true, false, "Online Payment Details Section- Merchant Track");
												} else {
													PrintTemplate.verifyTrue(ActualConfPage.getCon_ON_MerchantTrack(), ActualConfPage.getCon_ON_MerchantTrack(), "Online Payment Details Section- Merchant Track");
												}

												if (ActualConfPage.getCon_ON_AuthReference().equalsIgnoreCase("N/A")) {
													PrintTemplate.verifyTrue(true, false, "Online Payment Details Section- Auth Reference");
												} else {
													PrintTemplate.verifyTrue(ActualConfPage.getCon_ON_AuthReference(), ActualConfPage.getCon_ON_AuthReference(), "Online Payment Details Section- Auth Reference");
												}
												if (ActualConfPage.getCon_ON_PayID().equalsIgnoreCase("N/A")) {
													PrintTemplate.verifyTrue(true, false, "Online Payment Details Section- PayID");
												} else {
													PrintTemplate.verifyTrue(ActualConfPage.getCon_ON_PayID(), ActualConfPage.getCon_ON_PayID(), "Online Payment Details Section- PayID");
												}

												// TODO:Payment Gateway intermediate popup values
												/*
												 * PrintTemplate.verifyContains(ExpectedPaymentPg.getPay_ON_Shift_SummeryCurrency(),ActualConfPage.getCon_ON_Currency(),
												 * "Online Payment Details Section- Currency");
												 * PrintTemplate.verifyTrue(ActualPaymentGateway.getMerchantID(),ActualConfPage.getCon_ON_MerchantTrack(),"Online Payment Details Section- Merchant Track"
												 * ); PrintTemplate.verifyTrue(ActualPaymentGateway.getTransactionID(),ActualConfPage.getCon_ON_PayID(),"Online Payment Details Section-Pay ID");
												 */
												// PrintTemplate.verifyTrue(ActualPaymentGateway.getMerchantID(),ActualConfPage.getCon_ON_Amount(),"Online Payment Details Section- Amount");
											}
											PrintWriter.append("</table>");
										} else {
											PrintWriter.append("<br><br>");
											PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">Confirmation Page Payment Details Validation</p><br>");
											PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

											// Validating Offline Payment Details in Confirmation Page
											PrintTemplate.verifyTrue(true, ActualConfPage.isIs_Con_OFF_DetailsAvailble(), "Confirmation Page Offline Payment Details Availability");
										    // && (!ExpectedConfPage.getCon_OFF_Method().equalsIgnoreCase("none"
											if (ActualConfPage.isIs_Con_OFF_DetailsAvailble()) {
												PrintTemplate.verifyTrue(ExpectedConfPage.getCon_OFF_Method(), ActualConfPage.getCon_OFF_Method(), "Confirmation Page Offline Payment Details Payment Method");
												PrintTemplate.verifyContains(ExpectedConfPage.getCon_OFF_Currency(), ActualConfPage.getCon_OFF_Currency(), "Confirmation Page Offline Payment Details Payment Currency");
												PrintTemplate.verifyTrue(ExpectedConfPage.getCon_OFF_Amount(), ActualConfPage.getCon_OFF_Amount(), "Confirmation Page Offline Payment Details Payment Amount");
											}
											PrintWriter.append("</table>");
										}

									} else {
										PrintWriter.append("<td>Confirmation Page not Loaded</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td>");
										PrintWriter.append("<td></td></tr>");
										PrintWriter.append("</table>");
									}

								} else {
									PrintWriter.append("<td>Hotel Not Added to Cart</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td>");
									PrintWriter.append("<td></td></tr>");
									PrintTemplate.verifyTrue(false, ValidationCart.isRateChangeAlertAvailable(), "Rate Change Message Availaility");

									PrintWriter.append("<tr><td>" + TestCaseCount + "</td><td>Rate Got Change Error Message Availability</td>");
									TestCaseCount++;
									PrintWriter.append("<td>Rate Got Change Error Message Should Not Pop Up</td>");

									if (cart_details.isRateGotChangeAlertAvailable()) {
										PrintWriter.append("<td>Rate Got Change Error Message Available</td>");
										PrintWriter.append("<td class='Failed'>FAIL</td>");
										PrintWriter.append("<td>" + cart_details.getRateGotChangeText() + "</td></tr>");
									} else {
										PrintWriter.append("<td>Rate Got Change Error Message not Available</td>");
										PrintWriter.append("<td class='Passed'>PASS</td>");
										PrintWriter.append("<td></td></tr>");
									}

									PrintWriter.append("</table>");
								}

							} else {
								PrintWriter.append("<td>Specific Hotel not added to the cart </td>");
								PrintWriter.append("<td class='Failed'>FAIL</td>");

								if (cart_details.isRateGotChangeAlertAvailable())
									PrintWriter.append("<td>Rate Got Changed Alert Poped up </td></tr>");
								else
									PrintWriter.append("<td></td></tr>");
								PrintWriter.append("</table>");
							}

							// Perfomance Details
							PrintWriter.append("<br><br>");
							PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">Perfomance Details Summery</p><br>");
							PrintWriter.append("<br><table ><tr><th>MileStone</th><th>Expected</th><th>Time Taken(in Sec)</th><th>Test Status</th><th>Errors</th></tr>");

							PerfomanceSingleton PerfSing = PerfomanceSingleton.getInstance();
							PrintTemplate.verifyPerfomance(3, PerfSing.getTimeTakenToLoadBookingEngine(), "TimeTakenToLoadBookingEngine");
							PrintTemplate.verifyPerfomance(10, PerfSing.getTimeTakenToDisplayResults(), "TimeTakenToDisplayResults");
							PrintTemplate.verifyPerfomance(5, PerfSing.getTimeTakenToAddToCart(), "TimeTakenToAddToCart");
							PrintTemplate.verifyPerfomance(7, PerfSing.getTimeTakenToConfirmation(), "TimeTakenToConfirmation");

							PrintWriter.append("</table>");

							// ================Report Validation====================//

							if (ActualConfPage.isLoded()) {

								// *********************** Start Of Reservation Report ***************************************//

								ReportReader Rpreader = new ReportReader();
								ReportVerificationClass RpValClass = new ReportVerificationClass(CurrentSearch, ExpectedConfPage);
								String RpUrl = "";

								try {
									logger.info("Starting Reservation Report Verification");
									RpUrl = PropertySet.get("Portal.Url").concat("/reports/operational/mainReport.do?reportId=31&reportName=Reservation Report");
									ReservationReport ResReport = Rpreader.readReservationReport(RpUrl, driver, ActualConfPage.getBookingNumber().replace("Reservation No       :   ", "").trim());
									RpValClass.verifyReservationReport(ResReport, Loader);
								} catch (Exception e) {
									logger.fatal("Exception in reservation report validation ", e);
								}

								// *********************** End Of Reservation Report Start Of P&L Report **********************//

								try {
									logger.info("Starting P&L Report Verification");
									RpUrl = PropertySet.get("Portal.Url").concat("/reports/operational/mainReport.do?reportId=38&reportName=Profit and Loss Report");
									profitAndLostReport ProfReport = Rpreader.readProfitndLossReport(RpUrl, driver, ActualConfPage.getBookingNumber().replace("Reservation No       :   ", "").trim());
									RpValClass.verifyProfitAndLossReport(ProfReport, Loader);
								} catch (Exception e) {
									logger.fatal("Exception in P&L report validation ", e);
								}

								// *********************** End Of P&L Report and Start of SupplierPayble **********************//

							}

							/*
							 * private double TimeTakenToLoadBookingEngine = 1L; private double TimeTakenToDisplayResults = 1L; private double TimeTakenToAddToCart = 1L; private double
							 * TimeTakenToConfirmation = 1L;
							 */
						} else {

							PrintWriter.append("<td>Specified Hotel Not Available in results</td>");
							if (!CurrentSearch.getAvailability().equalsIgnoreCase("NotAvailable")) {
								PrintWriter.append("<td class='Failed'>FAIL</td>");
								PrintWriter.append("<td>" + CurrentResult.getErrorMessage() + "</td></tr>");
								PrintWriter.append("</table>");
							} else {
								PrintWriter.append("<td class='Passed'>PASS</td>");
								PrintWriter.append("<td>Check Screenshots</td></tr>");
								PrintWriter.append("</table>");

							}

						}
					} else {
						PrintWriter.append("<td>Hotel results not available</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td>");
						PrintWriter.append("<td>" + CurrentResult.getErrorMessage() + "</td></tr>");
						PrintWriter.append("</table>");

					}

				} else {

					logger.info("<-- Excecution state set as False Skipping the Scenario");
					continue ScenarioLoop;
				}

			}

			catch (Exception e) {
				File Scr = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(Scr, new File(Calender.getDate(Calendar.DATE, 0) + "_Errors/unexpected.jpg"));
				// Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getBasicFailedPath() + "/unexpected.jpg", driver);
				logger.fatal("", e);
				PrintWriter.append("</table>");

			}

			if (PropertySet.get("New.Driver.For.Each.Scenario").equalsIgnoreCase("YES")) {
				driver.quit();
				driver = methodClass.initalizeDriver();
				isLoginSuccess = methodClass.login(driver);
			}

			PrintWriter.append("</body></html>");
			BufferedWriter sbwr = new BufferedWriter(new FileWriter(new File("Reports/V3_Hotel_" + CurrentScenario + ".html")));
			sbwr.write(PrintWriter.toString());
			sbwr.flush();
			sbwr.close();
			PrintWriter.setLength(0);
		}
	}

	@After
	public void tearDown() throws IOException {

		/*
		 * PrintWriter.append("</body></html>");
		 * 
		 * BufferedWriter bwr = new BufferedWriter(new FileWriter(new File("NetRateCheckReport.html"))); bwr.write(PrintWriter.toString()); bwr.flush(); bwr.close();
		 */
		driver.quit();
	}

	public String getActualCancellation(SearchType CurrentSearch, RoomType CurrentRoom) {

		int ReducedDates = -(CurrentRoom.getApplyIflessThan() + 1);
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("E,dd-MMM-yyyy");

		try {
			cal.setTime(new SimpleDateFormat("MMM-dd-yyyy").parse(CurrentSearch.getArrivalDate()));
			cal.add(Calendar.DATE, ReducedDates);
			return sdf.format(cal.getTime());
		} catch (Exception e) {
			System.out.println("Parse Exception");
			return null;
		}

	}

	public String changeFirstPolicyDateTo(String PolicyChunk) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = Calendar.getInstance();
		String NewString = sdf.format(cal.getTime());

		String BeforeString = PolicyChunk.trim().split(" ")[4];

		return PolicyChunk.replace(BeforeString, NewString);
	}

	public String changePolicyCurrencyAndRate(String PolicyChunk, String HotelCurrency, String SellingCurrency, double Amount) {
		if (PolicyChunk.contains(HotelCurrency)) {
			ArrayList<String> cunkList = new ArrayList<String>(Arrays.asList(PolicyChunk.replace(" ", "#").split("#")));
			int RateIndex = cunkList.indexOf(HotelCurrency) + 1;
			String newRate = String.format("%.2f", Math.ceil(Loader.convertCurrency(cunkList.get(RateIndex).trim(), HotelCurrency, SellingCurrency) + Amount));
			String ReturnChunk = PolicyChunk.replace(HotelCurrency, SellingCurrency).replace(cunkList.get(RateIndex), newRate);

			return ReturnChunk;

		} else
			return PolicyChunk;

	}

	public boolean verifycart(SearchType CurrentSearch, RoomType CurrentRoom) {
		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame("selecedItems");
			driver.findElement(By.name("chk0"));
			return true;
		} catch (Exception ex) {
			System.out.println("Issue with adding to the Cart...");
			return false;
		}

	}

	public ResultsRoom CheckRoomAvailability(String UniqueString, ArrayList<ResultsRoom> ResultsRooms) {
		Iterator<ResultsRoom> it = ResultsRooms.iterator();
		ResultsRoom returnRoom = new ResultsRoom();
		WhileLoop: while (it.hasNext()) {
			ResultsRoom room = it.next();
			String RoomString = room.getRoomType() + "/" + room.getBedType() + "/" + room.getRatePlan();

			if (UniqueString.equalsIgnoreCase(RoomString.trim())) {
				returnRoom = room;
				break WhileLoop;
			}

		}

		return returnRoom;
	}

	public PaymentsPage createExpectedPaymentPg(ViewFullDetails viewFull, SearchType search, Hotel CurrentHotel) {
		PaymentsPage PaymentPG = new PaymentsPage();
		PaymentPG.setPay_HotelName(viewFull.getView_HotelName());
		PaymentPG.setPay_Address(CurrentHotel.getAddressLine1() + " " + CurrentHotel.getAddressLine2() + " " + CurrentHotel.getCity());
		PaymentPG.setPay_CheckIn(search.getBookingDate());
		PaymentPG.setPay_CheckOut(search.getCheckOutDate());
		PaymentPG.setPay_Nights(search.getNights());
		PaymentPG.setPay_Rooms(search.getRooms());

		if (search.getAvailability().equalsIgnoreCase("Available"))
			PaymentPG.setPay_BookingStatus("Available");
		else
			PaymentPG.setPay_BookingStatus("PENDING");

		if (search.getBookingType() == BookingType.OUTSIDECANCELLATION)
			PaymentPG.setPay_CandellationDeadLine(search.getPolicy().getDeadline());
		else
			PaymentPG.setPay_CandellationDeadLine("This hotel is within cancellation.");

		PaymentPG.setPay_SubTotal(viewFull.getView_SubTotal());
		PaymentPG.setPay_totalTaxAnServiceCharges(viewFull.getView_totalTaxAnServiceCharges());
		PaymentPG.setPay_TotalHotelBookingValue(viewFull.getView_TotalHotelBookingValue());
		PaymentPG.setPay_HotelAmountpayablenow(viewFull.getView_HotelAmountpayablenow());
		PaymentPG.setPay_HotelAmountToPaidAtCheckIn(viewFull.getView_HotelAmountToPaidAtCheckIn());
		PaymentPG.setPay_TotalGrossPackageBookingValue(viewFull.getView_SubTotal());

		String DefaultCardType = PropertySet.get("Portal.Default.Card");
		String CreditCardCharge = PropertySet.get(DefaultCardType.trim() + ".Charge");

		Double AmountProcessing = Double.parseDouble(viewFull.getView_AmountbeingProcessedNow().trim());

		Double CCFee = 0.0;

		if (Double.parseDouble(CreditCardCharge) < 1)
			CCFee = AmountProcessing * Double.parseDouble(CreditCardCharge);
		else
			CCFee = Loader.convertCurrency(CreditCardCharge, PropertySet.get("Gateway.Currency"), search.getSearchCurrency());
		// If credit card fee is a value , then it should be setup based on the payment gateway currency
		Double TotalTax = Double.parseDouble(viewFull.getView_TotalTaxesAndOtherCharges()) + CCFee;
		Double TotalBookingVal = Double.parseDouble(viewFull.getView_SubTotal()) + TotalTax;
		Double ReviseAmountProcessing = AmountProcessing + CCFee;

		PaymentPG.setPay_ON_CCFee(CCFee);
		PaymentPG.setPay_TotalTaxesAndOtherCharges(Double.toString(Math.ceil(TotalTax)));
		PaymentPG.setPay_TotalBookingValue(Double.toString(Math.ceil(TotalBookingVal)));
		PaymentPG.setPay_AmountbeingProcessedNow(Double.toString(Math.ceil(ReviseAmountProcessing)));
		PaymentPG.setPay_AmountdueatCheckIn(Double.toString(Math.abs(TotalBookingVal - ReviseAmountProcessing)));

		PaymentPG.setPay_ON_DefaultSelectedCard(DefaultCardType);
		PaymentPG.setPay_ON_TotalGrossBookingValue(Double.toString(Loader.convertCurrency(PaymentPG.getPay_TotalGrossPackageBookingValue(), search.getSearchCurrency(), PropertySet.get("Gateway.Currency"))));
		PaymentPG.setPay_ON_TotalTaxesAndFees(Double.toString(Loader.convertCurrency(PaymentPG.getPay_TotalTaxesAndOtherCharges(), search.getSearchCurrency(), PropertySet.get("Gateway.Currency"))));
		PaymentPG.setPay_ON_TotalPackageBookingValue(Double.toString(Loader.convertCurrency(PaymentPG.getPay_TotalBookingValue(), search.getSearchCurrency(), PropertySet.get("Gateway.Currency"))));
		PaymentPG.setPay_ON_AmountBeingProcessedNow(Double.toString(Loader.convertCurrency(PaymentPG.getPay_AmountbeingProcessedNow(), search.getSearchCurrency(), PropertySet.get("Gateway.Currency"))));
		PaymentPG.setPay_ON_AmountDueAtCheckIn(Double.toString(Loader.convertCurrency(PaymentPG.getPay_AmountdueatCheckIn(), search.getSearchCurrency(), PropertySet.get("Gateway.Currency"))));
		PaymentPG.setPay_ON_CurrencyLable(PropertySet.get("Gateway.Currency"));
		PaymentPG.setPay_ON_SummeryCurrency(PropertySet.get("Gateway.Currency"));
		PaymentPG.setPay_ON_SummeryValue(Double.toString(Loader.convertCurrency(PaymentPG.getPay_AmountbeingProcessedNow(), search.getSearchCurrency(), PropertySet.get("Gateway.Currency"))));

		PaymentPG.setPay_Off_TotalGrossPackageBookingValue(viewFull.getView_SubTotal());
		PaymentPG.setPay_Off_TotalTaxesAndOtherCharges(viewFull.getView_TotalTaxesAndOtherCharges());
		PaymentPG.setPay_Off_TotalBookingValue(viewFull.getView_TotalBookingValue());
		PaymentPG.setPay_Off_AmountbeingProcessedNow(viewFull.getView_AmountbeingProcessedNow());
		PaymentPG.setPay_Off_AmountdueatCheckIn(viewFull.getView_AmountdueatCheckIn());
		PaymentPG.setPay_OFF_Amount(viewFull.getView_AmountbeingProcessedNow());
		PaymentPG.setPay_OFF_Currency(search.getSearchCurrency());

		// TODO: Has to implement for pay@site and Pay Commission hotels

		if (search.getPaymentType().equalsIgnoreCase("online")) {
			DefaultCardType = search.getCardType();
			CreditCardCharge = PropertySet.get(DefaultCardType.toUpperCase() + ".Charge");

			AmountProcessing = Double.parseDouble(viewFull.getView_AmountbeingProcessedNow().trim());

			if (Double.parseDouble(CreditCardCharge) < 1)
				CCFee = AmountProcessing * Double.parseDouble(CreditCardCharge);
			else
				CCFee = Loader.convertCurrency(CreditCardCharge, PropertySet.get("Gateway.Currency"), search.getSearchCurrency());

			TotalTax = Double.parseDouble(viewFull.getView_TotalTaxesAndOtherCharges()) + CCFee;
			TotalBookingVal = Double.parseDouble(viewFull.getView_SubTotal()) + TotalTax;
			ReviseAmountProcessing = AmountProcessing + CCFee;

			PaymentPG.setPay_ShiftedCardType(DefaultCardType);
			PaymentPG.setPay_ON_Shift_CCFee(CCFee);
			PaymentPG.setPay_CreditShift_TotalGrossPackageBookingValue(PaymentPG.getPay_TotalGrossPackageBookingValue());
			PaymentPG.setPay_CreditShift_TotalTaxesAndOtherCharges(Double.toString(Math.ceil(TotalTax)));
			PaymentPG.setPay_CreditShift_TotalBookingValue(Double.toString(Math.ceil(TotalBookingVal)));
			PaymentPG.setPay_CreditShift_AmountbeingProcessedNow(Double.toString(Math.ceil(ReviseAmountProcessing)));
			PaymentPG.setPay_CreditShift_AmountdueatCheckIn(Double.toString(Math.abs(TotalBookingVal - ReviseAmountProcessing)));

			PaymentPG.setPay_ON_Shift_TotalGrossBookingValue(Double.toString(Loader.convertCurrency(PaymentPG.getPay_TotalGrossPackageBookingValue(), search.getSearchCurrency(), PropertySet.get("Gateway.Currency"))));
			PaymentPG.setPay_ON_Shift_TotalTaxesAndFees(Double.toString(Loader.convertCurrency(Double.toString(TotalTax), search.getSearchCurrency(), PropertySet.get("Gateway.Currency"))));
			PaymentPG.setPay_ON_Shift_TotalPackageBookingValue(Double.toString(Loader.convertCurrency(PaymentPG.getPay_TotalBookingValue(), search.getSearchCurrency(), PropertySet.get("Gateway.Currency"))));
			PaymentPG.setPay_ON_Shift_AmountBeingProcessedNow(Double.toString(Loader.convertCurrency(PaymentPG.getPay_AmountbeingProcessedNow(), search.getSearchCurrency(), PropertySet.get("Gateway.Currency"))));
			PaymentPG.setPay_ON_Shift_CurrencyLable(PropertySet.get("Gateway.Currency"));
			PaymentPG.setPay_ON_Shift_SummeryCurrency(PropertySet.get("Gateway.Currency"));
			PaymentPG.setPay_ON_Shift_SummeryValue(Double.toString(Loader.convertCurrency(PaymentPG.getPay_AmountbeingProcessedNow(), search.getSearchCurrency(), PropertySet.get("Gateway.Currency"))));

		}

		PaymentPG.setPay_RoomDetails(viewFull.getRoomDetails());
		PaymentPG.setPay_CancellationPolicy(viewFull.getCancellationPolicy());

		return PaymentPG;

	}

	public ConfirmationPage createExpectedConfPage(PaymentsPage ExpPayment, SearchType search, Hotel CurrentHotel) {
		ConfirmationPage ConfPG = new ConfirmationPage();
		ConfPG.setCon_HotelName(ExpPayment.getPay_HotelName());
		ConfPG.setCon_Address(CurrentHotel.getAddressLine1() + " " + CurrentHotel.getAddressLine2() + " " + CurrentHotel.getCity());
		ConfPG.setCon_CheckIn(search.getBookingDate());
		ConfPG.setCon_CheckOut(search.getCheckOutDate());
		ConfPG.setCon_Nights(search.getNights());
		ConfPG.setCon_Rooms(search.getRooms());

		if (search.getAvailability().equalsIgnoreCase("Available"))
			ConfPG.setCon_BookingStatus("Confirmed");
		else
			ConfPG.setCon_BookingStatus("On Request");

		if (search.getBookingType() == BookingType.OUTSIDECANCELLATION)
			ConfPG.setCon_CandellationDeadLine(search.getPolicy().getDeadline());
		else
			ConfPG.setCon_CandellationDeadLine("This hotel is within cancellation.");

		ConfPG.setCon_CandellationDeadLine(ExpPayment.getPay_CandellationDeadLine());

		ConfPG.setCon_SubTotal(ExpPayment.getPay_SubTotal());
		ConfPG.setCon_totalTaxAnServiceCharges(ExpPayment.getPay_totalTaxAnServiceCharges());
		ConfPG.setCon_TotalHotelBookingValue(ExpPayment.getPay_TotalHotelBookingValue());
		ConfPG.setCon_HotelAmountpayablenow(ExpPayment.getPay_HotelAmountpayablenow());
		ConfPG.setCon_HotelAmountToPaidAtCheckIn(ExpPayment.getPay_HotelAmountToPaidAtCheckIn());
		ConfPG.setCon_TotalGrossPackageBookingValue(ExpPayment.getPay_SubTotal());
		ConfPG.setCon_ON_CCFee(ExpPayment.getPay_ON_Shift_CCFee());

		// TODO: Has to implement for pay@site and Pay Commission hotels

		if (search.getPaymentType().equalsIgnoreCase("online")) {
			ConfPG.setCon_TotalTaxesAndOtherCharges(ExpPayment.getPay_CreditShift_TotalTaxesAndOtherCharges());
			ConfPG.setCon_TotalBookingValue(ExpPayment.getPay_CreditShift_TotalBookingValue());
			ConfPG.setCon_FinalSummerycurrency(ExpPayment.getPay_CreditShift_FinalSummerycurrency());
			ConfPG.setCon_AmountbeingProcessedNow(ExpPayment.getPay_CreditShift_AmountbeingProcessedNow());
			ConfPG.setCon_AmountdueatCheckIn(ExpPayment.getPay_CreditShift_AmountdueatCheckIn());

		} else {

			ConfPG.setCon_TotalTaxesAndOtherCharges(ExpPayment.getPay_Off_TotalTaxesAndOtherCharges());
			ConfPG.setCon_TotalBookingValue(ExpPayment.getPay_Off_TotalBookingValue());
			ConfPG.setCon_FinalSummerycurrency(ExpPayment.getPay_Off_FinalSummerycurrency());
			ConfPG.setCon_AmountbeingProcessedNow(ExpPayment.getPay_Off_AmountbeingProcessedNow());
			ConfPG.setCon_AmountdueatCheckIn(ExpPayment.getPay_Off_AmountdueatCheckIn());
			ConfPG.setCon_OFF_Method(search.getOfflineMethod());
			if (!search.getOfflineMethod().equalsIgnoreCase("None")) {
				ConfPG.setCon_OFF_Amount(ExpPayment.getPay_Off_AmountbeingProcessedNow());
				ConfPG.setCon_OFF_Currency(ExpPayment.getPay_Off_FinalSummerycurrency());
				ConfPG.setCon_OFF_Reference(search.getPaymentRef().trim());
			} else {
				ConfPG.setCon_OFF_Amount("N/A");
				ConfPG.setCon_OFF_Currency("N/A");
				ConfPG.setCon_OFF_Reference("N/A");
			}

		}

		ConfPG.setCon_RoomDetails(ExpPayment.getPay_RoomDetails());
		ConfPG.setCon_CancellationPolicy(ExpPayment.getPay_CancellationPolicy());

		// Setting Up expected customer details
		ConfPG.setCon_Customer_Title(PropertySet.get("CustomerTitle"));
		ConfPG.setCon_Customer_Fname(PropertySet.get("CustomerFname"));
		ConfPG.setCon_Customer_Lname(PropertySet.get("CustomerLastName"));
		ConfPG.setCon_Customer_AreaCode(PropertySet.get("CustomerTelephone").split("-")[0]);
		ConfPG.setCon_Customer_Phone(PropertySet.get("CustomerTelephone").split("-")[1]);
		ConfPG.setCon_Customer_Email(PropertySet.get("CustomerEmail"));
		ConfPG.setCon_Customer_Fax(PropertySet.get("CustomerFax"));
		ConfPG.setCon_Customer_Email2(PropertySet.get("CustomerEmail2"));
		ConfPG.setCon_Customer_Adderss1(PropertySet.get("CustomerAddress1"));
		ConfPG.setCon_Customer_Adderss2(PropertySet.get("CustomerAddress2"));
		ConfPG.setCon_Customer_Emergency(PropertySet.get("CustomerEmergency"));
		ConfPG.setCon_Customer_Country(PropertySet.get("CustomerCountry"));
		ConfPG.setCon_Customer_City(PropertySet.get("CustomerCity"));
		ConfPG.setCon_Customer_Tel(PropertySet.get("CustomerTelephone"));

		if (PropertySet.get("CustomerCountry").trim().equalsIgnoreCase("Australia") || PropertySet.get("CustomerCountry").trim().equalsIgnoreCase("USA") || PropertySet.get("CustomerCountry").trim().equalsIgnoreCase("USA")) {
			ConfPG.setCon_Customer_State(PropertySet.get("CustomerState"));
			ConfPG.setCon_Customer_PostCode(PropertySet.get("CustomerPostal"));
		} else {
			ConfPG.setCon_Customer_State("N/A");
			ConfPG.setCon_Customer_PostCode("N/A");
		}
		return ConfPG;
	}

	public ViewFullDetails createExpectedViewFull(CartDetails Cart, SearchType search, Hotel CurrentHotel) {
		ViewFullDetails viewFull = new ViewFullDetails();
		viewFull.setView_HotelName(CurrentHotel.getHotelName());
		viewFull.setView_Address(CurrentHotel.getAddressLine1() + " " + CurrentHotel.getAddressLine2() + " " + CurrentHotel.getCity());
		viewFull.setView_CheckIn(search.getBookingDate());
		viewFull.setView_CheckOut(search.getCheckOutDate());
		viewFull.setView_Nights(search.getNights());
		viewFull.setView_NumberOfRooms(search.getRooms());

		if (search.getAvailability().equalsIgnoreCase("Available"))
			viewFull.setView_BookingStatus("Available");
		else
			viewFull.setView_BookingStatus("PENDING");

		if (search.getBookingType() == BookingType.OUTSIDECANCELLATION)
			viewFull.setView_CancellationDeadLine(search.getPolicy().getDeadline());
		else
			viewFull.setView_CancellationDeadLine("This hotel is within cancellation.");

		viewFull.setView_SubTotal(Cart.getSubTotal());
		viewFull.setView_totalTaxAnServiceCharges(Cart.getTaxes());
		viewFull.setView_TotalHotelBookingValue(Cart.getTotaPaybel());
		viewFull.setView_TotalGrossPackageBookingValue(Cart.getSubTotal());
		viewFull.setView_TotalTaxesAndOtherCharges(Cart.getTaxes());
		viewFull.setView_TotalBookingValue(Cart.getTotaPaybel());

		HotelContract hotelcontract = getApplicableContract(CurrentHotel.getHotelContracts(), search.getArrivalDate());

		if (CurrentHotel.getRateContract() == RateContractByType.COMMISSIONABLE) {
			Double TotalPayble = Double.parseDouble(Cart.getTotaPaybel());
			Double ServiceFee = Cart.getCalculatedServiceFeeInSelling();

			if (CurrentHotel.getCommHotelType() == CommssionableType.PAYATSITE) {

				viewFull.setView_AmountbeingProcessedNow(Double.toString(ServiceFee));
				viewFull.setView_AmountdueatCheckIn(Double.toString(TotalPayble - ServiceFee));
				viewFull.setView_HotelAmountpayablenow(Double.toString(ServiceFee));
				viewFull.setView_HotelAmountToPaidAtCheckIn(Double.toString(TotalPayble - ServiceFee));

			} else if (CurrentHotel.getCommHotelType() == CommssionableType.PAYCOMMISION) {
				String CommissionInHotelCurr = "";
				Double CommissionInSellCurr = 0.00;

				if (hotelcontract.getCommType() == ChargeByType.PERCENTAGE)
					CommissionInHotelCurr = Double.toString(Cart.getNetRate() * (Double.parseDouble(hotelcontract.getCommvalue()) / 100));
				else
					CommissionInHotelCurr = Double.toString(Double.parseDouble(hotelcontract.getCommvalue()) * Integer.parseInt(search.getRooms()) * Integer.parseInt(search.getNights()));

				// Commission = Double.parseDouble(hotelcontract.getCommvalue())*Integer.parseInt(CurrentSearch.getRooms())*Integer.parseInt(CurrentSearch.getNights());

				CommissionInSellCurr = Loader.convertCurrency(CommissionInHotelCurr, CurrentHotel.getHotelCurrency(), search.getSearchCurrency());
				viewFull.setView_AmountbeingProcessedNow(Double.toString(ServiceFee + CommissionInSellCurr));
				viewFull.setView_AmountdueatCheckIn(Double.toString(TotalPayble - ServiceFee - CommissionInSellCurr));
				viewFull.setView_HotelAmountpayablenow(Double.toString(ServiceFee + CommissionInSellCurr));
				viewFull.setView_HotelAmountToPaidAtCheckIn(Double.toString(TotalPayble - ServiceFee - CommissionInSellCurr));
			} else {
				viewFull.setView_AmountbeingProcessedNow(Cart.getTotaPaybel());
				viewFull.setView_AmountdueatCheckIn("0");
			}
		} else {
			viewFull.setView_AmountbeingProcessedNow(Cart.getTotaPaybel());
			viewFull.setView_AmountdueatCheckIn("0");
		}

		ArrayList<RoomType> ViewFullList = new ArrayList<RoomType>(Integer.parseInt(viewFull.getView_NumberOfRooms()));
		Map<String, ArrayList<RoomType>> ExpectedRooms = search.getResultsRoomMap();
		Map<String, String> DefaultRooms = search.getSelectedRooms();

		for (int i = 1; i <= Integer.parseInt(search.getRooms()); i++) {
			ViewFullList.add(i - 1, getRoomFromList(DefaultRooms.get(Integer.toString(i)), ExpectedRooms.get(Integer.toString(i))));
		}
		viewFull.setRoomDetails(ViewFullList);
		viewFull.setCancellationPolicy(search.getCartPolicy());

		return viewFull;

	}

	public RoomType getRoomFromList(String RoomString, ArrayList<RoomType> rooms) {
		RoomType roomType = null;
		for (Iterator<RoomType> iterator = rooms.iterator(); iterator.hasNext();) {
			roomType = (RoomType) iterator.next();
			String UniqueStr = roomType.getRoomType() + "/" + roomType.getBedType() + "/" + roomType.getRateType();

			if (RoomString.trim().equalsIgnoreCase(UniqueStr)) {

				return roomType;
			}
		}

		return roomType;
	}

	public static String getDateInFormat(String CurrentFormat, String RequiredFormat, String Date) {
		SimpleDateFormat CurrentFormatter = new SimpleDateFormat(CurrentFormat);
		SimpleDateFormat RequiredFormatter = new SimpleDateFormat(RequiredFormat);

		try {
			return RequiredFormatter.format(CurrentFormatter.parse(Date));
		} catch (Exception e) {
			return Date;
		}

	}

	public HotelContract getApplicableContract(ArrayList<HotelContract> ContractList, String Checkin) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
		HotelContract returnCon = null;
		Date checkin = null;

		try {
			checkin = sdf.parse(Checkin);
		} catch (Exception e) {
			System.out.println("Parse Exception ");
		}

		for (Iterator<HotelContract> iterator = ContractList.iterator(); iterator.hasNext();) {
			HotelContract hotelContract = (HotelContract) iterator.next();

			Date ConFrom = null;
			Date ConTo = null;
			try {
				ConFrom = sdf2.parse(hotelContract.getContractFrom());
				ConTo = sdf2.parse(hotelContract.getContractTo());
			} catch (Exception e) {
				System.out.println("Parse Exception ");
			}

			if ((checkin.before(ConTo) || checkin.equals(ConTo)) && (checkin.after(ConFrom) || checkin.equals(ConFrom)))
				returnCon = hotelContract;

		}

		return returnCon;

	}
}
