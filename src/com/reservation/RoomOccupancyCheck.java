//package com.reservation;
//
//
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.net.UrlChecker.TimeoutException;
//
//import app.support.DataLoader;
//import app.support.MethodClass;
//import app.support.ResultsType;
//import app.support.RoomType;
//import app.support.SearchType;
//import com.utilities.ExcelReader;
//import com.utilities.ReadProperties;
//
//
//public class RoomOccupancyCheck {
//	
//private WebDriver                                   driver;
//private HashMap<String, String>                     PropertySet;
//private ArrayList<SearchType>                       SearchList ;
//private ArrayList<RoomType>                         RoomList ;
//private ArrayList<Map<Integer,String>>              SheeetList;  
//private MethodClass                                 methodClass; 
//private StringBuffer                                PrintWriter;
//private int                                        TestCaseCount;
//
//
//@Before
//
//public void setUp() throws IOException
//{ 
//	ExcelReader ReadExcel = new ExcelReader();
//	PropertySet           = ReadProperties.readpropeties();
//	DataLoader  Loader    = new DataLoader(PropertySet.get("portal.currency")) ;
//    methodClass           = new MethodClass(PropertySet);
//    SheeetList            = ReadExcel.init(PropertySet.get("ExcelPath"));
//    SearchList            = Loader.loadSearchDetails(SheeetList.get(0));
//  //  RoomList              = Loader.loadRoomDetails(SheeetList.get(2));
//    driver                = methodClass.initalizeDriver();
//    PrintWriter           = new StringBuffer();
//    
//    
//	
//}
//
//@Test
//
//public void runTest() throws IOException, InterruptedException
//{
//	PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
//	PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>V3 ROOM OCCUPANCY CHECK-Call Center</p></div>");
//	PrintWriter.append("<body>");
//
//
//	Iterator<SearchType>   it = SearchList.iterator();
//	RoomType     CurrentRoom  = RoomList.get(0);
//	
//	try {
//		if(!(methodClass.login(driver))){
//			PrintWriter.append("<p><font family='Arial' size='9px' color='Red'  >Login Failed !!!</font></p>");
//			driver.quit();
//			System.exit(0);
//		}
//	} catch (Exception e) {
//		    PrintWriter.append("<p><font family='Arial' size='9px' color='Red'  >An Error Occured !!!</font></p>");
//		
//			driver.quit();
//			System.exit(0);
//	}
//	
//	PrintWriter.append("</br></br><table ><tr><th>Test Case #</th><th>Test Description</th><th>Expected Hotel Availability</th><th>Actual Hotel Availability</th><th>Expected Rooms</th><th>Actual Available Rooms</th><th>Expected Default Room</th><th>Actual Default Room</th><th>Expected Rate Without rounding</th><th>Expected Rate After rounding</th><th>Rate in results</th><th>Test Status</th></tr>");
//    TestCaseCount = 0;
//	while (it.hasNext()) {
//		try {
//			
//         	ResultsType CurrentResult;
//			Double ExpectedRate   = null;
//		//	Double FloorExpectedRate;
//			String ExpectedRateString   = null;
//		//	String F_ExpectedRateString = null;
//			SearchType CurrentSearch    = it.next();
//			TestCaseCount ++;
//			PrintWriter.append("<tr><td>"+TestCaseCount+"</td>");
//			PrintWriter.append("<td >"+ CurrentSearch.getDescription() +"</td>");
//	        methodClass.getReservationPageCC(driver);
//			CurrentResult = methodClass.search(driver, CurrentSearch);
//			//CurrentSearch.setSearchCurrency("USD");
//			
//	         try {
//				 ExpectedRate         = Math.ceil(Double.parseDouble(CurrentSearch.getExpected()));
//				// FloorExpectedRate    = Math.floor(Double.parseDouble(CurrentSearch.getExpected()));
//				 ExpectedRateString   = CurrentSearch.getSearchCurrency()+" "+Double.toString(ExpectedRate);
//				// F_ExpectedRateString = Double.toString(FloorExpectedRate );
//			      } 
//			  catch (Exception e) {
//				
//			        }
//
//			
//			
//		if(CurrentResult.isAvailabilityStatus() == true)
//			{
//				
//				CurrentResult            = methodClass.getHotelRates(driver, CurrentRoom, CurrentResult);
//			//	Double CurrentResultRate = Double.parseDouble(CurrentResult.getResultsRate());
//				
//				if(CurrentSearch.isHotelAvailability())
//			    PrintWriter.append("<td>Available</td>");
//			    else
//			    PrintWriter.append("<td>Not Available</td>");
//					
//					if(CurrentResult.isHotelFound())
//					{
//					PrintWriter.append("<td>Available</td>");
//				   
//					ArrayList<String> ExpectedRooms = CurrentSearch.getRoomType();
//					 String RoomString               = "";
//					 for (String string : ExpectedRooms) {
//						 RoomString = RoomString.concat(string+"/");
//						
//					}
//					 
//					 ArrayList<String> ActualRooms = CurrentResult.getAvailableRooms();
//					 String ActualRoomString               = "";
//					 for (String string : ActualRooms) {
//						 ActualRoomString = ActualRoomString.concat(string+"/");
//						
//					}
//					 
//					 PrintWriter.append("<td>"+ RoomString+"</td>");
//					 PrintWriter.append("<td>"+ ActualRoomString +"</td>");
//					 
//					 PrintWriter.append("<td>"+ CurrentSearch.getDefaultRoom()+"</td>");
//					 PrintWriter.append("<td>"+ CurrentResult.getDefaultRoom() +"</td>");
//					 
//					 PrintWriter.append("<td>"+ CurrentSearch.getSearchCurrency()+" "+ CurrentSearch.getExpected()+"</td>");
//					 PrintWriter.append("<td>"+ ExpectedRateString +"</td>");
//					 
//					 PrintWriter.append("<td>"+ CurrentResult.getDefaultRate()+"</td>");
//					 
//					 if(checkDefault(CurrentSearch.getDefaultRoom(), CurrentResult.getDefaultRoom()) && checkRates(ExpectedRateString, CurrentResult.getDefaultRate()) && checkRoomTypes(CurrentSearch.getRoomType(), CurrentResult.getAvailableRooms()))
//					 PrintWriter.append("<td class='Passed'>PASS</td></tr>");
//					 else
//					 PrintWriter.append("<td class='Failed'>FAIL</td></tr>");
//						 
//				   
//						
//						
//						}
//					else
//					{
//						PrintWriter.append("<td >NOT AVAILABLE</td>");
//						PrintWriter.append("<td>-</td>");
//						PrintWriter.append("<td>-</td>");
//						PrintWriter.append("<td>-</td>");
//						PrintWriter.append("<td>-</td>");
//						PrintWriter.append("<td>-</td>");
//					    PrintWriter.append("<td>-</td>");
//						PrintWriter.append("<td>-</td>");
//						
//					    if((CurrentSearch.getExpected().trim()).equalsIgnoreCase("Not Available"))
//					    PrintWriter.append("<td class='Passed' >PASS</td></tr>");
//						else
//						PrintWriter.append("<td class='Failed' >FAIL</td></tr>");
//					}
//		        
//			}
//			else {
//				PrintWriter.append("<td>-</td>");
//				PrintWriter.append("<td>-</td>");
//				PrintWriter.append("<td>-</td>");
//				PrintWriter.append("<td>-</td>");
//				PrintWriter.append("<td>-</td>");
//				PrintWriter.append("<td>-</td>");
//			    PrintWriter.append("<td>-</td>");
//				PrintWriter.append("<td>-</td>");
//				PrintWriter.append("<td class='Failed'>FAIL"+CurrentResult.getErrorMessage()+"</td></tr>");
//			
//				  }
//			
//			
//			     
//			
//		} catch (org.openqa.selenium.TimeoutException e) {
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td class='Failed'>FAIL:TimeOut Occured While Waiting for results</td></tr>");
//		}
//		
//		catch (Exception e) {
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td>Error</td>");
//			   PrintWriter.append("<td class='Failed'>FAIL:An exception thrown</td></tr>");
//		}
//	}
//
//	PrintWriter.append("</table>");
//	
//}
//
//@After   
//
//public void tearDown() throws IOException
//{
//  PrintWriter.append("</body></html>");
//  
//  BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(ReadProperties.readpropety("Report.Path")+"/RoomOccupancyTest.html")));
//  bwr.write(PrintWriter.toString());
//  bwr.flush();
//  bwr.close();
//  driver.quit();	
//}
//
//
//
//public boolean checkRoomTypes(ArrayList<String> ExpectedRooms,ArrayList<String> ActualRooms)
//{
//	boolean Status = true;
//	if(ExpectedRooms.size() == ActualRooms.size())
//	{
//	  for (String room : ExpectedRooms) {
//	  if(!ActualRooms.contains(room))
//		  Status = false;
//	      }
//	  return Status;
//	}
//	else 
//	return false;
//	
//}
//
//public boolean checkRates(String ExpectedRate,String ActualRate)
//{
//	if(ExpectedRate.replace(".0", "").equalsIgnoreCase(ActualRate.trim()))
//		return true;
//		else
//		return false;
//	
//}
//
//public boolean checkDefault(String ExpectedRoom,String ActualRoom)
//{
//	if(ExpectedRoom.equalsIgnoreCase(ActualRoom.trim()))
//	return true;
//	else
//	return false;
//}
// 
//
//}
