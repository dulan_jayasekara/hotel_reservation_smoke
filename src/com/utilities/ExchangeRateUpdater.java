package com.utilities;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


import app.support.MethodClass;

public class ExchangeRateUpdater {
	
private HashMap<String, String>  CurrencyMap = new HashMap<String, String>();
private WebDriver                driver;
private boolean                 Validator   = true;
private int                      Count       = 0;


public Map<String, String> getExchangeRates()throws IOException
{   
	HashMap<String, String> properties = ReadProperties.readpropeties();
	MethodClass mtclass                = new MethodClass(properties);
	driver                             = mtclass.initalizeDriver();
	
    mtclass.login(driver);
    driver.get(properties.get("Portal.Url").concat("/admin/setup/CurrencyExchangeRateSetupPage.do"));
    

    for(int count = 0;Validator;count++)
    {
    	try {
    		
    		String Key   = driver.findElement(By.name("currencyCode["+count+"]")).getAttribute("value");
    		String Value = driver.findElement(By.name("exchangeRate["+count+"]")).getAttribute("value");
    		CurrencyMap.put(Key, Value);
    	
			
		} catch (Exception e) {
			
			Validator = false;
			Count     = count;
			
		}
    		
   
    	
    }
    
	System.out.println(Count);
	driver.quit();
	
	return CurrencyMap;
}




}
