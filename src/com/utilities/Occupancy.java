package com.utilities;

public class Occupancy {

private String  HotelName          = "";
private String  Title              = "";
private String  RoomLable          = "";
private String  FirstName          = "";
private String  LastName           = "";
private boolean isSmoking          = false;
private boolean isHandicapped      = false;
private boolean isWheelChairNeeded = false;


public String getTitle() {
	return Title;
}
public void setTitle(String title) {
	Title = title;
}
public String getHotelName() {
	return HotelName;
}
public void setHotelName(String hotelName) {
	HotelName = hotelName;
}
public String getRoomLable() {
	return RoomLable;
}
public void setRoomLable(String roomLable) {
	RoomLable = roomLable;
}
public String getFirstName() {
	return FirstName;
}
public void setFirstName(String firstName) {
	FirstName = firstName;
}
public String getLastName() {
	return LastName;
}
public void setLastName(String lastName) {
	LastName = lastName;
}
public boolean isSmoking() {
	return isSmoking;
}
public void setSmoking(boolean isSmoking) {
	this.isSmoking = isSmoking;
}
public boolean isHandicapped() {
	return isHandicapped;
}
public void setHandicapped(boolean isHandicapped) {
	this.isHandicapped = isHandicapped;
}
public boolean isWheelChairNeeded() {
	return isWheelChairNeeded;
}
public void setWheelChairNeeded(boolean isWheelChairNeeded) {
	this.isWheelChairNeeded = isWheelChairNeeded;
}




}
