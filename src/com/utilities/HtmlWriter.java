package com.utilities;

/*
 * By Dulan
 * Modified Date : 2013.03.11
 * 
 */
import java.io.*; 
public class HtmlWriter
{
	public static PrintStream orgStream;
	
 
 public void  Writetofile(String path,boolean flag)
       {
		orgStream 	= null;
		PrintStream fileStream 	= null;
		try
		{
			
	    	orgStream = System.out;
			fileStream = new PrintStream(new FileOutputStream(path,flag));
			System.setOut(fileStream);
			// Redirecting runtime exceptions to file
			System.setErr(fileStream);
			throw new Exception("Test Exception");
 
		}
		catch (FileNotFoundException fnfEx)
		{
			//System.out.println("Error in IO Redirection");
			fnfEx.printStackTrace();
		}
		catch (Exception ex)
		{
			//Gets printed in the file
			System.out.println();
			//ex.printStackTrace();
		}
		finally
		{
			//Restoring back to console
			//System.setOut(orgStream);
			//Gets printed in the console
			//System.out.println("Redirecting file output back to console");
 
		}
 
	}
}