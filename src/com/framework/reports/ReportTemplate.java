package com.framework.reports;

import java.util.HashMap;

import org.openqa.jetty.html.Break;

import com.reservation.ReservationScenarioRunner;

public class ReportTemplate {
	private StringBuffer    PrintWriter           = null;
	private static int      TestCaseCount;
	private HashMap<String, String>   PropertySet = null;
	    
	public void Initialize(StringBuffer printer,HashMap<String, String> properties)
	     {
	      	 this.PrintWriter = printer;
	      	 TestCaseCount    = ReservationScenarioRunner.TestCaseCount;
	      	 PropertySet      = properties;
	     }
	
	      private static class SingletonHolder { 
		    private static final ReportTemplate INSTANCE = new ReportTemplate();
		  }

		  public static ReportTemplate getInstance() {
		    return SingletonHolder.INSTANCE;
		  }
		  
		  
		  public void verifyTrue(String Expected,String Actual,String Message)
		  {
			  
		  	PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>"+Message+"</td>");
		  	TestCaseCount++;
		  	PrintWriter.append("<td>"+Expected+"</td>");
		  	PrintWriter.append("<td>"+Actual +"</td>");
		  	try {
		  		
		  		if(Actual.trim().equalsIgnoreCase(Expected.trim()))
		  			PrintWriter.append("<td class='Passed'>PASS</td>");
		  	    else
		  	        PrintWriter.append("<td class='Failed'>FAIL</td>");
		    
		          PrintWriter.append("<td></td></tr>");
		  	} catch (Exception e) {
		  		PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		  	}
		  }

		  public void verifyPerfomance(double Expected,double Actual,String Message)
		  {
		  	PrintWriter.append("<tr><td>"+Message+"</td>");
		  	PrintWriter.append("<td>"+Expected+"</td>");
		  	PrintWriter.append("<td>"+Actual +"</td>");
		  	try {
		  		
		  		if(Actual <= Expected)
		  			PrintWriter.append("<td class='Passed'>PASS</td>");
		  	    else
		  	        PrintWriter.append("<td class='Failed'>FAIL</td>");
		    
		          PrintWriter.append("<td></td></tr>");
		  	} catch (Exception e) {
		  		PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		  	}
		  }

		  public void verifyContains(String Expected,String Actual,String Message)
		  {
		  	PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>"+Message+"</td>");
		  	TestCaseCount++;
		  	PrintWriter.append("<td>"+Expected+"</td>");
		  	PrintWriter.append("<td>"+Actual +"</td>");
		  	try {
		  		
		  		if((Actual.trim().contains(Expected.trim()))||(Expected.trim().contains(Actual.trim())))
		  			PrintWriter.append("<td class='Passed'>PASS</td>");
		  	    else
		  	        PrintWriter.append("<td class='Failed'>FAIL</td>");
		    
		          PrintWriter.append("<td></td></tr>");
		  	} catch (Exception e) {
		  		PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		  	}
		  }

		  public void verifyTrueValue(String Expected,String Actual,String Message)
		  {
			if(!Expected.equalsIgnoreCase("N/A")){
			        Double AdjustedValue        = 0.0;
				  	try {
				  		AdjustedValue           = Double.parseDouble(PropertySet.get("Compared.Value.Range").trim());
				  	} catch (Exception e) {
				  		
				  	}
				  	
				  	Double ExpectedRate          = Math.ceil(Double.parseDouble(Expected));
				  	Double FloorExpectedRate     = Math.floor(Double.parseDouble(Expected));
				  	Double AdjustedCeil          = 0.00 ;
				  	Double AdjustedFloor         = 0.00 ;
				  	
				  	if(ExpectedRate != 0.00){
				  	AdjustedCeil          = ExpectedRate      + AdjustedValue ;
					AdjustedFloor         = FloorExpectedRate - AdjustedValue ;
					}
				  	
				  	String ExpectedRateString   = Double.toString(ExpectedRate).replace(".0", "");
				      String F_ExpectedRateString = Double.toString(FloorExpectedRate).replace(".0", "");
				      String Adj_ExpectedString   = Double.toString(AdjustedCeil).replace(".0", "");
				      String Adj_F_ExpectedString = Double.toString(AdjustedFloor).replace(".0", "");
				      Double ActualVal            = Double.parseDouble(Actual.replace(",", "")); 
				  	
				  	
				  	
				  	PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>"+Message+"</td>");
				  	TestCaseCount++;
				  	
				  	if(PropertySet.get("IsCompared.Value.Range").trim().equalsIgnoreCase("true"))
				  	{
				  	PrintWriter.append("<td>"+Adj_F_ExpectedString+" to "+Adj_ExpectedString+"</td>");
				  	PrintWriter.append("<td>"+Actual +"</td>");
				  	try {
				  		if((ActualVal >= AdjustedFloor) && (ActualVal <= AdjustedCeil))
				  		PrintWriter.append("<td class='Passed'>PASS</td>");
				  	    else
				  	    PrintWriter.append("<td class='Failed'>FAIL</td>");
				          PrintWriter.append("<td></td></tr>");
				  	} catch (Exception e) {
				  		PrintWriter.append("<td>"+e.toString()+"</td></tr>");
				  	}
				  	}
				  	else
				  		{
				  		PrintWriter.append("<td>"+ExpectedRateString+" Or "+F_ExpectedRateString+"</td>");
				  		PrintWriter.append("<td>"+Actual +"</td>");
				  		try {
				  			if((ExpectedRateString.equalsIgnoreCase(Actual.replace(",", ""))) || (F_ExpectedRateString.equalsIgnoreCase(Actual.replace(",", ""))))
				  			PrintWriter.append("<td class='Passed'>PASS</td>");
				  		    else
				  		    PrintWriter.append("<td class='Failed'>FAIL</td>");
				  	        PrintWriter.append("<td></td></tr>");
				  		} catch (Exception e) {
				  			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
				  		}
				  	  }
				  	
				
			}else{
				verifyTrue(Expected, Actual, Message);
			}
}

		  public void verifyTrue(double Expected,double Actual,String Message)
		  {
		  	PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>"+Message+"</td>");
		  	TestCaseCount++;
		  	PrintWriter.append("<td>"+Expected+"</td>");
		  	PrintWriter.append("<td>"+Actual +"</td>");
		  	try {
		  		if(Actual == Expected)
		  			PrintWriter.append("<td class='Passed'>PASS</td>");
		  	    else
		  	        PrintWriter.append("<td class='Failed'>FAIL</td>");
		    
		          PrintWriter.append("<td></td></tr>");
		  	} catch (Exception e) {
		  		PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		  	}
		  }

		  public void verifyTrue(boolean Expected,boolean Actual,String Message)
		  {
		  	PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>"+Message+"</td>");
		  	TestCaseCount++;
		  	PrintWriter.append("<td>"+Expected+"</td>");
		  	PrintWriter.append("<td>"+Actual +"</td>");
		  	try {
		  		if(Actual == Expected)
		  			PrintWriter.append("<td class='Passed'>PASS</td>");
		  	    else
		  	        PrintWriter.append("<td class='Failed'>FAIL</td>");
		    
		          PrintWriter.append("<td></td></tr>");
		  	} catch (Exception e) {
		  		PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		  	}
		  }

		  
		  public void setTableHeading(String TableHeader)
		  {
			  PrintWriter.append("<br>");
			  PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">"+TableHeader+"</p><br>");
			  PrintWriter.append("<table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");
			  
		  }
		  
		  public void markTableEnd()
		  {
			  
			  PrintWriter.append("</table>");
			  
		  }

}
