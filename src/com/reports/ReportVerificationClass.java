package com.reports;

import java.util.Calendar;

import org.apache.log4j.Logger;

import com.framework.reports.ReportTemplate;
import com.reservation.ReservationScenarioRunner;
import com.types.ChargeByType;
import com.types.HotelInventoryType;
import com.types.RateContractByType;
import com.utilities.Calender;

import Hotel.Utilities.Hotel;
import Hotel.Utilities.HotelContract;
import app.support.CartDetails;
import app.support.ConfirmationPage;
import app.support.DataLoader;
import app.support.SearchType;

public class ReportVerificationClass {

	private SearchType CurrentSearch;
	private ConfirmationPage Conf;
	private Logger logger;
	private ReportTemplate ReportTemp;
	private String BookingDate;
	private Hotel BookedHotel;
	private CartDetails CartDetails;

	public ReportVerificationClass(SearchType CurrentSearch, ConfirmationPage Conf) {

		logger = Logger.getLogger(this.getClass());
		logger.info("ReportVericationClass Constructor Called--->");

		try {
			this.CurrentSearch = CurrentSearch;
			this.Conf = Conf;
			ReportTemp = ReportTemplate.getInstance();
			BookingDate = Calender.getDate(Calendar.MONTH, 0, "dd-MMM-yyyy");
			BookedHotel = CurrentSearch.getValidationHotel();
			CartDetails = CurrentSearch.getCart();
			logger.info("ReportVericationClass Initialized Successfully");
		} catch (Exception e) {
			logger.fatal("ReportVericationClass Initialization Failed >" + e.toString());
		}

	}

	public void verifyReservationReport(ReservationReport resreport, DataLoader Loader) {

		double TaxInBaseCurre = CartDetails.getTotalHotelTaxesinBase();
		double NetRateInHotelCurr = CartDetails.getNetRate();
		double CCFeeinSeling = Conf.getCon_ON_CCFee();
		double TotalCostInBaseCurr = 0.00;
		double TotalCostInSellCurr = 0.00;
		double TotalRate = 0.00;

		if (BookedHotel.getRateContract() == RateContractByType.COMMISSIONABLE) {

			ReservationScenarioRunner allotment = new ReservationScenarioRunner();
			HotelContract contract = allotment.getApplicableContract(BookedHotel.getHotelContracts(), CurrentSearch.getArrivalDate());
			String CommVal = contract.getCommvalue();
			Double Commission = 0.0;
			;

			if (contract.getCommType() == ChargeByType.PERCENTAGE)
				Commission = NetRateInHotelCurr * (Double.parseDouble(CommVal) / 100);
			else if ((contract.getCommType() == ChargeByType.VALUE))
				Commission = Double.parseDouble(CommVal) * Integer.parseInt(CurrentSearch.getRooms()) * Integer.parseInt(CurrentSearch.getNights());

			TotalCostInBaseCurr = TaxInBaseCurre + (Loader.convertCurrency(Double.toString(NetRateInHotelCurr - Commission), BookedHotel.getHotelCurrency(), CurrentSearch.getBaseCurrency()))
					+ (Loader.convertCurrency(Double.toString(CCFeeinSeling), CurrentSearch.getSearchCurrency(), CurrentSearch.getBaseCurrency()));

		} else if (BookedHotel.getRateContract() == RateContractByType.NETRATE) {
			TotalCostInBaseCurr = TaxInBaseCurre + (Loader.convertCurrency(Double.toString(NetRateInHotelCurr), BookedHotel.getHotelCurrency(), CurrentSearch.getBaseCurrency()))
					+ (Loader.convertCurrency(Double.toString(CCFeeinSeling), CurrentSearch.getSearchCurrency(), CurrentSearch.getBaseCurrency()));

		}
		TotalCostInSellCurr = Loader.convertCurrency(Double.toString(TotalCostInBaseCurr), CurrentSearch.getBaseCurrency(), CurrentSearch.getSearchCurrency());
		TotalRate = Double.parseDouble(Conf.getCon_TotalBookingValue()) - CCFeeinSeling - CartDetails.getCalculatedServiceFeeInSelling();

		ReportTemp.setTableHeading("Reservation Report - Report View Validation");
		ReportTemp.verifyTrue(true, resreport.isReportLoaded(), "Reservation Report Loading Function");

		try {

			// ************* Validating ReservatioView Details ******************//
			if (resreport.isReportLoaded()) {
				ReportTemp.verifyTrue(true, resreport.isRecordAvailable(), "Reservation Report Loading Function");

				if (resreport.isRecordAvailable()) {

					ReportTemp.verifyContains(Conf.getBookingNumber(), resreport.getBookingNumber(), "Reservation Report Booking Number Validation");
					ReportTemp.verifyTrue(BookingDate, resreport.getBookingDate(), "Reservation Report Booking Date Validation");
					ReportTemp.verifyContains(Conf.getCon_CandellationDeadLine(), resreport.getConfDueDate(), "Reservation Report Confirmation Due Validation");
					ReportTemp.verifyTrue("DC", resreport.getCustomerType(), "Reservation Report customer type");
					ReportTemp.verifyTrue(Conf.getCon_Customer_Lname() + " " + Conf.getCon_Customer_Fname(), resreport.getCustomerName(), "Reservation Report Customer Name Validation");
					ReportTemp.verifyTrue(BookedHotel.getSupplier(), resreport.getSupplierName(), "Reservation Report Supplier Name");
					ReportTemp.verifyTrue("Hotels", resreport.getProductType(), "Reservation Report Product Type");
					ReportTemp.verifyTrue("Normal", resreport.getBookingStatus(), "Reservation Report Booking Status Validation");
					ReportTemp.verifyTrue(CurrentSearch.getBookingChannel(), resreport.getChannel(), "Reservation Report Booking Channel Validation");
					ReportTemp.verifyTrue("AutomationUser(Madhusha/gamage)", resreport.getConsultantName(), "Reservation Report Consultant Name");
					ReportTemp.verifyTrue("ROneAdultoneLast ROneAdultoneFirst", resreport.getLastName() + "" + resreport.getFirtName(), "Reservation Report Guest Name Validation");
					ReportTemp.verifyTrue(Conf.getCon_Customer_Tel(), resreport.getCustContactNumber(), "Reservation Report Customer Contact Number");
					ReportTemp.verifyTrue("N", resreport.getLpoReq(), "Reservation Report Lpo Required Status");
					ReportTemp.verifyTrue("N", resreport.getLpoStatus(), "Reservation Report Lpo Status Validation");
					ReportTemp.verifyTrue("-", resreport.getLpoNumber(), "Reservation Report Lpo Number Validation");
					ReportTemp.verifyTrue(CurrentSearch.getSearchCurrency().trim(), resreport.getSellingcurrency1(), "Reservation Report Selling Currency One Validation");
					ReportTemp.verifyTrue(CurrentSearch.getSearchCurrency().trim(), resreport.getSellingcurrency2(), "Reservation Report Selling Currency two Validation");
					ReportTemp.verifyTrue(CurrentSearch.getBaseCurrency(), resreport.getBaseCurrency(), "Reservation Report Base Currency Validation");
					ReportTemp.verifyTrueValue(Double.toString(TotalRate), resreport.getSel_totalRate(), "Reservation Report Total Rate Validation");
					ReportTemp.verifyTrueValue(Double.toString(CartDetails.getCalculatedServiceFeeInSelling()), resreport.getSel_bookingFee(), "Reservation Report Booking Fee And Charges Validation");
					ReportTemp.verifyTrueValue(Conf.getCon_TotalBookingValue(), resreport.getSel_GrossOrderVal(), "Reservation Report Gross Order Value Validation");
					// TODO:Agent Commission and net order value calculation has to be changed once agent flow implemented
					ReportTemp.verifyTrue("0.00", resreport.getSel_AgentComm(), "Reservation Report Agent Commission Validation");
					ReportTemp.verifyTrueValue(Conf.getCon_TotalBookingValue(), resreport.getSel_NetOrderVal(), "Reservation Report Net Order Value Validation-Selling Currency");

					if (CurrentSearch.getPaymentType().equalsIgnoreCase("ONLINE")) {
						ReportTemp.verifyTrue(BookingDate, resreport.getInvoiceIssueDate(), "Reservation Report Invoice Issue Date Validation");
						ReportTemp.verifyTrueValue(Double.toString(CCFeeinSeling), resreport.getSel_CCFee(), "Reservation Report Credit Card Fee");
						ReportTemp.verifyTrueValue(Conf.getCon_AmountbeingProcessedNow(), resreport.getSel_AmountPaid(), "Reservation Report Amount Paid Validation");
					} else {
						ReportTemp.verifyTrue("-", resreport.getInvoiceIssueDate(), "Reservation Report Invoice Issue Date Validation");
						ReportTemp.verifyTrue("0.00", resreport.getSel_CCFee(), "Reservation Report Credit Card Fee");
						ReportTemp.verifyTrue("0.00", resreport.getSel_AmountPaid(), "Reservation Report Amount Paid Validation");
					}
					ReportTemp.verifyTrueValue(Double.toString(TotalCostInSellCurr), resreport.getSel_TotalCost(), "Reservation Report Total Cost - Selling Currency- Validation");
					ReportTemp.verifyTrueValue(Double.toString(Loader.convertCurrency(Conf.getCon_TotalBookingValue(), CurrentSearch.getSearchCurrency(), CurrentSearch.getBaseCurrency())), resreport.getBase_NetOrderVal(),
							"Reservation Report NetOrder Value - Base Currency- Validation");
					ReportTemp.verifyTrueValue(Double.toString(TotalCostInBaseCurr), resreport.getBase_TotalCost(), "Reservation Report Total Cost - Base Currency- Validation");
					ReportTemp.verifyTrueValue(Double.toString(Loader.convertCurrency(Conf.getCon_TotalBookingValue(), CurrentSearch.getSearchCurrency(), CurrentSearch.getBaseCurrency())), resreport.getPageNetTotal(),
							"Reservation Report PageTotal Net - Base Currency- Validation");
					ReportTemp.verifyTrueValue(Double.toString(TotalCostInBaseCurr), resreport.getPageCostTotal(), "Reservation Report PageTotal Cost - Base Currency- Validation");
					ReportTemp.verifyTrueValue(Double.toString(Loader.convertCurrency(Conf.getCon_TotalBookingValue(), CurrentSearch.getSearchCurrency(), CurrentSearch.getBaseCurrency())), resreport.getGndNetTotal(),
							"Reservation Report Grand Total Net - Base Currency- Validation");
					ReportTemp.verifyTrueValue(Double.toString(TotalCostInBaseCurr), resreport.getGndCostTotal(), "Reservation Report Grand Total Cost -Base Currency- Validation");

					if (resreport.isEditframeLoaded()) {
						ReportTemp.verifyTrue(true, true, "Reservation Report Edit Frame Loading func");
						ReportTemp.verifyTrue(CurrentSearch.getPay_CustomerNote(), resreport.getCustomerNote(), "Reservation Report Edit Frame- Customer Note");
						ReportTemp.verifyTrue(CurrentSearch.getPay_InternalNote(), resreport.getInternalNote(), "Reservation Report Edit Frame- Internal Note");
						ReportTemp.verifyContains(Conf.getCon_CandellationDeadLine(), resreport.getAutoLoadedDueDate(), "Reservation Report Edit Frame-Auto loaded due date");
						// ReportTemp.verifyTrue(true,true,"Reservation Report Edit Frame Loading func");
					} else {
						ReportTemp.verifyTrue(true, false, "Reservation Report Edit Frame Loading func");
					}

				}

			}

		} catch (Exception e) {
			logger.fatal("Error Occured in reservation report- Report view  data validation ->" + e.toString());
		}
		ReportTemp.markTableEnd();

		ReportTemp.setTableHeading("Reservation Report - Product View Validation");

		try {

			// ************* Validating ProductView Details ******************//
			if (resreport.isReportLoaded()) {
				ReportTemp.verifyTrue(true, resreport.isProd_View_Rec_Avail(), "Reservation Report - Hotel View - Record Availability");

				if (resreport.isProd_View_Rec_Avail()) {

					ReportTemp.verifyContains(Conf.getBookingNumber(), resreport.getProd_View_BookingNumber(), "Reservation Report Product View - Booking Number Validation");
					ReportTemp.verifyTrue(BookingDate, resreport.getProd_View_BookingDate(), "Reservation Report Product View - Booking Date Validation");
					ReportTemp.verifyTrue(CurrentSearch.getPay_InternalNote(), resreport.getProd_View_Notes(), "Reservation Report -Product View-Notes ");
					ReportTemp.verifyTrue(BookedHotel.getSupplier(), resreport.getProd_View_SupplierName(), "Reservation Report-Product View - Supplier Name");
					ReportTemp.verifyTrue(BookedHotel.getHotelName(), resreport.getProd_View_HotelName(), "Reservation Report-Product View - Hotel Name");

					if (!(BookedHotel.getHotelContracts().get(0).getInventoryType() == HotelInventoryType.ONREQUEST))
						ReportTemp.verifyTrue("Confirmed", resreport.getProd_View_setBookingType(), "Reservation Report-Product View - Booking Type");
					else
						ReportTemp.verifyTrue("OnRequest", resreport.getProd_View_setBookingType(), "Reservation Report-Product View - Booking Type");

					ReportTemp.verifyTrue(CurrentSearch.getBookingChannel(), resreport.getProd_View_Channel(), "Reservation Report - Product View - Booking Channel Validation");
					ReportTemp.verifyTrue("ROneAdultoneFirst ROneAdultoneLast", resreport.getProd_View_FirtName() + " " + resreport.getProd_View_LastName(), "Reservation Report - Product View - First Name/Last Name");
					ReportTemp.verifyTrue("DC", resreport.getProd_View_CustomerType(), "Reservation Report - Product View - customer type");
					ReportTemp.verifyTrue(ReservationScenarioRunner.getDateInFormat("MM/dd/yyyy", "dd-MMM-yyyy", CurrentSearch.getArrivalDate()), resreport.getProd_View_CheckIn(), "Reservation Report - Product View - Check in");
					ReportTemp.verifyTrue(ReservationScenarioRunner.getDateInFormat("MM/dd/yyyy", "dd-MMM-yyyy", CurrentSearch.getCheckOutDate()), resreport.getProd_View_CheckOut(), "Reservation Report - Product View - Check Out");
					ReportTemp.verifyTrue(Conf.getCon_Customer_Tel(), resreport.getProd_View_HotelContactNumber(), "Reservation Report - Product View - Customer Contact Number ");
					ReportTemp.verifyTrue(CurrentSearch.getSearchCurrency().trim(), resreport.getProd_View_Currency(), "Reservation Report - Product View - Selling Currency One Validation");
					ReportTemp.verifyTrueValue(Double.toString(TotalCostInSellCurr), resreport.getProd_View_TotalCost(), "Reservation Report Total Cost in Selling Currency- Validation");
					ReportTemp.verifyTrueValue(Double.toString(CartDetails.getCalculatedServiceFeeInSelling()), resreport.getProd_View_BookingFee(), "Reservation Report - Product View - Booking Fee And Charges Validation");
					ReportTemp.verifyTrueValue(Conf.getCon_TotalBookingValue(), resreport.getProd_View_OrderValue(), "Reservation Report - Product View - Order Value Validation");
					ReportTemp.verifyTrue(CurrentSearch.getRooms(), resreport.getProd_View_NumOfRooms(), "Reservation Report - Product View - Number of rooms Validation");
					ReportTemp.verifyTrue(CurrentSearch.getNights(), resreport.getProd_View_NumOfNyts(), "Reservation Report - Product View - Number of Nights Validation");
					ReportTemp.verifyTrueValue(Double.toString(Loader.convertCurrency(Conf.getCon_TotalBookingValue(), CurrentSearch.getSearchCurrency(), CurrentSearch.getBaseCurrency())), resreport.getProd_View_OrderValueFinal(),
							"Reservation Report - Product View -Order Value - Base Currency- Validation");
					ReportTemp.verifyTrueValue(Double.toString(TotalCostInBaseCurr), resreport.getProd_View_TotalCostFinal(), "Reservation Report  - Product View -Total Cost - Base Currency- Validation");
				}

			}

		} catch (Exception e) {
			logger.fatal("Error Occured in reservation report- Product view  data validation ->" + e.toString());
		}
		ReportTemp.markTableEnd();
	}

	public void verifyProfitAndLossReport(profitAndLostReport Profreport, DataLoader Loader) {

		ReportTemp.setTableHeading("Profit And Loass Report - Report View Validation");
		ReportTemp.verifyTrue(true, Profreport.isReportLoaded(), "Profit And Loss Report Loading Function");

		try {

			// ************* Validating ReservatioView Details ******************//
			if (Profreport.isReportLoaded()) {
				ReportTemp.verifyTrue(true, Profreport.isRecordAvailable(), "Profit And Loss Report Record Availability Function");

				if (Profreport.isRecordAvailable()) {

					double TaxInBaseCurre = CartDetails.getTotalHotelTaxesinBase();
					double NetRateInHotelCurr = CartDetails.getNetRate();
					double CCFeeinSeling = Conf.getCon_ON_CCFee();
					double TotalCostInBaseCurr = 0.00;
					//
					if (BookedHotel.getRateContract() == RateContractByType.COMMISSIONABLE) {

						ReservationScenarioRunner allotment = new ReservationScenarioRunner();
						HotelContract contract = allotment.getApplicableContract(BookedHotel.getHotelContracts(), CurrentSearch.getArrivalDate());
						String CommVal = contract.getCommvalue();
						Double Commission;

						if (contract.getCommType() == ChargeByType.PERCENTAGE)
							Commission = NetRateInHotelCurr * (Double.parseDouble(CommVal) / 100);
						else
							Commission = Double.parseDouble(CommVal) * Integer.parseInt(CurrentSearch.getRooms()) * Integer.parseInt(CurrentSearch.getNights());
						TotalCostInBaseCurr = TaxInBaseCurre + (Loader.convertCurrency(Double.toString(NetRateInHotelCurr - Commission), BookedHotel.getHotelCurrency(), CurrentSearch.getBaseCurrency()))
								+ (Loader.convertCurrency(Double.toString(CCFeeinSeling), CurrentSearch.getSearchCurrency(), CurrentSearch.getBaseCurrency()));

					} else if (BookedHotel.getRateContract() == RateContractByType.NETRATE) {
						TotalCostInBaseCurr = TaxInBaseCurre + (Loader.convertCurrency(Double.toString(NetRateInHotelCurr), BookedHotel.getHotelCurrency(), CurrentSearch.getBaseCurrency()))
								+ (Loader.convertCurrency(Double.toString(CCFeeinSeling), CurrentSearch.getSearchCurrency(), CurrentSearch.getBaseCurrency()));

					}
					//
					double GrossBookingVal = Loader.convertCurrency(Conf.getCon_TotalBookingValue(), CurrentSearch.getSearchCurrency(), CurrentSearch.getBaseCurrency());
					double AgentCommission = 0.00;
					double NetBookingValue = GrossBookingVal - AgentCommission;
					double Profit = NetBookingValue - TotalCostInBaseCurr;
					double ProfitPercentage = (Profit / GrossBookingVal) * 100;
					String ProfitString = String.format("%.2f", ProfitPercentage) + " %";
					double AmountPaid = 0.00;
					double AmountDue = 0.00;

					if (CurrentSearch.getPaymentType().equalsIgnoreCase("ONLINE"))
						AmountPaid = Loader.convertCurrency(Conf.getCon_AmountbeingProcessedNow(), CurrentSearch.getSearchCurrency(), CurrentSearch.getBaseCurrency());
					else
						AmountDue = Loader.convertCurrency(Conf.getCon_AmountbeingProcessedNow(), CurrentSearch.getSearchCurrency(), CurrentSearch.getBaseCurrency());

					ReportTemp.verifyContains(Conf.getBookingNumber(), Profreport.getBookingNumber(), "P&L Report Booking Number Validation");
					ReportTemp.verifyTrue(BookingDate, Profreport.getBookingDate(), "P&L Report Booking Date Validation");
					ReportTemp.verifyTrue("-", Profreport.getConsultantName(), "P&L Report Consultant Name Validation");
					ReportTemp.verifyTrue("Hotel", Profreport.getCombination(), "P&L Report Product Combination Validation");
					ReportTemp.verifyTrue("-", Profreport.getCommissionTemplate(), "P&L Report Commission temp Validation");
					ReportTemp.verifyContains((BookedHotel.getRateContract() == RateContractByType.NETRATE) ? "Net Rate" : "Commissionable", Profreport.getContractType(), "P&L Report Contract Type Validation");
					ReportTemp.verifyTrue("DC", Profreport.getBookingType(), "P&L Report Booking Type Validation");
					ReportTemp.verifyTrue(Conf.getCon_Customer_Fname() + " " + Conf.getCon_Customer_Lname(), Profreport.getAgentName(), "P&L Report Agent  Name Validation");
					ReportTemp.verifyTrue(CurrentSearch.getBookingChannel(), Profreport.getBookingChannel(), "P&L Report Booking Channel Validation");
					ReportTemp.verifyTrue(ReservationScenarioRunner.getDateInFormat("MM/dd/yyyy", "yyyy-MM-dd", CurrentSearch.getArrivalDate()), Profreport.getChechIn().split("/")[0], "P&L Report  - Check in");
					ReportTemp.verifyTrue(ReservationScenarioRunner.getDateInFormat("MM/dd/yyyy", "yyyy-MM-dd", CurrentSearch.getCheckOutDate()), Profreport.getChechIn().split("/")[1], "P&L Report  - Check Out");
					ReportTemp.verifyTrue(CurrentSearch.getNights(), Profreport.getNumOfNyts(), "P&L Report Number of nights Validation");
					ReportTemp.verifyTrue(CurrentSearch.getBaseCurrency(), Profreport.getBaseCurrency(), "P&L Report Base Currency Validation");

					ReportTemp.verifyTrueValue(Double.toString(GrossBookingVal), Profreport.getGrossBookingValue(), "P&L Report Gross Booking Value Validation");
					ReportTemp.verifyTrueValue(Double.toString(AgentCommission), Profreport.getAgentCommission(), "P&L Report Agent Commission Validation");
					ReportTemp.verifyTrueValue(Double.toString(NetBookingValue), Profreport.getNetBookingValues(), "P&L Report Net Booking Values  Validation");
					ReportTemp.verifyTrueValue(Double.toString(TotalCostInBaseCurr), Profreport.getCostOfSales(), "P&L Report Cost of Sales (Incl. Taxes)Validation");
					ReportTemp.verifyTrueValue(Double.toString(Profit), Profreport.getProfit(), "P&L Report Profit Validation");
					ReportTemp.verifyTrue(ProfitString, Profreport.getProfitPercentage(), "P&L Report Profit (%) Validation");
					ReportTemp.verifyTrueValue(Double.toString(Loader.convertCurrency(Conf.getCon_AmountbeingProcessedNow(), CurrentSearch.getSearchCurrency(), CurrentSearch.getBaseCurrency())), Profreport.getInvoiceTotal(),
							"P&L Report invoice total Validation");
					ReportTemp.verifyTrueValue(Double.toString(AmountPaid), Profreport.getInvoicrpaid(), "P&L Report invoice paid Validation");
					ReportTemp.verifyTrueValue(Double.toString(AmountDue), Profreport.getInvoiceBalanceDue(), "P&L Report invoice due balance Validation");

				}

			}

		} catch (Exception e) {
			logger.fatal("Error Occured in reservation report view  report validation ->" + e.toString());
		}
		ReportTemp.markTableEnd();

	}

}
