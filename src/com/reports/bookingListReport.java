package com.reports;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.utilities.Calender;

public class bookingListReport {
	
    private boolean reportLoaded            = false;
	private boolean recordAvailable         = false;
	private boolean bookingCardLoaded       = false;
	private boolean VoucherIssued           = false;
	private boolean InvoiceIssued           = false;
	private boolean PaymentRecieved         = false;
	private String resNumber =""; 
	private String resDate =""; 
	private String cusCountry =""; 
	private String cusCity =""; 
	private String resTime =""; 
	private String cusType =""; 
	private String cusName =""; 
	private String leadName =""; 
	private String firstElement =""; 
	private String deadLine =""; 
	private String city =""; 
	private String country =""; 
	private String product =""; 
	private String status =""; 
	private String supplier =""; 
	private String hotelName =""; 
	private String checkin =""; 
	private String checkOut =""; 
	private String numOfRooms =""; 
	private String sectorBookingValue =""; 
	private String currencyCode ="";
	private Logger logger;	
	private String reportUrl;
	private bookingcard bookingcrd;

	public bookingListReport(String url) {
	logger    = Logger.getLogger(this.getClass());
	reportUrl = url;
	}
	
	
	public bookingcard getBc() {
		return bookingcrd;
	}

	public String getCusCountry() {
		return cusCountry;
	}

	public void setCusCountry(String cusCountry) {
		this.cusCountry = cusCountry;
	}

	

	public boolean isVoucherIssued() {
		return VoucherIssued;
	}


	public void setVoucherIssued(boolean voucherIssued) {
		VoucherIssued = voucherIssued;
	}


	public boolean isInvoiceIssued() {
		return InvoiceIssued;
	}


	public void setInvoiceIssued(boolean invoiceIssued) {
		InvoiceIssued = invoiceIssued;
	}


	public boolean isPaymentRecieved() {
		return PaymentRecieved;
	}


	public void setPaymentRecieved(boolean paymentRecieved) {
		PaymentRecieved = paymentRecieved;
	}


	public boolean isReportLoaded() {
		return reportLoaded;
	}


	public void setReportLoaded(boolean reportLoaded) {
		this.reportLoaded = reportLoaded;
	}


	public boolean isRecordAvailable() {
		return recordAvailable;
	}


	public void setRecordAvailable(boolean recordAvailable) {
		this.recordAvailable = recordAvailable;
	}


	public boolean isBookingCardLoaded() {
		return bookingCardLoaded;
	}


	public void setBookingCardLoaded(boolean bookingCardLoaded) {
		this.bookingCardLoaded = bookingCardLoaded;
	}


	public String getCusCity() {
		return cusCity;
	}

	public void setCusCity(String cusCity) {
		this.cusCity = cusCity;
	}

	public void setBc(bookingcard bc) {
		bookingcrd = bc;
	}

	public String getResNumber() {
		return resNumber;
	}

	public void setResNumber(String resNumber) {
		this.resNumber = resNumber;
	}

	public String getResDate() {
		return resDate;
	}

	public void setResDate(String resDate) {
		this.resDate = resDate;
	}

	public String getResTime() {
		return resTime;
	}

	public void setResTime(String resTime) {
		this.resTime = resTime;
	}

	public String getCusType() {
		return cusType;
	}

	public void setCusType(String cusType) {
		this.cusType = cusType;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getLeadName() {
		return leadName;
	}

	public void setLeadName(String leadName) {
		this.leadName = leadName;
	}

	public String getFirstElement() {
		return firstElement;
	}

	public void setFirstElement(String firstElement) {
		this.firstElement = firstElement;
	}

	public String getDeadLine() {
		return deadLine;
	}

	public void setDeadLine(String deadLine) {
		this.deadLine = deadLine;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getCheckin() {
		return checkin;
	}

	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}

	public String getNumOfRooms() {
		return numOfRooms;
	}

	public void setNumOfRooms(String numOfRooms) {
		this.numOfRooms = numOfRooms;
	}

	public String getSectorBookingValue() {
		return sectorBookingValue;
	}

	public void setSectorBookingValue(String sectorBookingValue) {
		this.sectorBookingValue = sectorBookingValue;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	
	public void loadReport(WebDriver driver) throws IOException
	{
		logger.info("Navigating To Booking List Report ->"+reportUrl);
		driver.get(reportUrl);
		try {
			Alert alert = driver.switchTo().alert();
	        alert.accept();
	        logger.info("Alert Accepted");
	        driver.switchTo().defaultContent();
	        driver.switchTo().frame("reportIframe");
		    driver.get(reportUrl);
		    alert = driver.switchTo().alert();
	        alert.accept();
		} catch (Exception e) {
			logger.info("Alert Not Present");
		}
		WebDriverWait wait	 			= new WebDriverWait(driver, 40);
		driver.switchTo().defaultContent();
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("reportIframe"));
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reservationno")));
			this.reportLoaded = true;
			logger.info("Booking List Report Loaded Successfully");
		} catch (Exception e) {
			this.reportLoaded = false;
			logger.fatal("Booking List Report Not Loaded check the screenshots for more details "+e.toString());
			File Scr        = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(Scr, new File(Calender.getDate(Calendar.DATE,0)+"_Errors/ReservationRepNotLoaded.jpg"));
			
		}
	}

	public void getReservationRecord(WebDriver driver, String ReservationNumber) throws IOException
	{
     logger.info("<--Getting Booking List Report Record : "+ReservationNumber);	

     try {
		driver.findElement(By.id("reservationno")).clear();
		System.out.println(ReservationNumber);
		driver.findElement(By.id("reservationno")).sendKeys(ReservationNumber.trim());
		Thread.sleep(1000);
		driver.findElement(By.id("reservationno_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		driver.findElement(By.className("rowstyle0")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
		WebDriverWait wait	 			= new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[1]/a")));
		this.recordAvailable = true;
		logger.info("Reservation Record Available");	
	} catch (Exception e) {
		this.recordAvailable = false;
		logger.fatal("Reservation Record Not Available "+ e.toString());
		File Scr        = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(Scr, new File(Calender.getDate(Calendar.DATE,0)+"_Errors/ReservationRecordNotAvail.jpg"));
	}
	
	}
	
	public void readRecordDetails(WebDriver driver) throws IOException
	{
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		WebElement Reportrow              = driver.findElement(By.id("reportDataRows0"));
		ArrayList<WebElement>  tdList     = new ArrayList<WebElement>(Reportrow.findElements(By.tagName("td")));
		
		resNumber  		= tdList.get(0).findElement(By.tagName("a")).getText().trim();
		resDate    		= tdList.get(1).getText().trim();
		resTime    		= tdList.get(2).getText().trim();
		cusType    		= tdList.get(3).getText().trim();
		cusName    		= tdList.get(4).getText().trim();
		leadName        = tdList.get(5).getText().trim();
		firstElement    = tdList.get(6).getText().trim();
		deadLine        = tdList.get(7).getText().trim();
		product         = tdList.get(8).getText().trim();
		//            = tdList.get(8).getText().trim();
		city            = tdList.get(8).getText().trim();
		status          = tdList.get(9).getText().trim();
		VoucherIssued   = tdList.get(10).findElement(By.tagName("img")).getAttribute("src").contains("green");
		InvoiceIssued   = tdList.get(11).findElement(By.tagName("img")).getAttribute("src").contains("green");
		PaymentRecieved = tdList.get(12).findElement(By.tagName("img")).getAttribute("src").contains("green");
		
		
	}

	public void readBookingCard(WebDriver driver) throws IOException
	{
		WebDriverWait wait = new WebDriverWait(driver, 60);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[1]/a")).click();
		System.out.println("test booking list report -- 6");
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[1]/td[4]")));
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("test booking list report -- 7");
		bookingcard bc = new bookingcard();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("dialogwindow");
		bc.setReservationNumber				(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[1]/td[4]")).getText());
		bc.setOrigin						(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[2]/td[4]")).getText());
		bc.setDestination					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[3]/td[4]")).getText());
		bc.setProductsBooked				(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[4]/td[4]")).getText());
		bc.setBookingStatus					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[5]/td[4]")).getText());
		bc.setReservationDate				(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[1]/td[7]")).getText());
		bc.setFirstElement					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[2]/td[7]")).getText());
		bc.setCancellationDeadline			(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[3]/td[7]")).getText());
		bc.setCurrencyCode					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[7]/td[7]")).getText());
		bc.setSubTotal						(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[8]/td[7]")).getText());
		bc.setBookingFee					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[9]/td[7]")).getText());
		bc.setTaxAndOther					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[10]/td[7]")).getText());
		bc.setCreditCardFee					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[11]/td[7]")).getText());
		bc.setTotalBookingvalue				(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[12]/td[7]")).getText());
		bc.setPayableUpfront				(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[13]/td[7]")).getText());
		bc.setUtilization					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[14]/td[7]")).getText());
		bc.setAmountPaid					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[15]/td[7]")).getText());
		bc.setFirstName						(driver.findElement(By.xpath(".//*[@id='customerDetailsId']/table/tbody/tr/td/table/tbody/tr[1]/td[4]")).getText());
		bc.setAddress						(driver.findElement(By.xpath(".//*[@id='customerDetailsId']/table/tbody/tr/td/table/tbody/tr[2]/td[4]")).getText());
		bc.setEmail							(driver.findElement(By.xpath(".//*[@id='customerDetailsId']/table/tbody/tr/td/table/tbody/tr[3]/td[4]")).getText());
		bc.setCity							(driver.findElement(By.xpath(".//*[@id='customerDetailsId']/table/tbody/tr/td/table/tbody/tr[4]/td[4]")).getText());
		bc.setCountry						(driver.findElement(By.xpath(".//*[@id='customerDetailsId']/table/tbody/tr/td/table/tbody/tr[5]/td[4]")).getText());
		bc.setEmergencyContact				(driver.findElement(By.xpath(".//*[@id='customerDetailsId']/table/tbody/tr/td/table/tbody/tr[1]/td[7]")).getText());
		bc.setPhoneNumber					(driver.findElement(By.xpath(".//*[@id='customerDetailsId']/table/tbody/tr/td/table/tbody/tr[2]/td[7]")).getText());
		driver.findElement(By.xpath(".//*[@id='hotelDetailsimgTD']/img")).click();
		bc.setHotelName						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[2]/td[4]")).getText());
		bc.setHotelAddress					(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[3]/td[4]")).getText());
		bc.setCheckOut						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[4]/td[4]")).getText());
		bc.setSupplier						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[5]/td[4]")).getText());
		bc.setHotelBookingStatus			(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[2]/td[7]")).getText());
		bc.setCheckin						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[3]/td[7]")).getText());
		bc.setNumOfNyts						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[5]/td[7]")).getText());
		bc.setNoOfRooms						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[6]/td[7]")).getText());
		bc.setHotelContractType				(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[7]/td[7]")).getText());
		bc.sethCurrencyCode					(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[1]/td[4]")).getText());
		bc.setHsubTotal						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[2]/td[4]")).getText());
		bc.sethTax							(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[3]/td[4]")).getText());
		bc.setHtotalBookingValue			(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[4]/td[4]")).getText());
		bc.sethUpfront						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[5]/td[4]")).getText());
		bc.sethAmountpaid					(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[6]/td[4]")).getText());
		bc.sethDueAtCheckin					(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[7]/td[4]")).getText());
		System.out.println("test booking list report -- 8");
		ArrayList<bookingReportRoom> brrList = new ArrayList<bookingReportRoom>();
		for(int i = 2 ; i > 0  ; i++)
		{
			try {
				bookingReportRoom brr = new bookingReportRoom();
				brr.setRoomNumber				(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[2]/td/table/tbody/tr["+ i +"]/td[1]")).getText());
				String[] temp		 		= 			driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[2]/td/table/tbody/tr["+ i +"]/td[2]")).getText().split("/");
				brr.setRoomType					(temp[0]);
				brr.setBedType					(temp[1]);
				//System.out.println(temp[2]);
				brr.setRatePlan					(temp[2]);
				brr.setCusname					(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[2]/td/table/tbody/tr["+ i +"]/td[3]/table/tbody/tr/td[1]")).getText());
				brr.setAgeDroup					(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[2]/td/table/tbody/tr["+ i +"]/td[3]/table/tbody/tr/td[2]")).getText());
				brr.setChildAge					(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[2]/td/table/tbody/tr["+ i +"]/td[3]/table/tbody/tr/td[3]")).getText());
				brr.setTotalRate				(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[2]/td/table/tbody/tr["+ i+"]/td[4]")).getText());
				brrList.add(brr);
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
			
		}
		System.out.println("test booking list report -- 9");
		bc.setRoom(brrList);
		bookingcrd = bc;
		
	}

}
