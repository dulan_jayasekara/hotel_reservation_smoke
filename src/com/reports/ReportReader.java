package com.reports;
import java.io.IOException;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.utilities.ReadProperties;
import app.support.MethodClass;



public class ReportReader {
	
	private Logger logger;
	
	public ReportReader() {
		logger = Logger.getLogger(this.getClass());
	}
	
	public ReservationReport readReservationReport(String reportUrl, WebDriver driver,String ReservationNumber) throws IOException
	{
		ReservationReport report = new ReservationReport(reportUrl);
		//	logger.fatal("<--- Change Notes Method Called --->");
		report.loadReport(driver);
    	if(report.isReportLoaded()){
    	report.getReservationRecord(driver, ReservationNumber);
    	   
    	   if(report.isRecordAvailable()){
    	   report.readDetails(driver);
    	   report.readAditionalDetails(driver, ReservationNumber);
	       }
	
    	}
    	report.loadReport(driver);
    	if(report.isReportLoaded()){
        	report.getProductViewReservationRecord(driver, ReservationNumber);
        	if(report.isProd_View_Rec_Avail())
        	report.readProductViewDetails(driver);
        }
        	
    	
    	
    	return report;
	}
	
	
	public static void main(String[] args) throws IOException {
		DOMConfigurator.configure("log4j.xml");
		ReportReader            Reader     = new ReportReader();
		HashMap<String, String> properties = ReadProperties.readpropeties();
		MethodClass             mtclass    = new MethodClass(properties);
		WebDriver               driver     = mtclass.initalizeDriver();
		String                  ReportUrl  = properties.get("Portal.Url").concat("/reports/operational/mainReport.do?reportId=39&reportName=Booking List Report");
		mtclass.login(driver);
		
	/*	ReservationReport       RReport    = Reader.readReservationReport(ReportUrl, driver,"H13346C141014");
		System.out.println(RReport.getBookingDate());*/
		
		bookingListReport    bookingList = Reader.readBookingListReport(ReportUrl, driver, "H13858C041114");
	    System.out.println(bookingList.getLeadName());
	    
		
		//Reader.readProfitndLossReport(ReportUrl, driver, "F3879C241014");
		
	}
	
	
	public profitAndLostReport readProfitndLossReport(String reportUrl, WebDriver driver,String ReservationNumber) throws IOException
	{
		profitAndLostReport report = new profitAndLostReport(reportUrl);
		//	logger.fatal("<--- Change Notes Method Called --->");
		report.loadReport(driver);
    	if(report.isReportLoaded()){
    	report.getReservationRecord(driver, ReservationNumber);
    	
           if(report.isRecordAvailable()){
	        	
	    	WebElement myElement = driver.findElement(By.partialLinkText(ReservationNumber));
			WebElement parent     = myElement.findElement(By.xpath(".."));
			WebElement parent1    = parent.findElement(By.xpath(".."));
			System.out.println(parent.getAttribute("align"));
			System.out.println(parent1.getAttribute("class"));
			report.readDetails(driver, parent1);
			
	    	}
		}
        	
    	
    	
    	return report;
	}
	
	public bookingListReport readBookingListReport(String reportUrl, WebDriver driver,String ReservationNumber) throws IOException
	{
		logger.info("Read Booking List Method Called");
		bookingListReport report = new bookingListReport(reportUrl);
		//logger.fatal("<--- Change Notes Method Called --->");
		report.loadReport(driver);
    	if(report.isReportLoaded()){
           	report.getReservationRecord(driver, ReservationNumber);
    	     if(report.isRecordAvailable()){
             report.readRecordDetails(driver);
        	 report.readBookingCard(driver);
	    	}
		}
        	
    	
    	
    	return report;
	}
	

}
