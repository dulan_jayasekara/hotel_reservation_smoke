package com.reports;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.utilities.Calender;



public class ReservationReport {
	
   private boolean reportLoaded            = false;
   private boolean recordAvailable         = false;
   private boolean bookingCardLoaded       = false;
   private boolean editframeLoaded         = false;
   private boolean lpoProvided             = false;
   private boolean Prod_View_Rec_Avail     = false;
  
   private  String voucherIssued           = "";
   private  String InvoiceIssued           = "";
   private  String lpoReq                  = "";
   private  String LpoStatus               = "";
   private  String LpoNumber               = "";
   private  String InvoiceIssueDate        = "";
   private  String bookingNumber           = "";
   private	String documentNum             = "";
   private	String ConfDueDate             = "";
   private	String appCode                 = "";
   private	String bookingDate             = "";
   private	String SuppConfirmationNumber  = "";
   private	String ProductType             = "";
   private	String transId                 = "";
   private	String supplierName            = "";
   private	String bookingStatus           = "";
   private	String channel                 = "";
   private	String firtName                = "";
   private	String lastName                = "";
   private	String customerType            = "";
   private	String customerName            = "";
   private	String consultantName          = "";
   private	String CustContactNumber       = "";
   private	String Sellingcurrency1        = "";
   private	String Sellingcurrency2        = "";
   private	String Sel_totalRate           = "";
   private	String Sel_bookingFee          = "";
   private	String Sel_orderValue          = "";
   private	String Sel_CCFee               = "";
   private	String Sel_AmountPaid          = "";
   private	String Sel_GrossOrderVal       = "";
   private	String Sel_AgentComm           = "";
   private	String Sel_NetOrderVal         = "";
   private	String Sel_TotalCost           = "";
   private	String BaseCurrency            = "";
   private	String Base_NetOrderVal        = "";
   private	String Base_TotalCost          = "";
   private	String pageNetTotal            = "";
   private	String pageCostTotal           = "";
   private	String AutoLoadedDueDate       = "";
   private	String InternalNote            = "";
   private	String CustomerNote            = "";
   private	String reportUrl               = "";
   private	String GndNetTotal             = "";
   private	String GndCostTotal            = "";
   
   //=================================================================
   private	String Prod_View_BookingNumber			    =  "";
   private	String Prod_View_BookingDate			    =  "";
   private	String Prod_View_SupplierName			    =  "";
   private	String Prod_View_HotelName				    =  "";
   private	String Prod_View_HotelConfirmationNumber	=  "";
   private	String Prod_View_TransId					=  "";
   private	String Prod_View_setBookingType			    =  "";
   private	String Prod_View_Channel					=  "";
   private	String Prod_View_FirtName				    =  "";
   private	String Prod_View_LastName				    =  "";
   private	String Prod_View_CustomerType			    =  "";
   private	String Prod_View_CheckIn				    =  "";
   private	String Prod_View_CheckOut				    =  "";
   private	String Prod_View_RoomType				    =  "";
   private	String Prod_View_BedType					=  "";
   private	String Prod_View_RatePlan				    =  "";
   private	String Prod_View_HotelContactNumber		    =  "";
   private	String Prod_View_Currency					=  "";
   private	String Prod_View_TotalCost					=  "";
   private	String Prod_View_BookingFee				    =  "";
   private	String Prod_View_OrderValue				    =  "";

   private	String Prod_View_NumOfNyts				    =  "";
   private	String Prod_View_TotalCostFinal	    	    =  "";
   private	String Prod_View_OrderValueFinal	    	=  "";
   private	String Prod_View_NumOfRooms     	    	=  "";
   private	String Prod_View_Notes          	    	=  "";
	
   private  Logger logger                  = null;
	

	
	
	public String getProd_View_Notes() {
	return Prod_View_Notes;
}


	public String getGndNetTotal() {
	return GndNetTotal;
}


public String getGndCostTotal() {
	return GndCostTotal;
}


	public ReservationReport(String reportUrl) {
		logger           = Logger.getLogger(this.getClass());
		this.reportUrl   = reportUrl;
	}
	

	public boolean isProd_View_Rec_Avail() {
		return Prod_View_Rec_Avail;
	}


	public String getProd_View_NumOfRooms() {
		return Prod_View_NumOfRooms;
	}


	public String getProd_View_BookingNumber() {
		return Prod_View_BookingNumber;
	}


	public String getProd_View_BookingDate() {
		return Prod_View_BookingDate;
	}


	public String getProd_View_SupplierName() {
		return Prod_View_SupplierName;
	}


	public String getProd_View_HotelName() {
		return Prod_View_HotelName;
	}


	public String getProd_View_HotelConfirmationNumber() {
		return Prod_View_HotelConfirmationNumber;
	}


	public String getProd_View_TransId() {
		return Prod_View_TransId;
	}


	public String getProd_View_setBookingType() {
		return Prod_View_setBookingType;
	}


	public String getProd_View_Channel() {
		return Prod_View_Channel;
	}


	public String getProd_View_FirtName() {
		return Prod_View_FirtName;
	}


	public String getProd_View_LastName() {
		return Prod_View_LastName;
	}


	public String getProd_View_CustomerType() {
		return Prod_View_CustomerType;
	}


	public String getProd_View_CheckIn() {
		return Prod_View_CheckIn;
	}


	public String getProd_View_CheckOut() {
		return Prod_View_CheckOut;
	}


	public String getProd_View_RoomType() {
		return Prod_View_RoomType;
	}


	public String getProd_View_BedType() {
		return Prod_View_BedType;
	}


	public String getProd_View_RatePlan() {
		return Prod_View_RatePlan;
	}


	public String getProd_View_HotelContactNumber() {
		return Prod_View_HotelContactNumber;
	}


	public String getProd_View_Currency() {
		return Prod_View_Currency;
	}


	public String getProd_View_TotalCost() {
		return Prod_View_TotalCost;
	}


	public String getProd_View_BookingFee() {
		return Prod_View_BookingFee;
	}


	public String getProd_View_OrderValue() {
		return Prod_View_OrderValue;
	}

    public String getProd_View_NumOfNyts() {
		return Prod_View_NumOfNyts;
	}


	public String getProd_View_TotalCostFinal() {
		return Prod_View_TotalCostFinal;
	}


	public String getProd_View_OrderValueFinal() {
		return Prod_View_OrderValueFinal;
	}


	public String getVoucherIssued() {
		return voucherIssued;
	}


	public String getInvoiceIssued() {
		return InvoiceIssued;
	}


	public String getLpoReq() {
		return lpoReq;
	}


	public String getLpoStatus() {
		return LpoStatus;
	}


	public boolean isReportLoaded() {
		return reportLoaded;
	}



	public void setReportLoaded(boolean reportLoaded) {
		this.reportLoaded = reportLoaded;
	}



	public boolean isRecordAvailable() {
		return recordAvailable;
	}



	public void setRecordAvailable(boolean recordAvailable) {
		this.recordAvailable = recordAvailable;
	}



	public boolean isBookingCardLoaded() {
		return bookingCardLoaded;
	}



	public void setBookingCardLoaded(boolean bookingCardLoaded) {
		this.bookingCardLoaded = bookingCardLoaded;
	}



	public boolean isEditframeLoaded() {
		return editframeLoaded;
	}



	public void setEditframeLoaded(boolean editframeLoaded) {
		this.editframeLoaded = editframeLoaded;
	}



	public boolean isLpoProvided() {
		return lpoProvided;
	}



	public void setLpoProvided(boolean lpoProvided) {
		this.lpoProvided = lpoProvided;
	}



	public String getLpoNumber() {
		return LpoNumber;
	}



	public void setLpoNumber(String lpoNumber) {
		LpoNumber = lpoNumber;
	}



	public String getInvoiceIssueDate() {
		return InvoiceIssueDate;
	}



	public void setInvoiceIssueDate(String invoiceIssueDate) {
		InvoiceIssueDate = invoiceIssueDate;
	}



	public String getBookingNumber() {
		return bookingNumber;
	}



	public void setBookingNumber(String bookingNumber) {
		this.bookingNumber = bookingNumber;
	}



	public String getDocumentNum() {
		return documentNum;
	}



	public void setDocumentNum(String documentNum) {
		this.documentNum = documentNum;
	}



	public String getConfDueDate() {
		return ConfDueDate;
	}



	public void setConfDueDate(String confDueDate) {
		ConfDueDate = confDueDate;
	}



	public String getAppCode() {
		return appCode;
	}



	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}



	public String getBookingDate() {
		return bookingDate;
	}



	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}



	public String getSuppConfirmationNumber() {
		return SuppConfirmationNumber;
	}



	public void setSuppConfirmationNumber(String suppConfirmationNumber) {
		SuppConfirmationNumber = suppConfirmationNumber;
	}



	public String getProductType() {
		return ProductType;
	}



	public void setProductType(String productType) {
		ProductType = productType;
	}



	public String getTransId() {
		return transId;
	}



	public void setTransId(String transId) {
		this.transId = transId;
	}



	public String getSupplierName() {
		return supplierName;
	}



	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}



	public String getBookingStatus() {
		return bookingStatus;
	}



	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}



	public String getChannel() {
		return channel;
	}



	public void setChannel(String channel) {
		this.channel = channel;
	}



	public String getFirtName() {
		return firtName;
	}



	public void setFirtName(String firtName) {
		this.firtName = firtName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getCustomerType() {
		return customerType;
	}



	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}



	public String getCustomerName() {
		return customerName;
	}



	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}



	public String getConsultantName() {
		return consultantName;
	}



	public void setConsultantName(String consultantName) {
		this.consultantName = consultantName;
	}



	public String getCustContactNumber() {
		return CustContactNumber;
	}



	public void setCustContactNumber(String custContactNumber) {
		CustContactNumber = custContactNumber;
	}



	public String getSellingcurrency1() {
		return Sellingcurrency1;
	}



	public void setSellingcurrency1(String sellingcurrency1) {
		Sellingcurrency1 = sellingcurrency1;
	}



	public String getSellingcurrency2() {
		return Sellingcurrency2;
	}



	public void setSellingcurrency2(String sellingcurrency2) {
		Sellingcurrency2 = sellingcurrency2;
	}



	public String getSel_totalRate() {
		return Sel_totalRate;
	}


	public String getSel_bookingFee() {
		return Sel_bookingFee;
	}



	public void setSel_bookingFee(String sel_bookingFee) {
		Sel_bookingFee = sel_bookingFee;
	}



	public String getSel_orderValue() {
		return Sel_orderValue;
	}



	public void setSel_orderValue(String sel_orderValue) {
		Sel_orderValue = sel_orderValue;
	}



	public String getSel_CCFee() {
		return Sel_CCFee;
	}



	public void setSel_CCFee(String sel_CCFee) {
		Sel_CCFee = sel_CCFee;
	}



	public String getSel_AmountPaid() {
		return Sel_AmountPaid;
	}



	public void setSel_AmountPaid(String sel_AmountPaid) {
		Sel_AmountPaid = sel_AmountPaid;
	}



	public String getSel_GrossOrderVal() {
		return Sel_GrossOrderVal;
	}



	public void setSel_GrossOrderVal(String sel_GrossOrderVal) {
		Sel_GrossOrderVal = sel_GrossOrderVal;
	}



	public String getSel_AgentComm() {
		return Sel_AgentComm;
	}



	public void setSel_AgentComm(String sel_AgentComm) {
		Sel_AgentComm = sel_AgentComm;
	}



	public String getSel_NetOrderVal() {
		return Sel_NetOrderVal;
	}



	public void setSel_NetOrderVal(String sel_NetOrderVal) {
		Sel_NetOrderVal = sel_NetOrderVal;
	}



	public String getSel_TotalCost() {
		return Sel_TotalCost;
	}



	public void setSel_TotalCost(String sel_TotalCost) {
		Sel_TotalCost = sel_TotalCost;
	}



	public String getBaseCurrency() {
		return BaseCurrency;
	}



	public void setBaseCurrency(String baseCurrency) {
		BaseCurrency = baseCurrency;
	}



	public String getBase_NetOrderVal() {
		return Base_NetOrderVal;
	}



	public void setBase_NetOrderVal(String base_NetOrderVal) {
		Base_NetOrderVal = base_NetOrderVal;
	}



	public String getBase_TotalCost() {
		return Base_TotalCost;
	}



	public void setBase_TotalCost(String base_TotalCost) {
		Base_TotalCost = base_TotalCost;
	}



	public String getPageNetTotal() {
		return pageNetTotal;
	}


	public String getPageCostTotal() {
		return pageCostTotal;
	}


	public String getAutoLoadedDueDate() {
		return AutoLoadedDueDate;
	}



	public void setAutoLoadedDueDate(String autoLoadedDueDate) {
		AutoLoadedDueDate = autoLoadedDueDate;
	}



	public String getInternalNote() {
		return InternalNote;
	}



	public void setInternalNote(String internalNote) {
		InternalNote = internalNote;
	}



	public String getCustomerNote() {
		return CustomerNote;
	}



	public void setCustomerNote(String customerNote) {
		CustomerNote = customerNote;
	}



	public String getReportUrl() {
		return reportUrl;
	}



	public void setReportUrl(String reportUrl) {
		this.reportUrl = reportUrl;
	}



	public Logger getLogger() {
		return logger;
	}



	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	//=====================================================================//

	public void loadReport(WebDriver driver) throws IOException
	{
		logger.info("Navigating to Reservation Report ->"+reportUrl);
		driver.get(reportUrl);
		try {
			Alert alert = driver.switchTo().alert();
	        alert.accept();
	        logger.info("Alert Accepted");
	        driver.switchTo().defaultContent();
	        driver.switchTo().frame("reportIframe");
		    driver.get(reportUrl);
		    alert = driver.switchTo().alert();
	        alert.accept();
		} catch (Exception e) {
			logger.info("Alert Not Present");
		}
		WebDriverWait wait	 			= new WebDriverWait(driver, 40);
		driver.switchTo().defaultContent();
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("reportIframe"));
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("productType_hotels")));
			this.reportLoaded = true;
			logger.info("Report Loaded Successfully");
		} catch (Exception e) {
			this.reportLoaded = false;
			logger.fatal("Report Not Loaded check the screenshots for more details "+e.toString());
			File Scr        = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(Scr, new File(Calender.getDate(Calendar.DATE,0)+"_Errors/ReservationRepNotLoaded.jpg"));
			
		}
	}
	
	
	public void getReservationRecord(WebDriver driver, String ReservationNumber) throws IOException
	{
     logger.info("<--Getting Reservation Report Record : "+ReservationNumber);	
	//driver.findElement(By.id("productType_hotels")).click();
	driver.findElement(By.id("searchby_reservationno")).click();
	
    try {
	((JavascriptExecutor)driver).executeScript("showSearchParam('reservationnoRow');");
	

    } catch (Exception e) {
	// TODO: handle exception
    }
	
    try {
		driver.findElement(By.id("reservationno")).clear();
		System.out.println(ReservationNumber);
		driver.findElement(By.id("reservationno")).sendKeys(ReservationNumber);
		driver.findElement(By.id("reservationno_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		driver.findElement(By.className("rowstyle0")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
		WebDriverWait wait	 			= new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='reportHeaderRow1']/td[3]")));
		this.recordAvailable = true;
		logger.info("Reservation Record Available");	
	} catch (Exception e) {
		this.recordAvailable = false;
		logger.fatal("Reservation Record Not Available "+ e.toString());
		File Scr        = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(Scr, new File(Calender.getDate(Calendar.DATE,0)+"_Errors/ReservationRecordNotAvail.jpg"));
	}
	
	}
	
	public void getProductViewReservationRecord(WebDriver driver, String ReservationNumber) throws IOException
	{
     logger.info("<--Getting Product View Reservation Report Record : "+ReservationNumber);	
     driver.findElement(By.id("productType_hotels")).click();
	 driver.findElement(By.id("searchby_reservationno")).click();
	
    try {
	((JavascriptExecutor)driver).executeScript("showSearchParam('reservationnoRow');");
	

    } catch (Exception e) {
	// TODO: handle exception
    }
	
    try {
		driver.findElement(By.id("reservationno")).clear();
		System.out.println(ReservationNumber);
		driver.findElement(By.id("reservationno")).sendKeys(ReservationNumber);
		driver.findElement(By.id("reservationno_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		driver.findElement(By.className("rowstyle0")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
		WebDriverWait wait	 			= new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='reportHeaderRow1']/td[3]")));
		this.Prod_View_Rec_Avail = true;
		logger.info("Reservation Report Product View  Reservation Record Available");	
	} catch (Exception e) {
		this.Prod_View_Rec_Avail = false;
		logger.fatal("Product View -Reservation Record Not Available "+ e.toString());
		File Screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyDirectory(Screenshot, new File("ReservationReport_Record_"+new SimpleDateFormat("dd-MMM-yyyy").format(Calendar.getInstance().getTime())));
	}
	
	}
	
	
	public void readDetails(WebDriver driver)
	{
		logger.info("readDetails method called -->getting reservation record details");
		try {
			
			bookingNumber                     = driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[1]/a")).getText();
			bookingDate                       = driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[1]")).getText();
			documentNum                       = driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[1]")).getText();
			ConfDueDate                       = driver.findElement(By.xpath(".//*[@id='RowNo_1_5']/td[1]")).getText();
			customerType                      = driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[2]")).getText();
			customerName                      = driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[2]")).getText();
			supplierName                      = driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[2]")).getText();
			SuppConfirmationNumber            = driver.findElement(By.xpath(".//*[@id='RowNo_1_5']/td[2]")).getText();
			ProductType                       = driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[3]")).getText();
			bookingStatus                     = driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[3]")).getText();
			channel                           = driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[3]")).getText();
			consultantName                    = driver.findElement(By.xpath(".//*[@id='RowNo_1_5']/td[3]")).getText();
			
			String[] temp 		              = driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[4]")).getText().split(" ");
		
			try {
				firtName                          = temp[1];
				lastName                          = temp[0];
			} catch (Exception e) {
				firtName                          = "ERR";
				lastName                          = "ERR";
			}
			
			CustContactNumber                 = driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[4]")).getText();
			appCode                           = driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[4]")).getText();
			transId                           = driver.findElement(By.xpath(".//*[@id='RowNo_1_5']/td[4]")).getText();
			voucherIssued                     = driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[5]")).getText();
			InvoiceIssued                     = driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[5]")).getText();
			lpoReq                            = driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[6]")).getText();
			LpoStatus                         = driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[6]")).getText();
			LpoNumber                         = driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[6]")).getText();
            InvoiceIssueDate                  = driver.findElement(By.xpath(".//*[@id='RowNo_1_5']/td[5]")).getText();
            
            Sellingcurrency1                  = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[8]")).getText();
            Sel_totalRate                     = driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[7]")).getText();
            Sel_CCFee                         = driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[7]")).getText();
            Sel_bookingFee                    = driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[7]")).getText();
            Sel_AmountPaid                    = driver.findElement(By.xpath(".//*[@id='RowNo_1_5']/td[7]")).getText();
            Sellingcurrency2                  = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[9]")).getText();
            Sel_GrossOrderVal                 = driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[8]")).getText();
            Sel_AgentComm                     = driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[8]")).getText();
            Sel_NetOrderVal                   = driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[8]")).getText();
            Sel_TotalCost                     = driver.findElement(By.xpath(".//*[@id='RowNo_1_5']/td[8]")).getText();
            
            BaseCurrency                      = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[10]")).getText();
            Base_NetOrderVal                  = driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[9]")).getText();
            Base_TotalCost                    = driver.findElement(By.xpath(".//*[@id='RowNo_1_5']/td[9]")).getText();
            pageNetTotal                      = driver.findElement(By.xpath(".//*[@id='reportData']/table/tbody/tr[14]/td[2]")).getText();
            pageCostTotal                     = driver.findElement(By.xpath(".//*[@id='reportData']/table/tbody/tr[15]/td")).getText();
            GndNetTotal                       = driver.findElement(By.xpath(".//*[@id='reportDataHeading11']/td[2]")).getText();
            GndCostTotal                      = driver.findElement(By.xpath(".//*[@id='reportDataHeading12']/td")).getText();
	       
            logger.info("End of Reservation Record Basic Data Extraction");	
		} catch (Exception e) {
			logger.fatal("Error in Reservation Record Basic Data Extraction "+e.toString());
		}
	}
	
	public void readProductViewDetails(WebDriver driver)
	{
		logger.info("readProductViewDetails method called -->Getting reservation record product view details");
		try {
			
			Prod_View_BookingNumber			    =  driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[3]/a")).getText();
			Prod_View_BookingDate			    =  driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[1]")).getText();
			Prod_View_Notes     			    =  driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[3]")).getText();
			Prod_View_SupplierName			    =  driver.findElement(By.xpath(".//*[@id='RowNo_1_5']/td[3]")).getText();
			Prod_View_HotelName				    =  driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[4]")).getText();
			Prod_View_HotelConfirmationNumber	=  driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[4]")).getText();
			Prod_View_TransId					=  driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[6]")).getText();
			String[] temp 		                =  driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[2]")).getText().split("/");
			Prod_View_setBookingType			=  temp[0];
			Prod_View_Channel					=  temp[1];
			temp				                =  driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[2]")).getText().split(" ");
			Prod_View_FirtName				    =  temp[0];
			Prod_View_LastName				    =  temp[1];
			Prod_View_CustomerType			    =  driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[4]")).getText();
			temp				                =  driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[5]")).getText().split("/");
			Prod_View_CheckIn				    =  temp[0];
			Prod_View_CheckOut				    =  temp[1];
			Prod_View_RoomType				    =  driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[3]")).getText();
			temp				                =  driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[3]")).getText().split("/");
			//System.out.println(temp[0]);
			Prod_View_BedType					=  temp[0];
			try {
				Prod_View_RatePlan				=  temp[1];
			} catch (Exception e) {
				// TODO: handle exception
			}
			Prod_View_HotelContactNumber		=  driver.findElement(By.xpath(".//*[@id='RowNo_1_5']/td[5]")).getText();
			Prod_View_Currency					=  driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[7]")).getText();
			Prod_View_TotalCost					=  driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[5]")).getText();
			Prod_View_BookingFee				=  driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[5]")).getText();
			Prod_View_OrderValue				=  driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[7]")).getText();
			temp				                =  driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[8]")).getText().split("/");
			Prod_View_NumOfRooms			    =  temp[0];
			Prod_View_NumOfNyts				    =  temp[1];
			Prod_View_TotalCostFinal	    	=  driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[6]")).getText();
			Prod_View_OrderValueFinal	    	=  driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[8]")).getText();
	       
            logger.info("End of Product View Reservation Record Basic Data Extraction");	
		} catch (Exception e) {
			logger.fatal("Error in Reservation Record Basic Data Extraction "+e.toString());
		}
	}
	
	public void readAditionalDetails(WebDriver driver, String ReservationNumber) throws IOException
	{
		WebDriverWait   wait = new WebDriverWait(driver, 40);
		try {
			driver.findElement(By.partialLinkText(ReservationNumber.trim())).click();
			driver.switchTo().defaultContent();
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("dialogwindow"));
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reservationDetailsId")));
			logger.info("Reservation Report - Booking Card Loaded");
			this.bookingCardLoaded = true;
		} catch (Exception e) {
			logger.fatal("Reservation Report - Booking Card not Loaded "+e.toString());
			this.bookingCardLoaded = false;
			File Screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyDirectory(Screenshot, new File("ReservationReport_Card_"+new SimpleDateFormat("dd-MMM-yyyy").format(Calendar.getInstance().getTime())));
		}
		
		try {
			logger.info("Closing Booking Card Frame...");
			driver.findElement(By.xpath(".//*[@id='dialogwindowdragdrop']/td[3]/a/img")).click();
			logger.info("Edit Frame Closed");
		} catch (Exception e) {
			logger.fatal("Either Booking Card Frame not present or failed to close "+e.toString());
		}
		
		
		logger.info("<----- Checking Reservation Report Edit Option ------->");
		
		try {
			driver.switchTo().defaultContent();
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("reportIframe"));
			driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[1]/a/img")).click();
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("dialogwindow"));
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("finpaymentdate")));
			logger.info("Reservation Report - Edit Frame Loaded");
			this.editframeLoaded = true;
		} catch (Exception e) {
			logger.fatal("Reservation Report - Edit Frame not Loaded "+e.toString());
			this.editframeLoaded = false;
			File Screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyDirectory(Screenshot, new File("ReservationReport_Edit_"+new SimpleDateFormat("dd-MMM-yyyy").format(Calendar.getInstance().getTime())));
		}
		
		
		if(editframeLoaded)
		{
			try {
				String DueMonth   = new Select(driver.findElement(By.id("cancelationdate_Month_ID"))).getFirstSelectedOption().getText().trim();
				String DueDate    = driver.findElement(By.id("cancelationdate_Day_ID")).getAttribute("value").trim();
				String DueYear    = driver.findElement(By.id("cancelationdate_Year_ID")).getAttribute("value").trim();
			
				AutoLoadedDueDate = DueDate+"-"+DueMonth+"-"+DueYear;
				InternalNote      = driver.findElement(By.id("notes")).getText();
				CustomerNote      = driver.findElement(By.id("cusNotes")).getText();
				lpoProvided       = driver.findElement(By.id("lpoProvided_Y")).isSelected();
				logger.info("Reservation Report - Edit Details Extracted Successfully");
			} catch (Exception e) {
				logger.fatal("Reservation Report - Edit Details Not Extracted Successfully"+e.toString());
				File Screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyDirectory(Screenshot, new File("ReservationReport_Edit_Det"+new SimpleDateFormat("dd-MMM-yyyy").format(Calendar.getInstance().getTime())));
			}
		}
		
		try {
			logger.info("Closing Edit Frame...");
			driver.findElement(By.xpath(".//*[@id='dialogwindowdragdrop']/td[3]/a/img")).click();
			logger.info("Edit Frame Closed");
		} catch (Exception e) {
			logger.fatal("Either edit frame not present or failed to close "+e.toString());
		}
		
		
	}

    public boolean changeNotes(WebDriver driver,String CusNote,String InternalNote,String ReservationNum) throws IOException
    {
    	logger.fatal("<--- Change Notes Method Called --->");
    	loadReport(driver);
    	if(reportLoaded){
    	  getReservationRecord(driver, ReservationNum);
    	  if(recordAvailable){
    		   WebDriverWait   wait = new WebDriverWait(driver, 40);
    		   try {
    				driver.switchTo().defaultContent();
    				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("reportIframe"));
    				driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[1]/a/img")).click();
    				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("dialogwindow"));
    				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("finpaymentdate")));
    				logger.info("Reservation Report - Edit Frame Loaded");
    				
    				driver.findElement(By.id("notes")).clear();
    				driver.findElement(By.id("cusNotes")).clear();
    				
    				driver.findElement(By.id("notes")).sendKeys(InternalNote);
    				driver.findElement(By.id("cusNotes")).sendKeys(CusNote);
    				
    				logger.info("Clicking on Save Button");
    				driver.findElement(By.name("submit")).click();

    				try {
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("finpaymentdate")));
						logger.info("<---Notes Saved Successfully---->");
						return true;
					} catch (Exception e) {
						return false;
					}
    				
    				
    			} catch (Exception e) {
    				logger.fatal("Reservation Report - Edit Frame not Loaded "+e.toString());
    				File Screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    				FileUtils.copyDirectory(Screenshot, new File("ReservationReport_Edit_"+new SimpleDateFormat("dd-MMM-yyyy").format(Calendar.getInstance().getTime())));
    			}
    		    
    			try {
    				logger.info("Closing Booking Card Frame...");
    				driver.findElement(By.xpath(".//*[@id='dialogwindowdragdrop']/td[3]/a/img")).click();
    				logger.info("Edit Frame Closed");
    			} catch (Exception e) {
    				logger.fatal("Either Booking Card Frame not present or failed to close "+e.toString());
    			}
    		}
    	}
    	return false;
    }

    public boolean changeDueDate(WebDriver driver,String NewDueDate,String ReservationNum) throws IOException
    {
    	logger.fatal("<--- Change Due Date Method Called --->");
    	loadReport(driver);
    	if(reportLoaded){
    	  getReservationRecord(driver, ReservationNum);
    	  if(recordAvailable){
    		   WebDriverWait   wait = new WebDriverWait(driver, 40);
    		   try {
    				driver.switchTo().defaultContent();
    				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("reportIframe"));
    				driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[1]/a/img")).click();
    				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("dialogwindow"));
    				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("finpaymentdate")));
    				logger.info("Reservation Report - Edit Frame Loaded");
    				
    				String[] Values = NewDueDate.split("-");
    				
    			    new Select(driver.findElement(By.id("cancelationdate_Day_ID"))).selectByVisibleText(Values[0].trim());
    			    new Select(driver.findElement(By.id("cancelationdate_Month_ID"))).selectByVisibleText(Values[1].trim());
    			    new Select(driver.findElement(By.id("cancelationdate_Year_ID"))).selectByVisibleText(Values[2].trim());
    				
    				logger.info("Clicking on Save Button");
    				driver.findElement(By.name("submit")).click();

    				try {
						wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("finpaymentdate")));
						logger.info("<---New Due Date Saved Successfully--->");
						return true;
					} catch (Exception e) {
						logger.fatal("<---New Due Date Not Saved Successfully--->");
						return false;
					}
    				
    				
    			} catch (Exception e) {
    				logger.fatal("Reservation Report - Edit Frame not Loaded "+e.toString());
    				File Screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    				FileUtils.copyDirectory(Screenshot, new File("ReservationReport_Edit_"+new SimpleDateFormat("dd-MMM-yyyy").format(Calendar.getInstance().getTime())));
    			}
    		    
    			try {
    				logger.info("Closing Booking Card Frame...");
    				driver.findElement(By.xpath(".//*[@id='dialogwindowdragdrop']/td[3]/a/img")).click();
    				logger.info("Edit Frame Closed");
    			} catch (Exception e) {
    				logger.fatal("Either Booking Card Frame not present or failed to close "+e.toString());
    			}
    		}
    	}
    	return false;
    }

}
