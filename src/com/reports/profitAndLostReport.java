package com.reports;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.utilities.Calender;

public class profitAndLostReport {
	
	private boolean reportLoaded            = false;
	private boolean recordAvailable         = false;
	private boolean bookingCardLoaded       = false;
	private boolean editframeLoaded         = false;
	private boolean lpoProvided             = false;
	private boolean Prod_View_Rec_Avail     = false;
    private String bookingNumber; 
	private String bookingDate; 
	private String consultantName; 
	private String combination; 
	private String commissionTemplate; 
	private String contractType; 
	private String bookingType; 
	private String agentName; 
	private String bookingChannel; 
	private String poslocation; 
	private String state; 
	private String noOfGuests; 
	private String chechIn; 
	private String numOfNyts; 
	private String baseCurrency; 
	private String grossBookingValue; 
	private String agentCommission; 
	private String netBookingValues; 
	private String costOfSales; 
	private String profit; 
	private String profitPercentage; 
	private String invoiceTotal; 
	private String invoicrpaid; 
	private String invoiceBalanceDue;
	private String reportUrl;
    private  Logger logger                  = null;
    
    
    
	public profitAndLostReport(String reproturl) {
		reportUrl = reproturl;
		logger    = Logger.getLogger(this.getClass());
	}
    public boolean isReportLoaded() {
		return reportLoaded;
	}

	public boolean isRecordAvailable() {
		return recordAvailable;
	}

	public boolean isBookingCardLoaded() {
		return bookingCardLoaded;
	}

	public boolean isEditframeLoaded() {
		return editframeLoaded;
	}

	public boolean isLpoProvided() {
		return lpoProvided;
	}

	public boolean isProd_View_Rec_Avail() {
		return Prod_View_Rec_Avail;
	}

    public String getBookingNumber() {
		return bookingNumber;
	}
	public void setBookingNumber(String bookingNumber) {
		this.bookingNumber = bookingNumber;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getConsultantName() {
		return consultantName;
	}
	public void setConsultantName(String consultantName) {
		this.consultantName = consultantName;
	}
	public String getCombination() {
		return combination;
	}
	public void setCombination(String combination) {
		this.combination = combination;
	}
	public String getCommissionTemplate() {
		return commissionTemplate;
	}
	public void setCommissionTemplate(String commissionTemplate) {
		this.commissionTemplate = commissionTemplate;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public String getBookingType() {
		return bookingType;
	}
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getBookingChannel() {
		return bookingChannel;
	}
	public void setBookingChannel(String bookingChannel) {
		this.bookingChannel = bookingChannel;
	}
	public String getPoslocation() {
		return poslocation;
	}
	public void setPoslocation(String poslocation) {
		this.poslocation = poslocation;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getNoOfGuests() {
		return noOfGuests;
	}
	public void setNoOfGuests(String noOfGuests) {
		this.noOfGuests = noOfGuests;
	}
	public String getChechIn() {
		return chechIn;
	}
	public void setChechIn(String chechIn) {
		this.chechIn = chechIn;
	}
	public String getNumOfNyts() {
		return numOfNyts;
	}
	public void setNumOfNyts(String numOfNyts) {
		this.numOfNyts = numOfNyts;
	}
	public String getBaseCurrency() {
		return baseCurrency;
	}
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	public String getGrossBookingValue() {
		return grossBookingValue;
	}
	public void setGrossBookingValue(String grossBookingValue) {
		this.grossBookingValue = grossBookingValue;
	}
	public String getAgentCommission() {
		return agentCommission;
	}
	public void setAgentCommission(String agentCommission) {
		this.agentCommission = agentCommission;
	}
	public String getNetBookingValues() {
		return netBookingValues;
	}
	public void setNetBookingValues(String netBookingValues) {
		this.netBookingValues = netBookingValues;
	}
	public String getCostOfSales() {
		return costOfSales;
	}
	public void setCostOfSales(String costOfSales) {
		this.costOfSales = costOfSales;
	}
	public String getProfit() {
		return profit;
	}
	public void setProfit(String profit) {
		this.profit = profit;
	}
	public String getProfitPercentage() {
		return profitPercentage;
	}
	public void setProfitPercentage(String profitPercentage) {
		this.profitPercentage = profitPercentage;
	}
	public String getInvoiceTotal() {
		return invoiceTotal;
	}
	public void setInvoiceTotal(String invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}
	public String getInvoicrpaid() {
		return invoicrpaid;
	}
	public void setInvoicrpaid(String invoicrpaid) {
		this.invoicrpaid = invoicrpaid;
	}
	public String getInvoiceBalanceDue() {
		return invoiceBalanceDue;
	}
	public void setInvoiceBalanceDue(String invoiceBalanceDue) {
		this.invoiceBalanceDue = invoiceBalanceDue;
	}
	
	
	public void loadReport(WebDriver driver) throws IOException
	{
		logger.info("Navigating to Profit and loss Report ->"+reportUrl);
		driver.get(reportUrl);
		try {
			Alert alert = driver.switchTo().alert();
	        alert.accept(); 
		} catch (Exception e) {
			// TODO: handle exception
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		WebDriverWait wait	 			= new WebDriverWait(driver, 20);
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("element_td_partner_all")));
			this.reportLoaded = true;
			logger.info("Report Loaded Successfully");
		} catch (Exception e) {
			this.reportLoaded = false;
			logger.fatal("Report Not Loaded check the screenshots for more details "+e.toString());
			File Scr        = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(Scr, new File(Calender.getDate(Calendar.DATE,0)+"_Errors/ReservationRepNotLoaded.jpg"));
			
		}
	}
	
	public void getReservationRecord(WebDriver driver, String ReservationNumber) throws IOException
	{
     logger.info("<--P&L Report Getting Reservation Report Record : "+ReservationNumber);	
    
     try {
	 new Select(driver.findElement(By.xpath(".//*[@id='reportdateFilterRow']/table/tbody/tr/td[2]/table/tbody/tr/td[2]/select"))).selectByVisibleText("Today");
	     } catch (Exception e) {
	// TODO: handle exception
    }
	
    try {
        driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
		WebDriverWait wait	 			= new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='reportHeaderRow1']/td[3]")));
		
		boolean MorePagesAvailable   = true;
		int     CurrentPage          = 1;
	
		while(MorePagesAvailable)
		{
			try {
				driver.findElement(By.partialLinkText(ReservationNumber));
				this.recordAvailable  = true;
				logger.info("Reservation Record Available");
				break;
			} catch (Exception e) {
				this.recordAvailable  = false;
			}
			
			try {
				driver.findElement(By.xpath(".//*[@id='pagingStyleRpt']/table/tbody/tr/td["+(CurrentPage+2)+"]/a")).click();
				CurrentPage ++;
			} catch (Exception e) {
				MorePagesAvailable = false;
			}
		}
		
			
	} catch (Exception e) {
		this.recordAvailable = false;
		logger.fatal("Reservation Record Not Available "+ e.toString());
		File Scr        = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(Scr, new File(Calender.getDate(Calendar.DATE,0)+"_Errors/ReservationRecordNotAvail.jpg"));
	}
	
	}

	public void readDetails(WebDriver driver,WebElement element)
	{
		logger.info("P&L Report readDetails method called -->getting reservation record details");
		try {
			String RawId         = element.getAttribute("id");
			WebElement  FirstRaw = element;
			WebElement  SecRaw   = driver.findElement(By.id(RawId.replace("0", "2")));
			WebElement  ThirdRaw = driver.findElement(By.id(RawId.replace("0", "3")));
            
			ArrayList<WebElement> firstrwelements = new ArrayList<WebElement>(FirstRaw.findElements(By.tagName("td")));
			ArrayList<WebElement> Secrwelements   = new ArrayList<WebElement>(SecRaw.findElements(By.tagName("td")));
			ArrayList<WebElement> Thirdrwelements = new ArrayList<WebElement>(ThirdRaw.findElements(By.tagName("td")));
			
			baseCurrency        = driver.findElement(By.id("currencycode")).getText().trim();
			bookingNumber       = firstrwelements.get(0).getText();
			combination         = firstrwelements.get(1).getText();
			bookingType         = firstrwelements.get(2).getText();
			bookingChannel      = firstrwelements.get(3).getText();
			noOfGuests          = firstrwelements.get(4).getText();
			grossBookingValue   = firstrwelements.get(5).getText();
			costOfSales         = firstrwelements.get(6).getText();
			invoiceTotal        = firstrwelements.get(7).getText();
			
			///////////////////////////////////////////////////////
			bookingDate         = Secrwelements.get(0).getText();
			commissionTemplate  = Secrwelements.get(1).getText();
			agentName           = Secrwelements.get(2).getText();
			poslocation         = Secrwelements.get(3).getText();
			chechIn             = Secrwelements.get(4).getText();
			agentCommission     = Secrwelements.get(5).getText();
			profit              = Secrwelements.get(6).getText();
			invoicrpaid         = Secrwelements.get(7).getText();
			///////////////////////////////////////////////////////
			consultantName           = Thirdrwelements.get(0).getText();
			contractType             = Thirdrwelements.get(1).getText();
			state                    = Thirdrwelements.get(3).getText();
			numOfNyts                = Thirdrwelements.get(4).getText();
			netBookingValues         = Thirdrwelements.get(5).getText();
			profitPercentage         = Thirdrwelements.get(6).getText();
			invoiceBalanceDue        = Thirdrwelements.get(7).getText();
	       
            logger.info("End of Reservation Record Basic Data Extraction");	
		} catch (Exception e) {
			logger.fatal("Error in Reservation Record Basic Data Extraction "+e.toString());
		}
	}
	
	
}
