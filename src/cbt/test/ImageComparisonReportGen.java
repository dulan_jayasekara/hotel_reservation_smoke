package cbt.test;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

import com.utilities.Calender;

public class ImageComparisonReportGen {

	/**
	 * @param args
	 * @throws IOException 
	 */
	
	
	public static void main(String[] args) throws IOException {
		
	StringBuffer      PrintWriter = new StringBuffer();
     int TestCaseCount =1;
	 PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
	 PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>CrossBrowser Screenshot Comparison("+Calender.getDate(Calendar.DATE,0)+")</p></div>");
	 PrintWriter.append("<body>");

     PrintWriter.append("<br><p class='InfoSup'>Firefox vs Safari</p><br>");
	 PrintWriter.append("<br><table ><tr><th>TestCase#</th><th>ScreenShot Name</th><th>FireFox 31</th><th>Safari 8</th><th>Test Status</th><th>Errors</th></tr>");
	 
	 PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>LoginPage</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/LoginPage.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/LoginPage.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>MainPage</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/MainPage.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/MainPage.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>ReservationsDropDown</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/ReservationsDropDown.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/ReservationsDropDown.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>SearchPage</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/SearchPage.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/SearchPage.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>CurrencyList</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/CurrencyList.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/CurrencyList.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Failed'>FAIL</td>");
	 PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>CityList</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/CityList.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/CityList.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Failed'>FAIL</td>");
	 PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>CheckInCal</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/CheckInCal.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/CheckInCal.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>CheckOutCal</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/CheckOutCal.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/CheckOutCal.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>NightsDropDown</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/NightsDropDown.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/NightsDropDown.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Failed'>FAIL</td>");
	 PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>RoomsDropDown</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/RoomsDropDown.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/RoomsDropDown.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>Processing</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/Processing.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/Processing.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>Results</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/Results.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/Results.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>ExpandedRooms</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/ExpandedRooms.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/ExpandedRooms.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>DealsCal</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/DealsCal.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/DealsCal.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>CancellationPolicy</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/CancellationPolicy.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/CancellationPolicy.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>AditionalFilters</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/AditionalFilters.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/AditionalFilters.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>HotelInfo</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/HotelInfo.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/HotelInfo.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Failed'>FAIL</td>");
	 PrintWriter.append("<td></td></tr>");
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>DailyRates</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/DailyRates.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/DailyRates.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>Cart</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/Cart.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/Cart.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>ViewFull</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/ViewFull.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/ViewFull.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>PaymentPage</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/PaymentPage.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/PaymentPage.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Failed'>FAIL</td>");
	 PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>CustomerDetails</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/CustomerDetails.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/CustomerDetails.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>CustomerLookUp</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/CustomerLookUp.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/CustomerLookUp.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>Intermediate</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/Intermediate.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/Intermediate.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Passed'>PASS</td>");
     PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>PaymentGateway</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/PaymentGateway.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/PaymentGateway.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Failed'>FAIL</td>");
	  PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>ConfPage</td>");
	 TestCaseCount++;
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/ConfPage.png\">ClickToView</a></td>");
	 PrintWriter.append("<td><a href=\"file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Mac/ConfPage.png\">ClickToView</a></td>");
	 
	 PrintWriter.append("<td class='Failed'>FAIL</td>");
	 PrintWriter.append("<td></td></tr>");
     
     
     PrintWriter.append("</table>");
     
     
     PrintWriter.append("</body></html>");
	   BufferedWriter sbwr = new BufferedWriter(new FileWriter(new File("Reports/CBTComparison.html")));
	   sbwr.write(PrintWriter.toString());
	   sbwr.flush();
	   sbwr.close();
	   PrintWriter.setLength(0);
	 
	/*
	 
	  PrintWriter.append("<td class='Passed'>PASS</td>");
	      PrintWriter.append("<td></td></tr>");*/
	     
	
/*	  PrintWriter.append("<td class='Failed'>FAIL</td>");
	  PrintWriter.append("<td></td></tr>");*/
	 
	 // <a href="http://www.w3schools.com">Visit W3Schools.com!</a> 
	 //file:///C:/Users/Dulan/git/hotel_reservation_smoke/CBTTest/06-May-2015/Windows/LoginPage.png

	}

}
