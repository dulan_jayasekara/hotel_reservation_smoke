package cbt.test;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class CBTExample {
	
	
	public static void main(String[] args) throws Exception {
		
	                     
    DesiredCapabilities caps             = new DesiredCapabilities();
    HashMap<String, String>PropertySet   = ReadProperties.readpropeties();
    
    caps.setCapability("name",PropertySet.get("cbt.name"));
    caps.setCapability("build",PropertySet.get("cbt.build"));
    caps.setCapability("browser_api_name",PropertySet.get("cbt.browser_api_name").trim());
    caps.setCapability("os_api_name",PropertySet.get("cbt.os_api_name").trim());
    caps.setCapability("screen_resolution",PropertySet.get("cbt.screen_resolution"));
    caps.setCapability("record_video",PropertySet.get("cbt.record_video"));
    caps.setCapability("record_network",PropertySet.get("cbt.record_network"));
    caps.setCapability("record_snapshot",PropertySet.get("cbt.record_snapshot"));
    

    WebDriver driver = new RemoteWebDriver(new URL("http://rezgqa%40gmail.com:uc0ed54828e2228c@hub.crossbrowsertesting.com:80/wd/hub"), caps);
    WebDriverWait    wait         = new WebDriverWait(driver, 10);
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  
    driver.get("http://tpapp1.secure-reservation.com/travelplan/admin/common/LoginPage.do");
	driver.get(PropertySet.get("Portal.Url").concat(
			"/admin/common/LoginPage.do"));
	driver.findElement(By.id("user_id")).clear();
	driver.findElement(By.id("user_id")).sendKeys(
			PropertySet.get("portal.username"));
	driver.findElement(By.id("password")).clear();
	driver.findElement(By.id("password")).sendKeys(
			PropertySet.get("portal.password"));
	driver.findElement(By.id("loginbutton")).click();

	try {
		//driver.findElement(By.id("mainmenurezglogo"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("mainmenurezglogo")));;
		System.out.println("Logged in Successfully");
	} catch (Exception e) {
		//logger.fatal("Login Failed -->" + e.toString());
		System.out.println("Logged in Failed " +e.toString());
		File Scr = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(Scr, new File(Calendar.getInstance().getTimeInMillis()+ "_Errors/Login Failed.jpg"));
		
	}

  }
}