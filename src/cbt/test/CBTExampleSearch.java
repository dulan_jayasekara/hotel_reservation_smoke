package cbt.test;


import org.apache.commons.io.FileUtils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.TimeUnit;


public class CBTExampleSearch {
	
	private static String        From          ="Greece";
	private static String        TO            ="Athens";
	//private String             FromHidden    ="Miami|MIA|1241|Miami International Airport|-|55||Usa";
	private static String        ToHidden      ="553|Athens||224||Greece|";
    private static int           NumberOfRooms = 3;
    
	public static void main(String[] args) throws Exception {
		
	                     
    DesiredCapabilities caps             = new DesiredCapabilities();
    HashMap<String, String>PropertySet   = ReadProperties.readpropeties();
    
    caps.setCapability("name",PropertySet.get("cbt.name"));
    caps.setCapability("build",PropertySet.get("cbt.build"));
    caps.setCapability("browser_api_name",PropertySet.get("cbt.browser_api_name").trim());
    caps.setCapability("os_api_name",PropertySet.get("cbt.os_api_name").trim());
    caps.setCapability("screen_resolution",PropertySet.get("cbt.screen_resolution"));
    caps.setCapability("record_video",PropertySet.get("cbt.record_video"));
    caps.setCapability("record_network",PropertySet.get("cbt.record_network"));
    caps.setCapability("record_snapshot",PropertySet.get("cbt.record_snapshot"));
    

    WebDriver driver = new RemoteWebDriver(new URL("http://rezuserqa%40gmail.com:u8a1f691d89188f4@hub.crossbrowsertesting.com:80/wd/hub"), caps);
    WebDriverWait    wait         = new WebDriverWait(driver, 10);
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  
    driver.get("http://tpapp1.secure-reservation.com/travelplan/admin/common/LoginPage.do");
	driver.get(PropertySet.get("Portal.Url").concat(
			"/admin/common/LoginPage.do"));
	driver.findElement(By.id("user_id")).clear();
	driver.findElement(By.id("user_id")).sendKeys(
			PropertySet.get("portal.username"));
	driver.findElement(By.id("password")).clear();
	driver.findElement(By.id("password")).sendKeys(
			PropertySet.get("portal.password"));
	driver.findElement(By.id("loginbutton")).click();

	try {
		//driver.findElement(By.id("mainmenurezglogo"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("mainmenurezglogo")));;
		System.out.println("Logged in Successfully");
	} catch (Exception e) {
		//logger.fatal("Login Failed -->" + e.toString());
		System.out.println("Logged in Failed " +e.toString());
		File Scr = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(Scr, new File(Calendar.getInstance().getTimeInMillis()+ "_Errors/Login Failed.jpg"));
		
	}
	
	try {
   	 wait = new WebDriverWait(driver, 120);
   	 driver.get(PropertySet.get("Portal.Url").concat("/operations/reservation/CallCenterWarSearchCriteria.do?module=operations"));
   	 driver.switchTo().frame("live_message_frame");
   	 driver.switchTo().frame("bec_container_frame");
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("hotels")));
	    driver.findElement(By.id("hotels")).click();
	    new Select(driver.findElement(By.name("H_Country"))).selectByVisibleText(From);
		driver.findElement(By.name("H_Loc")).sendKeys(TO);
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("document.getElementById('hid_H_Loc').setAttribute('value','"+ ToHidden + "');");
		
		Random rand = new Random();
	    int  n = rand.nextInt(100) + 15;
	    Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, n);
	    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	    String date1 = sdf.format(cal.getTime());
	    cal.add(Calendar.DATE, 3);
	    String date2 = sdf.format(cal.getTime());
	    
	    ((JavascriptExecutor) driver).executeScript("$('#ho_departure').val('"+date1+"');");
	    ((JavascriptExecutor) driver).executeScript("$('#ho_arrival').val('"+date2+"');");

		new Select(driver.findElement(By.id("H_nights"))).selectByVisibleText("3");
		new Select(driver.findElement(By.id("norooms_H"))).selectByVisibleText(Integer.toString(NumberOfRooms));

		for (int i = 1; i <= NumberOfRooms; i++) {
			String Adults = "3";
			String Child =  "1";
		
			new Select(driver.findElement(By.id("R" + i + "occAdults_H"))).selectByVisibleText(Adults);
			new Select(driver.findElement(By.id("R" + i + "occChildren_H"))).selectByVisibleText(Child);

			int NOC = 1;
			String[] CAges = {"7"};

			if (NOC != 0) {

				if (NOC == CAges.length) {
					for (int k = 0; k < CAges.length; k++) {
						new Select(driver.findElement(By.id("H_R" + i
								+ "childage_" + (k + 1))))
								.selectByVisibleText(CAges[k].trim());
					}
				} else

					System.out.println("Problem with child ages room " + i);

			}

		}

		double StartTime = System.currentTimeMillis();
		((JavascriptExecutor) driver).executeScript("search('H');");

		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
		Thread.sleep(3000);

		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame("live_message_frame");
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("hotel_1")));

			double EndTime = System.currentTimeMillis();
			Double  ResultsAvailabilityTime ; 
			ResultsAvailabilityTime  = (EndTime - StartTime)/1000;
	        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		    String NumberOfHotels = driver.findElement(By.className("pagination_itom")).getText().split(" ")[6];
		    System.out.println("Results Available Hotels :"+NumberOfHotels+" Time Taken: "+ResultsAvailabilityTime);
		//	logger.info("Results Available From :"+From + " To :"+ TO+":Results "+NumberOfHotels+" Time:"+ResultsAvailabilityTime);
			//logger.debug(ResultsAvailabilityTime);
			File Scr = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(Scr, new File("PassedScreens/ResultsAvailable"+Calendar.getInstance().getTimeInMillis()+".jpg"));

		} catch (Exception e) {
			System.out.println("Results Not Available..");
			File Scr = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(Scr, new File("FailedScreens/ResultsNotAvailable"+Calendar.getInstance().getTimeInMillis()+".jpg"));
		
		//	logger.info("<p>Results Not Available From :"+From + " To :"+ TO+"<p>");
			String ErrorMessage = "-";
			try {
				ErrorMessage = driver.findElement(By.id("result_available_message")).getText();
			} catch (Exception e2) {
				ErrorMessage = "N/A";
			}
		}

} catch (Exception e) {
	File Scr = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	FileUtils.copyFile(Scr, new File("FailedScreens/ResultsNotAvailable"+Calendar.getInstance().getTimeInMillis()+".jpg"));
	//logger.info("<p>Results Not Available From :"+From + " To :"+ TO+"<p>");
}

  }
}