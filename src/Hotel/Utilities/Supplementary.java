package Hotel.Utilities;

import java.util.Date;

public class Supplementary {

private String   SupplementaryName  = "";
private String   ApplicableRoomType = "";
private String   ApplicableRoomName = "";
private String   AdultCharge        = "";
private String   ChildCharge        = "";
private String   TotalCharge        = "";
private boolean  isMandatory        = false;
private double   NetRate            = 0.0; 
private Date     ApplicableFrom     = null;
private Date     ApplicableTo       = null;


public double getNetRate() {
	return NetRate;
}
public void setNetRate(double netRate) {
	NetRate = netRate;
}
public String getSupplementaryName() {
	return SupplementaryName;
}
public void setSupplementaryName(String supplementaryName) {
	SupplementaryName = supplementaryName;
}
public String getApplicableRoomType() {
	return ApplicableRoomType;
}
public void setApplicableRoomType(String applicableRoomType) {
	ApplicableRoomType = applicableRoomType;
}
public String getApplicableRoomName() {
	return ApplicableRoomName;
}
public void setApplicableRoomName(String applicableRoomName) {
	ApplicableRoomName = applicableRoomName;
}
public String getAdultCharge() {
	return AdultCharge;
}
public void setAdultCharge(String adultCharge) {
	AdultCharge = adultCharge;
}
public String getChildCharge() {
	return ChildCharge;
}
public void setChildCharge(String childCharge) {
	ChildCharge = childCharge;
}
public String getTotalCharge() {
	return TotalCharge;
}
public void setTotalCharge(String totalCharge) {
	TotalCharge = totalCharge;
}
public boolean isMandatory() {
	return isMandatory;
}
public void setMandatory(boolean isMandatory) {
	this.isMandatory = isMandatory;
}


}
