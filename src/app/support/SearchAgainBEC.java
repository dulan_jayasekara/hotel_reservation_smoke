package app.support;

import java.util.Map;

public class SearchAgainBEC {

private String               Destination;
private String               Checkin;
private String               Checkout;
private String               Nights;
private String               StarRating;
private String               HotelType;
private String               HotelName;
private String               PrefferedCurrency;
private String               PriceLevelFrom;
private String               PriceLevelTo;
private String               Promocode;
private boolean              isBECAvailable            = true;
private boolean              isCORAvailable            = true;
private boolean              isSearchButtonAvailable   = true;
private boolean              isAvailableChecked        = true;
private boolean              isOnRequestChecked        = true;
private String               Rooms                     = "";
private int                  RoomsAvailableForOccupancy= 0;
private String[]             Adults                    = null;
private String[]             Children                  = null;
private Map<Integer, String[]> ChildAges               = null;



public int getRoomsAvailableForOccupancy() {
	return RoomsAvailableForOccupancy;
}
public void setRoomsAvailableForOccupancy(int roomsAvailableForOccupancy) {
	RoomsAvailableForOccupancy = roomsAvailableForOccupancy;
}
public boolean isAvailableChecked() {
	return isAvailableChecked;
}
public void setAvailableChecked(boolean isAvailableChecked) {
	this.isAvailableChecked = isAvailableChecked;
}
public boolean isOnRequestChecked() {
	return isOnRequestChecked;
}
public void setOnRequestChecked(boolean isOnRequestChecked) {
	this.isOnRequestChecked = isOnRequestChecked;
}
public boolean isBECAvailable() {
	return isBECAvailable;
}
public void setBECAvailable(boolean isBECAvailable) {
	this.isBECAvailable = isBECAvailable;
}
public String getDestination() {
	return Destination;
}
public void setDestination(String destination) {
	Destination = destination;
}
public String getCheckin() {
	return Checkin;
}
public void setCheckin(String checkin) {
	Checkin = checkin;
}
public String getCheckout() {
	return Checkout;
}
public void setCheckout(String checkout) {
	Checkout = checkout;
}
public String getNights() {
	return Nights;
}
public void setNights(String nights) {
	Nights = nights;
}
public String getStarRating() {
	return StarRating;
}
public void setStarRating(String starRating) {
	StarRating = starRating;
}
public String getHotelType() {
	return HotelType;
}
public void setHotelType(String hotelType) {
	HotelType = hotelType;
}
public String getHotelName() {
	return HotelName;
}
public void setHotelName(String hotelName) {
	HotelName = hotelName;
}
public String getPrefferedCurrency() {
	return PrefferedCurrency;
}
public void setPrefferedCurrency(String prefferedCurrency) {
	PrefferedCurrency = prefferedCurrency;
}
public String getPriceLevelFrom() {
	return PriceLevelFrom;
}
public void setPriceLevelFrom(String priceLevelFrom) {
	PriceLevelFrom = priceLevelFrom;
}
public String getPriceLevelTo() {
	return PriceLevelTo;
}
public void setPriceLevelTo(String priceLevelTo) {
	PriceLevelTo = priceLevelTo;
}
public String getPromocode() {
	return Promocode;
}
public void setPromocode(String promocode) {
	Promocode = promocode;
}
public boolean isCORAvailable() {
	return isCORAvailable;
}
public void setCORAvailable(boolean isCORAvailable) {
	this.isCORAvailable = isCORAvailable;
}
public boolean isSearchButtonAvailable() {
	return isSearchButtonAvailable;
}
public void setSearchButtonAvailable(boolean isSearchButtonAvailable) {
	this.isSearchButtonAvailable = isSearchButtonAvailable;
}
public String getRooms() {
	return Rooms;
}
public void setRooms(String rooms) {
	Rooms = rooms;
}
public String[] getAdults() {
	return Adults;
}
public void setAdults(String[] adults) {
	Adults = adults;
}
public String[] getChildren() {
	return Children;
}
public void setChildren(String[] children) {
	Children = children;
}
public Map<Integer, String[]> getChildAges() {
	return ChildAges;
}
public void setChildAges(Map<Integer, String[]> childAges) {
	ChildAges = childAges;
}



}
