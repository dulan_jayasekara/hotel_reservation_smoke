package app.support;

//import mx4j.log.Logger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.firefox.*;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Options;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.*;

import bsh.This;
import com.types.*;
import com.utilities.Calender;
import com.utilities.Occupancy;
import com.utilities.PerfomanceSingleton;
import com.utilities.Screenshot;
import com.utilities.ScreenshotPathSingleton;

import java.lang.String;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.io.File;
import java.io.IOException;

public class MethodClass {

	private WebDriver driver = null;
	private HashMap<String, String> PropertySet;
	private org.apache.log4j.Logger logger;

	public MethodClass(HashMap<String, String> map) {
		PropertySet = map;
		logger = org.apache.log4j.Logger.getLogger(this.getClass());
	}

	public WebDriver initalizeDriver() throws IOException {
		logger.info("====== Driver initialization started ========");
		String Type = PropertySet.get("BinaryType").trim();

		if (Type.equalsIgnoreCase("FirefoxBin")) {
			logger.info("FirefoxBin type selected ---> No profile");
			FirefoxProfile prof = new FirefoxProfile();
			FirefoxBinary bin = new FirefoxBinary();
			driver = new FirefoxDriver(bin, prof);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			return driver;
		}
		if (Type.equalsIgnoreCase("Phantom")) {
			logger.info("Phantom Driver type selected ---> No profile");
			String ExecutablePath = PropertySet.get("phantomjs.executable.path");
			logger.info("PhantomJs Path --> " + ExecutablePath);
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, ExecutablePath);
			cap.setCapability("TakeScreenshot", true);
			driver = new PhantomJSDriver(cap);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			return driver;

		}
		if (Type.equalsIgnoreCase("Firefoxpro")) {

			File profile = new File(PropertySet.get("firefox.profile.path").trim());
			logger.info("Firefox Profile type selected --->Profile --->" + profile.getAbsolutePath());
			FirefoxProfile prof = new FirefoxProfile(profile);
			driver = new FirefoxDriver(prof);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			return driver;

		} else {
			FirefoxProfile prof = new FirefoxProfile();
			logger.info("Driver Type Not Selected");
			driver = new FirefoxDriver(prof);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			return driver;
		}

	}

	public WebDriver initializeRemoteDriver(String Browser) throws MalformedURLException {

		if (Browser.equalsIgnoreCase("firefox")) {
			System.out.println(" Executing on FireFox");
			String Node = PropertySet.get("FirefoxNode").trim();
			DesiredCapabilities cap = DesiredCapabilities.firefox();
			cap.setBrowserName("firefox");
			driver = new RemoteWebDriver(new URL(Node), cap);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			return driver;
		} else if (Browser.equalsIgnoreCase("chrome")) {
			System.out.println(" Executing on CHROME");
			DesiredCapabilities cap = DesiredCapabilities.chrome();
			cap.setBrowserName("chrome");
			String Node = PropertySet.get("ChromeNode").trim();
			driver = new RemoteWebDriver(new URL(Node), cap);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			return driver;
		} else if (Browser.equalsIgnoreCase("ie")) {
			System.out.println(" Executing on IE");
			DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
			cap.setBrowserName("ie");
			String Node = PropertySet.get("IENode").trim();
			driver = new RemoteWebDriver(new URL(Node), cap);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			return driver;
		} else if (Browser.equalsIgnoreCase("safari")) {
			System.out.println(" Executing on Safari");
			DesiredCapabilities cap = DesiredCapabilities.safari();
			cap.setBrowserName("safari");
			String Node = PropertySet.get("SafariNode").trim();
			driver = new RemoteWebDriver(new URL(Node), cap);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			return driver;
		} else {
			throw new IllegalArgumentException("The Browser Type is Undefined");

		}
	}

	public WebDriver initializeCbtDriver(String Browser) throws IOException {
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("name", PropertySet.get("cbt.name"));
		caps.setCapability("build", PropertySet.get("cbt.build"));
		caps.setCapability("browser_api_name", PropertySet.get(Browser + ".cbt.browser_api_name").trim());
		caps.setCapability("os_api_name", PropertySet.get(Browser + ".cbt.os_api_name").trim());
		caps.setCapability("screen_resolution", PropertySet.get("cbt.screen_resolution"));
		caps.setCapability("record_video", PropertySet.get("cbt.record_video"));
		caps.setCapability("record_network", PropertySet.get("cbt.record_network"));
		caps.setCapability("record_snapshot", PropertySet.get("cbt.record_snapshot"));

		WebDriver driver = new RemoteWebDriver(new URL(PropertySet.get("Cbt.Url")), caps);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		return driver;
	}

	public boolean login(WebDriver driver) throws IOException {
		driver.get(PropertySet.get("Portal.Url").concat("/admin/common/LoginPage.do"));

		try {
			driver.findElement(By.id("user_id")).clear();
			Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getBasicCommonPath() + "/LoginPage.jpg", driver);
		} catch (Exception e) {
			Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getBasicFailedPath() + "/SystemUnavailable.jpg", driver);
			return false;
		}

		driver.findElement(By.id("user_id")).sendKeys(PropertySet.get("portal.username"));
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys(PropertySet.get("portal.password"));
		driver.findElement(By.id("loginbutton")).click();

		try {
			driver.findElement(By.id("mainmenurezglogo"));
			Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getBasicCommonPath() + "/MainPage.jpg", driver);
			return true;
		} catch (Exception e) {
			logger.fatal("Login Failed -->" + e.toString());
			Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getBasicFailedPath() + "/Login_Failed.jpg", driver);
			return false;
		}

	}

	public void getReservationPageCC(WebDriver driver) throws IOException, InterruptedException {
		PerfomanceSingleton perf = PerfomanceSingleton.getInstance();
		driver.get(PropertySet.get("Portal.Url").concat("/operations/common/Main.do?module=operations"));
		WebDriverWait wait = new WebDriverWait(driver, 3);

		try {
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			System.out.println(alert.getText());
			alert.accept();
		} catch (Exception e) {
			System.out.println("No Alert Present");
		}
		Thread.sleep(3000);
		double StartTime = System.currentTimeMillis();
		driver.get(PropertySet.get("Portal.Url").concat("/operations/reservation/CallCenterWarSearchCriteria.do?module=operations"));
		try {
			WebDriverWait wait1 = new WebDriverWait(driver, 180, 5);
			wait1.until(ExpectedConditions.presenceOfElementLocated(By.id("live_message_frame")));
			Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioCommonPath() + "/ReservationPage.jpg", driver);
			double EndTime = System.currentTimeMillis();
			perf.setTimeTakenToLoadBookingEngine((EndTime - StartTime) / 1000);
		} catch (Exception e) {
			Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioFailedPath() + "/ReservationPageFailed.jpg", driver);
			System.out.println("Problem with Navigating to the reservation page..");
		}

	}

	public void getReservationPageWeb(WebDriver driver) throws IOException {
		driver.get(PropertySet.get("Portal.Url").concat("Reservations/"));

		try {

			WebDriverWait wait1 = new WebDriverWait(driver, 180, 5);
			wait1.until(ExpectedConditions.presenceOfElementLocated(By.id("bec_container_frame")));
		} catch (Exception e) {
			Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioFailedPath() + "/BEC_NavigationFailed.jpg", driver);
			System.out.println("Problem With Navigating Web Page");
			org.apache.log4j.Logger.getLogger(This.class.getClass());
		}

	}

	public ResultsType search(WebDriver driver, SearchType CurrentSearch, StringBuffer printer) throws IOException, InterruptedException, TimeoutException {
		printer.append("<br><br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");

		ResultsType ResultObject = new ResultsType();
		WebDriverWait wait = new WebDriverWait(driver, 120);

		driver.switchTo().frame("live_message_frame");

		try {
			printer.append("<tr><td>1</td><td>Availability of the Search Page</td>");
			printer.append("<td>Search Page Page Should Be Loaded</td>");
			driver.findElement(By.id("select_currency"));
			printer.append("<td>Reservation Page Available</td>");
			printer.append("<td class='Passed'>PASS</td>");
			ResultObject.setSearchPageLoaded(true);

		} catch (Exception e) {

			printer.append("<td>Search Page Page Navigation Failed</td>");
			printer.append("<td class='Failed'>FAIL</td>");
			ResultObject.setSearchPageLoaded(false);
			Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioFailedPath() + "/ReservationPageFailed.jpg", driver);
			return ResultObject;
		}
		if (!(CurrentSearch.getSearchCurrency().equalsIgnoreCase(PropertySet.get("portal.currency"))))
			new Select(driver.findElement(By.id("select_currency"))).selectByValue(CurrentSearch.getSearchCurrency());

		printer.append("<tr><td>1</td><td>Availability of the booking engine</td>");
		printer.append("<td>BookingEngine Should Visible</td>");
		driver.switchTo().frame("bec_container_frame");
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("hotels")));
			printer.append("<td>Booking Engine Available</td>");
			printer.append("<td class='Passed'>PASS</td>");
		} catch (Exception e) {
			printer.append("<td>Issue With Booking Engine</td>");
			printer.append("<td class='Failed'>FAIL</td>");
		}
		driver.findElement(By.id("hotels")).click();

		printer.append("<tr><td>1</td><td>Availability of the Country Of Residance </td>");
		if (PropertySet.get("Is.CCCountryOfResidance.Enabled").equals("true"))
			printer.append("<td>Country Of Residence Should Visible</td>");
		else
			printer.append("<td>Country Of Residence Should not Visible</td>");

		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("H_Country")));
			Thread.sleep(2000);

			if (PropertySet.get("Is.CCCountryOfResidance.Enabled").equals("true")) {

				if (driver.findElement(By.id("H_Country")).isDisplayed()) {
					printer.append("<td>Country Of Residence Available</td>");
					printer.append("<td class='Passed'>PASS</td>");
				} else {
					printer.append("<td>Booking Engine Not Available</td>");
					printer.append("<td class='Failed'>FAIL</td>");
				}
			} else {
				if (driver.findElement(By.id("H_Country")).isDisplayed()) {
					printer.append("<td>Country Of Residence Available</td>");
					printer.append("<td class='Failed'>FAIL</td>");
				} else {
					printer.append("<td>Booking Engine Not Available</td>");
					printer.append("<td class='Passed'>PASS</td>");
				}

			}

		} catch (Exception e) {

			if (PropertySet.get("Is.CCCountryOfResidance.Enabled").equals("false") && driver.findElement(By.id("H_Country")).isDisplayed()) {
				printer.append("<td>Country Of Residence Available</td>");
				printer.append("<td class='Failed'>FAIL</td>");
			} else {
				printer.append("<td>Country Of Residence Not Available</td>");
				printer.append("<td class='Passed'>PASS</td>");
				;

			}
		}

		printer.append("<tr><td>1</td><td>Availability of the Departure and arrival Calendar Triggers</td>");
		printer.append("<td>Calenders triggers Should be visible</td>");

		ArrayList<WebElement> CalendarTriggers = null;
		ArrayList<WebElement> VisibleTriggers = new ArrayList<WebElement>();
		try {
			CalendarTriggers = new ArrayList<WebElement>(driver.findElements(By.className("ui-datepicker-trigger")));
			for (WebElement webElement : CalendarTriggers) {
				if (webElement.isDisplayed())
					VisibleTriggers.add(webElement);
			}
			if (VisibleTriggers.size() == 2) {
				printer.append("<td>Arrival Departure Calendar triggers Visible</td>");
				printer.append("<td class='Passed'>PASS</td>");
			} else {
				printer.append("<td>Arrival Departure Calendar triggers Not Visible</td>");
				printer.append("<td class='Passed'>FAIL</td>");

			}
		} catch (Exception e1) {
			printer.append("<td>Arrival Departure Calendar triggers Not Visible</td>");
			printer.append("<td class='Passed'>FAIL</td>");
			logger.warn(e1.toString());
		}

		printer.append("<tr><td>1</td><td>Availability of the Departure and arrival Calendars</td>");
		printer.append("<td>Calenders triggers Should be visible</td>");

		if (VisibleTriggers.size() != 0) {
			VisibleTriggers.get(0).click();
			if (driver.findElement(By.className("ui-datepicker-month")).isDisplayed()) {
				Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioCommonPath() + "/CheckInDatePicker.jpg", driver);
				VisibleTriggers.get(1).click();
				if (driver.findElement(By.className("ui-datepicker-month")).isDisplayed()) {
					printer.append("<td>Arrival Departure Calendars Visible</td>");
					printer.append("<td class='Passed'>PASS</td>");
					Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioCommonPath() + "/CheckOutDatePicker.jpg", driver);
				} else {
					printer.append("<td>Check out Calendar Not Visible</td>");
					printer.append("<td class='Passed'>FAIL</td>");
					Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioFailedPath() + "/CheckOutDatePickerNotVisible.jpg", driver);
				}
			} else {
				printer.append("<td>Check in Calendar Not Visible</td>");
				printer.append("<td class='Passed'>FAIL</td>");
				Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioFailedPath() + "/CheckInDatePickerNotVisible.jpg", driver);

			}

		} else {
			printer.append("<td>No triggers to view the calendars</td>");
			printer.append("<td class='Passed'>FAIL</td>");
		}

		printer.append("<tr><td>1</td><td>Availability of Star Rating Field</td>");
		printer.append("<td>Hotel Rating Field Should be visible</td>");

		if (driver.findElement(By.id("star_rating_H")).isDisplayed()) {
			printer.append("<td>Star Rating Field  Visible</td>");
			printer.append("<td class='Passed'>PASS</td>");
		} else {
			printer.append("<td>Star Rating Field Not Visible</td>");
			printer.append("<td class='Failed'>FAIL</td>");

		}

		printer.append("<tr><td>1</td><td>Availability of the Hotel Type Field</td>");
		printer.append("<td>HotelT Should be visible</td>");

		if (driver.findElement(By.id("hType_h")).isDisplayed()) {
			printer.append("<td>Hotel Type Field Visible</td>");
			printer.append("<td class='Passed'>PASS</td>");
		} else {
			printer.append("<td>Hotel Type Field Not Visible</td>");
			printer.append("<td class='Failed'>FAIL</td>");

		}

		printer.append("<tr><td>1</td><td>Availability of the Hotel Name s</td>");
		printer.append("<td>Hotel Name Filetr Should be visible</td>");

		if (driver.findElement(By.id("H_name")).isDisplayed()) {
			printer.append("<td>Hotel Name Filetr Visible</td>");
			printer.append("<td class='Passed'>PASS</td>");
		} else {
			printer.append("<td>Hotel Name Filetr Not Visible</td>");
			printer.append("<td class='Failed'>FAIL</td>");

		}
		// ////////////////////////////////////////////////////

		printer.append("<tr><td>1</td><td>Availability of Inventory Type Available TickBox</td>");
		printer.append("<td>Inventory Type Available TickBox Should be visible</td>");

		if (driver.findElement(By.id("availableHotels_H")).isDisplayed()) {
			printer.append("<td>Inventory Type Available TickBox Visible</td>");
			printer.append("<td class='Passed'>PASS</td>");
		} else {
			printer.append("<td>Inventory Type Available TickBox Not Visible</td>");
			printer.append("<td class='Failed'>FAIL</td>");

		}

		printer.append("<tr><td>1</td><td>Availability of Inventory Type OnRequest TickBox</td>");
		printer.append("<td>Inventory Type OnRequest TickBox Should be visible</td>");

		if (driver.findElement(By.id("onRequestHotels_H")).isDisplayed()) {
			printer.append("<td>Inventory Type OnRequest TickBox Visible</td>");
			printer.append("<td class='Passed'>PASS</td>");
		} else {
			printer.append("<td>Inventory Type OnRequest TickBox Not Visible</td>");
			printer.append("<td class='Failed'>FAIL</td>");

		}

		// //////////////////////////////////////////////////////////////////////////////////////////////////////////////

		printer.append("<tr><td>1</td><td>Availability of Preferred Currency Field</td>");
		printer.append("<td>Preferred Currency Field Should be visible</td>");

		if (driver.findElement(By.id("H_consumerCurrencyCode")).isDisplayed()) {
			printer.append("<td>Preferred Currency Field Visible</td>");
			printer.append("<td class='Passed'>PASS</td>");
		} else {
			printer.append("<td>Preferred Currency Field Not Visible</td>");
			printer.append("<td class='Failed'>FAIL</td>");

		}

		printer.append("<tr><td>1</td><td>Availability of I Have a Promotion Code Field</td>");
		printer.append("<td>I Have a Promotion Code Field Should be visible</td>");

		if (driver.findElement(By.id("discountCoupon_No_H")).isDisplayed()) {
			printer.append("<td>I Have a Promotion Code Field  Visible</td>");
			printer.append("<td class='Passed'>PASS</td>");
		} else {
			printer.append("<td>I Have a Promotion Code Field  Not Visible</td>");
			printer.append("<td class='Failed'>FAIL</td>");

		}

		printer.append("<tr><td>1</td><td>Availability of Price Level Filter Fileds</td>");
		printer.append("<td>Price Level Filter Fileds Should be visible</td>");

		if (driver.findElement(By.id("priceLevelFrom_H")).isDisplayed() && driver.findElement(By.id("priceLevelTo_H")).isDisplayed()) {
			printer.append("<td>Price Level Filter Fileds Visible</td>");
			printer.append("<td class='Passed'>PASS</td>");
		} else {
			printer.append("<td>Price Level Filter Fileds Not Visible</td>");
			printer.append("<td class='Failed'>FAIL</td>");

		}

		if (driver.findElement(By.id("H_Country")).isDisplayed())
			new Select(driver.findElement(By.name("H_Country"))).selectByVisibleText(CurrentSearch.getCountryOFResidance());

		driver.findElement(By.name("H_Loc")).sendKeys(CurrentSearch.getLocation().trim());
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("document.getElementById('hid_H_Loc').setAttribute('value','" + CurrentSearch.getHiddenLoc().trim() + "');");
		Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioCommonPath() + "/BEC_CountryList.jpg", driver);

		// ************************* Calendar Function Validations *******************************************//
		String DefaultCheckinDate = driver.findElement(By.id("ho_departure")).getAttribute("value");
		String DefaultCheckOutDate = driver.findElement(By.id("ho_arrival")).getAttribute("value");

		new Select(driver.findElement(By.id("H_nights"))).selectByVisibleText(CurrentSearch.getNights().trim());
		String AdjustedCheckOutDate = driver.findElement(By.id("ho_arrival")).getAttribute("value");

		printer.append("<tr><td>1</td><td>Calendar Default Check in date</td>");
		printer.append("<td>" + Calender.getDate(Calendar.DATE, Integer.parseInt(PropertySet.get("Portal.Cutoff")), "dd MMM yyyy") + "</td>");

		if (Calender.getDate(Calendar.DATE, Integer.parseInt(PropertySet.get("Portal.Cutoff")), "MM/dd/yyyy").equalsIgnoreCase(DefaultCheckinDate)) {
			printer.append("<td>" + DefaultCheckinDate + "</td>");
			printer.append("<td class='Passed'>PASS</td>");
		} else {
			printer.append("<td>" + DefaultCheckOutDate + "</td>");
			printer.append("<td class='Failed'>FAIL</td>");

		}

		printer.append("<tr><td>1</td><td>Calendar Default Check out date</td>");
		printer.append("<td>" + Calender.getDate(Calendar.DATE, Integer.parseInt(PropertySet.get("Portal.Cutoff")) + 1, "dd MMM yyyy") + "</td>");

		if (Calender.getDate(Calendar.DATE, Integer.parseInt(PropertySet.get("Portal.Cutoff")) + 1, "MM/dd/yyyy").equalsIgnoreCase(DefaultCheckOutDate)) {
			printer.append("<td>" + DefaultCheckOutDate + "</td>");
			printer.append("<td class='Passed'>PASS</td>");
		} else {
			printer.append("<td>" + DefaultCheckOutDate + "</td>");
			printer.append("<td class='Failed'>FAIL</td>");

		}

		printer.append("<tr><td>1</td><td>Calendar Adjusted Check out date</td>");
		printer.append("<td>" + Calender.getDate(Calendar.DATE, Integer.parseInt(PropertySet.get("Portal.Cutoff")) + Integer.parseInt(CurrentSearch.getNights()), "dd MMM yyyy") + "</td>");

		if (Calender.getDate(Calendar.DATE, Integer.parseInt(PropertySet.get("Portal.Cutoff")) + Integer.parseInt(CurrentSearch.getNights()), "MM/dd/yyyy").equalsIgnoreCase(AdjustedCheckOutDate)) {
			printer.append("<td>" + AdjustedCheckOutDate + "</td>");
			printer.append("<td class='Passed'>PASS</td>");
		} else {
			printer.append("<td>" + AdjustedCheckOutDate + "</td>");
			printer.append("<td class='Failed'>FAIL</td>");

		}

		printer.append("</table>");

		// ************************* Calendar Function Validations Ends *******************************************//
		((JavascriptExecutor) driver).executeScript("$('#ho_departure').val('" + CurrentSearch.getArrivalDate().trim() + "');");
		((JavascriptExecutor) driver).executeScript("$('#ho_arrival').val('" + CurrentSearch.getCheckOutDate().trim() + "');");

		// new Select(driver.findElement(By.id("H_nights"))).selectByVisibleText(CurrentSearch.getNights().trim());
		new Select(driver.findElement(By.id("norooms_H"))).selectByVisibleText(CurrentSearch.getRooms());

		for (int i = 1; i <= Integer.parseInt(CurrentSearch.getRooms()); i++) {
			String Adults = "";
			String Child = "";
			try {
				Adults = CurrentSearch.getAdults()[i - 1];
				Child = CurrentSearch.getChildren()[i - 1];
			} catch (Exception e) {
				// TODO: handle exception
			}

			new Select(driver.findElement(By.id("R" + i + "occAdults_H"))).selectByVisibleText(Adults);
			new Select(driver.findElement(By.id("R" + i + "occChildren_H"))).selectByVisibleText(Child);

			int NOC = Integer.parseInt(CurrentSearch.getChildren()[i - 1]);
			String[] CAges = CurrentSearch.getChildAges().get(i);

			if (NOC != 0) {

				if (NOC == CAges.length) {
					for (int k = 0; k < CAges.length; k++) {
						new Select(driver.findElement(By.id("H_R" + i + "childage_" + (k + 1)))).selectByVisibleText(CAges[k].trim());
					}
				} else

					System.out.println("Problem with child ages room " + i);

			}

		}

		PerfomanceSingleton perf = PerfomanceSingleton.getInstance();
		double StartTime = System.currentTimeMillis();
		((JavascriptExecutor) driver).executeScript("search('H');");

		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
		Thread.sleep(3000);

		try {
			driver.switchTo().defaultContent();
			// driver.switchTo().frame("live_message_frame");
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("live_message_frame"));
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("hotel_1")));

			double EndTime = System.currentTimeMillis();
			perf.setTimeTakenToDisplayResults((EndTime - StartTime) / 1000);

			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			ResultObject.setAvailabilityStatus(true);
			System.out.println("Results Available..");
			String NumberOfHotels = driver.findElement(By.className("pagination_itom")).getText().split(" ")[6];
			ResultObject.setNumberOfHotels(Integer.parseInt(NumberOfHotels));

			// ====================================================================//
			int PageHotelCount = 0;
			ForLoop: for (int count = 1; count <= ResultObject.getNumberOfHotels(); count++) {
				// String Title;
				// String HiddenID;

				try {
					driver.findElement(By.id("hotel_id_" + Integer.toString(count))).getAttribute("value");
					PageHotelCount++;
				} catch (Exception e) {
					break ForLoop;
				}
			}

			ResultObject.setItemsPerPage(PageHotelCount);
			// ====================================================================//

		} catch (Exception e) {

			logger.fatal("Results Not Available-->", e);
			Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getBasicFailedPath() + "/ResultsNotAvailable.jpg", driver);
			ResultObject.setAvailabilityStatus(false);
			ResultObject.setNumberOfHotels(0);

			try {
				ResultObject.setErrorMessage(driver.findElement(By.id("result_available_message")).getText());
			} catch (Exception e2) {
				ResultObject.setErrorMessage("-");
			}
		}

		return ResultObject;

		/*
		 * Iterator<Map.Entry<Integer, String> > iterator = Repository.OccupancyMap.entrySet().iterator();
		 * 
		 * while (iterator.hasNext()) { Map.Entry<Integer, String> entry = (Map.Entry<Integer, String>)iterator.next(); String[] occupancy = entry.getValue().split(""); String NumberOfAdults =
		 * occupancy[0]; String NumberOfChilds = occupancy[1];
		 * 
		 * String AdultID = "R"+Integer.toString(entry.getKey()+1)+"occAdults_H"; String ChildID = "R"+Integer.toString(entry.getKey()+1)+"occChildren_H";
		 * 
		 * new Select(driver.findElement(By.id(AdultID))).selectByVisibleText( NumberOfAdults); new Select(driver.findElement(By.id(ChildID))).selectByVisibleText (NumberOfChilds);
		 * 
		 * for(int Count = 1 ; Count <= Integer.parseInt(NumberOfChilds);Count++) { new Select (driver.findElement (By.id("H_R1childage_"+Integer.toString(Count)))).selectByVisibleText ("7"); }
		 * 
		 * 
		 * 
		 * }
		 * 
		 * driver.findElement(By.id("search_btns_h")).click();
		 */

	}

	public ResultsType getHotelAvailability(WebDriver driver, SearchType Search, ResultsType result) {
		try {
			WebElement HotelElement;
			int hotelPage = 1;

			ExitForLoop: for (int count = 1; count <= result.getNumberOfHotels(); count++) {
				String Title;
				String HiddenID;

				try {
					HiddenID = driver.findElement(By.id("hotel_id_" + Integer.toString(count))).getAttribute("value");
				} catch (Exception e) {
					if (!(count == result.getNumberOfHotels())) {
						int page = hotelPage + 1;
						((JavascriptExecutor) driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':" + page + "},'','WJ_2'));");
						hotelPage++;
						HiddenID = driver.findElement(By.id("hotel_id_" + Integer.toString(count))).getAttribute("value");
					} else {
						result.setHotelFound(false);
						break ExitForLoop;
					}
				}

				Title = driver.findElement(By.id("title_" + HiddenID)).getText();
				System.out.println(Title);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				if (Title.trim().equalsIgnoreCase(Search.getHotelName())) {
					HotelElement = driver.findElement(By.id("hotel_" + Integer.toString(count)));
					result.setHotelElement(HotelElement);
					try {

						result.setHiddenId(HiddenID);
						result.setHotelFound(true);
						result.setHotelName(Title.trim());
						result.setHotelAddress(driver.findElement(By.id("address_" + HiddenID)).getText());
						result.setHotelCurrency(driver.findElement(By.id("ratebox_" + HiddenID + "-" + count)).getText().split(" ")[0]);
						result.setStarRating(driver.findElement(By.xpath(".//*[@id='hotel_" + count + "']/tbody/tr[1]/th/div")).getAttribute("class").split(" ")[0].replace("hotel_star", ""));
						result.setTotalRate(driver.findElement(By.id("ratebox_" + HiddenID + "-" + count)).getText());
						logger.info("Hotel Block Major details extraction Success");
					} catch (Exception e) {
						logger.info("Hotel Block Major details extraction Failed " + e.getMessage());
					}

					// Checking the extra properties
					driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
					// Checking Whether a featured property
					for (int i = 3; i < 6; i++) {
						try {
							WebElement element = HotelElement.findElement(By.xpath("./tbody/tr[1]/th/p[" + i + "]/img"));
							String Property = element.getAttribute("title");
							if (Property.trim().equalsIgnoreCase("Featured Hotel"))
								result.setFeaturedProperty(true);
							else {
								if (Property.trim().equalsIgnoreCase("Deal of the Day"))
									result.setDealOfTheDay(true);
								else {
									if (Property.trim().equalsIgnoreCase("Best Rate"))
										result.setBestRate(true);
								}

							}

						} catch (Exception e) {
							logger.warn("Dont Panic", e);
						}

					}
					// =======================================================================================//

					// //////////////////////////////////////////////////
					driver.switchTo().defaultContent();
					driver.switchTo().frame("live_message_frame");
					// //////////////////////////////////////////////////

					if (driver.findElement(By.id("availability_" + HiddenID)).getText().equalsIgnoreCase("Available"))
						result.setHotelStatus("Available");
					else if (driver.findElement(By.id("availability_" + HiddenID)).getText().equalsIgnoreCase("OnRequest"))
						result.setHotelStatus("OnRequest");

					result.setNumberOfResultsRooms(Integer.toString(HotelElement.findElements(By.className("row")).size()));

					try {
						HotelElement.findElement(By.linkText("Show/Hide More Room Types")).click();
						result.setMoreRoomButtonAvailable(true);
						Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioCommonPath() + "/ExpandedRoomList.jpg", driver);
						if (HotelElement.findElement(By.linkText("Show/Hide More Room Types")).isDisplayed())
							result.setHideRoomButtonAvailable(false);
					} catch (Exception e) {
						result.setMoreRoomButtonAvailable(false);
						Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioFailedPath() + "/MoreRoomError.jpg", driver);
					}

					ArrayList<ArrayList<ResultsRoom>> roomArrayList = new ArrayList<ArrayList<ResultsRoom>>(Integer.parseInt(Search.getRooms()));
					ArrayList<WebElement> DailyRateWindows = new ArrayList<WebElement>(HotelElement.findElements(By.className("load_rate_modal_window")));
					TreeMap<String, String> SelectedRooms = (TreeMap<String, String>) Search.getSelectedRooms();

					int Count = 0;
					while (isElementPresent(By.id("roominfo_" + HiddenID + "-" + count + "_" + Count))) {
						ResultsRoom Room = new ResultsRoom();
						// Radio_HT_3214_0
						Room.setRoomString(driver.findElement(By.id("roominfo_" + HiddenID + "-" + count + "_" + Count)).getText());
						Room.setRoomType(driver.findElement(By.id("room_" + HiddenID + "-" + count + "_" + Count)).getText());
						Room.setPromotionApplied(isElementPresent(By.xpath(".//*[@id='room_" + HiddenID + "-" + count + "_" + Count + "']/img"), driver));

						if (Room.isPromotionApplied())
							Room.setPromoNote(driver.findElement(By.xpath(".//*[@id='room_" + HiddenID + "-" + count + "_" + Count + "']/img")).getAttribute("title"));

						Room.setBedType(driver.findElement(By.id("bed_" + HiddenID + "-" + count + "_" + Count)).getText());
						Room.setRatePlan(driver.findElement(By.id("rateplan_" + HiddenID + "-" + count + "_" + Count)).getText());
						Room.setRoomRate(driver.findElement(By.id("HT_rate_" + HiddenID + "-" + count + "_" + (Count + 1))).getText());

						try {
							DailyRateWindows.get(Count).click();
							if (driver.findElement(By.id("Drate_HT_" + HiddenID + "-" + count + "_" + (Count + 1))).isDisplayed())
								Room.setDailyRatePopUpAvailable(true);
							Room.setDailyRatsses(getDailyRates(driver.findElement(By.id("Drate_HT_" + HiddenID + "-" + count + "_" + (Count + 1))), Search.getNights()));
							((JavascriptExecutor) driver).executeScript("$('#Drate_HT_" + HiddenID + "-" + count + "_" + (Count + 1) + "').toggle(); return false;");

						} catch (Exception e) {
							logger.fatal("Issue when extracting the daily rates" + e.toString());
							Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioFailedPath() + "/DailyRateWindowIssue.jpg", driver);
						}

						if (driver.findElement(By.id("Radio_HT_" + HiddenID + "-" + count + "_" + Count)).isSelected()) {
							// Integer.parseInt(Room.getRoomString().split(" ")[1])
							result.addToDefaultRooms(Room.getRoomString().split(" ")[1], Room.getRoomType() + "/" + Room.getBedType() + "/" + Room.getRatePlan());
						}

						try {
							roomArrayList.get(Integer.parseInt(Room.getRoomString().split(" ")[1]) - 1);
						} catch (Exception e) {
							roomArrayList.add(Integer.parseInt(Room.getRoomString().split(" ")[1]) - 1, new ArrayList<ResultsRoom>());
						}

						String RoomNumber = Room.getRoomString().split(" ")[1].trim();
						String SelectedRoomString = SelectedRooms.get(RoomNumber);
						String ActualRoomString = Room.getRoomType() + "/" + Room.getBedType() + "/" + Room.getRatePlan();

						try {
							logger.info("Actual room ->" + ActualRoomString + " Room To be selected-->" + SelectedRoomString);
							if ((SelectedRoomString.trim().equalsIgnoreCase(ActualRoomString.trim()))) {
								logger.info("Rooms Matched Clicking on check box");
								driver.findElement(By.id("Radio_HT_" + HiddenID + "-" + count + "_" + Count)).click();
							}
						} catch (Exception e) {
							logger.fatal(e.toString());
						}

						roomArrayList.get(Integer.parseInt(RoomNumber) - 1).add(Room);
						Count++;
					}
					result.setAdjustedRate(driver.findElement(By.id("ratebox_" + HiddenID + "-" + count)).getText());

					for (int i = 0; i < roomArrayList.size(); i++) {
						result.addToActualMap(Integer.toString(i + 1), roomArrayList.get(i));

					}

					CancellationPolicy ResultsPolicy = new CancellationPolicy();
					try {
						driver.findElement(By.id("cancellationpolicy_" + HiddenID)).click();

						WebElement PolicyElement = driver.findElement(By.className("cxl_policy"));
						new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("cxl_policy_main_WJ_28")));

						try {
							WebElement element = PolicyElement.findElement(By.id("cxl_policy_main_WJ_28"));
							Thread.sleep(2000);
							result.setRP_Can_PopAvailable(element.isDisplayed());
							System.out.println("Cancellation --->" + result.isRP_Can_PopAvailable());
							Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioCommonPath() + "/CancellationPopUp.jpg", driver);
							ArrayList<WebElement> PolicyElements = new ArrayList<WebElement>(element.findElements(By.tagName("div")));
							System.out.println(element.getText());
							ResultsPolicy.setNumberofpolicies(Integer.toString(PolicyElements.size()));

							for (Iterator<WebElement> iterator = PolicyElements.iterator(); iterator.hasNext();) {
								WebElement webElement = (WebElement) iterator.next();
								ResultsPolicy.addPolicies(webElement.getText().trim());
							}
						} catch (Exception e) {
							result.setRP_Can_PopAvailable(false);
							Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioFailedPath() + "/CancellationPopUpNotAvailable.jpg", driver);
						}

						((JavascriptExecutor) driver).executeScript("$('.close_modal_window').click();");
						Thread.sleep(3000);
					} catch (Exception e) {
						result.setRP_Can_PopAvailable(false);
						Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioFailedPath() + "/CancellationPopUpNotAvailable.jpg", driver);
						logger.fatal("Exception Thrown When Extracting Results page Cancellation Policies");
					}

					result.setResultsPageCancellation(ResultsPolicy);
					break ExitForLoop;
				}
			}
		} catch (Exception e) {
			logger.fatal("Error in Results Page " + e.toString());
		}
		return result;
	}

	public ResultsType getHotelRatesWeb(WebDriver driver, RoomType roomType, ResultsType result) {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// int HotelPages = result.getResultsPages();
		int hotelPage = 1;

		ExitForLoop: for (int count = 1; count <= result.getNumberOfHotels(); count++) {
			String Title;
			// String HiddenID;

			List<WebElement> HotelList = driver.findElements(By.className("result-container"));
			Iterator<WebElement> hotelItertoIterator = HotelList.iterator();
			int IteratorCount = 0;

			while (hotelItertoIterator.hasNext()) {
				IteratorCount++;
				WebElement CurrentHotel = hotelItertoIterator.next();
				count++;

				if (IteratorCount == 1)
					Title = CurrentHotel.findElement(By.xpath("/html/body/div[11]/div/div[2]/div[2]/div[5]/div[7]/div/div/div[5]/div[2]/div")).getText();
				else
					Title = CurrentHotel.findElement(By.xpath("/html/body/div[11]/div/div[2]/div[2]/div[5]/div[7]/div[" + Integer.toString(IteratorCount) + "]/div/div[5]/div[2]/div")).getText();

				System.out.println(Title);

				if (Title.trim().equalsIgnoreCase(PropertySet.get("Ocupancy.Hotel").trim())) {
					ArrayList<String> Rooms = new ArrayList<String>();
					boolean isRoomAvailable = true;
					int index = 0;

					result.setHotelFound(true);

					try {
						driver.findElement(By.id("show_rooms_d_".concat(Integer.toString(count - 1)))).click();
					} catch (Exception e) {
						// TODO: handle exception
					}

					List<WebElement> RoomTypes = CurrentHotel.findElements(By.className("roomtype-col-type-data"));
					// List<WebElement> RateList =
					// CurrentHotel.findElements(By.className("roomtype-col-price-data"));

					/*
					 * result.setDefaultRoom(RoomTypes.get(0).getText().split("-" )[0] .trim()); result.setDefaultRate(driver.findElement( By.id("ratebox_" + (count - 1))).getText());
					 */

					while (isRoomAvailable) {
						try {
							Rooms.add(RoomTypes.get(index).getText().split("-")[0].trim());
							index++;
						} catch (Exception e) {
							isRoomAvailable = false;
						}
					}
					// result.setAvailableRooms(Rooms);
					break ExitForLoop;

				}

			}

			if (!(count == result.getNumberOfHotels())) {
				int page = hotelPage + 1;
				((JavascriptExecutor) driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':" + page + "},'','WJ_2'));");
				hotelPage++;
				// HiddenID =
				// driver.findElement(By.id("hotel_id_"+Integer.toString(count))).getAttribute("value");
			} else {
				result.setHotelFound(false);
				break ExitForLoop;
			}
		}

		return result;

	}

	/*
	 * public Map<String, String> getResultsPageRates() { Map<String, String> map = new HashMap<String, String>();
	 * 
	 * //for(int i=0 ) }
	 */
	// TODO:Need to implement this for two hotels

	public CartDetails addToCart(WebDriver driver, WebElement HotelElement) {

		StopWatch st = new StopWatch();
		CartDetails cart = new CartDetails();
		try {
			Thread.sleep(4000);
			driver.switchTo().defaultContent();
			// driver.switchTo().frame("live_message_frame");
			new WebDriverWait(driver, 30).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("live_message_frame"));
			HotelElement.findElement(By.linkText("Select this hotel")).click();
			st.start();

			try {
				driver.switchTo().defaultContent();
				new WebDriverWait(driver, 20).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("live_message_frame"));
				new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(By.id("remove_h_0")));
				Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioCommonPath() + "/AddedToCart.jpg", driver);
				cart.setIsAddedToCart(true);
				st.stop();
			} catch (Exception ex) {
				try {
					Thread.sleep(4000);
					new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("dialog-warning-message-WJ_22")));
					Thread.sleep(2000);
					if (driver.findElement(By.id("dialog-warning-message-WJ_22")).isDisplayed()) {
						WebElement RateChangeElement = driver.findElement(By.id("dialog-warning-message-WJ_22"));
						cart.setRateChangeAlertAvailable(true);
						Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioFailedPath() + "/RateChangeAlert.jpg", driver);
						// cart.setRateChangeTitle(RateChangeElement.findElement(By.id("ui-dialog-title")).getText());
						cart.setRateChangeText(RateChangeElement.getText());
						st.stop();
						Thread.sleep(2000);

						try {
							if (driver.findElements(By.className("ui-button")).get(0).findElement(By.className("ui-button-text")).getText().trim().equalsIgnoreCase("yes"))
								driver.findElements(By.className("ui-button")).get(0).click();
							else
								driver.findElements(By.className("ui-button")).get(1).click();
							driver.switchTo().defaultContent();

							new WebDriverWait(driver, 20).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("live_message_frame"));
							new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("remove_h_0")));
							cart.setIsAddedToCart(true);
							st.stop();
						} catch (Exception e) {
							logger.fatal("Rate Change Message Box Did not closed properly " + e.toString());
							Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioFailedPath() + "/RateGotChangedBoxNotClosed.jpg", driver);

						}

					} else {
						cart.setRateChangeAlertAvailable(false);
					}
				} catch (Exception e) {
					cart.setRateChangeAlertAvailable(false);

					try {
						if (driver.findElement(By.id("dialog-error-message-WJ_21")).isDisplayed()) {
							cart.setRateGotChangeAlertAvailable(true);
							Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioFailedPath() + "/RateGotChanged.jpg", driver);
							cart.setRateGotChangeText(driver.findElement(By.id("dialog-error-message-WJ_21")).getText());
							cart.setIsAddedToCart(false);
						} else {
							cart.setRateGotChangeAlertAvailable(false);
							cart.setIsAddedToCart(false);
						}
					} catch (Exception e2) {
						cart.setRateGotChangeAlertAvailable(false);
						cart.setIsAddedToCart(false);
					}
				}
			}

			try {

				if (cart.isIsAddedToCart()) {
					PerfomanceSingleton perf = PerfomanceSingleton.getInstance();
					perf.setTimeTakenToAddToCart(st.getTime() / 1000);
					cart.setHotelLable(driver.findElement(By.id("hotelname_0")).getText().trim());
					cart.setRate(driver.findElement(By.id("rate_h_0")).getText().trim());
					cart.setSubTotal(driver.findElement(By.id("cart_sub_total")).getText().trim());
					cart.setTaxes(driver.findElement(By.id("cart_tax")).getText().trim());
					cart.setTotaPaybel(driver.findElement(By.id("cart_totalpayable")).getText().trim());
					cart.setCartCurrency(driver.findElement(By.xpath(".//*[@id='cart_display_WJ_16']/div/table/tbody/tr[3]/td[1]")).getText().split(" ")[2].replace(")", "").replace("(", "").trim());
				}

			} catch (Exception ex) {

				cart.setIsAddedToCart(false);
				logger.fatal("Issue When Adding to cart " + ex.toString());
				Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioFailedPath() + "/NotAddedToCart.jpg", driver);

			}

		} catch (Exception e) {
			logger.fatal("Issue When Adding to cart may be select this hotel option not available " + e.toString());
			Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getScenarioFailedPath() + "/NotAddedToCart.jpg", driver);
		}
		return cart;

	}

	public SearchAgainBEC getSearchAgainBEC(WebDriver driver) {
		SearchAgainBEC SeachAgain = new SearchAgainBEC();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		driver.switchTo().frame("bec_container_frame");

		try {
			new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.id("H_Loc")));
			SeachAgain.setBECAvailable(true);
		} catch (Exception e) {
			SeachAgain.setBECAvailable(false);
		}

		if (SeachAgain.isBECAvailable()) {
			try {

				SeachAgain.setCORAvailable(driver.findElement(By.id("H_Country")).isDisplayed());
				SeachAgain.setDestination(getElementValue(By.id("H_Loc"), driver));
				SeachAgain.setCheckin(getElementValue(By.id("ho_departure_temp"), driver));
				SeachAgain.setCheckout(getElementValue(By.id("ho_arrival_temp"), driver));
				SeachAgain.setNights(getSelectedOptionValue(By.id("H_nights"), driver));
				SeachAgain.setRooms(getSelectedOptionValue(By.id("norooms_H"), driver));
				SeachAgain.setStarRating(getSelectedOptionValue(By.id("star_rating_H"), driver));
				SeachAgain.setHotelType(getSelectedOptionValue(By.id("hType_h"), driver));
				SeachAgain.setPrefferedCurrency(getSelectedOptionValue(By.id("H_consumerCurrencyCode"), driver));
				SeachAgain.setHotelName(getElementValue(By.id("H_name"), driver));
				SeachAgain.setPriceLevelFrom(getElementValue(By.id("priceLevelFrom_H"), driver));
				SeachAgain.setPriceLevelTo(getElementValue(By.id("priceLevelTo_H"), driver));
				SeachAgain.setPromocode(getElementValue(By.id("discountCoupon_No_H"), driver));
				SeachAgain.setAvailableChecked(driver.findElement(By.id("availableHotels_H")).isSelected());
				SeachAgain.setOnRequestChecked(driver.findElement(By.id("onRequestHotels_H")).isSelected());
				SeachAgain.setSearchButtonAvailable(driver.findElement(By.id("search_btns_h")).isDisplayed());

				// **************** Occupancy Test *****************************//

				driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
				int Rooms = 0;
				String[] Adults = new String[4];
				String[] Childs = new String[4];

				Map<Integer, String[]> childages = new HashMap<Integer, String[]>();

				RoomIter: for (int i = 1; i <= 4; i++) {
					try {
						String RoomAdult = new Select(driver.findElement(By.id("R" + i + "occAdults_H"))).getFirstSelectedOption().getText();
						Rooms++;
						Adults[i] = RoomAdult;
						String RoomChild = new Select(driver.findElement(By.id("R" + i + "occChildren_H"))).getFirstSelectedOption().getText();
						Childs[i] = RoomChild;

						String[] ChildAge = new String[4];

						AgeIter: for (int j = 1; j <= 4; j++) {
							try {
								String Age = new Select(driver.findElement(By.id("H_R" + i + "childage_" + j))).getFirstSelectedOption().getText();
								ChildAge[j - 1] = Age;
							} catch (Exception e) {
								break AgeIter;
							}
						}
						childages.put(i, ChildAge);
					} catch (Exception e) {
						break RoomIter;
					}
				}
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

				SeachAgain.setAdults(Adults);
				SeachAgain.setChildren(Childs);
				SeachAgain.setChildAges(childages);
				SeachAgain.setRoomsAvailableForOccupancy(Rooms);
				// **************** Occupancy Test Ends*****************************//
			} catch (Exception e) {

				logger.fatal("Exception Occured when reading Search Again BEC Details " + e.toString());
			}
		}

		return SeachAgain;
	}

	public String getPolicy(WebDriver driver, WebElement HotelElement) {
		HotelElement.findElement(By.className("cclink")).click();

		try {
			driver.switchTo().frame("_iframe-test");
			String CancellationDeadline = driver.findElement(By.className("canceltd")).findElement(By.tagName("td")).getText().replace("Cancellation Deadline : ", "");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("hotelsresultsRow");
			driver.findElement(By.xpath(".//*[@id='test']/div[1]/div/img")).click();
			return CancellationDeadline;

		} catch (Exception e) {
			System.out.println("Cancellation Policy Link Not Loading or No Cancellation Policy displayed");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("hotelsresultsRow");
			driver.findElement(By.className("drag-controls")).click();

			return null;
		}

	}

	public boolean isElementPresent(By by, WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {

			driver.findElement(by);
			return true;
		} catch (Exception ex) {
			return false;
		} finally {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}
	}

	public String getStarRatingLink(WebDriver driver, WebElement HotelElement) {
		String StarString;
		try {
			StarString = HotelElement.findElement(By.className("links")).findElement(By.tagName("img")).getAttribute("src");
			StarString = StarString.replace("../../admin/images/", "").replace("EST.gif", "");

		} catch (Exception e) {

			StarString = null;
		}

		return StarString;

	}

	public boolean navigateToPaymentsPage(WebDriver driver) {
		logger.info("Navigating to payment page");
		driver.switchTo().defaultContent();
		((JavascriptExecutor) driver).executeScript("submitSelectedData()");

		try {
			driver.switchTo().frame("live_message_frame");
			driver.findElement(By.id("b_s_summery"));
			logger.info("Payment page loaded successfully");
			return true;
		} catch (Exception e) {
			logger.fatal("Error in loading  Payment page !!! " + e.toString());
			return false;
		}

	}

	public PaymentsPage getPayments_Pg_BK_Details(String numberofrooms) {

		logger.info("##--Extracting Payment Page Details--##");
		PaymentsPage paymentPg = new PaymentsPage();

		try {
			driver.findElement(By.id("b_s_summery"));
			paymentPg.setLoded(true);
		} catch (Exception e) {
			return paymentPg;
		}

		try {
			logger.info("Payment Page Booking Details Extraction");
			paymentPg.setOverrideOptionAvailable(driver.findElement(By.id("activate_mg")).isDisplayed());
			paymentPg.setPay_HotelName(driver.findElement(By.id("hotel_name")).getText().trim());
			paymentPg.setPay_Address(driver.findElement(By.id("hotel_address")).getText().trim());
			paymentPg.setPay_CheckIn(driver.findElement(By.id("checkin_h")).getText().trim());
			paymentPg.setPay_CheckOut(driver.findElement(By.id("checkout_h")).getText().trim());
			paymentPg.setPay_Nights(driver.findElement(By.id("nights_h")).getText().trim());
			paymentPg.setPay_Rooms(driver.findElement(By.id("rooms_h")).getText().trim());
			paymentPg.setPay_BookingStatus(driver.findElement(By.id("booktype_h")).getText().trim());
			paymentPg.setPay_CandellationDeadLine(driver.findElement(By.id("duedate_h")).getText().trim());
			paymentPg.setPay_SummeryHotelcurrency(driver.findElement(By.className("sell_curr")).getText().trim());
			logger.info("Payment Page Booking Details Extraction Successfull");
		} catch (Exception e) {
			logger.info("Payment Page Booking Details Extraction Failed " + e.toString());
		}

		// ********************* Getting Room Details
		// *************************************//

		ArrayList<RoomType> PaymentPageRoomList = new ArrayList<RoomType>(Integer.parseInt(numberofrooms));

		try {
			logger.info("Starting Payment Page Individual Room Details Extraction ");
			for (int i = 1; i <= Integer.parseInt(numberofrooms); i++) {
				RoomType room = new RoomType();
				room.setRoomLable(driver.findElement(By.id("roomdata_" + i)).getText());
				room.setRoomType(driver.findElement(By.id("roomtype_" + i)).getText());
				room.setRateType(driver.findElement(By.id("boardtype_" + i)).getText());
				room.setBedType(driver.findElement(By.id("bedtype_" + i)).getText());
				room.setTotalRate(driver.findElement(By.id("rate_" + i)).getText());
				PaymentPageRoomList.add(room);
			}
			logger.info("Payment Page Individual Room Details Extraction Successfull");
		} catch (Exception e) {
			logger.fatal("Payment Page Individual Room Details Extraction Failed " + e.toString());
		}

		paymentPg.setPay_RoomDetails(PaymentPageRoomList);

		try {
			logger.info("Payment Page Hotel Rate Detail Extraction Started");
			paymentPg.setPay_SubTotal(driver.findElement(By.id("subtotal_h")).getText().trim());
			paymentPg.setPay_totalTaxAnServiceCharges(driver.findElement(By.id("tax_h")).getText().trim());
			paymentPg.setPay_TotalHotelBookingValue(driver.findElement(By.id("totalbookval_h")).getText().trim());
			logger.info("Payment Page Hotel Rate Detail Extraction Success");

		} catch (Exception e) {
			logger.fatal("Payment Page Hotel Rate Extraction Extraction Failed " + e.toString());
		}

		// ****This Section Only Applied for Commssionable pay@site and Pay
		// Commission Hotels***//

		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		try {
			logger.info("Payment Page - Hotel Commissionable Paybles Extraction Started");
			paymentPg.setPay_HotelAmountpayablenow(driver.findElement(By.id("totalbookval_h")).findElement(By.xpath("../following-sibling::tr[1]/td[2]")).getText().trim());
			paymentPg.setPay_HotelAmountToPaidAtCheckIn(driver.findElement(By.id("totalbookval_h")).findElement(By.xpath("../following-sibling::tr[2]/td[2]")).getText().trim());
			logger.info("Payment Page - Hotel Commissionable Paybles Extraction Success");
		} catch (Exception e) {
			paymentPg.setPay_HotelAmountpayablenow("N/A");
			paymentPg.setPay_HotelAmountToPaidAtCheckIn("N/A");
			logger.info("Payment Page - Hotel Commissionable Paybles Not Available - May be net rate hotel");
		}
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		// ***************************************************************************************//

		// TODO: Total Value Currency
		// paymentPg.setPay_FinalSummerycurrency(driver.findElement(By.className(className)))

		try {
			logger.info("Payment Page Package Rate Detail Extraction Started");
			paymentPg.setPay_TotalGrossPackageBookingValue(driver.findElement(By.id("pkg_totalrate")).getText().trim());
			paymentPg.setPay_TotalTaxesAndOtherCharges(driver.findElement(By.id("tottaxamountdivid")).getText().trim());
			paymentPg.setPay_TotalBookingValue(driver.findElement(By.id("totchargeamt")).getText().trim());

			paymentPg.setPay_AmountbeingProcessedNow(driver.findElement(By.id("payNwpaymentsection")).getText().trim());
			logger.info("Payment Page Package Rate Detail Extraction Success");
		} catch (Exception e) {
			logger.info("Payment Page Package Rate Detail Extraction Failed " + e.toString());
		}

		try {
			paymentPg.setPay_AmountdueatCheckIn(driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[7]/td[2]")).getText().trim());
		} catch (Exception e) {
			paymentPg.setPay_AmountdueatCheckIn(driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[6]/td[2]")).getText().trim());
		}

		// Availability Of Discount Coupon
		if (driver.findElement(By.id("discount_coupon_user_id")).isDisplayed())
			paymentPg.setPay_DiscountFiledAvailable(true);
		else
			paymentPg.setPay_DiscountFiledAvailable(false);

		if (driver.findElement(By.id("discountcoupon_validate")).isDisplayed())
			paymentPg.setPay_DiscountActButtonAvailable(true);
		else
			paymentPg.setPay_DiscountActButtonAvailable(true);

		if (driver.findElement(By.name("managerCode")).isDisplayed())
			paymentPg.setPay_MngOverUserFiledAvailable(true);
		else
			paymentPg.setPay_MngOverUserFiledAvailable(false);

		if (driver.findElement(By.name("managerPsWd")).isDisplayed())
			paymentPg.setPay_MngOverPassFiledAvailable(true);
		else
			paymentPg.setPay_MngOverPassFiledAvailable(false);

		if (driver.findElement(By.id("activate_mg")).isDisplayed())
			paymentPg.setPay_MngOverActButtonAvailable(true);
		else
			paymentPg.setPay_MngOverActButtonAvailable(false);

		if (driver.findElement(By.id("activate_mg")).isDisplayed())
			paymentPg.setPay_MngOverActButtonAvailable(true);
		else
			paymentPg.setPay_MngOverActButtonAvailable(false);

		if (driver.findElement(By.id("remove")).isDisplayed())
			paymentPg.setPay_ProductRmButtonAvailable(true);
		else
			paymentPg.setPay_ProductRmButtonAvailable(false);

		if (driver.findElement(By.id("submit_quote")).isDisplayed())
			paymentPg.setPay_QuoteSaveButtonAvailable(true);
		else
			paymentPg.setPay_QuoteSaveButtonAvailable(false);

		if (driver.findElement(By.id("save_quoteation_send_yes")).isSelected())
			paymentPg.setPay_QuoteMailSentByDafault(true);
		else
			paymentPg.setPay_QuoteMailSentByDafault(false);

		if (driver.findElement(By.id("copy_hotel_yes_1")).isSelected())
			paymentPg.setPay_HtNotesCopiedInMailByDafault(true);
		else
			paymentPg.setPay_HtNotesCopiedInMailByDafault(false);

		try {
			if (driver.findElement(By.id("voucher_mail_send_yes")).isSelected())
				paymentPg.setPay_VoucherMailSentByDafault(true);
			else
				paymentPg.setPay_VoucherMailSentByDafault(true);

			if (driver.findElement(By.id("customer_mail_send_yes")).isSelected())
				paymentPg.setPay_CusMailSentByDafault(true);
			else
				paymentPg.setPay_CusMailSentByDafault(false);

			if (driver.findElement(By.id("supplier_mail_send_yes")).isSelected())
				paymentPg.setPay_VoucherMailSentByDafault(true);
			else
				paymentPg.setPay_VoucherMailSentByDafault(false);

			paymentPg.setPay_MailSendingOptionsAvailable(true);
		} catch (Exception e) {
			paymentPg.setPay_MailSendingOptionsAvailable(false);
		}

		CancellationPolicy PayPolicy = new CancellationPolicy();

		try {
			logger.info("Payment Page Cancellation Details Extraction Started..");
			WebElement CancellationElement = driver.findElement(By.id("cxlpolicy_h_0"));
			String CancellationPolicyInText = CancellationElement.getText();
			ArrayList<String> PolicyElements = new ArrayList<String>(Arrays.asList(CancellationPolicyInText.split("\n")));
			PayPolicy.setNumberofpolicies(Integer.toString(PolicyElements.size()));

			for (Iterator<String> iterator = PolicyElements.iterator(); iterator.hasNext();) {
				PayPolicy.addPolicies(iterator.next().trim());
			}

			try {
				PayPolicy.setRefundable(driver.findElement(By.className("DarkRed")).getText().trim());
			} catch (Exception e) {
				PayPolicy.setRefundable("Not Available");
			}

			try {
				PayPolicy.setDeadline(driver.findElement(By.id("cxlpolicyduedate_h_0")).getText().trim());
			} catch (Exception e) {
				PayPolicy.setDeadline("Not Available");
			}

			logger.info("Payment Page Cancellation Details Extraction Successfull");
		} catch (Exception e) {
			logger.fatal("Payment Page Cancellation Details Extraction Failed " + e.toString());
		}

		paymentPg.setPay_CancellationPolicy(PayPolicy);

		return paymentPg;
	}

	public PaymentsPage getPayments_Pg_OC_Lables(PaymentsPage paymentPg) {

		return paymentPg;
	}

	public PaymentsPage getPayments_Pg_OC_Details(PaymentsPage paymentPg) {

		return paymentPg;
	}

	public PaymentsPage fillPayments_Pg_OC_Details(SearchType search, PaymentsPage paymentPg) {
		logger.info("Payment Page Occupancy Details Filling Started....");
		HashMap<Integer, HashMap<String, Occupancy>> RoomOccMap = new HashMap<Integer, HashMap<String, Occupancy>>();
		HashMap<Integer, String> NameMap = new HashMap<Integer, String>();

		NameMap.put(1, "One");
		NameMap.put(2, "two");
		NameMap.put(3, "three");
		NameMap.put(4, "Four");
		NameMap.put(5, "Five");
		NameMap.put(6, "six");
		NameMap.put(7, "seven");

		int rooms = Integer.parseInt(search.getRooms());

		for (int i = 0; i < rooms; i++) {

			int RoomAdult = Integer.parseInt(search.getAdults()[i]);
			int RoomChild = Integer.parseInt(search.getChildren()[i]);
			HashMap<String, Occupancy> OccupancyDetailMap = new HashMap<String, Occupancy>();

			for (int j = 0; j < RoomAdult; j++) {

				String AdultFirstName = "R" + NameMap.get(i + 1) + "Adult" + NameMap.get(j + 1) + "First";
				String AdultLastName = "R" + NameMap.get(i + 1) + "Adult" + NameMap.get(j + 1) + "Last";

				Occupancy occ = new Occupancy();
				occ.setRoomLable("Room" + (i + 1));
				occ.setTitle("Mr");
				occ.setFirstName(AdultFirstName);
				occ.setLastName(AdultLastName);
				occ.setSmoking(true);
				occ.setWheelChairNeeded(true);
				occ.setHandicapped(true);

				new Select(driver.findElement(By.id("adult_title_0_" + i + "_" + j + ""))).selectByVisibleText("Mr");
				driver.findElement(By.id("adult_fname_0_" + i + "_" + j + "")).sendKeys(AdultFirstName);
				driver.findElement(By.id("adult_lname_0_" + i + "_" + j + "")).sendKeys(AdultLastName);
				driver.findElement(By.id("adult_smoking_y_0_" + i + "_" + j + "")).click();
				driver.findElement(By.id("adult_weelchair_0_" + i + "_" + j + "")).click();
				driver.findElement(By.id("adult_handycap_0_" + i + "_" + j + "")).click();

				OccupancyDetailMap.put(AdultFirstName + "/" + AdultLastName, occ);
				logger.info("Room " + i + " Adult " + j + "-->" + AdultFirstName + "/" + AdultLastName);
			}

			for (int j = 0; j < RoomChild; j++) {

				String ChildFirstName = "R" + NameMap.get(i + 1) + "Child" + NameMap.get(j + 1) + "First";
				String ChildLastName = "R" + NameMap.get(i + 1) + "Child" + NameMap.get(j + 1) + "Last";

				Occupancy occ = new Occupancy();
				occ.setRoomLable("Room" + (i + 1));
				occ.setTitle("Mst");
				occ.setFirstName(ChildFirstName);
				occ.setLastName(ChildLastName);
				occ.setSmoking(false);
				occ.setWheelChairNeeded(true);
				occ.setHandicapped(true);

				new Select(driver.findElement(By.id("child_title_0_" + i + "_" + j + ""))).selectByValue("Mst");
				driver.findElement(By.id("child_fname_0_" + i + "_" + j + "")).sendKeys(ChildFirstName);
				driver.findElement(By.id("child_lname_0_" + i + "_" + j + "")).sendKeys(ChildLastName);
				// driver.findElement(By.id("child_smoking_y_0_"+i+"_"+j+"")).click();
				driver.findElement(By.id("child_weelchair_0_" + i + "_" + j + "")).click();
				driver.findElement(By.id("child_handycap_0_" + i + "_" + j + "")).click();

				OccupancyDetailMap.put(ChildFirstName + "/" + ChildLastName, occ);
				logger.info("Room " + i + " Child " + j + "-->" + ChildFirstName + "/" + ChildLastName);
			}

			RoomOccMap.put((i + 1), OccupancyDetailMap);

		}

		paymentPg.setFilledOccuDetailsMap(RoomOccMap);

		try {
			logger.info("Setting Up Arrival Time");
			new Select(driver.findElement(By.id("arrivaltimehrs_0"))).selectByVisibleText("01");
			new Select(driver.findElement(By.id("arrivaltimemin_0"))).selectByVisibleText("30");

		} catch (Exception e) {
			logger.fatal("Issue in Setting Up Arrival Time", e);
		}
		ArrayList<WebElement> LableList = new ArrayList<WebElement>(driver.findElement(By.id("table_room_occupancy_details")).findElements(By.className("amo_width_16")));

		String Text = "";

		for (Iterator<WebElement> iterator = LableList.iterator(); iterator.hasNext();) {
			WebElement webElement = (WebElement) iterator.next();
			Text = Text + "/" + webElement.getText();
		}
		paymentPg.setPay_Occupancy_String(Text);

		return paymentPg;

	}

	public PaymentsPage getPayments_Pg_Pay_Details(PaymentsPage paymentPg) {

		logger.info("Payment Page Payment Details Extraction Started..");

		if (PropertySet.get("Portal.Online.Available").trim().equalsIgnoreCase("YES")) {
			logger.info("Portal Online Payment Option Set As  Available Extracting Online Payment Details");
			try {
				paymentPg.setPay_ON_DefaultSelectedCard(driver.findElement(By.id("credit_card_type")).findElement(By.tagName("option")).getAttribute("value"));
				paymentPg.setPay_ON_TotalGrossBookingValue(driver.findElement(By.id("pkg_depositval")).getText());
				paymentPg.setPay_ON_TotalTaxesAndFees(driver.findElement(By.id("totaltaxpgcurrid")).getText());
				paymentPg.setPay_ON_TotalPackageBookingValue(driver.findElement(By.id("totalchargeamtpgcurrid")).getText());
				paymentPg.setPay_ON_AmountBeingProcessedNow(driver.findElement(By.id("totalpaynowamountpgcurrid")).getText());

				// TODO: Change This Once Amount Due at check calculation done
				paymentPg.setPay_ON_AmountDueAtCheckIn(driver.findElement(By.id("totalamountatutilizationpgcurrid")).getText().trim());
				paymentPg.setPay_ON_CurrencyLable(driver.findElement(By.id("pkg_depositcur")).getText());
				paymentPg.setPay_ON_SummeryCurrency(driver.findElement(By.id("totchgpgcurrency")).getText());
				paymentPg.setPay_ON_SummeryValue(driver.findElement(By.id("totchgpgamt")).getText().trim());
			} catch (Exception e) {
				logger.info("Portal  Online Payment Details Extraction Failed " + e.toString());
			}

		}

		driver.findElement(By.id("pay_offline")).click();

		try {
			logger.info("Payment Page Offline Payment Details Extraction Started--->");
			Thread.sleep(4000);
			(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.id("payment_method_1")));
			paymentPg.setIsOfflinePaymentDetailsLoaded(true);
			logger.info("Payment Page Offline Payment Details Available");
			Thread.sleep(3000);

			try {
				paymentPg.setPay_Off_TotalGrossPackageBookingValue(driver.findElement(By.id("pkg_totalrate")).getText().trim());
				paymentPg.setPay_Off_TotalTaxesAndOtherCharges(driver.findElement(By.id("tottaxamountdivid")).getText().trim());
				paymentPg.setPay_Off_TotalBookingValue(driver.findElement(By.id("totchargeamt")).getText().trim());

				paymentPg.setPay_Off_AmountbeingProcessedNow(driver.findElement(By.id("payNwpaymentsection")).getText().trim());

				try {
					try {
						paymentPg.setPay_Off_AmountdueatCheckIn(driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[7]/td[2]")).getText().trim());
					} catch (Exception e) {
						paymentPg.setPay_Off_AmountdueatCheckIn(driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[6]/td[2]")).getText().trim());
					}
				} catch (Exception e) {
					paymentPg.setPay_Off_AmountdueatCheckIn("-");
				}

				paymentPg.setPay_OFF_Amount(driver.findElement(By.id("crdAmountDisplayOff_sell")).getAttribute("value").trim());
				paymentPg.setPay_OFF_Currency((driver.findElement(By.id("table_offline_payment_details")).findElements(By.className("bluetext")).get(4).getText()));
			} catch (Exception e) {
				logger.fatal("Payment Page Offline Payment Details Extraction Failed", e);
			}
		} catch (Exception e) {
			paymentPg.setIsOfflinePaymentDetailsLoaded(false);
			logger.fatal("Payment Page Offline Payment Details Not Available/loaded", e);
		}

		return paymentPg;
	}

	public boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public ArrayList<String> getDailyRates(WebElement DailyRatesEle, String Nights) {
		ArrayList<String> Rates = new ArrayList<String>(Integer.parseInt(Nights));
		ArrayList<WebElement> trList = new ArrayList<WebElement>(DailyRatesEle.findElements(By.tagName("tr")));
		trList.remove(0);
		trList.remove(0);

		for (Iterator<WebElement> iterator = trList.iterator(); iterator.hasNext();) {
			WebElement webElement = (WebElement) iterator.next();

			ArrayList<WebElement> DayList = new ArrayList<WebElement>(webElement.findElements(By.tagName("td")));

			for (Iterator<WebElement> iterator2 = DayList.iterator(); iterator2.hasNext();) {
				WebElement DailyRateCell = (WebElement) iterator2.next();

				try {
					String Rate = DailyRateCell.getText().trim();

					if (!Rate.equals("null"))
						Rates.add(Rate);
				} catch (Exception e) {
					// TODO: handle exception
				}

			}

		}

		return Rates;

	}

	public ViewFullDetails getViewFullDetails(String numberofrooms) {
		logger.info("Starting View Full Details Details Extraction");
		ViewFullDetails view = new ViewFullDetails();
		driver.findElement(By.id("full_details")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");

		try {
			new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("ui-dialog-title-VCT"))));
			view.setLoded(true);
		} catch (Exception e) {
			logger.fatal("ViewFull Details Frame Loading Issue ", e);
			view.setLoded(false);
		}
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		if (driver.findElement(By.id("ui-dialog-title-VCT")).isDisplayed()) {

			try {
				view.setLoded(true);
				driver.switchTo().frame("productdetailsframe");
				view.setView_HotelName(driver.findElement(By.id("hotel_name")).getText().trim());
				view.setView_Address(driver.findElement(By.id("hotel_address")).getText().trim());
				view.setView_CheckIn(driver.findElement(By.id("checkin_h")).getText().trim());
				view.setView_CheckOut(driver.findElement(By.id("checkout_h")).getText().trim());
				view.setView_Nights(driver.findElement(By.id("nights_h")).getText().trim());
				view.setView_NumberOfRooms(driver.findElement(By.id("rooms_h")).getText().trim());
				view.setView_BookingStatus(driver.findElement(By.id("booktype_h")).getText().trim());
				view.setView_CancellationDeadLine(driver.findElement(By.id("duedate_h")).getText().trim());
				view.setView_Currency(driver.findElement(By.className("sell_curr")).getText().trim());
			} catch (Exception e) {
				logger.fatal("View Full Detail Booking Summery Extraction Issue", e);
			}

			// ********************* Getting Room Details
			// *************************************//
			ArrayList<RoomType> ViewFullList = new ArrayList<RoomType>(Integer.parseInt(numberofrooms));

			try {
				logger.info("Starting ViewFull Individual Room Details Extraction ");
				for (int i = 1; i <= Integer.parseInt(numberofrooms); i++) {
					RoomType room = new RoomType();
					room.setRoomLable(driver.findElement(By.id("roomdata_" + i)).getText());
					room.setRoomType(driver.findElement(By.id("roomtype_" + i)).getText());
					room.setRateType(driver.findElement(By.id("boardtype_" + i)).getText());
					room.setBedType(driver.findElement(By.id("bedtype_" + i)).getText());
					room.setTotalRate(driver.findElement(By.id("rate_" + i)).getText());
					ViewFullList.add(room);
				}

				view.setRoomDetails(ViewFullList);
				logger.info("ViewFull Individual Room Details Extraction Successfull");
			} catch (Exception e) {
				logger.fatal("ViewFull Individual Room Details Extraction Failed " + e.toString());
			}

			try {
				view.setView_SubTotal(driver.findElement(By.id("subtotal_h")).getText().trim());
				view.setView_totalTaxAnServiceCharges(driver.findElement(By.id("tax_h")).getText().trim());
				view.setView_TotalHotelBookingValue(driver.findElement(By.id("totalbookval_h")).getText().trim());

				// ****This Section Only Applied for Commssionable pay@site and
				// Pay Commission Hotels***//
				driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
				try {
					view.setView_HotelAmountpayablenow(driver.findElement(By.id("totalbookval_h")).findElement(By.xpath("../following-sibling::tr[1]/td[2]")).getText().trim());
					view.setView_HotelAmountToPaidAtCheckIn(driver.findElement(By.id("totalbookval_h")).findElement(By.xpath("../following-sibling::tr[2]/td[2]")).getText().trim());
				} catch (Exception e) {
					logger.info("Commissionable Payble Fileds Not Available May be hotel is Net Rate");
					view.setView_HotelAmountpayablenow("N/A");
					view.setView_HotelAmountToPaidAtCheckIn("N/A");
				}
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

				//

				view.setView_TotalGrossPackageBookingValue(driver.findElement(By.id("pkg_totalrate")).getText().trim());
				view.setView_TotalTaxesAndOtherCharges(driver.findElement(By.id("tottaxamountdivid")).getText().trim());
				view.setView_TotalBookingValue(driver.findElement(By.id("totchargeamt")).getText().trim());

				view.setView_AmountbeingProcessedNow(driver.findElement(By.id("payNwpaymentsection")).getText().trim());

				try {
					view.setView_AmountdueatCheckIn(driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[7]/td[2]")).getText().trim());
				} catch (Exception e) {
					view.setView_AmountdueatCheckIn(driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[6]/td[2]")).getText().trim());
				}

			} catch (Exception e) {
				logger.info("Issue with ViewFull details rate details extraction", e);
			}

			CancellationPolicy ViewFullPolicy = new CancellationPolicy();
			// ArrayList<WebElement> PolicyElements = new
			// ArrayList<WebElement>(driver.findElements(By.className("cnmsgbg")));
			WebElement CancellationElement = driver.findElement(By.id("cxlpolicy_h_0"));
			String CancellationPolicyInText = CancellationElement.getText();
			ArrayList<String> PolicyElements = new ArrayList<String>(Arrays.asList(CancellationPolicyInText.split("\n")));
			ViewFullPolicy.setNumberofpolicies(Integer.toString(PolicyElements.size()));

			for (Iterator<String> iterator = PolicyElements.iterator(); iterator.hasNext();) {
				ViewFullPolicy.addPolicies(iterator.next().trim());
			}

			try {
				ViewFullPolicy.setRefundable(driver.findElements(By.className("DarkRed")).get(1).getText().trim());
			} catch (Exception e) {
				ViewFullPolicy.setRefundable("Not Available");
			}

			try {
				ViewFullPolicy.setDeadline(driver.findElement(By.id("cxlpolicyduedate_h_0")).getText().trim());
			} catch (Exception e) {
				ViewFullPolicy.setDeadline("Not Available");
			}

			view.setCancellationPolicy(ViewFullPolicy);
			// driver.findElement(By.className("ui-dialog-titlebar-close")).click();

			try {
				driver.switchTo().defaultContent();
				driver.switchTo().frame("live_message_frame");
				driver.findElement(By.cssSelector("html body div.ui-dialog div.ui-dialog-titlebar a.ui-dialog-titlebar-close span.ui-icon")).click();

			} catch (Exception e) {
				((JavascriptExecutor) driver).executeScript("$('.ui-dialog-titlebar-close').click();");

			}

		}

		return view;
	}

	public PaymentsPage fillCompleteCustomerInfo(PaymentsPage PayPg) {

		logger.info("Filling Customer Info-->");
		driver.findElement(By.id("cusTitle")).sendKeys(PropertySet.get("CustomerTitle"));
		logger.info("CustomerTitle-->" + PropertySet.get("CustomerTitle"));
		driver.findElement(By.id("cusFName")).sendKeys(PropertySet.get("CustomerFname"));
		logger.info("Customer First Name-->" + PropertySet.get("CustomerFname"));
		driver.findElement(By.id("cusLName")).sendKeys(PropertySet.get("CustomerLastName"));
		logger.info("cusLName-->" + PropertySet.get("CustomerLastName"));
		driver.findElement(By.id("cusareacodetext")).sendKeys(PropertySet.get("CustomerTelephone").split("-")[0]);
		logger.info("cusareacodetext-->" + PropertySet.get("CustomerTelephone").split("-")[0]);
		driver.findElement(By.id("cusPhonetext")).sendKeys(PropertySet.get("CustomerTelephone").split("-")[1]);
		logger.info("cusPhonetext-->" + PropertySet.get("CustomerTelephone").split("-")[1]);
		driver.findElement(By.id("cusEmail")).sendKeys(PropertySet.get("CustomerEmail"));
		logger.info("CusEmail-->" + PropertySet.get("CustomerEmail"));
		driver.findElement(By.id("cusFax")).sendKeys(PropertySet.get("CustomerFax"));
		logger.info("CusFax-->" + PropertySet.get("CustomerFax"));
		driver.findElement(By.id("cusEmail2")).sendKeys(PropertySet.get("CustomerEmail2"));
		logger.info("CusEmail2-->" + PropertySet.get("CustomerEmail2"));
		driver.findElement(By.id("cusAdd1")).sendKeys(PropertySet.get("CustomerAddress1"));
		logger.info("CusAdd1-->" + PropertySet.get("CustomerAddress1"));
		driver.findElement(By.id("cusAdd2")).sendKeys(PropertySet.get("CustomerAddress2"));
		logger.info("CusAdd2-->" + PropertySet.get("CustomerAddress2"));
		driver.findElement(By.id("cusPhoneemgtext")).sendKeys(PropertySet.get("CustomerEmergency"));
		logger.info("CusPhoneemgtext-->" + PropertySet.get("CustomerEmergency"));

		PayPg.setAutoLoadedCityInCustomerInfo(new Select(driver.findElement(By.id("cusCountry"))).getFirstSelectedOption().getText());
		new Select(driver.findElement(By.id("cusCountry"))).selectByVisibleText(PropertySet.get("CustomerCountry"));
		logger.info("Cus Country-->" + PropertySet.get("CustomerCountry"));

		if (PropertySet.get("CustomerCountry").trim().equalsIgnoreCase("Australia") || PropertySet.get("CustomerCountry").trim().equalsIgnoreCase("USA") || PropertySet.get("CustomerCountry").trim().equalsIgnoreCase("USA")) {

			driver.findElement(By.id("cusState")).sendKeys(PropertySet.get("CustomerState"));
			driver.findElement(By.id("State_lkup")).click();
			// driver.switchTo().frame("lookup");
			((JavascriptExecutor) driver).executeScript("selectedRecord(0,'loadAutoFillSelect();');");
			driver.switchTo().defaultContent();

			driver.findElement(By.id("cusZip")).sendKeys(PropertySet.get("CustomerPostal"));
			logger.info("Cus Postal-->" + PropertySet.get("CustomerPostal"));

		} else {
			PayPg.setIsOfflinePaymentDetailsLoaded(driver.findElement(By.id("cusZip")).isDisplayed());
		}

		driver.findElement(By.id("cusCity")).sendKeys(PropertySet.get("CustomerCity"));
		logger.info("CustomerCity -->" + PropertySet.get("CustomerCity"));
		driver.findElement(By.id("City_lkup")).click();
		// driver.switchTo().frame("lookup");
		driver.findElement(By.className("top_li")).click();
		// ((JavascriptExecutor)driver).executeScript("selectedRecord(0,'loadAutoFillSelect();');");
		// driver.switchTo().defaultContent();

		return PayPg;
	}

	public void fillCustomerInfoByLookUp() {
		driver.findElement(By.id("cusFName")).sendKeys(PropertySet.get("CustomerFname"));
		driver.findElement(By.id("cusFName_lkup")).click();
		driver.switchTo().frame("lookup");
		((JavascriptExecutor) driver).executeScript("selectedRecord(0,'loadAutoFillSelect();');");
		driver.switchTo().defaultContent();

	}

	public void fillNotes(SearchType search, String HiddenId) {

		logger.info("Filling Notes------>");

		try {
			driver.findElement(By.id("txt_cus_notes")).sendKeys(search.getPay_CustomerNote());
			driver.findElement(By.id("intl_notes")).sendKeys(search.getPay_InternalNote());

		} catch (Exception e) {
			logger.fatal("Fill Notes", e);
		}

		try {

			String NoteElementId = "hotelnotes_" + HiddenId + "@@" + Calender.getDate("MM/dd/yyyy", "yyyy-MMM-dd", search.getArrivalDate()) + "@@" + PropertySet.get("Portal.Name").toLowerCase();
			driver.findElement(By.id(NoteElementId)).sendKeys(search.getPay_HotelNote());
			driver.findElement(By.id("copy_hotel_yes_1")).click();
			logger.info("Hotel Notes Added-->" + search.getPay_HotelNote());
		} catch (Exception e) {
			logger.fatal("Hotel Notes Adding Failed", e);
		}

	}

	public boolean CheckOut() {
		try {
			Thread.sleep(3000);
			driver.switchTo().defaultContent();
			new WebDriverWait(driver, 20).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("live_message_frame"));
			driver.findElement(By.id("loadpayment")).click();
			driver.switchTo().defaultContent();
			new WebDriverWait(driver, 20).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("live_message_frame"));
			driver.findElement(By.id("activate_mg"));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean RemoveFromCart(int index) {
		driver.findElement(By.id("remove_h_" + index)).click();

		try {
			new WebDriverWait(driver, 20).until(ExpectedConditions.invisibilityOfElementLocated(By.id("hotelname_" + index)));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public ResultsType ReservationConfirmation(ResultsType result, SearchType CurrentSearch) throws InterruptedException, IOException {

		logger.info("## -- Starting Confirmation Process -- ##");

		if (CurrentSearch.getPaymentType().equalsIgnoreCase("online")) {
			logger.info("Selected Payment Method : Online ");

			driver.findElement(By.id("pay_online")).click();
			try {
				logger.info("Getting Online Payment Details");
				new WebDriverWait(driver, 40).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("credit_card_type"))));
				new Select(driver.findElement(By.id("credit_card_type"))).selectByVisibleText(CurrentSearch.getCardType());
				Thread.sleep(3000);
				PaymentsPage paymentPg = result.getResultPayment();

				paymentPg.setPay_ShiftedCardType(driver.findElement(By.id("credit_card_type")).findElement(By.tagName("option")).getAttribute("value"));
				Thread.sleep(3000);
				paymentPg.setPay_CreditShift_TotalGrossPackageBookingValue(driver.findElement(By.id("pkg_totalrate")).getText().trim());
				paymentPg.setPay_CreditShift_TotalTaxesAndOtherCharges(driver.findElement(By.id("tottaxamountdivid")).getText().trim());
				paymentPg.setPay_CreditShift_TotalBookingValue(driver.findElement(By.id("totchargeamt")).getText().trim());

				paymentPg.setPay_CreditShift_AmountbeingProcessedNow(driver.findElement(By.id("payNwpaymentsection")).getText().trim());

				try {
					paymentPg.setPay_CreditShift_AmountdueatCheckIn(driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[7]/td[2]")).getText().trim());
				} catch (Exception e) {
					paymentPg.setPay_CreditShift_AmountdueatCheckIn(driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[6]/td[2]")).getText().trim());
				}

				paymentPg.setPay_ON_Shift_TotalGrossBookingValue(driver.findElement(By.id("pkg_depositval")).getText());
				paymentPg.setPay_ON_Shift_TotalTaxesAndFees(driver.findElement(By.id("totaltaxpgcurrid")).getText());
				paymentPg.setPay_ON_Shift_TotalPackageBookingValue(driver.findElement(By.id("totalchargeamtpgcurrid")).getText());
				paymentPg.setPay_ON_Shift_AmountBeingProcessedNow(driver.findElement(By.id("totalpaynowamountpgcurrid")).getText());
				paymentPg.setPay_ON_Shift_CurrencyLable(driver.findElement(By.id("pkg_depositcur")).getText());
				// paymentPg.setPay_ON_Shift_AmountBeingProcessedNow(driver.findElement(By.id("pkg_depositcur")).getText());
				paymentPg.setPay_ON_Shift_SummeryCurrency(driver.findElement(By.id("totchgpgcurrency")).getText());
				paymentPg.setPay_ON_Shift_SummeryValue(driver.findElement(By.id("totchgpgamt")).getText().trim());

				logger.info("Getting Online Payment Details Succeeded");
			} catch (Exception e) {
				logger.fatal("Payments Page Online Payment Details Extraction Issue", e);
			}
		} else {
			driver.findElement(By.id("pay_offline")).click();

			try {
				new Select(driver.findElement(By.id("payment_method_1"))).selectByVisibleText(CurrentSearch.getOfflineMethod().trim());
				new WebDriverWait(driver, 40).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("payment_reference"))));
				driver.findElement(By.id("payment_reference")).sendKeys(CurrentSearch.getPaymentRef());
			} catch (Exception e) {
				logger.fatal("Payments Page Offline Payment Details Issue", e);
			}

		}

		driver.findElement(By.id("cancellation")).click();

		try {
			driver.findElement(By.id("customer_mail_send_yes")).click();
			driver.findElement(By.id("voucher_mail_send_yes")).click();
			driver.findElement(By.id("supplier_mail_send_yes")).click();
		} catch (Exception e) {
			logger.fatal(e.toString());
		}

		if (PropertySet.get("Confirmation.Allowed").equalsIgnoreCase("true")) {
			logger.info("Confirmation Allowed Continueing with Reservation");
			driver.findElement(By.id("continue_submit")).click();
		} else {
			logger.info("Confirmation Not Allowed Returning Result");
			return result;
		}

		// TODO: Check For The Alerts

		try {
			PerfomanceSingleton perf = PerfomanceSingleton.getInstance();
			ConfirmationPage Conf = new ConfirmationPage();

			try {
				new WebDriverWait(driver, 80).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("confirmbooking"))));
			} catch (Exception e) {
				logger.fatal("Error Occured in intermediate Pop up", e);
				Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getBasicFailedPath() + "/IntermadiatePopUp.jpg", driver);
			}
			Thread.sleep(3000);

			if (driver.findElement(By.id("confirmbooking")).isDisplayed()) {
				result.setIntermediateFrameLoaded(true);
				result.setIntermediateMessage(driver.findElements(By.className("verify_message")).get(1).getText());
				result.setIntermediateStatus(driver.findElements(By.className("verify_status")).get(1).getText());
				driver.findElement(By.id("confirmbooking")).click();
				driver.switchTo().defaultContent();
				try {

					if (CurrentSearch.getPaymentType().equalsIgnoreCase("online")) {
						logger.info("Payment Gateway Called!!!");
						result.setGateWay(PayOnline());
					}

					double StartTime = System.currentTimeMillis();

					try {
						new WebDriverWait(driver, 30).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("live_message_frame")));
						new WebDriverWait(driver, 100).until(ExpectedConditions.presenceOfElementLocated(By.id("p_hotel_bookings_1")));
						Conf.setLoded(true);
					} catch (Exception e) {
						logger.fatal("Confirmation Page Not Loaded", e);
						Conf.setLoded(false);
					}

					double EndTime = System.currentTimeMillis();
					perf.setTimeTakenToConfirmation((EndTime - StartTime) / 1000);
					Conf.setConfirmationMessage(driver.findElement(By.id("p_hotel_bookings_1")).getText());
					Conf.setBookingNumber(driver.findElement(By.id("p_hotel_booking_reservation_no_1")).getText());

					String numberofrooms = CurrentSearch.getRooms();
					try {
						Conf.setCon_HotelName(driver.findElement(By.id("hotelname_0")).getText().trim());
						Conf.setCon_Address(driver.findElement(By.id("hoteladdress_0")).getText().trim());
						Conf.setCon_CheckIn(driver.findElement(By.id("hotelchkin_0")).getText().trim());
						Conf.setCon_CheckOut(driver.findElement(By.id("hotelchkout_0")).getText().trim());
						Conf.setCon_Nights(driver.findElement(By.id("hotelnonights_0")).getText().trim());
						Conf.setCon_Rooms(driver.findElement(By.id("hotelnorooms")).getText().trim());
						Conf.setCon_BookingStatus(driver.findElement(By.id("hotelstatus_0")).getText().trim());
						// Conf.setCon_CandellationDeadLine(driver.findElement(By.id("hotelduedate_0")).getText().trim());
						try {
							driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
							Conf.setCon_CandellationDeadLine(driver.findElement(By.id("hotelstatus_0")).findElement(By.xpath("../following-sibling::tr[1]/td[2]")).getText().trim());
						} catch (Exception e) {
							Conf.setCon_CandellationDeadLine(driver.findElement(By.id("hotelstatus_0")).findElement(By.xpath("../following-sibling::tr[1]/td[1]")).getText().trim());
						}
						driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
						Conf.setCon_SummeryHotelcurrency(driver.findElement(By.id("hotelcurrency_0")).getText().trim());
					} catch (Exception e) {
						logger.fatal("Issue with extarcting Confirmation Page Booking Details  " + e.toString());
					}

					// ../following-sibling::tr[2]/td[2]
					// ********************* Getting Room Details
					// *************************************//
					ArrayList<RoomType> PaymentPageRoomList = new ArrayList<RoomType>(Integer.parseInt(numberofrooms));

					try {
						logger.info("Extracting Confirmation Page Room Details");
						for (int i = 0; i < Integer.parseInt(numberofrooms); i++) {
							RoomType room = new RoomType();
							room.setRoomLable(driver.findElement(By.id("roominfo_0_" + i)).getText());
							room.setRoomType(driver.findElement(By.id("roomtype_0_" + i)).getText());
							room.setRateType(driver.findElement(By.id("broadtype_0_" + i)).getText());
							room.setBedType(driver.findElement(By.id("bedtype_0_" + i)).getText());
							room.setTotalRate(driver.findElement(By.id("sellrate_0_" + i)).getText());
							PaymentPageRoomList.add(room);
						}
					} catch (Exception e) {
						logger.fatal("Confirmation Page Room Details Issue", e);
					}

					Conf.setCon_RoomDetails(PaymentPageRoomList);

					try {
						Conf.setCon_SubTotal(driver.findElement(By.id("hotel_subtotal_0")).getText().trim());
						Conf.setCon_totalTaxAnServiceCharges(driver.findElement(By.id("hotel_tax_0")).getText().trim());
						Conf.setCon_TotalHotelBookingValue(driver.findElement(By.id("hotelbookingvalue_0")).getText().trim());

						driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
						try {
							Conf.setCon_HotelAmountpayablenow(driver.findElement(By.id("hotelbookingvalue_0")).findElement(By.xpath("../following-sibling::tr[1]/td[2]")).getText().trim());
							Conf.setCon_HotelAmountToPaidAtCheckIn(driver.findElement(By.id("hotelbookingvalue_0")).findElement(By.xpath("../following-sibling::tr[2]/td[2]")).getText().trim());
						} catch (Exception e) {
							Conf.setCon_HotelAmountpayablenow("N/A");
							Conf.setCon_HotelAmountToPaidAtCheckIn("N/A");
						}
						driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

						// TODO: Total Value Currency
						// paymentPg.setPay_FinalSummerycurrency(driver.findElement(By.className(className)))

						Conf.setCon_TotalGrossPackageBookingValue(driver.findElement(By.id("pkg_totalbeforetax")).getText().trim());
						Conf.setCon_TotalTaxesAndOtherCharges(driver.findElement(By.id("pkg_totaltaxandothercharges")).getText().trim());
						Conf.setCon_TotalBookingValue(driver.findElement(By.id("pkg_totalpayable")).getText().trim());

						Conf.setCon_AmountbeingProcessedNow(driver.findElement(By.xpath(".//*[@id='main_content']/div/div/div/table[2]/tbody/tr/td/table/tbody/tr[4]/td[2]")).getText().trim());
						Conf.setCon_AmountdueatCheckIn(driver.findElement(By.xpath(".//*[@id='main_content']/div/div/div/table[2]/tbody/tr/td/table/tbody/tr[5]/td[2]")).getText().trim());

					} catch (Exception e) {
						logger.fatal("Confirmation Page -Hotel And Package Rate Details Issue" + e);
					}
					CancellationPolicy ConPolicy = new CancellationPolicy();

					try {
						WebElement CancellationElement = driver.findElement(By.id("cxlpolicy_h_0"));
						String CancellationPolicyInText = CancellationElement.getText();
						ArrayList<String> PolicyElements = new ArrayList<String>(Arrays.asList(CancellationPolicyInText.split("\n")));
						ConPolicy.setNumberofpolicies(Integer.toString(PolicyElements.size()));

						for (Iterator<String> iterator = PolicyElements.iterator(); iterator.hasNext();) {
							ConPolicy.addPolicies(iterator.next().trim());
						}

						try {
							driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
							ConPolicy.setRefundable(driver.findElements(By.className("DarkRed")).get(1).getText().trim());
						} catch (Exception e) {
							ConPolicy.setRefundable("Not Available");
						}

						try {
							ConPolicy.setDeadline(driver.findElement(By.id("cxlpolicyduedate_h_0")).getText().trim());
						} catch (Exception e) {
							ConPolicy.setDeadline("Not Available");
						}

					} catch (Exception e) {
						logger.fatal("Confirmation Page - Cancellation Policy Details Extraction Issue", e);
					}
					Conf.setCon_CancellationPolicy(ConPolicy);
					driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					// getting room occupancy details

					HashMap<Integer, HashMap<String, Occupancy>> ActualRoomOccMap = new HashMap<Integer, HashMap<String, Occupancy>>();
					HashMap<Integer, String> AdditionalDetails = new HashMap<Integer, String>();
					ArrayList<WebElement> OccupancyRoomElements = new ArrayList<WebElement>(driver.findElements(By.className("table_room_occupancy_confirmation")));

					try {
						for (int i = 0; i < OccupancyRoomElements.size(); i++) {
							HashMap<String, Occupancy> InnerRoomOccMap = new HashMap<String, Occupancy>();
							WebElement OccuRoomElem = OccupancyRoomElements.get(i);
							ArrayList<WebElement> InnerTrList = new ArrayList<WebElement>(OccuRoomElem.findElements(By.tagName("tr")));

							String HotelName = InnerTrList.get(0).getText().trim();
							String Checkin = InnerTrList.get(1).getText().trim();
							String Key = HotelName + "," + Checkin;
							AdditionalDetails.put((i + 1), Key);

							for (int j = 3; j < InnerTrList.size(); j++) {

								WebElement OccupancyElement = InnerTrList.get(j);
								Occupancy occupancy = new Occupancy();
								ArrayList<WebElement> InnerTdList = new ArrayList<WebElement>(OccupancyElement.findElements(By.tagName("td")));

								logger.info("Room " + (i + 1) + " Occupancy " + (j - 2) + "Details >>>");
								occupancy.setRoomLable(InnerTdList.get(0).getText().trim());
								logger.info(InnerTdList.get(0).getText().trim());

								occupancy.setTitle(InnerTdList.get(1).getText().trim());
								logger.info(InnerTdList.get(1).getText().trim());
								occupancy.setFirstName(InnerTdList.get(2).getText().trim());
								logger.info(InnerTdList.get(2).getText().trim());
								occupancy.setLastName(InnerTdList.get(3).getText().trim());
								logger.info(InnerTdList.get(3).getText().trim());
								occupancy.setSmoking(InnerTdList.get(4).getText().trim().equalsIgnoreCase("Y") ? true : false);
								logger.info(InnerTdList.get(4).getText().trim());
								occupancy.setHandicapped(InnerTdList.get(5).getText().trim().equalsIgnoreCase("Y") ? true : false);
								logger.info(InnerTdList.get(5).getText().trim());
								occupancy.setWheelChairNeeded(InnerTdList.get(6).getText().trim().equalsIgnoreCase("Y") ? true : false);
								logger.info(InnerTdList.get(6).getText().trim());

								InnerRoomOccMap.put(occupancy.getFirstName() + "/" + occupancy.getLastName(), occupancy);

							}
							AdditionalDetails.put((i + 1), Key);
							ActualRoomOccMap.put((i + 1), InnerRoomOccMap);
						}
					} catch (Exception e) {
						logger.fatal("Exception in room occupancy reading process " + e.toString());
					}
					// End getting room occupancy details
					Conf.setAdditionalDetails(AdditionalDetails);
					Conf.setActualOccuDetailsMap(ActualRoomOccMap);

					// Payment Details Extraction
					try {
						if (CurrentSearch.getPaymentType().equalsIgnoreCase("online")) {
							WebElement PaymentRefElement = driver.findElement(By.id("paymentrefno"));
							if (PaymentRefElement.isDisplayed()) {
								Conf.setIs_Con_ON_DetailsAvailble(true);
								Conf.setCon_ON_MerchantTrack(PaymentRefElement.getText().trim());
								// ../following-sibling::tr[1]/td[2]
								// Conf.setCon_ON_AuthReference(PaymentRefElement.findElement(By.xpath("../following-sibling::tr[1]/td[2]")).getText().trim());
								Conf.setCon_ON_PayID(PaymentRefElement.findElement(By.xpath("./following-sibling::td[2]")).getText().trim());
								Conf.setCon_ON_Currency(PaymentRefElement.findElement(By.xpath("../following-sibling::tr[1]/td[3]")).getText().trim());
								Conf.setCon_ON_Amount(PaymentRefElement.findElement(By.xpath("../following-sibling::tr[1]/td[4]")).getText().trim());
							} else {
								Conf.setIs_Con_ON_DetailsAvailble(false);
								try {
									((JavascriptExecutor) driver).executeScript("scroll(0,250);");
								} catch (Exception e) {
									logger.fatal("Issue When Scrolling", e);
								}
								Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getBasicFailedPath() + "/ConfPgOnlinePayDetails.jpg", driver);
							}
						} else {
							try {
								WebElement PaymentDetailsDiv = driver.findElement(By.xpath(".//*[@id='main_content']/div/div/div/div[5]"));
								if (PaymentDetailsDiv.findElement(By.xpath("./table/tbody/tr/td[2]")).isDisplayed()) {
									Conf.setIs_Con_OFF_DetailsAvailble(true);
									Conf.setCon_OFF_Method(PaymentDetailsDiv.findElement(By.xpath("./table/tbody/tr/td[2]")).getText());

									driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
									try {
										String string = PaymentDetailsDiv.findElement(By.xpath("./table/tbody/tr/td[4]")).getText();
										Conf.setCon_OFF_Reference(string.equalsIgnoreCase("") ? "N/A" : string);
									} catch (Exception e) {
										Conf.setCon_OFF_Reference("N/A");
									}
									try {
										String string = PaymentDetailsDiv.findElement(By.xpath("./table/tbody/tr/td[5]")).getText();
										Conf.setCon_OFF_Currency(string.equalsIgnoreCase("") ? "N/A" : string);
									} catch (Exception e) {
										Conf.setCon_OFF_Reference("N/A");
									}
									try {
										String string = PaymentDetailsDiv.findElement(By.xpath("./table/tbody/tr/td[6]")).getText();
										Conf.setCon_OFF_Amount(string.equalsIgnoreCase("") ? "N/A" : string);
									} catch (Exception e) {
										Conf.setCon_OFF_Reference("N/A");
									}
									driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
								} else {
									Conf.setIs_Con_OFF_DetailsAvailble(false);
									Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getBasicFailedPath() + "/ConfPageOfflinePayDetails.jpg", driver);

								}
							} catch (Exception e) {

								logger.fatal("Error With Locating Offline Payment Details ", e);
								Conf.setIs_Con_OFF_DetailsAvailble(false);
								Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getBasicFailedPath() + "/ConfPageOfflinePayDetails.jpg", driver);
							}
						}
					} catch (Exception e) {
						logger.fatal("Error With Locating online Payment Details ", e);
						Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getBasicFailedPath() + "/ConfPageOfflinePayDetails.jpg", driver);
					}

					// Customer Details Exraction

					// Conf.setCon_Customer_Title(driver.findElement(By.id("")).getText().trim());
					try {
						Conf.setCon_Customer_Fname(driver.findElement(By.id("cusfirstname")).getText().trim());
						Conf.setCon_Customer_Lname(driver.findElement(By.id("cuslastname")).getText().trim());
						Conf.setCon_Customer_Tel(driver.findElement(By.id("cuslastname")).findElement(By.xpath("../following-sibling::tr[1]/td[2]")).getText().trim());
						Conf.setCon_Customer_Email(driver.findElement(By.id("cusemail1")).getText().trim());
						Conf.setCon_Customer_Email2(driver.findElement(By.id("cusemail2")).getText().trim());
						Conf.setCon_Customer_Adderss1(driver.findElement(By.id("cusaddress1")).getText().trim());
						Conf.setCon_Customer_Adderss2(driver.findElement(By.id("cusaddress2")).getText().trim());

						//
						// Conf.setCon_Customer_Tel(driver.findElement(By.id("cuslastname")).findElement(By.xpath("../following-sibling::tr[1]/td[2]")).getText().trim());
						Conf.setCon_Customer_Fax(driver.findElement(By.id("cuslastname")).findElement(By.xpath("../following-sibling::tr[1]/td[4]")).getText().trim());
						Conf.setCon_Customer_Emergency(driver.findElement(By.id("cuscity")).findElement(By.xpath("../following-sibling::tr[1]/td[2]")).getText().trim());
						Conf.setCon_Customer_Country(driver.findElement(By.id("cuscountry")).getText().trim());
						Conf.setCon_Customer_City(driver.findElement(By.id("cuscity")).getText().trim());

						try {
							Conf.setCon_Customer_PostCode(driver.findElements(By.id("cuspostcode")).get(1).getText().trim());
						} catch (Exception e) {
							Conf.setCon_Customer_PostCode("N/A");
						}
						try {
							Conf.setCon_Customer_State(driver.findElement(By.id("cusstate")).getText().trim());
						} catch (Exception e) {
							Conf.setCon_Customer_State("N/A");
						}
					} catch (Exception e) {
						logger.fatal("Error With getting confirmation page customer  Details ", e);
						Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getBasicFailedPath() + "/ConfPageCusDetails.jpg", driver);

					}

					result.setCon_Page(Conf);
				} catch (Exception e) {
					result.setCon_Page(Conf);
					logger.fatal("Error in loading Extracting Confirmation Page  ", e);
					Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getBasicFailedPath() + "/ConfPageCusDetails.jpg", driver);

				}
			}
		} catch (Exception e) {

			logger.fatal("Error Occured Reservation Confirmation  " + e.toString());
			Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getBasicFailedPath() + "/ConfirmationProcess.jpg", driver);

		}

		return result;
	}

	public PaymentGateway PayOnline() throws IOException {
		PaymentGateway Pay_Gateway = new PaymentGateway();

		try {
			driver.switchTo().defaultContent();
			new WebDriverWait(driver, 40).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("live_message_frame")));
			new WebDriverWait(driver, 40).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("pay-gateway-modal"))));
			new WebDriverWait(driver, 40).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("paygatewayFrame")));
			new WebDriverWait(driver, 40).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("cardnumberpart1"))));
			Pay_Gateway.setPaymentGatewayLoded(true);
		} catch (Exception e) {
			Pay_Gateway.setPaymentGatewayLoded(false);
			logger.fatal("Error in locatining Payment Gateway ---> :", e);
			Screenshot.takeScreenshot(ScreenshotPathSingleton.getInstance().getBasicFailedPath() + "/Payment_Gateway_Error.jpg", driver);
		}

		if (Pay_Gateway.isPaymentGatewayLoded()) {
			Pay_Gateway.setDisplayCardType(driver.findElement(By.id("cardtype")).getAttribute("value"));
			((JavascriptExecutor) driver).executeScript("$('#cardnumberpart1').val(4111);");
			((JavascriptExecutor) driver).executeScript("$('#cardnumberpart2').val(1111);");
			((JavascriptExecutor) driver).executeScript("$('#cardnumberpart3').val(1111);");
			((JavascriptExecutor) driver).executeScript("$('#cardnumberpart4').val(1111);");

			new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText("10");
			new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText("2018");

			driver.findElement(By.id("cardholdername")).sendKeys("Dulan");
			((JavascriptExecutor) driver).executeScript("$('#cv2').val(123);");
			// driver.findElement(By.id("cv2")).sendKeys("123");

			Pay_Gateway.setAmount(driver.findElement(By.xpath(".//*[@id='main_dev']/div[6]/div[2]")).getText());
			Pay_Gateway.setCurrencyLable(driver.findElement(By.xpath(".//*[@id='main_dev']/div[6]/div[1]/h3")).getText());

			driver.findElement(By.className("proceed_btn")).click();

			try {
				String Message = driver.findElement(By.id("loader")).getText();
				Pay_Gateway.setPaymentStatus("Failed");
				Pay_Gateway.setCardFailureCardNumber(Message);
			} catch (Exception e) {

				Pay_Gateway.setPaymentStatus("PASS");

			}

		}

		return Pay_Gateway;
	}

	public String getElementValue(By by, WebDriver driver) {
		try {
			return driver.findElement(by).getAttribute("value");

		} catch (Exception e) {
			logger.fatal("Cant Find Element " + e.getMessage());
			return "Not Available";
		}
	}

	public String getSelectedOptionValue(By by, WebDriver driver) {
		try {
			return new Select(driver.findElement(by)).getFirstSelectedOption().getText();

		} catch (Exception e) {
			logger.fatal("Cant Find Element " + e.getMessage());
			return "Not Available";
		}
	}
}
