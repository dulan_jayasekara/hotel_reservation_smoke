package app.support;

import java.util.Map;

public class ThirdPartyResultsPage {
	
	
private String  TracerID;
private int     NumberOfPages;
private int     NumberOfHotels;
private String   DispalyingCurrency;
private int     ThirdPartyHotels;
private Map<String, Integer>  ResultsMap;



public String getTracerID() {
	return TracerID;
}	
public void setTracerID(String tracerID) {
	TracerID = tracerID;
}
public int getNumberOfPages() {
	return NumberOfPages;
}
public void setNumberOfPages(int numberOfPages) {
	NumberOfPages = numberOfPages;
}
public int getNumberOfHotels() {
	return NumberOfHotels;
}
public void setNumberOfHotels(String numberOfHotels) {
	NumberOfHotels = Integer.parseInt(numberOfHotels);
}
public String getDispalyingCurrency() {
	return DispalyingCurrency;
}
public void setDispalyingCurrency(String dispalyingCurrency) {
	DispalyingCurrency = dispalyingCurrency;
}
public int getThirdPartyHotels() {
	return ThirdPartyHotels;
}
public void setThirdPartyHotels(int thirdPartyHotels) {
	ThirdPartyHotels = thirdPartyHotels;
}
public Map<String, Integer> getResultsMap() {
	return ResultsMap;
}
public void setResultsMap(Map<String, Integer> resultsMap) {
	ResultsMap = resultsMap;
}






}
