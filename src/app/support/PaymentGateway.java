package app.support;

public class PaymentGateway {
	
	private String TransactionID           = "-";
	private String MerchantID              = "-";
	private String Amount                  = "-";
	private String CardFailureCardNumber   = "-";
	private String CardFailureExpiration   = "-";
	private boolean isPaymentGatewayLoded  = false;
	private String PaymentStatus           = "Failed";
	private String DisplayCardType         = "-";
	private String CurrencyLable           = "-";
	
	
	
	
	
	public String getCurrencyLable() {
		return CurrencyLable;
	}
	public void setCurrencyLable(String currencyLable) {
		CurrencyLable = currencyLable;
	}
	public String getDisplayCardType() {
		return DisplayCardType;
	}
	public void setDisplayCardType(String displayCardType) {
		DisplayCardType = displayCardType;
	}
	public String getPaymentStatus() {
		return PaymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		PaymentStatus = paymentStatus;
	}
	public boolean isPaymentGatewayLoded() {
		return isPaymentGatewayLoded;
	}
	public void setPaymentGatewayLoded(boolean isPaymentGatewayLoded) {
		this.isPaymentGatewayLoded = isPaymentGatewayLoded;
	}
	public String getTransactionID() {
		return TransactionID;
	}
	public void setTransactionID(String transactionID) {
		TransactionID = transactionID;
	}
	public String getMerchantID() {
		return MerchantID;
	}
	public void setMerchantID(String merchantID) {
		MerchantID = merchantID;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getCardFailureCardNumber() {
		return CardFailureCardNumber;
	}
	public void setCardFailureCardNumber(String cardFailureCardNumber) {
		CardFailureCardNumber = cardFailureCardNumber;
	}
	public String getCardFailureExpiration() {
		return CardFailureExpiration;
	}
	public void setCardFailureExpiration(String cardFailureExpiration) {
		CardFailureExpiration = cardFailureExpiration;
	}
	
	
	

}
