package app.support;

import java.util.ArrayList;
import java.util.HashMap;

import com.utilities.Occupancy;

public class ConfirmationPage {

	 
private boolean  isLoded                                 = false; 

private String    BookingNumber                          = "-";
private String    ConfirmationMessage                    = "-";
private String    Con_HotelName                          = "";
private String    Con_Address                            = "";
private String    Con_CheckIn                            = "";
private String    Con_CandellationDeadLine               = "";

private String    Con_CheckOut                           = "";
private String    Con_Nights                             = "";
private String    Con_BookingStatus 	                 = "";
private String    Con_Rooms                              = "";
private String    Con_SummeryHotelcurrency               = "";
private String    Con_totalTaxAnServiceCharges           = "";
private String    Con_SubTotal                           = "";
private String    Con_TotalHotelBookingValue             = "";
private String    Con_HotelAmountpayablenow              = "N/A";
private String    Con_HotelAmountToPaidAtCheckIn         = "N/A";
private String    Con_TotalGrossPackageBookingValue      = "";
private String    Con_TotalTaxesAndOtherCharges          = "";
private String    Con_TotalBookingValue                  = "";
private String    Con_FinalSummerycurrency               = "";
private String    Con_AmountbeingProcessedNow            = "";
private String    Con_AmountdueatCheckIn                 = "";



private String    Con_ON_TotalGrossBookingValue          = "";
private String    Con_ON_DefaultSelectedCard             = "";
private String    Con_ON_TotalTaxesAndFees               = "";
private String    Con_ON_TotalPackageBookingValue        = "";
private String    Con_ON_AmountBeingProcessedNow         = "";



//Customer Details

private String    Con_Customer_Title                     = "";
private String    Con_Customer_Fname                     = "";
private String    Con_Customer_Lname                     = "";
private String    Con_Customer_Tel                       = "";
private String    Con_Customer_Email                     = "";
private String    Con_Customer_Email2                    = "";
private String    Con_Customer_Fax	                     = "";
private String    Con_Customer_Adderss1                  = "";
private String    Con_Customer_Adderss2                  = "";
private String    Con_Customer_Country                   = "";
private String    Con_Customer_City                      = "";
private String    Con_Customer_PostCode                  = "N/A";
private String    Con_Customer_Emergency                 = "";
private String    Con_Customer_State                     = "N/A";
private String    Con_Customer_AreaCode                  = "";
private String    Con_Customer_Phone                     = "";
 


private boolean   Is_Con_OFF_DetailsAvailble            = false;
private String    Con_OFF_Method                        = "";
private String    Con_OFF_Reference                     = "";
private String    Con_OFF_Currency                      = "";
private String    Con_OFF_Amount                        = "";


private boolean   Is_Con_ON_DetailsAvailble             = false;
private String    Con_ON_AuthReference                   = "N/A";
private String    Con_ON_PayID                           = "N/A";
private String    Con_ON_MerchantTrack                   = "N/A";
private String    Con_ON_Amount                          = "";
private String    Con_ON_Currency                        = "";
private double    Con_ON_CCFee                           = 0.0;


private  boolean IsOfflinePaymentDetailsLoaded             = false;
private  boolean IsOnlinePaymentDetailsLoaded              = false;

private ArrayList<RoomType>                           Con_RoomDetails              = null;
private CancellationPolicy                            Con_CancellationPolicy       = new CancellationPolicy();
private HashMap<Integer, String>                      AdditionalRoomDetails        = new HashMap<Integer, String>();
private HashMap<Integer, HashMap<String, Occupancy>>  ActualOccuDetailsMap         = new HashMap<Integer, HashMap<String,Occupancy>>();







public String getCon_HotelAmountpayablenow() {
	return Con_HotelAmountpayablenow;
}

public void setCon_HotelAmountpayablenow(String con_HotelAmountpayablenow) {
	Con_HotelAmountpayablenow = con_HotelAmountpayablenow;
}

public String getCon_HotelAmountToPaidAtCheckIn() {
	return Con_HotelAmountToPaidAtCheckIn;
}

public void setCon_HotelAmountToPaidAtCheckIn(
		String con_HotelAmountToPaidAtCheckIn) {
	Con_HotelAmountToPaidAtCheckIn = con_HotelAmountToPaidAtCheckIn;
}

public double getCon_ON_CCFee() {
	return Con_ON_CCFee;
}

public void setCon_ON_CCFee(double con_ON_CCFee) {
	Con_ON_CCFee = con_ON_CCFee;
}

public String getCon_Customer_Title() {
	return Con_Customer_Title;
}

public void setCon_Customer_Title(String con_Customer_Title) {
	Con_Customer_Title = con_Customer_Title;
}

public String getCon_Customer_AreaCode() {
	return Con_Customer_AreaCode;
}

public void setCon_Customer_AreaCode(String con_Customer_AreaCode) {
	Con_Customer_AreaCode = con_Customer_AreaCode;
}

public String getCon_Customer_Phone() {
	return Con_Customer_Phone;
}

public void setCon_Customer_Phone(String con_Customer_Phone) {
	Con_Customer_Phone = con_Customer_Phone;
}

public boolean isIs_Con_OFF_DetailsAvailble() {
	return Is_Con_OFF_DetailsAvailble;
}

public void setIs_Con_OFF_DetailsAvailble(boolean is_Con_OFF_DetailsAvailble) {
	Is_Con_OFF_DetailsAvailble = is_Con_OFF_DetailsAvailble;
}

public boolean isIs_Con_ON_DetailsAvailble() {
	return Is_Con_ON_DetailsAvailble;
}

public void setIs_Con_ON_DetailsAvailble(boolean is_Con_ON_DetailsAvailble) {
	Is_Con_ON_DetailsAvailble = is_Con_ON_DetailsAvailble;
}

public HashMap<Integer, String> getAdditionalDetails() {
	return AdditionalRoomDetails;
}

public void setAdditionalDetails(HashMap<Integer, String> additionalDetails) {
	AdditionalRoomDetails = additionalDetails;
}

public HashMap<Integer, HashMap<String, Occupancy>> getActualOccuDetailsMap() {
	return ActualOccuDetailsMap;
}

public void setActualOccuDetailsMap(
		HashMap<Integer, HashMap<String, Occupancy>> actualOccuDetailsMap) {
	ActualOccuDetailsMap = actualOccuDetailsMap;
}


public String getCon_ON_AuthReference() {
	return Con_ON_AuthReference;
}

public void setCon_ON_AuthReference(String con_ON_Reference) {
	Con_ON_AuthReference = con_ON_Reference;
}

public String getCon_ON_PayID() {
	return Con_ON_PayID;
}

public void setCon_ON_PayID(String con_ON_PayID) {
	Con_ON_PayID = con_ON_PayID;
}

public String getCon_ON_MerchantTrack() {
	return Con_ON_MerchantTrack;
}

public void setCon_ON_MerchantTrack(String con_ON_MerchantTrack) {
	Con_ON_MerchantTrack = con_ON_MerchantTrack;
}

public String getCon_ON_Amount() {
	return Con_ON_Amount;
}

public void setCon_ON_Amount(String con_ON_Amount) {
	Con_ON_Amount = con_ON_Amount;
}

public String getCon_ON_Currency() {
	return Con_ON_Currency;
}

public void setCon_ON_Currency(String con_ON_Currency) {
	Con_ON_Currency = con_ON_Currency;
}

public boolean isIsOnlinePaymentDetailsLoaded() {
	return IsOnlinePaymentDetailsLoaded;
}

public void setIsOnlinePaymentDetailsLoaded(boolean isOnlinePaymentDetailsLoaded) {
	IsOnlinePaymentDetailsLoaded = isOnlinePaymentDetailsLoaded;
}

public String getBookingNumber() {
	return BookingNumber;
}

public void setBookingNumber(String bookingNumber) {
	BookingNumber = bookingNumber;
}

public String getConfirmationMessage() {
	return ConfirmationMessage;
}

public void setConfirmationMessage(String confirmationMessage) {
	ConfirmationMessage = confirmationMessage;
}

public boolean isIsOfflinePaymentDetailsLoaded() {
	return IsOfflinePaymentDetailsLoaded;
}

public void setIsOfflinePaymentDetailsLoaded(
		boolean isOfflinePaymentDetailsLoaded) {
	IsOfflinePaymentDetailsLoaded = isOfflinePaymentDetailsLoaded;
}



public String getCon_ON_TotalGrossBookingValue() {
	return Con_ON_TotalGrossBookingValue;
}

public void setCon_ON_TotalGrossBookingValue(
		String pay_ON_TotalGrossBookingValue) {
	Con_ON_TotalGrossBookingValue = pay_ON_TotalGrossBookingValue;
}

public String getCon_ON_DefaultSelectedCard() {
	return Con_ON_DefaultSelectedCard;
}

public void setCon_ON_DefaultSelectedCard(String pay_ON_DefaultSelectedCard) {
	Con_ON_DefaultSelectedCard = pay_ON_DefaultSelectedCard;
}

public String getCon_ON_TotalTaxesAndFees() {
	return Con_ON_TotalTaxesAndFees;
}

public void setCon_ON_TotalTaxesAndFees(String pay_ON_TotalTaxesAndFees) {
	Con_ON_TotalTaxesAndFees = pay_ON_TotalTaxesAndFees;
}

public String getCon_ON_TotalPackageBookingValue() {
	return Con_ON_TotalPackageBookingValue;
}

public void setCon_ON_TotalPackageBookingValue(
		String pay_ON_TotalPackageBookingValue) {
	Con_ON_TotalPackageBookingValue = pay_ON_TotalPackageBookingValue;
}

public String getCon_ON_AmountBeingProcessedNow() {
	return Con_ON_AmountBeingProcessedNow;
}

public void setCon_ON_AmountBeingProcessedNow(
		String pay_ON_AmountBeingProcessedNow) {
	Con_ON_AmountBeingProcessedNow = pay_ON_AmountBeingProcessedNow;
}


public String getCon_OFF_Method() {
	return Con_OFF_Method;
}

public void setCon_OFF_Method(String pay_OFF_Method) {
	Con_OFF_Method = pay_OFF_Method;
}

public String getCon_OFF_Reference() {
	return Con_OFF_Reference;
}

public void setCon_OFF_Reference(String pay_OFF_Reference) {
	Con_OFF_Reference = pay_OFF_Reference;
}

public String getCon_OFF_Currency() {
	return Con_OFF_Currency;
}

public void setCon_OFF_Currency(String pay_OFF_Currency) {
	Con_OFF_Currency = pay_OFF_Currency;
}

public String getCon_OFF_Amount() {
	return Con_OFF_Amount;
}

public void setCon_OFF_Amount(String pay_OFF_Amount) {
	Con_OFF_Amount = pay_OFF_Amount;
}

public String getCon_Customer_Fname() {
	return Con_Customer_Fname;
}

public void setCon_Customer_Fname(String pay_Customer_Fname) {
	Con_Customer_Fname = pay_Customer_Fname;
}

public String getCon_Customer_Lname() {
	return Con_Customer_Lname;
}

public void setCon_Customer_Lname(String pay_Customer_Lname) {
	Con_Customer_Lname = pay_Customer_Lname;
}

public String getCon_Customer_Tel() {
	return Con_Customer_Tel;
}

public void setCon_Customer_Tel(String pay_Customer_Tel) {
	Con_Customer_Tel = pay_Customer_Tel;
}

public String getCon_Customer_Email() {
	return Con_Customer_Email;
}

public void setCon_Customer_Email(String pay_Customer_Email) {
	Con_Customer_Email = pay_Customer_Email;
}

public String getCon_Customer_Email2() {
	return Con_Customer_Email2;
}

public void setCon_Customer_Email2(String pay_Customer_Email2) {
	Con_Customer_Email2 = pay_Customer_Email2;
}

public String getCon_Customer_Fax() {
	return Con_Customer_Fax;
}

public void setCon_Customer_Fax(String pay_Customer_Fax) {
	Con_Customer_Fax = pay_Customer_Fax;
}

public String getCon_Customer_Adderss1() {
	return Con_Customer_Adderss1;
}

public void setCon_Customer_Adderss1(String pay_Customer_Adderss1) {
	Con_Customer_Adderss1 = pay_Customer_Adderss1;
}

public String getCon_Customer_Adderss2() {
	return Con_Customer_Adderss2;
}

public void setCon_Customer_Adderss2(String pay_Customer_Adderss2) {
	Con_Customer_Adderss2 = pay_Customer_Adderss2;
}

public String getCon_Customer_Country() {
	return Con_Customer_Country;
}

public void setCon_Customer_Country(String pay_Customer_Country) {
	Con_Customer_Country = pay_Customer_Country;
}

public String getCon_Customer_City() {
	return Con_Customer_City;
}

public void setCon_Customer_City(String pay_Customer_City) {
	Con_Customer_City = pay_Customer_City;
}

public String getCon_Customer_PostCode() {
	return Con_Customer_PostCode;
}

public void setCon_Customer_PostCode(String pay_Customer_PostCode) {
	Con_Customer_PostCode = pay_Customer_PostCode;
}

public String getCon_Customer_Emergency() {
	return Con_Customer_Emergency;
}

public void setCon_Customer_Emergency(String pay_Customer_Emergency) {
	Con_Customer_Emergency = pay_Customer_Emergency;
}

public String getCon_Customer_State() {
	return Con_Customer_State;
}

public void setCon_Customer_State(String pay_Customer_State) {
	Con_Customer_State = pay_Customer_State;
}

public String getCon_FinalSummerycurrency() {
	return Con_FinalSummerycurrency;
}

public void setCon_FinalSummerycurrency(String pay_FinalSummerycurrency) {
	Con_FinalSummerycurrency = pay_FinalSummerycurrency;
}

public String getCon_totalTaxAnServiceCharges() {
	return Con_totalTaxAnServiceCharges;
	

}

public void setCon_totalTaxAnServiceCharges(String pay_totalTaxAnServiceCharges) {
	Con_totalTaxAnServiceCharges = pay_totalTaxAnServiceCharges;
}

public String getCon_TotalGrossPackageBookingValue() {
	return Con_TotalGrossPackageBookingValue;
}

public void setCon_TotalGrossPackageBookingValue(
		String pay_TotalGrossPackageBookingValue) {
	Con_TotalGrossPackageBookingValue = pay_TotalGrossPackageBookingValue;
}

public String getCon_TotalTaxesAndOtherCharges() {
	return Con_TotalTaxesAndOtherCharges;
}

public void setCon_TotalTaxesAndOtherCharges(
		String pay_TotalTaxesAndOtherCharges) {
	Con_TotalTaxesAndOtherCharges = pay_TotalTaxesAndOtherCharges;
}

public String getCon_TotalBookingValue() {
	return Con_TotalBookingValue;
}

public void setCon_TotalBookingValue(String pay_TotalBookingValue) {
	Con_TotalBookingValue = pay_TotalBookingValue;
}

public String getCon_AmountbeingProcessedNow() {
	return Con_AmountbeingProcessedNow;
}

public void setCon_AmountbeingProcessedNow(String pay_AmountbeingProcessedNow) {
	Con_AmountbeingProcessedNow = pay_AmountbeingProcessedNow;
}

public String getCon_AmountdueatCheckIn() {
	return Con_AmountdueatCheckIn;
}

public void setCon_AmountdueatCheckIn(String pay_AmountdueatCheckIn) {
	Con_AmountdueatCheckIn = pay_AmountdueatCheckIn;
}

public String getCon_CandellationDeadLine() {
	return Con_CandellationDeadLine;
}

public void setCon_CandellationDeadLine(String pay_CandellationDeadLine) {
	Con_CandellationDeadLine = pay_CandellationDeadLine;
}



public ArrayList<RoomType> getCon_RoomDetails() {
	return Con_RoomDetails;
}

public void setCon_RoomDetails(ArrayList<RoomType> pay_RoomDetails) {
	Con_RoomDetails = pay_RoomDetails;
}

public CancellationPolicy getCon_CancellationPolicy() {
	return Con_CancellationPolicy;
}

public void setCon_CancellationPolicy(CancellationPolicy pay_CancellationPolicy) {
	Con_CancellationPolicy = pay_CancellationPolicy;
}



public boolean isLoded() {
	return isLoded;
}

public void setLoded(boolean isLoded) {
	this.isLoded = isLoded;
}

public String getCon_HotelName() {
	return Con_HotelName;
}

public void setCon_HotelName(String pay_HotelName) {
	Con_HotelName = pay_HotelName;
}

public String getCon_Address() {
	return Con_Address;
}

public void setCon_Address(String pay_Address) {
	Con_Address = pay_Address;
}

public String getCon_CheckIn() {
	return Con_CheckIn;
}

public void setCon_CheckIn(String pay_CheckIn) {
	Con_CheckIn = pay_CheckIn;
}

public String getCon_CheckOut() {
	return Con_CheckOut;
}

public void setCon_CheckOut(String pay_CheckOut) {
	Con_CheckOut = pay_CheckOut;
}

public String getCon_Nights() {
	return Con_Nights;
}

public void setCon_Nights(String pay_Nights) {
	Con_Nights = pay_Nights;
}

public String getCon_BookingStatus() {
	return Con_BookingStatus;
}

public void setCon_BookingStatus(String pay_BookingStatus) {
	Con_BookingStatus = pay_BookingStatus;
}

public String getCon_Rooms() {
	return Con_Rooms;
}

public void setCon_Rooms(String pay_Rooms) {
	Con_Rooms = pay_Rooms;
}


public String getCon_SummeryHotelcurrency() {
	return Con_SummeryHotelcurrency;
}

public void setCon_SummeryHotelcurrency(String pay_currency) {
	Con_SummeryHotelcurrency = pay_currency;
}


public String getCon_SubTotal() {
	return Con_SubTotal;
}

public void setCon_SubTotal(String pay_SubTotal) {
	Con_SubTotal = pay_SubTotal;
}

public String getCon_TotalHotelBookingValue() {
	return Con_TotalHotelBookingValue;
}

public void setCon_TotalHotelBookingValue(String pay_TotalHotelBookingValue) {
	Con_TotalHotelBookingValue = pay_TotalHotelBookingValue;
}










}
