package app.support;

import java.util.ArrayList;

public class ViewFullDetails {

	
private boolean  isLoded; 
private String    View_HotelName                          = "";
private String    View_Address                            = "";
private String    View_CheckIn                            = "";
private String    View_CheckOut                           = "";
private String    View_Nights                             = "";
private String    View_BookingStatus  	                  = "";
private String    View_NumberOfRooms                      = "";
private String    View_CancellationDeadLine               = "";
private String    View_Currency                           = "";
private String    View_totalTaxAnServiceCharges           = "";
private String    View_SubTotal                           = "";
private String    View_TotalHotelBookingValue             = "";
private String    View_HotelAmountpayablenow               = "N/A";
private String    View_HotelAmountToPaidAtCheckIn          = "N/A";
private String    View_TotalGrossPackageBookingValue      = "";
private String    View_TotalTaxesAndOtherCharges          = "";
private String    View_TotalBookingValue                  = "";
private String    View_AmountbeingProcessedNow            = "";
private String    View_AmountdueatCheckIn                 = "";
private ArrayList<RoomType> RoomDetails                   = null;
private CancellationPolicy  CancellationPolicy            = new CancellationPolicy();





public String getView_HotelAmountpayablenow() {
	return View_HotelAmountpayablenow;
}

public void setView_HotelAmountpayablenow(String view_HotelAmountpayablenow) {
	View_HotelAmountpayablenow = view_HotelAmountpayablenow;
}

public String getView_HotelAmountToPaidAtCheckIn() {
	return View_HotelAmountToPaidAtCheckIn;
}

public void setView_HotelAmountToPaidAtCheckIn(
		String view_HotelAmountToPaidAtCheckIn) {
	View_HotelAmountToPaidAtCheckIn = view_HotelAmountToPaidAtCheckIn;
}

public CancellationPolicy getCancellationPolicy() {
	return CancellationPolicy;
}

public void setCancellationPolicy(CancellationPolicy cancellationPolicy) {
	CancellationPolicy = cancellationPolicy;
}

public String getView_Currency() {
	return View_Currency;
}

public void setView_Currency(String view_Currency) {
	View_Currency = view_Currency;
}

public ArrayList<RoomType> getRoomDetails() {
	return RoomDetails;
}

public void setRoomDetails(ArrayList<RoomType> roomDetails) {
	RoomDetails = roomDetails;
}

public String getView_CancellationDeadLine() {
	return View_CancellationDeadLine;
}

public void setView_CancellationDeadLine(String view_CancellationDeadLine) {
	View_CancellationDeadLine = view_CancellationDeadLine;
}

public String getView_NumberOfRooms() {
	return View_NumberOfRooms;
}

public void setView_NumberOfRooms(String view_NumberOfRooms) {
	View_NumberOfRooms = view_NumberOfRooms;
}

public String getView_totalTaxAnServiceCharges() {
	return View_totalTaxAnServiceCharges;
}

public void setView_totalTaxAnServiceCharges(
		String view_totalTaxAnServiceCharges) {
	View_totalTaxAnServiceCharges = view_totalTaxAnServiceCharges;
}

public String getView_TotalGrossPackageBookingValue() {
	return View_TotalGrossPackageBookingValue;
}

public void setView_TotalGrossPackageBookingValue(
		String view_TotalGrossPackageBookingValue) {
	View_TotalGrossPackageBookingValue = view_TotalGrossPackageBookingValue;
}

public String getView_TotalTaxesAndOtherCharges() {
	return View_TotalTaxesAndOtherCharges;
}

public void setView_TotalTaxesAndOtherCharges(
		String view_TotalTaxesAndOtherCharges) {
	View_TotalTaxesAndOtherCharges = view_TotalTaxesAndOtherCharges;
}

public String getView_TotalBookingValue() {
	return View_TotalBookingValue;
}

public void setView_TotalBookingValue(String view_TotalBookingValue) {
	View_TotalBookingValue = view_TotalBookingValue;
}

public String getView_AmountbeingProcessedNow() {
	return View_AmountbeingProcessedNow;
}

public void setView_AmountbeingProcessedNow(String view_AmountbeingProcessedNow) {
	View_AmountbeingProcessedNow = view_AmountbeingProcessedNow;
}

public String getView_AmountdueatCheckIn() {
	return View_AmountdueatCheckIn;
}

public void setView_AmountdueatCheckIn(String view_AmountdueatCheckIn) {
	View_AmountdueatCheckIn = view_AmountdueatCheckIn;
}




public boolean isLoded() {
	return isLoded;
}

public void setLoded(boolean isLoded) {
	this.isLoded = isLoded;
}

public String getView_HotelName() {
	return View_HotelName;
}

public void setView_HotelName(String view_HotelName) {
	View_HotelName = view_HotelName;
}

public String getView_Address() {
	return View_Address;
}

public void setView_Address(String view_Address) {
	View_Address = view_Address;
}

public String getView_CheckIn() {
	return View_CheckIn;
}

public void setView_CheckIn(String view_CheckIn) {
	View_CheckIn = view_CheckIn;
}

public String getView_CheckOut() {
	return View_CheckOut;
}

public void setView_CheckOut(String view_CheckOut) {
	View_CheckOut = view_CheckOut;
}

public String getView_Nights() {
	return View_Nights;
}

public void setView_Nights(String view_Nights) {
	View_Nights = view_Nights;
}

public String getView_BookingStatus() {
	return View_BookingStatus;
}

public void setView_BookingStatus(String view_BookingStatus) {
	View_BookingStatus = view_BookingStatus;
}



public String getView_SubTotal() {
	return View_SubTotal;
}

public void setView_SubTotal(String view_SubTotal) {
	View_SubTotal = view_SubTotal;
}

public String getView_TotalHotelBookingValue() {
	return View_TotalHotelBookingValue;
}

public void setView_TotalHotelBookingValue(String view_TotalHotelBookingValue) {
	View_TotalHotelBookingValue = view_TotalHotelBookingValue;
}









}
