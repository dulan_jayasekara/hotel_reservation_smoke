package app.support;

import java.util.ArrayList;
import java.util.HashMap;

import com.utilities.Occupancy;

public class PaymentsPage {
	
private boolean  isLoded                = false; 
private boolean OverrideOptionAvailable = false;

private String    Pay_HotelName                          = "";
private String    Pay_Address                            = "";
private String    Pay_CheckIn                            = "";
private String    Pay_CandellationDeadLine               = "";


private boolean   Pay_DiscountFiledAvailable             = false;
private boolean   Pay_DiscountActButtonAvailable         = false;
private boolean   Pay_MngOverUserFiledAvailable          = false;
private boolean   Pay_MngOverPassFiledAvailable          = false;
private boolean   Pay_MngOverActButtonAvailable          = false;
private boolean   Pay_ProductRmButtonAvailable           = false;
private boolean   Pay_QuoteSaveButtonAvailable           = false;
private boolean   Pay_QuoteMailSentByDafault             = true;
private boolean   Pay_CusMailSentByDafault               = true;
private boolean   Pay_VoucherMailSentByDafault           = true;
private boolean   Pay_SupMailSentByDafault               = false;
private boolean   Pay_HtNotesCopiedInMailByDafault       = true;
private boolean   Pay_MailSendingOptionsAvailable        = true;



private String    Pay_CheckOut                           = "";
private String    Pay_Nights                             = "";
private String    Pay_BookingStatus 	                 = "";
private String    Pay_Rooms                              = "";
private String    Pay_SummeryHotelcurrency               = "";
private String    Pay_totalTaxAnServiceCharges           = "";
private String    Pay_SubTotal                           = "";
private String    Pay_TotalHotelBookingValue             = "";
private String    Pay_HotelAmountpayablenow              = "N/A";
private String    Pay_HotelAmountToPaidAtCheckIn         = "N/A";

private String    Pay_TotalGrossPackageBookingValue      = "";
private String    Pay_TotalTaxesAndOtherCharges          = "";
private String    Pay_TotalBookingValue                  = "";
private String    Pay_FinalSummerycurrency               = "";
private String    Pay_AmountbeingProcessedNow            = "";
private String    Pay_AmountdueatCheckIn                 = "";

private String    Pay_CreditShift_TotalGrossPackageBookingValue      = "";
private String    Pay_CreditShift_TotalTaxesAndOtherCharges          = "";
private String    Pay_CreditShift_TotalBookingValue                  = "";
private String    Pay_CreditShift_FinalSummerycurrency               = "";
private String    Pay_CreditShift_AmountbeingProcessedNow            = "";
private String    Pay_CreditShift_AmountdueatCheckIn                 = "";
private String    Pay_ShiftedCardType                                = "";


private String    Pay_Off_TotalGrossPackageBookingValue      = "";
private String    Pay_Off_TotalTaxesAndOtherCharges          = "";
private String    Pay_Off_TotalBookingValue                  = "";
private String    Pay_Off_FinalSummerycurrency               = "";
private String    Pay_Off_AmountbeingProcessedNow            = "";
private String    Pay_Off_AmountdueatCheckIn                 = "";



private String    Pay_ON_TotalGrossBookingValue         = "";
private String    Pay_ON_DefaultSelectedCard            = "";
private String    Pay_ON_TotalTaxesAndFees              = "";
private String    Pay_ON_TotalPackageBookingValue       = "";
private String    Pay_ON_AmountBeingProcessedNow        = "";
private String    Pay_ON_AmountDueAtCheckIn             = "";
private String    Pay_ON_CurrencyLable                  = "";
private String    Pay_ON_SummeryCurrency                = "";
private String    Pay_ON_SummeryValue                   = "";
private double    Pay_ON_CCFee                          = 0.0;

private String    Pay_ON_Shift_TotalGrossBookingValue         = "";
private String    Pay_ON_Shift_DefaultSelectedCard            = "";
private String    Pay_ON_Shift_TotalTaxesAndFees              = "";
private String    Pay_ON_Shift_TotalPackageBookingValue       = "";
private String    Pay_ON_Shift_AmountBeingProcessedNow        = "";
private String    Pay_ON_Shift_CurrencyLable                  = "";
private String    Pay_ON_Shift_SummeryCurrency                = "";
private String    Pay_ON_Shift_SummeryValue                   = "";
private double    Pay_ON_Shift_CCFee                          = 0.0;

private String    Pay_OFF_DefMethod                     = "";
private String    Pay_OFF_Method                        = "";
private String    Pay_OFF_Reference                     = "";
private String    Pay_OFF_Currency                      = "";
private String    Pay_OFF_Amount                        = "";
private boolean   isZipAvailable                        = false;
private String    AutoLoadedCityInCustomerInfo          = "";




public boolean isPay_MailSendingOptionsAvailable() {
	return Pay_MailSendingOptionsAvailable;
}

public void setPay_MailSendingOptionsAvailable(boolean pay_MailSendingOptionsAvailable) {
	Pay_MailSendingOptionsAvailable = pay_MailSendingOptionsAvailable;
}

public boolean isPay_ProductRmButtonAvailable() {
	return Pay_ProductRmButtonAvailable;
}

public void setPay_ProductRmButtonAvailable(boolean pay_ProductRmButtonAvailable) {
	Pay_ProductRmButtonAvailable = pay_ProductRmButtonAvailable;
}

public boolean isPay_QuoteSaveButtonAvailable() {
	return Pay_QuoteSaveButtonAvailable;
}

public void setPay_QuoteSaveButtonAvailable(boolean pay_QuoteSaveButtonAvailable) {
	Pay_QuoteSaveButtonAvailable = pay_QuoteSaveButtonAvailable;
}

public boolean isPay_QuoteMailSentByDafault() {
	return Pay_QuoteMailSentByDafault;
}

public void setPay_QuoteMailSentByDafault(boolean pay_QuoteMailSentByDafault) {
	Pay_QuoteMailSentByDafault = pay_QuoteMailSentByDafault;
}

public boolean isPay_CusMailSentByDafault() {
	return Pay_CusMailSentByDafault;
}

public void setPay_CusMailSentByDafault(boolean pay_CusMailSentByDafault) {
	Pay_CusMailSentByDafault = pay_CusMailSentByDafault;
}

public boolean isPay_VoucherMailSentByDafault() {
	return Pay_VoucherMailSentByDafault;
}

public void setPay_VoucherMailSentByDafault(boolean pay_VoucherMailSentByDafault) {
	Pay_VoucherMailSentByDafault = pay_VoucherMailSentByDafault;
}

public boolean isPay_SupMailSentByDafault() {
	return Pay_SupMailSentByDafault;
}

public void setPay_SupMailSentByDafault(boolean pay_SupMailSentByDafault) {
	Pay_SupMailSentByDafault = pay_SupMailSentByDafault;
}

public boolean isPay_HtNotesCopiedInMailByDafault() {
	return Pay_HtNotesCopiedInMailByDafault;
}

public void setPay_HtNotesCopiedInMailByDafault(boolean pay_HtNotesCopiedInMailByDafault) {
	Pay_HtNotesCopiedInMailByDafault = pay_HtNotesCopiedInMailByDafault;
}

public String getPay_OFF_DefMethod() {
	return Pay_OFF_DefMethod;
}

public void setPay_OFF_DefMethod(String pay_OFF_DefMethod) {
	Pay_OFF_DefMethod = pay_OFF_DefMethod;
}

public boolean isPay_DiscountFiledAvailable() {
	return Pay_DiscountFiledAvailable;
}

public void setPay_DiscountFiledAvailable(boolean pay_DiscountFiledAvailable) {
	Pay_DiscountFiledAvailable = pay_DiscountFiledAvailable;
}

public boolean isPay_DiscountActButtonAvailable() {
	return Pay_DiscountActButtonAvailable;
}

public void setPay_DiscountActButtonAvailable(boolean pay_DiscountActButtonAvailable) {
	Pay_DiscountActButtonAvailable = pay_DiscountActButtonAvailable;
}

public boolean isPay_MngOverUserFiledAvailable() {
	return Pay_MngOverUserFiledAvailable;
}

public void setPay_MngOverUserFiledAvailable(boolean pay_MngOverUserFiledAvailable) {
	Pay_MngOverUserFiledAvailable = pay_MngOverUserFiledAvailable;
}

public boolean isPay_MngOverPassFiledAvailable() {
	return Pay_MngOverPassFiledAvailable;
}

public void setPay_MngOverPassFiledAvailable(boolean pay_MngOverPassFiledAvailable) {
	Pay_MngOverPassFiledAvailable = pay_MngOverPassFiledAvailable;
}

public boolean isPay_MngOverActButtonAvailable() {
	return Pay_MngOverActButtonAvailable;
}

public void setPay_MngOverActButtonAvailable(boolean pay_MngOverActButtonAvailable) {
	Pay_MngOverActButtonAvailable = pay_MngOverActButtonAvailable;
}

public boolean isZipAvailable() {
	return isZipAvailable;
}

public void setZipAvailable(boolean isZipAvailable) {
	this.isZipAvailable = isZipAvailable;
}

public String getAutoLoadedCityInCustomerInfo() {
	return AutoLoadedCityInCustomerInfo;
}

public void setAutoLoadedCityInCustomerInfo(String autoLoadedCityInCustomerInfo) {
	AutoLoadedCityInCustomerInfo = autoLoadedCityInCustomerInfo;
}

private  boolean IsOfflinePaymentDetailsLoaded             = false;

private ArrayList<RoomType> Pay_RoomDetails                                        = null;
private CancellationPolicy  Pay_CancellationPolicy                                 = new CancellationPolicy();
private HashMap<Integer, HashMap<String, Occupancy>>  FilledOccuDetailsMap         = new HashMap<Integer, HashMap<String,Occupancy>>();



private String    Pay_Customer_Fname       = "";
private String    Pay_Customer_Lname       = "";
private String    Pay_Customer_Tel         = "";
private String    Pay_Customer_Email       = "";
private String    Pay_Customer_Email2      = "";
private String    Pay_Customer_Fax	       = "";
private String    Pay_Customer_Adderss1    = "";
private String    Pay_Customer_Adderss2    = "";
private String    Pay_Customer_Country     = "";
private String    Pay_Customer_City        = "";
private String    Pay_Customer_PostCode    = "";
private String    Pay_Customer_Emergency   = "";
private String    Pay_Customer_State       = "";


private String    Pay_Occupancy_String       = "";







public String getPay_HotelAmountpayablenow() {
	return Pay_HotelAmountpayablenow;
}

public void setPay_HotelAmountpayablenow(String pay_HotelAmountpayablenow) {
	Pay_HotelAmountpayablenow = pay_HotelAmountpayablenow;
}

public String getPay_HotelAmountToPaidAtCheckIn() {
	return Pay_HotelAmountToPaidAtCheckIn;
}

public void setPay_HotelAmountToPaidAtCheckIn(
		String pay_HotelAmountToPaidAtCheckIn) {
	Pay_HotelAmountToPaidAtCheckIn = pay_HotelAmountToPaidAtCheckIn;
}

public HashMap<Integer, HashMap<String, Occupancy>> getFilledOccuDetailsMap() {
	return FilledOccuDetailsMap;
}

public void setFilledOccuDetailsMap(
		HashMap<Integer, HashMap<String, Occupancy>> filledOccuDetailsMap) {
	FilledOccuDetailsMap = filledOccuDetailsMap;
}

public double getPay_ON_CCFee() {
	return Pay_ON_CCFee;
}

public void setPay_ON_CCFee(double pay_ON_CCFee) {
	Pay_ON_CCFee = pay_ON_CCFee;
}

public double getPay_ON_Shift_CCFee() {
	return Pay_ON_Shift_CCFee;
}

public void setPay_ON_Shift_CCFee(double pay_ON_Shift_CCFee) {
	Pay_ON_Shift_CCFee = pay_ON_Shift_CCFee;
}

public String getPay_ON_AmountDueAtCheckIn() {
	return Pay_ON_AmountDueAtCheckIn;
}

public void setPay_ON_AmountDueAtCheckIn(String pay_ON_AmountDueAtCheckIn) {
	Pay_ON_AmountDueAtCheckIn = pay_ON_AmountDueAtCheckIn;
}

public String getPay_ON_Shift_TotalGrossBookingValue() {
	return Pay_ON_Shift_TotalGrossBookingValue;
}

public void setPay_ON_Shift_TotalGrossBookingValue(
		String pay_ON_Shift_TotalGrossBookingValue) {
	Pay_ON_Shift_TotalGrossBookingValue = pay_ON_Shift_TotalGrossBookingValue;
}

public String getPay_ON_Shift_DefaultSelectedCard() {
	return Pay_ON_Shift_DefaultSelectedCard;
}

public void setPay_ON_Shift_DefaultSelectedCard(
		String pay_ON_Shift_DefaultSelectedCard) {
	Pay_ON_Shift_DefaultSelectedCard = pay_ON_Shift_DefaultSelectedCard;
}

public String getPay_ON_Shift_TotalTaxesAndFees() {
	return Pay_ON_Shift_TotalTaxesAndFees;
}

public void setPay_ON_Shift_TotalTaxesAndFees(
		String pay_ON_Shift_TotalTaxesAndFees) {
	Pay_ON_Shift_TotalTaxesAndFees = pay_ON_Shift_TotalTaxesAndFees;
}

public String getPay_ON_Shift_TotalPackageBookingValue() {
	return Pay_ON_Shift_TotalPackageBookingValue;
}

public void setPay_ON_Shift_TotalPackageBookingValue(
		String pay_ON_Shift_TotalPackageBookingValue) {
	Pay_ON_Shift_TotalPackageBookingValue = pay_ON_Shift_TotalPackageBookingValue;
}

public String getPay_ON_Shift_AmountBeingProcessedNow() {
	return Pay_ON_Shift_AmountBeingProcessedNow;
}

public void setPay_ON_Shift_AmountBeingProcessedNow(
		String pay_ON_Shift_AmountBeingProcessedNow) {
	Pay_ON_Shift_AmountBeingProcessedNow = pay_ON_Shift_AmountBeingProcessedNow;
}

public String getPay_ON_Shift_CurrencyLable() {
	return Pay_ON_Shift_CurrencyLable;
}

public void setPay_ON_Shift_CurrencyLable(String pay_ON_Shift_CurrencyLable) {
	Pay_ON_Shift_CurrencyLable = pay_ON_Shift_CurrencyLable;
}

public String getPay_ON_Shift_SummeryCurrency() {
	return Pay_ON_Shift_SummeryCurrency;
}

public void setPay_ON_Shift_SummeryCurrency(String pay_ON_Shift_SummeryCurrency) {
	Pay_ON_Shift_SummeryCurrency = pay_ON_Shift_SummeryCurrency;
}

public String getPay_ON_Shift_SummeryValue() {
	return Pay_ON_Shift_SummeryValue;
}

public void setPay_ON_Shift_SummeryValue(String pay_ON_Shift_SummeryValue) {
	Pay_ON_Shift_SummeryValue = pay_ON_Shift_SummeryValue;
}

public boolean isIsOfflinePaymentDetailsLoaded() {
	return IsOfflinePaymentDetailsLoaded;
}

public void setIsOfflinePaymentDetailsLoaded(
		boolean isOfflinePaymentDetailsLoaded) {
	IsOfflinePaymentDetailsLoaded = isOfflinePaymentDetailsLoaded;
}

public String getPay_ShiftedCardType() {
	return Pay_ShiftedCardType;
}

public void setPay_ShiftedCardType(String pay_ShiftedCardType) {
	Pay_ShiftedCardType = pay_ShiftedCardType;
}

public String getPay_CreditShift_TotalGrossPackageBookingValue() {
	return Pay_CreditShift_TotalGrossPackageBookingValue;
}

public void setPay_CreditShift_TotalGrossPackageBookingValue(
		String pay_CreditShift_TotalGrossPackageBookingValue) {
	Pay_CreditShift_TotalGrossPackageBookingValue = pay_CreditShift_TotalGrossPackageBookingValue;
}

public String getPay_CreditShift_TotalTaxesAndOtherCharges() {
	return Pay_CreditShift_TotalTaxesAndOtherCharges;
}

public void setPay_CreditShift_TotalTaxesAndOtherCharges(
		String pay_CreditShift_TotalTaxesAndOtherCharges) {
	Pay_CreditShift_TotalTaxesAndOtherCharges = pay_CreditShift_TotalTaxesAndOtherCharges;
}

public String getPay_CreditShift_TotalBookingValue() {
	return Pay_CreditShift_TotalBookingValue;
}

public void setPay_CreditShift_TotalBookingValue(
		String pay_CreditShift_TotalBookingValue) {
	Pay_CreditShift_TotalBookingValue = pay_CreditShift_TotalBookingValue;
}

public String getPay_CreditShift_FinalSummerycurrency() {
	return Pay_CreditShift_FinalSummerycurrency;
}

public void setPay_CreditShift_FinalSummerycurrency(
		String pay_CreditShift_FinalSummerycurrency) {
	Pay_CreditShift_FinalSummerycurrency = pay_CreditShift_FinalSummerycurrency;
}

public String getPay_CreditShift_AmountbeingProcessedNow() {
	return Pay_CreditShift_AmountbeingProcessedNow;
}

public void setPay_CreditShift_AmountbeingProcessedNow(
		String pay_CreditShift_AmountbeingProcessedNow) {
	Pay_CreditShift_AmountbeingProcessedNow = pay_CreditShift_AmountbeingProcessedNow;
}

public String getPay_CreditShift_AmountdueatCheckIn() {
	return Pay_CreditShift_AmountdueatCheckIn;
}

public void setPay_CreditShift_AmountdueatCheckIn(
		String pay_CreditShift_AmountdueatCheckIn) {
	Pay_CreditShift_AmountdueatCheckIn = pay_CreditShift_AmountdueatCheckIn;
}

public String getPay_Off_TotalGrossPackageBookingValue() {
	return Pay_Off_TotalGrossPackageBookingValue;
}

public void setPay_Off_TotalGrossPackageBookingValue(
		String pay_Off_TotalGrossPackageBookingValue) {
	Pay_Off_TotalGrossPackageBookingValue = pay_Off_TotalGrossPackageBookingValue;
}

public String getPay_Off_TotalTaxesAndOtherCharges() {
	return Pay_Off_TotalTaxesAndOtherCharges;
}

public void setPay_Off_TotalTaxesAndOtherCharges(
		String pay_Off_TotalTaxesAndOtherCharges) {
	Pay_Off_TotalTaxesAndOtherCharges = pay_Off_TotalTaxesAndOtherCharges;
}

public String getPay_Off_TotalBookingValue() {
	return Pay_Off_TotalBookingValue;
}

public void setPay_Off_TotalBookingValue(String pay_Off_TotalBookingValue) {
	Pay_Off_TotalBookingValue = pay_Off_TotalBookingValue;
}

public String getPay_Off_FinalSummerycurrency() {
	return Pay_Off_FinalSummerycurrency;
}

public void setPay_Off_FinalSummerycurrency(String pay_Off_FinalSummerycurrency) {
	Pay_Off_FinalSummerycurrency = pay_Off_FinalSummerycurrency;
}

public String getPay_Off_AmountbeingProcessedNow() {
	return Pay_Off_AmountbeingProcessedNow;
}

public void setPay_Off_AmountbeingProcessedNow(
		String pay_Off_AmountbeingProcessedNow) {
	Pay_Off_AmountbeingProcessedNow = pay_Off_AmountbeingProcessedNow;
}

public String getPay_Off_AmountdueatCheckIn() {
	return Pay_Off_AmountdueatCheckIn;
}

public void setPay_Off_AmountdueatCheckIn(String pay_Off_AmountdueatCheckIn) {
	Pay_Off_AmountdueatCheckIn = pay_Off_AmountdueatCheckIn;
}

public String getPay_ON_TotalGrossBookingValue() {
	return Pay_ON_TotalGrossBookingValue;
}

public void setPay_ON_TotalGrossBookingValue(
		String pay_ON_TotalGrossBookingValue) {
	Pay_ON_TotalGrossBookingValue = pay_ON_TotalGrossBookingValue;
}

public String getPay_ON_DefaultSelectedCard() {
	return Pay_ON_DefaultSelectedCard;
}

public void setPay_ON_DefaultSelectedCard(String pay_ON_DefaultSelectedCard) {
	Pay_ON_DefaultSelectedCard = pay_ON_DefaultSelectedCard;
}

public String getPay_ON_TotalTaxesAndFees() {
	return Pay_ON_TotalTaxesAndFees;
}

public void setPay_ON_TotalTaxesAndFees(String pay_ON_TotalTaxesAndFees) {
	Pay_ON_TotalTaxesAndFees = pay_ON_TotalTaxesAndFees;
}

public String getPay_ON_TotalPackageBookingValue() {
	return Pay_ON_TotalPackageBookingValue;
}

public void setPay_ON_TotalPackageBookingValue(
		String pay_ON_TotalPackageBookingValue) {
	Pay_ON_TotalPackageBookingValue = pay_ON_TotalPackageBookingValue;
}

public String getPay_ON_AmountBeingProcessedNow() {
	return Pay_ON_AmountBeingProcessedNow;
}

public void setPay_ON_AmountBeingProcessedNow(
		String pay_ON_AmountBeingProcessedNow) {
	Pay_ON_AmountBeingProcessedNow = pay_ON_AmountBeingProcessedNow;
}

public String getPay_ON_CurrencyLable() {
	return Pay_ON_CurrencyLable;
}

public void setPay_ON_CurrencyLable(String pay_ON_CurrencyLable) {
	Pay_ON_CurrencyLable = pay_ON_CurrencyLable;
}

public String getPay_ON_SummeryCurrency() {
	return Pay_ON_SummeryCurrency;
}

public void setPay_ON_SummeryCurrency(String pay_ON_SummeryCurrency) {
	Pay_ON_SummeryCurrency = pay_ON_SummeryCurrency;
}

public String getPay_ON_SummeryValue() {
	return Pay_ON_SummeryValue;
}

public void setPay_ON_SummeryValue(String pay_ON_SummeryValue) {
	Pay_ON_SummeryValue = pay_ON_SummeryValue;
}

public String getPay_OFF_Method() {
	return Pay_OFF_Method;
}

public void setPay_OFF_Method(String pay_OFF_Method) {
	Pay_OFF_Method = pay_OFF_Method;
}

public String getPay_OFF_Reference() {
	return Pay_OFF_Reference;
}

public void setPay_OFF_Reference(String pay_OFF_Reference) {
	Pay_OFF_Reference = pay_OFF_Reference;
}

public String getPay_OFF_Currency() {
	return Pay_OFF_Currency;
}

public void setPay_OFF_Currency(String pay_OFF_Currency) {
	Pay_OFF_Currency = pay_OFF_Currency;
}

public String getPay_OFF_Amount() {
	return Pay_OFF_Amount;
}

public void setPay_OFF_Amount(String pay_OFF_Amount) {
	Pay_OFF_Amount = pay_OFF_Amount;
}

public String getPay_Occupancy_String() {
	return Pay_Occupancy_String;
}

public void setPay_Occupancy_String(String pay_Occupancy_String) {
	Pay_Occupancy_String = pay_Occupancy_String;
}

public String getPay_Customer_Fname() {
	return Pay_Customer_Fname;
}

public void setPay_Customer_Fname(String pay_Customer_Fname) {
	Pay_Customer_Fname = pay_Customer_Fname;
}

public String getPay_Customer_Lname() {
	return Pay_Customer_Lname;
}

public void setPay_Customer_Lname(String pay_Customer_Lname) {
	Pay_Customer_Lname = pay_Customer_Lname;
}

public String getPay_Customer_Tel() {
	return Pay_Customer_Tel;
}

public void setPay_Customer_Tel(String pay_Customer_Tel) {
	Pay_Customer_Tel = pay_Customer_Tel;
}

public String getPay_Customer_Email() {
	return Pay_Customer_Email;
}

public void setPay_Customer_Email(String pay_Customer_Email) {
	Pay_Customer_Email = pay_Customer_Email;
}

public String getPay_Customer_Email2() {
	return Pay_Customer_Email2;
}

public void setPay_Customer_Email2(String pay_Customer_Email2) {
	Pay_Customer_Email2 = pay_Customer_Email2;
}

public String getPay_Customer_Fax() {
	return Pay_Customer_Fax;
}

public void setPay_Customer_Fax(String pay_Customer_Fax) {
	Pay_Customer_Fax = pay_Customer_Fax;
}

public String getPay_Customer_Adderss1() {
	return Pay_Customer_Adderss1;
}

public void setPay_Customer_Adderss1(String pay_Customer_Adderss1) {
	Pay_Customer_Adderss1 = pay_Customer_Adderss1;
}

public String getPay_Customer_Adderss2() {
	return Pay_Customer_Adderss2;
}

public void setPay_Customer_Adderss2(String pay_Customer_Adderss2) {
	Pay_Customer_Adderss2 = pay_Customer_Adderss2;
}

public String getPay_Customer_Country() {
	return Pay_Customer_Country;
}

public void setPay_Customer_Country(String pay_Customer_Country) {
	Pay_Customer_Country = pay_Customer_Country;
}

public String getPay_Customer_City() {
	return Pay_Customer_City;
}

public void setPay_Customer_City(String pay_Customer_City) {
	Pay_Customer_City = pay_Customer_City;
}

public String getPay_Customer_PostCode() {
	return Pay_Customer_PostCode;
}

public void setPay_Customer_PostCode(String pay_Customer_PostCode) {
	Pay_Customer_PostCode = pay_Customer_PostCode;
}

public String getPay_Customer_Emergency() {
	return Pay_Customer_Emergency;
}

public void setPay_Customer_Emergency(String pay_Customer_Emergency) {
	Pay_Customer_Emergency = pay_Customer_Emergency;
}

public String getPay_Customer_State() {
	return Pay_Customer_State;
}

public void setPay_Customer_State(String pay_Customer_State) {
	Pay_Customer_State = pay_Customer_State;
}

public String getPay_FinalSummerycurrency() {
	return Pay_FinalSummerycurrency;
}

public void setPay_FinalSummerycurrency(String pay_FinalSummerycurrency) {
	Pay_FinalSummerycurrency = pay_FinalSummerycurrency;
}

public String getPay_totalTaxAnServiceCharges() {
	return Pay_totalTaxAnServiceCharges;
	

}

public void setPay_totalTaxAnServiceCharges(String pay_totalTaxAnServiceCharges) {
	Pay_totalTaxAnServiceCharges = pay_totalTaxAnServiceCharges;
}

public String getPay_TotalGrossPackageBookingValue() {
	return Pay_TotalGrossPackageBookingValue;
}

public void setPay_TotalGrossPackageBookingValue(
		String pay_TotalGrossPackageBookingValue) {
	Pay_TotalGrossPackageBookingValue = pay_TotalGrossPackageBookingValue;
}

public String getPay_TotalTaxesAndOtherCharges() {
	return Pay_TotalTaxesAndOtherCharges;
}

public void setPay_TotalTaxesAndOtherCharges(
		String pay_TotalTaxesAndOtherCharges) {
	Pay_TotalTaxesAndOtherCharges = pay_TotalTaxesAndOtherCharges;
}

public String getPay_TotalBookingValue() {
	return Pay_TotalBookingValue;
}

public void setPay_TotalBookingValue(String pay_TotalBookingValue) {
	Pay_TotalBookingValue = pay_TotalBookingValue;
}

public String getPay_AmountbeingProcessedNow() {
	return Pay_AmountbeingProcessedNow;
}

public void setPay_AmountbeingProcessedNow(String pay_AmountbeingProcessedNow) {
	Pay_AmountbeingProcessedNow = pay_AmountbeingProcessedNow;
}

public String getPay_AmountdueatCheckIn() {
	return Pay_AmountdueatCheckIn;
}

public void setPay_AmountdueatCheckIn(String pay_AmountdueatCheckIn) {
	Pay_AmountdueatCheckIn = pay_AmountdueatCheckIn;
}

public String getPay_CandellationDeadLine() {
	return Pay_CandellationDeadLine;
}

public void setPay_CandellationDeadLine(String pay_CandellationDeadLine) {
	Pay_CandellationDeadLine = pay_CandellationDeadLine;
}

public boolean isOverrideOptionAvailable() {
	return OverrideOptionAvailable;
}

public void setOverrideOptionAvailable(boolean overrideOptionAvailable) {
	OverrideOptionAvailable = overrideOptionAvailable;
}

public ArrayList<RoomType> getPay_RoomDetails() {
	return Pay_RoomDetails;
}

public void setPay_RoomDetails(ArrayList<RoomType> pay_RoomDetails) {
	Pay_RoomDetails = pay_RoomDetails;
}

public CancellationPolicy getPay_CancellationPolicy() {
	return Pay_CancellationPolicy;
}

public void setPay_CancellationPolicy(CancellationPolicy pay_CancellationPolicy) {
	Pay_CancellationPolicy = pay_CancellationPolicy;
}



public boolean isLoded() {
	return isLoded;
}

public void setLoded(boolean isLoded) {
	this.isLoded = isLoded;
}

public String getPay_HotelName() {
	return Pay_HotelName;
}

public void setPay_HotelName(String pay_HotelName) {
	Pay_HotelName = pay_HotelName;
}

public String getPay_Address() {
	return Pay_Address;
}

public void setPay_Address(String pay_Address) {
	Pay_Address = pay_Address;
}

public String getPay_CheckIn() {
	return Pay_CheckIn;
}

public void setPay_CheckIn(String pay_CheckIn) {
	Pay_CheckIn = pay_CheckIn;
}

public String getPay_CheckOut() {
	return Pay_CheckOut;
}

public void setPay_CheckOut(String pay_CheckOut) {
	Pay_CheckOut = pay_CheckOut;
}

public String getPay_Nights() {
	return Pay_Nights;
}

public void setPay_Nights(String pay_Nights) {
	Pay_Nights = pay_Nights;
}

public String getPay_BookingStatus() {
	return Pay_BookingStatus;
}

public void setPay_BookingStatus(String pay_BookingStatus) {
	Pay_BookingStatus = pay_BookingStatus;
}

public String getPay_Rooms() {
	return Pay_Rooms;
}

public void setPay_Rooms(String pay_Rooms) {
	Pay_Rooms = pay_Rooms;
}


public String getPay_SummeryHotelcurrency() {
	return Pay_SummeryHotelcurrency;
}

public void setPay_SummeryHotelcurrency(String pay_currency) {
	Pay_SummeryHotelcurrency = pay_currency;
}


public String getPay_SubTotal() {
	return Pay_SubTotal;
}

public void setPay_SubTotal(String pay_SubTotal) {
	Pay_SubTotal = pay_SubTotal;
}

public String getPay_TotalHotelBookingValue() {
	return Pay_TotalHotelBookingValue;
}

public void setPay_TotalHotelBookingValue(String pay_TotalHotelBookingValue) {
	Pay_TotalHotelBookingValue = pay_TotalHotelBookingValue;
}








}
