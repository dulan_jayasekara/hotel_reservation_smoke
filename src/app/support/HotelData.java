package app.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.types.InventoryObtainedByType;
import com.types.RateContractByType;
import com.types.RateSetupBy;
import com.utilities.CancellationPolicy;

public class HotelData {
	
private String HoteName        = "";
private String Address         = "";
private String StarRating      = "";
private String ContractFrom    = "";
private String ContractTO      = "";
private String isFeatured      = "";
private String Longtitude      = "";
private String Latitude        = "";
private String Currency        = "";
private String Region          = "ALL";
private int    ChildLowerLimit =  5;
private int    ChildUpperLimit = 18;
private String HotelDescription = "";
private CancellationPolicy cancellationPolicy      = null;
private InventoryObtainedByType invType            = null;
private RateSetupBy             rateSetType        = null;
private RateContractByType      rateContByType     = null;
private Map<String,RoomType>      RoomsSet     = new HashMap<String, RoomType>(); 
private Map<Integer,ArrayList<RoomType>>  ApplicableRooms = new HashMap<Integer, ArrayList<RoomType>>();






public String getIsFeatured() {
	return isFeatured;
}

public void setIsFeatured(String isFeatured) {
	this.isFeatured = isFeatured;
}

public void AddRoom(RoomType rmtype)
{
	RoomsSet.put(rmtype.getRoomType(), rmtype);
}

public Map<Integer, ArrayList<RoomType>> getApplicableRooms() {
	return ApplicableRooms;
}
public void setApplicableRooms(Map<Integer, ArrayList<RoomType>> applicableRooms) {
	ApplicableRooms = applicableRooms;
}
public Map<String,RoomType> getRoomsSet() {
	return RoomsSet;
}
public void setRoomsSet(Map<String,RoomType> roomsSet) {
	RoomsSet = roomsSet;
}
public String getHoteName() {
	return HoteName;
}
public void setHoteName(String hoteName) {
	HoteName = hoteName;
}
public String getAddress() {
	return Address;
}
public void setAddress(String address) {
	Address = address.replace(";", ",");
}
public String getStarRating() {
	return StarRating;
}
public void setStarRating(String starRating) {
	StarRating = starRating;
}
public String getContractFrom() {
	return ContractFrom;
}
public void setContractFrom(String contractFrom) {
	ContractFrom = contractFrom;
}
public String getContractTO() {
	return ContractTO;
}
public void setContractTO(String contractTO) {
	ContractTO = contractTO;
}
public String getLongtitude() {
	return Longtitude;
}
public void setLongtitude(String longtitude) {
	Longtitude = longtitude;
}
public String getLatitude() {
	return Latitude;
}
public void setLatitude(String latitude) {
	Latitude = latitude;
}
public String getCurrency() {
	return Currency;
}
public void setCurrency(String currency) {
	Currency = currency;
}
public String getRegion() {
	return Region;
}
public void setRegion(String region) {
	Region = region;
}
public int getChildLowerLimit() {
	return ChildLowerLimit;
}
public void setChildLowerLimit(int childLowerLimit) {
	ChildLowerLimit = childLowerLimit;
}
public int getChildUpperLimit() {
	return ChildUpperLimit;
}
public void setChildUpperLimit(int childUpperLimit) {
	ChildUpperLimit = childUpperLimit;
}
public String getHotelDescription() {
	return HotelDescription;
}
public void setHotelDescription(String hotelDescription) {
	HotelDescription = hotelDescription;
}
public CancellationPolicy getCancellationPolicy() {
	return cancellationPolicy;
}
public void setCancellationPolicy(CancellationPolicy cancellationPolicy) {
	this.cancellationPolicy = cancellationPolicy;
}
public InventoryObtainedByType getInvType() {
	return invType;
}
public void setInvType(InventoryObtainedByType invType) {
	this.invType = invType;
}
public RateSetupBy getRateSetType() {
	return rateSetType;
}
public void setRateSetType(RateSetupBy rateSetType) {
	this.rateSetType = rateSetType;
}
public RateContractByType getRateContByType() {
	return rateContByType;
}
public void setRateContByType(RateContractByType rateContByType) {
	this.rateContByType = rateContByType;
}







}
