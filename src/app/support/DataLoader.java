package app.support;

import java.math.BigDecimal;
import java.util.ArrayList;

import java.util.Arrays;

import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;
import org.apache.log4j.Logger;

import com.types.BookingChannelType;
import com.types.DiscountRateApplicableType;
import com.types.PartnerType;
import com.types.PromotionApplicablePeriodType;
import com.types.PromotionBasedOnType;
import com.types.PromotionSpecialRateApplicableType;
import com.types.PromotionType;
import Hotel.Utilities.Hotel;
import Hotel.Utilities.HotelPromotion;
import Hotel.Utilities.HotelSupplementary;
import com.types.ChargeByType;
import com.types.InventoryObtainedByType;
import com.types.RateContractByType;
import com.types.RateSetupBy;
import com.utilities.CancellationPolicy;
import com.utilities.ExchangeRateUpdater;
import com.utilities.HotelTax;

public class DataLoader {

	private Map<String, String> CurrencyMap;
	private String PortalCurrency;
	private Logger logger = null;

	/*
	 * public DataLoader() { logger = Logger.getLogger(this.getClass().getName()); }
	 */
	public DataLoader(String Currency) {
		logger = Logger.getLogger(this.getClass().getName());
		ExchangeRateUpdater updater = new ExchangeRateUpdater();
		try {
			PortalCurrency = Currency;
			CurrencyMap = updater.getExchangeRates();

		} catch (Exception e) {
			logger.fatal("Error when updating the currency list -->" + e.toString());
			// System.out.println("ErrorOccured While updating the CurrencyList");
		}

	}

	public ArrayList<SearchType> loadOccupancySearchDetails(Map<Integer, String> map) {
		ArrayList<SearchType> searchList = new ArrayList<SearchType>();

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			SearchType CurrentSearch = new SearchType();
			String[] AllValues = it.next().getValue().split(",");
			CurrentSearch.setCountryOFResidance(AllValues[0]);
			CurrentSearch.setLocation(AllValues[1]);
			CurrentSearch.setHiddenLoc(AllValues[2]);
			CurrentSearch.setNights(AllValues[4]);
			CurrentSearch.setBookingType(AllValues[3]);
			CurrentSearch.setRooms(AllValues[5]);
			CurrentSearch.setAdults(AllValues[6]);
			CurrentSearch.setChildren(AllValues[7]);
			CurrentSearch.setChildAges(AllValues[8]);
			CurrentSearch.setHotelName(AllValues[9]);
			CurrentSearch.setHotelAvailability(AllValues[10]);
			CurrentSearch.setHotelAvailability(AllValues[11]);
			CurrentSearch.setRoomTypes(AllValues[12]);
			// CurrentSearch.setDefaultRoom(AllValues[13]);
			CurrentSearch.setDescription(AllValues[14]);
			CurrentSearch.setSearchCurrency(AllValues[15]);

			if (!(AllValues[13].equalsIgnoreCase("Not Available")) && !(PortalCurrency.equalsIgnoreCase(AllValues[14])))
				CurrentSearch.setExpected(AllValues[13], CurrencyMap.get(AllValues[14]));
			else
				CurrentSearch.setExpected(AllValues[13]);

			searchList.add(CurrentSearch);
		}
		return searchList;

	}

	public Map<String, SearchType> loadSearchDetails(Map<Integer, String> map, Map<String, Hotel> HotelList) {
		Map<String, SearchType> searchMap = new HashMap<String, SearchType>();

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			SearchType CurrentSearch = new SearchType();
			String[] AllValues = it.next().getValue().split(",");

			CurrentSearch.setCountryOFResidance(AllValues[1].trim());
			CurrentSearch.setLocation(AllValues[2].trim());
			CurrentSearch.setHiddenLoc(AllValues[3].trim());
			CurrentSearch.setNights(AllValues[6].trim());
			CurrentSearch.setBookingDate(AllValues[4].trim());
			// CurrentSearch.setBookinDateType(AllValues[4].trim());
			CurrentSearch.setBookingType(AllValues[5].trim());
			CurrentSearch.setRooms(AllValues[7].trim());
			CurrentSearch.setAdults(AllValues[8].trim());
			CurrentSearch.setChildren(AllValues[9].trim());
			CurrentSearch.setChildAges(AllValues[10].trim());
			CurrentSearch.setHotelName(AllValues[11].trim());
			CurrentSearch.setValidationHotel(HotelList.get(CurrentSearch.getHotelName()));
			CurrentSearch.setAvailability(AllValues[12].trim());
			// CurrentSearch.setHotelAvailability(AllValues[11]);
			// CurrentSearch.setRoomTypes(AllValues[13].trim());
			// CurrentSearch.setDefaultRoom(AllValues[14].trim());
			// CurrentSearch.setSelectedRooms(AllValues[15].trim());
			CurrentSearch.setDescription(AllValues[13].trim());
			CurrentSearch.setSearchCurrency(AllValues[14].trim());
		//	CurrentSearch.setTotalRate(AllValues[15].trim());
			CurrentSearch.setPaymentType(AllValues[16].trim());
			CurrentSearch.setCardType(AllValues[17].trim());
			CurrentSearch.setOfflineMethod(AllValues[18].trim());
			CurrentSearch.setPaymentRef(AllValues[19].trim());
			//CurrentSearch.setAvailability(AllValues[20].trim());
			CurrentSearch.setOptionalSupplementarySelected(AllValues[21].trim().equalsIgnoreCase("true") ? true : false);
			CurrentSearch.setScenarioExcecutionState(AllValues[22].trim());
			CurrentSearch.setScenarioExcecutionState(AllValues[22].trim());
			CurrentSearch.setBestRateApplied(AllValues[23].trim());
			CurrentSearch.setDealOfTheDayApplied(AllValues[24].trim());
			CurrentSearch.setBaseCurrency(PortalCurrency);
			/*
			 * if(!(AllValues[13].equalsIgnoreCase("Not Available")) && !(PortalCurrency.equalsIgnoreCase(AllValues[14]))) CurrentSearch.setExpected (AllValues[13],CurrencyMap.get(AllValues[14]));
			 * else CurrentSearch.setExpected(AllValues[13]);
			 */

			searchMap.put(AllValues[0].trim(), CurrentSearch);
		}
		return searchMap;

	}

	public TreeMap<String, Hotel> loadTaxDetails(Map<Integer, String> map, TreeMap<String, Hotel> HotelList) {

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			HotelTax tax = new HotelTax();
			String[] AllValues = it.next().getValue().split(",");

			tax.setFromT(AllValues[1]);
			tax.setToT(AllValues[2]);
			tax.setSalesTaxChargeType(com.types.ChargeByType.getChargeType(AllValues[3].trim()));
			tax.setSalesTaxValue(AllValues[4]);
			tax.setOccupancyChargeType(com.types.ChargeByType.getChargeType(AllValues[5].trim()));
			tax.setOccupancyTaxValue(AllValues[6]);
			tax.setEnergyChargeType(com.types.ChargeByType.getChargeType(AllValues[7].trim()));
			tax.setEnergyTaxValue(AllValues[8]);
			tax.setMiscellaneousFees(AllValues[9]);

			try {
				HotelList.get(AllValues[0].trim()).AddTax(tax);
			} catch (Exception e) {
				/* Logger.warn(AllValues[0]+" not found in the hotel list"); */
			}

		}
		return HotelList;

	}

	public Map<String, HotelData> loadHotelDetails(Map<Integer, String> map) {
		Map<String, HotelData> hotelList = new HashMap<String, HotelData>();

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			HotelData CurrentHotel = new HotelData();
			String[] AllValues = it.next().getValue().split(",");

			CurrentHotel.setHoteName(AllValues[0]);
			CurrentHotel.setAddress(AllValues[1]);
			CurrentHotel.setStarRating(AllValues[2]);
			CurrentHotel.setContractFrom(AllValues[3]);
			CurrentHotel.setContractTO(AllValues[4]);
			CurrentHotel.setIsFeatured(AllValues[5]);
			CurrentHotel.setLongtitude(AllValues[6]);
			CurrentHotel.setLatitude(AllValues[7]);
			CurrentHotel.setCurrency(AllValues[8]);
			CurrentHotel.setRegion(AllValues[9]);
			CurrentHotel.setChildLowerLimit(Integer.parseInt(AllValues[10]));
			CurrentHotel.setChildUpperLimit(Integer.parseInt(AllValues[11]));
			CurrentHotel.setHotelDescription(AllValues[12]);
			CurrentHotel.setInvType(InventoryObtainedByType.getinveType(AllValues[13]));
			CurrentHotel.setRateSetType(RateSetupBy.getRateSetupType(AllValues[14]));
			CurrentHotel.setRateContByType(RateContractByType.getRateContractType(AllValues[15]));

			CancellationPolicy CPolicy = new CancellationPolicy();
			CPolicy.setFrom(AllValues[16]);
			CPolicy.setTo(AllValues[17]);
			CPolicy.setApplyLessThan(Integer.parseInt(AllValues[18].replace(" ", "")));
			CPolicy.setBuffer(Integer.parseInt(AllValues[19].replace(" ", "")));
			CPolicy.setStandardChargeByType(ChargeByType.getChargeType(AllValues[20]));
			CPolicy.setStandardValue(AllValues[21]);
			CPolicy.setNoShowChargeByType(ChargeByType.getChargeType(AllValues[22]));
			CPolicy.setNoShowValue(AllValues[23]);

			CurrentHotel.setCancellationPolicy(CPolicy);

			hotelList.put(AllValues[0], CurrentHotel);
		}
		return hotelList;

	}

	public Map<String, SearchType> loadRoomDetails(Map<Integer, String> map, Map<String, SearchType> Searchmap) {

		Iterator<Map.Entry<String, SearchType>> it = Searchmap.entrySet().iterator();

		while (it.hasNext()) {
			Map.Entry<String, SearchType> CurrentEntry = it.next();
			String CurrentIndex = CurrentEntry.getKey();
			SearchType CurrentSearch = CurrentEntry.getValue();
			ArrayList<ArrayList<RoomType>> RoomListArray = new ArrayList<ArrayList<RoomType>>(Integer.parseInt(CurrentSearch.getRooms()));
			Double DefaultRoomRateTotal   = 0.0;
			Double SelectedRoomRateTotal  = 0.0;
			// ArrayList<ArrayList<RoomType>> RoomListArray = new
			// ArrayList<ArrayList<RoomType>>(3);

			for (int i = 0; i < Integer.parseInt(CurrentSearch.getRooms()); i++) {
				RoomListArray.add(new ArrayList<RoomType>());

			}

			Iterator<Map.Entry<Integer, String>> rawit = map.entrySet().iterator();

			while (rawit.hasNext()) {
				String[] AllValues = rawit.next().getValue().split(",");

				if (AllValues[0].equalsIgnoreCase(CurrentIndex)) {
					RoomType room = new RoomType();
					int RoomNum = Integer.parseInt(AllValues[1]);
					room.setRoomLable(AllValues[2]);
					room.setRoomType(AllValues[3]);
					room.setBedType(AllValues[4]);
					room.setRateType(AllValues[5]);
					room.setDailyRates(new ArrayList<String>(Arrays.asList(AllValues[6].split(";"))));
					room.setTotalRate(AllValues[7]);
					//room.setCurrency(AllValues[8]);
					room.setPromotionApplied((AllValues[10].trim().equalsIgnoreCase("yes") ? true : false));
					room.setPromoCode(AllValues[11].trim());

					if (AllValues[8].trim().equalsIgnoreCase("Yes")){
						CurrentSearch.addToDefaultRooms(Integer.toString(RoomNum), room.getRoomType() + "/" + room.getBedType() + "/" + room.getRateType());
						DefaultRoomRateTotal  += Double.parseDouble(room.getTotalRate());
					}
					if (AllValues[9].trim().equalsIgnoreCase("Yes")) {
						CurrentSearch.addToSelectedRooms(Integer.toString(RoomNum), room.getRoomType() + "/" + room.getBedType() + "/" + room.getRateType());
						CurrentSearch.addToSelectedRoomObj(room);
						SelectedRoomRateTotal += Double.parseDouble(room.getTotalRate());
					}
					try {
						// RoomListArray.get(1);
						RoomListArray.get(RoomNum - 1).add(room);
					} catch (Exception e) {
						System.out.println("sds");
					}

				}
			}

			int count = 1;
			for (ListIterator<ArrayList<RoomType>> iterator = RoomListArray.listIterator(); iterator.hasNext();) {
				try {
					ArrayList<RoomType> arrayList = (ArrayList<RoomType>) iterator.next();
					CurrentSearch.addToResultsRoomMap(arrayList, Integer.toString(count));
					// Searchmap.remove(CurrentIndex);
					Searchmap.put(CurrentIndex, CurrentSearch);
				} catch (Exception e) {
					System.out.println("Errorrrrr");
				}

				count++;

			}
			CurrentSearch.setTotalRate(new BigDecimal(Math.ceil(DefaultRoomRateTotal)).stripTrailingZeros().toPlainString());
			
	//		System.out.println(CurrentSearch.getTotalRate());
			
		}
		
		
		return Searchmap;

	}

	public Map<String, SearchType> loadCartDetails(Map<Integer, String> map, Map<String, SearchType> Searchmap) {
		Iterator<Map.Entry<Integer, String>> rawit = map.entrySet().iterator();

		while (rawit.hasNext()) {
			CartDetails cart = new CartDetails();
			String[] AllValues = rawit.next().getValue().split(",");
			cart.setHotelLable(AllValues[1]);
			cart.setRate(AllValues[2]);
			cart.setNetRate(AllValues[3]);
			cart.setServiceFeeType(AllValues[4]);
			cart.setServiceFeeInPortalCurrency(AllValues[5]);

			Searchmap.get(AllValues[0].trim()).setCart(cart);

		}

		return Searchmap;

	}

	public Map<String, SearchType> loadCancellationDetails(Map<Integer, String> map, Map<String, SearchType> Searchmap) {
		Iterator<Map.Entry<Integer, String>> rawit = map.entrySet().iterator();

		while (rawit.hasNext()) {
			app.support.CancellationPolicy cancellation = new app.support.CancellationPolicy();
			String[] AllValues = rawit.next().getValue().split(",");
			cancellation.setNumberofpolicies(AllValues[1]);

			for (int i = 1; i <= Integer.parseInt(AllValues[1]); i++) {
				cancellation.addPolicies(AllValues[i + 1]);

			}

			cancellation.setRefundable(AllValues[8]);
			cancellation.setDeadline(AllValues[7]);
			Searchmap.get(AllValues[0]).setPolicy(cancellation);

		}

		return Searchmap;

	}

	public double convertCurrency(String Value, String FromCurrency, String ToCurrency) {
		double returnValue = 0.0;

		try {
			double value = Double.parseDouble(Value.trim());
			double Portalvalue = value / (Double.parseDouble(CurrencyMap.get(FromCurrency.trim())));
			returnValue = Portalvalue * (Double.parseDouble(CurrencyMap.get(ToCurrency.trim())));
		} catch (Exception e) {
			System.out.println("Error in exchange rate conversion" + e.toString());
		}

		return returnValue;

	}

	public TreeMap<String, Hotel> loadSupplimentaryDetails(Map<Integer, String> map, TreeMap<String, Hotel> HotelList) {

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {

			HotelSupplementary hotelSupplementary = new HotelSupplementary();
			String[] values = it.next().getValue().split(",");

			hotelSupplementary.setSupplementary_Name(values[1]);
			hotelSupplementary.setSupplier_Name(values[2]);
			hotelSupplementary.setHotel_Name(values[0]);
			hotelSupplementary.setRoom_Type(values[3]);
			hotelSupplementary.setRegion(values[4]);
			hotelSupplementary.setTour_Operator(values[5]);
			hotelSupplementary.setDate_From(values[6]);
			hotelSupplementary.setDate_To(values[7]);
			hotelSupplementary.setSupplementary_Applicable(values[8]);
			hotelSupplementary.setMendatory(values[9]);

			hotelSupplementary.setActive(values[10]);
			hotelSupplementary.setRateBased_On(values[11]);
			hotelSupplementary.setNetRate(values[12]);
			hotelSupplementary.setChild_NetRate(values[13]);
			hotelSupplementary.setProfit_By(values[14]);
			hotelSupplementary.setProfit_Value(values[15]);
			hotelSupplementary.setChild_Profit_Value(values[16]);
			hotelSupplementary.setMin_Child_Age(values[17]);
			hotelSupplementary.setMax_Child_Age(values[18]);
			hotelSupplementary.setPercentage_Room_Rate(values[19]);

			try {
				HotelList.get(values[0]).addHotelSupplimentary(values[1], hotelSupplementary);
			} catch (Exception e) {
				System.out.println(values[0] + " not found in the hotel list");
			}
		}
		return HotelList;

	}

	public TreeMap<String, Hotel> loadPromoDetails(Map<Integer, String> map, TreeMap<String, Hotel> HotelList) {

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {

			HotelPromotion hotelPromotion = new HotelPromotion();
			String[] values = it.next().getValue().split(",");

			hotelPromotion.setSupp_Name(values[1]);
			hotelPromotion.setHotel_Name(values[0]);
			hotelPromotion.setRoom_Type(values[2]);
			hotelPromotion.setBed_Type(values[3]);
			hotelPromotion.setRate_Plan(values[4]);
			hotelPromotion.setCalculation_Logic(values[5]);
			hotelPromotion.setPromotion_BasedOn(PromotionBasedOnType.getInventoryType(values[6]));
			hotelPromotion.setNights_Book(values[7]);
			hotelPromotion.setPrior_Arrival(values[8]);
			hotelPromotion.setPromotionType(PromotionType.getInventoryType(values[9]));

			hotelPromotion.setFN_FreeNights(values[10]);
			hotelPromotion.setFN_MaxNumberofNights(values[11]);
			hotelPromotion.setFN_Surchage_Fee(values[12]);
			hotelPromotion.setSpecialRateApplicableType(PromotionSpecialRateApplicableType.getInventoryType(values[13]));
			hotelPromotion.setDiscountrateapplicabletype(DiscountRateApplicableType.getChargeType(values[14]));
			hotelPromotion.setPromo_Value(values[15]);
			hotelPromotion.setNote(values[16]);
			hotelPromotion.setPromoCode(values[17]);
			hotelPromotion.setIsbestrateguarantee(values[18]);
			hotelPromotion.setApplicablePeriod(PromotionApplicablePeriodType.getChargeType(values[19]));
			hotelPromotion.setBooking_Date_From(values[20]);
			hotelPromotion.setBooking_Date_To(values[21]);
			hotelPromotion.setStay_Date_From(values[22]);
			hotelPromotion.setStay_Date_To(values[23]);
			hotelPromotion.setBooking_Channel(BookingChannelType.getChargeType(values[24]));
			hotelPromotion.setPartnerType(PartnerType.getChargeType(values[25]));
			hotelPromotion.setAgentRegion(values[26]);
			hotelPromotion.setAgentName(values[27]);
			hotelPromotion.setIs_Active(values[28]);

			try {
				HotelList.get(values[0]).addHotelPromotions(values[17].trim(), hotelPromotion);
			} catch (Exception e) {
				System.out.println(values[0] + " not found in the hotel list");
			}

		}
		return HotelList;

	}

}
