package app.support;

import java.util.ArrayList;
import java.util.Map;

import Hotel.Utilities.Hotel;

public class ValidationClass {

private Hotel                              ValidationHotel                      = null;
private Map<String, ArrayList<RoomType>>   ResultsRoomMap                       = null;
private CartDetails                        Cart                                 = null;
private CancellationPolicy                 policy                               = null;

public Hotel getValidationHotel() {
	return ValidationHotel;
}

public void setValidationHotel(Hotel validationHotel) {
	ValidationHotel = validationHotel;
}

public Map<String, ArrayList<RoomType>> getResultsRoomMap() {
	return ResultsRoomMap;
}

public void addToResultsRoomMap(ArrayList<RoomType> RoomList, String Room) {
	ResultsRoomMap.put(Room, RoomList);
}

public CartDetails getCart() {
	return Cart;
}
public void setCart(CartDetails cart) {
	Cart = cart;
}
public CancellationPolicy getPolicy() {
	return policy;
}
public void setPolicy(CancellationPolicy policy) {
	this.policy = policy;
}



}
